function error_message(message = '', title = general_error, time = 3000){
	$(".jq-toast-single.jq-has-icon.jq-icon-success").remove();
	$(".jq-toast-single.jq-has-icon.jq-icon-error").remove();
	$(".jq-toast-single.jq-has-icon.jq-icon-warning").remove();
	
	$.toast({
	    heading: title,
	    text: message,
	    showHideTransition: 'slide',
	    icon: 'error',
	    hideAfter: time
	});
}

function warning_message(message = '', title = general_warning, time = 3000){
	$(".jq-toast-single.jq-has-icon.jq-icon-success").remove();
	$(".jq-toast-single.jq-has-icon.jq-icon-error").remove();
	$(".jq-toast-single.jq-has-icon.jq-icon-warning").remove();
	
	$.toast({
	    heading: title,
	    text: message,
	    showHideTransition: 'slide',
	    icon: 'warning',
	    hideAfter: time
	});
}

function success_message(message = '', title = general_success, time = 3000){
	$(".jq-toast-single.jq-has-icon.jq-icon-success").remove();
	$(".jq-toast-single.jq-has-icon.jq-icon-error").remove();
	$(".jq-toast-single.jq-has-icon.jq-icon-warning").remove();
	
	$.toast({
	    heading: title,
	    text: message,
	    showHideTransition: 'slide',
	    icon: 'success',
	    hideAfter: time
	});
}

function mask_background(){
	$("body").prepend('<div class="mask-background"><i class="fa fa-refresh fa-spin mask-animation"></i></div>');
}

function unmask_background(){
	$('.mask-background').remove();
}