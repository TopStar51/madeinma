<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('getCurrentTimeStamp')) {

    function currentTimeStamp()
    {

        $date = date("Y-m-d")." 00:00:00";
        $datetime = new DateTime($date);

        return $datetime->getTimestamp();        

    }   
    
}

if ( ! function_exists('userLang')) {

    function userLang()
    {
        $result = '';
        $CI = & get_instance();

        $lang = $CI->session->userdata('lang');

        if (isset($lang)) {
            $result = $lang;
        } else {
            $result = DEFAULT_LANG;
        }

        return $result;
    }   
    
}

if ( ! function_exists('getCompanyRating')) {

    function getCompanyRating($id)
    {

        $CI = & get_instance();
        $CI->load->model('Review_Model');

        $ratings = $CI->Review_Model->all(array('company_id' => $id, 'status' => 1));

        $total_rating = 0;
        foreach ($ratings as $row) {
            $total_rating += $row->rating;
        }

        $avg_rating = empty($ratings) ? 0 : $total_rating / count($ratings);

        return round($avg_rating, 1);
    }   
    
}


if ( ! function_exists('getReviewCount')) {

    function getReviewCount($id)
    {

        $CI = & get_instance();
        $CI->load->model('Review_Model');

        $count = $CI->Review_Model->count(array('company_id' => $id, 'status' => 1));

        return $count;
    }   
    
}

if ( ! function_exists('employeeCountStr')) {

    function employeeCountStr($val)
    {
        $emp_cnt_kind = array('', '<5', '5+', '10+', '20+', '50+', '100+', '500+', '1000+');

        if (empty($emp_cnt_kind[$val])) {
            return '';
        }

        return $emp_cnt_kind[$val];
    }   
    
}

if ( ! function_exists('favoriteCount')) {

    function favoriteCount()
    {
        $result = 0;

        $CI = & get_instance();

        $favorites = $CI->session->userdata('favorites');

        if (!isset($favorites)) {
            $result = 0;
        } else {
            $result = count($favorites);
        }

        return $result;
    }   
    
}

if ( ! function_exists('isFavorite')) {

    function isFavorite($company_id)
    {
        $result = 0;

        $CI = & get_instance();

        $favorites = $CI->session->userdata('favorites');

        if (!empty($favorites)) {
            $result = array_key_exists($company_id, $favorites) ? 1 : 0;
        }

        return $result;
    }   
    
}


if ( ! function_exists('getSEOStr')) {

    function getSEOStr($str)
    {
        $str = str_replace(" ", "_", $str);
        $str = str_replace("/", ":", $str);
        $str = str_replace("&", "-", $str);
        $str = urlencode($str);
    
        return $str;
    }   
    
}

if ( ! function_exists('parseSEOStr')) {

    function parseSEOStr($str)
    {
        $str = (urldecode($str));
        
        $str = str_replace("-", "&", $str);
        $str = str_replace(":", "/", $str);
        $str = str_replace("_", " ", $str);

        return $str;
    }   
    
}

if ( ! function_exists('parseCompanySEOStr')) {

    function parseCompanySEOStr($str)
    {
        $str = (urldecode($str));
        
        $str = str_replace(":", "/", $str);
        $str = str_replace("_", " ", $str);

        return $str;
    }   
    
}


if ( ! function_exists('logoURL')) {

    function logoURL($str)
    {
        if (empty($str)) {
            return base_url('assets/madinma/front/images/company_avatar.png');
        }

        return base_url('upload/logos/' . $str);
    }   
    
}

if ( ! function_exists('productURL')) {

    function productURL($str)
    {
        if (empty($str)) {
            return base_url('assets/madinma/front/images/preview.png');
        }

        return base_url('upload/products/' . $str);
    }   
    
}

if ( ! function_exists('productThumbURL')) {

    function productThumbURL($str)
    {
        if (empty($str)) {
            return base_url('assets/madinma/front/images/preview.png');
        }

        return base_url('upload/products/thumb/' . $str);
    }       
}

if ( ! function_exists('avatarURL')) {

    function avatarURL($str, $gender='Mr')
    {
        if (empty($str)) {
            if ($gender == 'Ms'|| $gender == 'ms') {
                return base_url('assets/madinma/front/images/default_wavatar.jpg');
            } else {
                return base_url('assets/madinma/front/images/default_avatar.jpg');
            }
        }
        return base_url('upload/photos/' . $str);
    }   
    
}


if ( ! function_exists('symbolURL')) {

    function symbolURL($str)
    {
        if (empty($str)) {
            return base_url('assets/madinma/back/images/add_img.png');
        }

        return base_url('assets/icons_svg/' . $str);
    }   
    
}


if ( ! function_exists('symbolSVG')) {

    function symbolSVG($str)
    {
        if (is_file('./assets/icons_svg/' . $str)) {
            echo file_get_contents('./assets/icons_svg/' . $str);
        } else {
            echo '<img src="' . base_url('assets/madinma/back/images/add_img.png') . '"/>';
        }
    }   
    
}

if(!function_exists('validateRecaptcha')) {

    function validateRecaptcha($recaptchaResponse)
    {
        $CI = & get_instance();

        $userIp = $CI->input->ip_address();

        $url="https://www.google.com/recaptcha/api/siteverify?secret=".RECAPTCHA_SECRET_KEY."&response=".$recaptchaResponse."&remoteip=".$userIp;

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      
         
        $status= json_decode($output, true);
        return $status['success'];
    }

}
if(!function_exists('prefixCutStr')) {
    
    function prefixCutStr($str, $len, $suffix = "...") {
        $s = substr($str, 0, $len);
        $cnt = 0;
        for ($i = 0; $i < strlen($s); $i++) {
            if (ord($s[$i]) > 127) {
                $cnt++;
            }
        }

        $s = substr($s, 0, $len - ($cnt % 3));

        if (strlen($s) >= strlen($str)) {
            $suffix = "";
        }
        return $s . $suffix;
    }
}
