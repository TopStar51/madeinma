<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('send_sms')) {
    /**
   * XML-Objekt in Array umwandeln.
   *
   * Diese Funktion wandelt ein SimpleXMLElement {@link http://www.php.net/manual/de/class.simplexmlelement.php}
   * in ein normales mehrdimensionales Array um.
   *
   * @param SimpleXMLElement XML-Objekt welches konvertiert werden soll
   */
    function simpleXMLToArray($xml) {
        $return = array();
        if(!($xml instanceof SimpleXMLElement)) return $return;
        $name = $xml->getName();
        $_value = trim((string)$xml);
        if(mb_strlen($_value) == 0) $_value = null;
        if($_value !== null) $return = html_entity_decode($_value);
        $children = array();
        $first = true;
        foreach($xml->children() as $elementName => $child) {
            $value = simpleXMLToArray($child);
            if(isset($children[$elementName])) {
                if($first) {
                    $temp = $children[$elementName];
                    unset($children[$elementName]);
                    $children[$elementName][] = $temp;
                    $first = false;
                }
                $children[$elementName][] = $value;
            } else
                $children[$elementName] = $value;
        }
        if(count($children) > 0) $return = array_merge($return,$children);

        $attributes = array();
        foreach($xml->attributes() as $name=>$value){
            $attributes[$name] = trim($value);
        }
        if(count($attributes) > 0) $return = array_merge($return, $attributes);

        return $return;
    }
    /**
   * Webseite als String zurückgeben.
   *
   * Diese Funktion lädt eine Webseite via CURL {@link http://www.php.net/manual/de/book.curl.php} herunter.
   *
   * @param string Adresse der Webseite
   * @param mixed URL-konforme Daten für POST-Anfrage oder FALSE wenn GET verwendet werden soll
   */
    function curlhttp($url,$post = false) {
        if($url == "") return false;
        $VersionArray = curl_version();
        $Optionen = array(CURLOPT_RETURNTRANSFER => true, CURLOPT_HEADER => 0, CURLOPT_USERAGENT => "PHP ".phpversion()." + cURL ".$VersionArray["version"], CURLOPT_TIMEOUT => 30);
        if($post != false) {
            $Optionen[CURLOPT_POST] = 1;
            $Optionen[CURLOPT_POSTFIELDS] = $post;
        }
        $s = curl_init($url);
        foreach($Optionen as $CURLCONST => $Wert) curl_setopt($s,$CURLCONST,$Wert);
        $Quellcode = curl_exec($s);
        $StatusArray = curl_getinfo($s);
        $HttpHead = array(100 => "Continue", 101 => "Switching Protocols", 200 => "OK", 201 => "Created", 202 => "Accepted", 203 => "Non-Authoritative Information",
                            204 => "No Content", 205 => "Reset Content", 206 => "Partial Content", 300 => "Multiple Choices", 301 => "Moved Permanently",
                            302 => "Found", 303 => "See Other", 304 => "Not Modified", 305 => "Use Proxy", 306 => "(Unused)", 307 => "Temporary Redirect",
                            400 => "Bad Request", 401 => "Unauthorized", 402 => "Payment Required", 403 => "Forbidden", 404 => "Not Found", 405 => "Method Not Allowed",
                            406 => "Not Acceptable", 407 => "Proxy Authentication Required", 408 => "Request Timeout", 409 => "Conflict", 410 => "Gone",
                            411 => "Length Required", 412 => "Precondition Failed", 413 => "Request Entity Too Large", 414 => "Request-URI Too Long",
                            415 => "Unsupported Media Type", 416 => "Requested Range Not Satisfiable", 417 => "Expectation Failed", 500 => "Internal Server Error",
                            501 => "Not Implemented", 502 => "Bad Gateway", 503 => "Service Unavailable", 504 => "Gateway Timeout", 505 => "HTTP Version Not Supported");
        return array("HTTP_Code" => $StatusArray["http_code"], "HTTP_Text" => $HttpHead[$StatusArray["http_code"]], "HTTP_Body" => $Quellcode);
    }

    function send_sms($sms_receiver, $text) {
        /* SMS Gateway https://www.lox24.de */

        $sms_user = 1148;			     	         // Account-ID
        $sms_password = md5("121182");	     // Account-Password
        $sms_service = 338;				           // Service-ID Premium - mit Eigener ID

        // $sms_receiver = "+212642421044";    // Phone number sender
        $sms_sender = "made-in.ma";         // Phone number sender
        $sms_text = $text;       // Message Text

        // Prüfen ob $sms_receiver Numerisch!
        if(is_numeric($sms_receiver)) {
            /*
            // Get cURL resource
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://www.lox24.eu/API/httpsms.php?konto=".$sms_user."&password=".$sms_password."&service=".$sms_service."&text=".urlencode($sms_text)."&to=".urlencode($sms_receiver)."&from=".urlencode($sms_sender)."&httphead=0&return=xml",
                CURLOPT_USERAGENT => ''
            ]);
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);
            */
            $resp = curlhttp("http://www.lox24.eu/API/httpsms.php?konto=".$sms_user."&password=".$sms_password."&service=".$sms_service."&text=".urlencode($sms_text)."&to=".urlencode($sms_receiver)."&from=".urlencode($sms_sender)."&httphead=0&return=xml");

            $Test[] = simpleXMLToArray(simplexml_load_string($resp["HTTP_Body"]));

            if($Test['0']['code'] == "100") {

                $numerofcharacters = $Test['0']['info']['Zeichen'];
                $price = $Test['0']['info']['Kosten'];
                $msgid = $Test['0']['info']['MSGID'];
                $return_text = $Test['0']['codetext'];
                $return_code = $Test['0']['code'];

                return array('success' => TRUE);
                // return $price . "<br>"."SMS sended sussesful!";

                /*    SAMPLE RETURN
                Array
                (
                    [code] => 100
                    [codetext] => SMS erfolgreich versendet
                    [info] => Array
                        (
                            [Text] => asadasdasd ** FREE Sendet by iNetStart.de **
                            [Zeichen] => 44
                            [SMS] => 1
                            [Absenderkennung] => +49160223344
                            [Ziel] => +49(160)5123456
                            [Kosten] => 0.024
                            [Versenden] => Sofort
                            [MSGID] => ac9b3f889ec234b48b3d954fa17ad163
                        )
                )
                */
            } else {
                return array('success' => FALSE, 'error' => $Test['0']['codetext']);
            }
        } else {
            return array('success' => FALSE, 'error' => lang('message.sms_error'));
        }
    }
}




























?>
