<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inquery_model extends AbstractModel {

	var $_table = "inqueries";

	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->select("companies.company_name_en");
        $this->db->join("companies","inqueries.company_id = companies.id", "LEFT");
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.name', $filter['search'], 'both');
            $this->db->group_end();
        }
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        $this->db->select("companies.company_name_en");
        $this->db->join("companies","inqueries.company_id = companies.id", "LEFT");
        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.name', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }

}
