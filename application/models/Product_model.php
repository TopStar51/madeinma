<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends AbstractModel {

	var $_table = "products";

	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->join("companies", "companies.id = products.company_id", "LEFT");
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.keywords', $filter['search'], 'both');
            $this->db->or_like('companies.company_name_'. userLang(), $filter['search'], 'both');
            $this->db->or_like('companies.desc_' . userLang(), $filter['search'], 'both');
            $this->db->group_end();
		}
		$this->db->order_by('order','asc');
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        $this->db->join("companies", "companies.id = products.company_id", "LEFT");
        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.keywords', $filter['search'], 'both');
            $this->db->or_like('companies.company_name_'. userLang(), $filter['search'], 'both');
            $this->db->or_like('companies.desc_' . userLang(), $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }

}
