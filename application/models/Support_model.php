<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support_model extends AbstractModel {

	var $_table = "supports";

    public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->select('supports.name, phone, company_email, company_name_fr, cities.name as city_name, content, ip_addr');
        $this->db->join("cities", "cities.id = supports.company_id", "left");

        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('supports.name', $filter['search'], 'both');
            $this->db->or_like('supports.content', $filter['search'], 'both');
            $this->db->or_like('supports.company_name_fr', $filter['search'], 'both');
            $this->db->or_like('supports.company_email', $filter['search'], 'both');
            $this->db->or_like('supports.phone', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::all($filter, $order);
    }

    public function count($filter = NULL) {
        $this->db->join("cities", "cities.id = supports.company_id", "left");

        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('supports.name', $filter['search'], 'both');
            $this->db->or_like('supports.content', $filter['search'], 'both');
            $this->db->or_like('supports.company_name_fr', $filter['search'], 'both');
            $this->db->or_like('supports.company_email', $filter['search'], 'both');
            $this->db->or_like('supports.phone', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }
}
    