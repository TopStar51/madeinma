<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favorite_model extends AbstractModel {

	var $_table = "favorites";

	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->select("companies.company_name_en company_name, users.name user_name");
        $this->db->join("companies","favorites.company_id = companies.id", "LEFT");
        $this->db->join("users","favorites.user_id = users.id", "LEFT");
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.company_name_en', $filter['search'], 'both');
            $this->db->or_like('users.name', $filter['search'], 'both');
            $this->db->group_end();
        }
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        $this->db->select("companies.company_name_en as company_name, users.name user_name");
        $this->db->join("companies","favorites.company_id = companies.id", "LEFT");
        $this->db->join("users","favorites.user_id = users.id", "LEFT");
        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.company_name_en', $filter['search'], 'both');
            $this->db->or_like('users.name', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }

}
