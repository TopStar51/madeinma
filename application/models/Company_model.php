<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_model extends AbstractModel {

	var $_table = "companies";

    public function one($filter = NULL) {
        $this->db->select('company_name_'.userLang().' as company_name, categories.symbol_url as symbol_url');
        $this->db->select('users.id user_id, cities.name city_name');
        $this->db->join("users", "users.company_id = companies.id", "LEFT");
		$this->db->join("cities", "cities.id = companies.city_id", "LEFT");
		$this->db->join("categories", "categories.id = companies.category_id", "LEFT");
        return parent::one($filter);
    }

    public function one_like($company_name) {
        $this->db->select('company_name_fr, company_name_'.userLang().' as company_name');
        $this->db->group_start();
        $this->db->like('company_name_en', $company_name);
        $this->db->or_like('company_name_fr', $company_name);
        $this->db->or_like('company_name_it', $company_name);
        $this->db->or_like('company_name_es', $company_name);
        $this->db->or_like('company_name_de', $company_name);
        $this->db->or_like('company_name_ar', $company_name);
        $this->db->or_like('company_name_cn', $company_name);
        $this->db->or_like('company_name_ir', $company_name);
        $this->db->group_end();
        $this->db->where('status','1');        
        return $this->db->get('companies')->row();
    }

	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->select('company_name_'.userLang().' as company_name');
        $this->db->select('users.id user_id, cities.name city_name, categories.symbol_url, categories.*');
        $this->db->join("users", "users.company_id = companies.id", "LEFT");
        $this->db->join("cities", "cities.id = companies.city_id", "LEFT");
        $this->db->join("categories", "categories.id = companies.category_id", "LEFT");
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.keywords', $filter['search'], 'both');
            $this->db->or_like('companies.company_name_'.userLang(), $filter['search'], 'both');
            $this->db->or_like('companies.desc_' . userLang(), $filter['search'], 'both');
            $this->db->group_end();
        }
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        $this->db->select('company_name_'.userLang().' as company_name');
        $this->db->join("users", "users.company_id = companies.id", "LEFT");
        $this->db->join("cities", "cities.id = companies.city_id", "LEFT");
        $this->db->join("categories", "categories.id = companies.category_id", "LEFT");
        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.keywords', $filter['search'], 'both');
            $this->db->or_like('companies.company_name_'.userLang(), $filter['search'], 'both');
            $this->db->or_like('companies.desc_' . userLang(), $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }
	public function count_fr($filter = NULL) {
        $this->db->select('count(id) as cnt');
		$this->db->group_start();
		$this->db->like('company_name_fr', $filter['company_name_fr']);
		if($filter['company_name_en']!='')
			$this->db->or_like('company_name_en', $filter['company_name_en']);
		if($filter['company_name_it']!='')
			$this->db->or_like('company_name_it', $filter['company_name_it']);
		if($filter['company_name_es']!='')
			$this->db->or_like('company_name_es', $filter['company_name_es']);
		if($filter['company_name_de']!='')
			$this->db->or_like('company_name_de', $filter['company_name_de']);
		if($filter['company_name_ar']!='')
			$this->db->or_like('company_name_ar', $filter['company_name_ar']);
		if($filter['company_name_cn']!='')
			$this->db->or_like('company_name_cn', $filter['company_name_cn']);
		// if($filter['company_name_ir']!='')
			// $this->db->or_like('company_name_ir', $filter['company_name_ir']);

		$this->db->group_end();
		$this->db->where('status','1');
		if(isset($filter['id']))        
			$this->db->where('id<>',$filter['id']);        
		$result = $this->db->get('companies')->row_array();
		return $result['cnt'];
    }
}
