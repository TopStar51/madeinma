<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class log_model extends AbstractModel {

	var $_table = "logs";

	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('logs.text', $filter['search'], 'both');
            $this->db->group_end();
        }
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('logs.text', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }

}
