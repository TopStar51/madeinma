<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review_model extends AbstractModel {

	var $_table = "reviews";

	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->select("companies.company_name_fr company_name, companies.logo_url, country.name_".userLang()." country_name");
        $this->db->join("companies","reviews.company_id = companies.id", "inner");
        $this->db->join("country", "country.id = reviews.country_id", "inner");
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('reviews.name', $filter['search'], 'both');
            $this->db->or_like('reviews.content', $filter['search'], 'both');
            $this->db->group_end();
        }
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        $this->db->join("companies","reviews.company_id = companies.id", "inner");
        $this->db->join("country", "country.id = reviews.country_id", "inner");
        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('reviews.name', $filter['search'], 'both');
            $this->db->or_like('reviews.content', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }

}
