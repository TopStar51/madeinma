<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City_model extends AbstractModel {

	var $_table = "cities";

	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('name', $filter['search'], 'both');
            $this->db->group_end();
        }
        $this->db->order_by('name', 'asc');
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('name', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }

}
