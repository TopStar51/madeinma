<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends AbstractModel {

	var $_table = "categories";

	public function one_like($category_name) {
        $this->db->select('name_en, name_'.userLang().' as name');
        $this->db->group_start();
        $this->db->like('name_en', $category_name);
        $this->db->or_like('name_fr', $category_name);
        $this->db->or_like('name_it', $category_name);
        $this->db->or_like('name_es', $category_name);
        $this->db->or_like('name_de', $category_name);
        $this->db->or_like('name_ar', $category_name);
        $this->db->or_like('name_cn', $category_name);
        $this->db->or_like('name_ir', $category_name);
        $this->db->group_end();
        $this->db->where('status','1');        
        return $this->db->get($this->_table)->row();
	}
	
	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->select("*");
		if(!empty($filter['search'])) {
			$this->db->group_start();
			$this->db->like('name_en', $filter['search'], 'both');
			$this->db->or_like('name_fr', $filter['search'], 'both');
			$this->db->or_like('name_it', $filter['search'], 'both');
			$this->db->or_like('name_es', $filter['search'], 'both');
			$this->db->or_like('name_de', $filter['search'], 'both');
			$this->db->or_like('name_ar', $filter['search'], 'both');
			$this->db->or_like('name_cn', $filter['search'], 'both');
			$this->db->or_like('name_ir', $filter['search'], 'both');
            $this->db->group_end();
        }
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->like('name_en', $filter['search'], 'both');
			$this->db->or_like('name_fr', $filter['search'], 'both');
			$this->db->or_like('name_it', $filter['search'], 'both');
			$this->db->or_like('name_es', $filter['search'], 'both');
			$this->db->or_like('name_de', $filter['search'], 'both');
			$this->db->or_like('name_ar', $filter['search'], 'both');
			$this->db->or_like('name_cn', $filter['search'], 'both');
			$this->db->or_like('name_ir', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }

}
