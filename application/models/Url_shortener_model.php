<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Url_shortener_model extends AbstractModel {

	var $_table = "short_urls";

    public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->select('*');

        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('keyword', $filter['search'], 'both');
            $this->db->or_like('company_url', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::all($filter, $order);
    }

    public function count($filter = NULL) {

        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('keyword', $filter['search'], 'both');
            $this->db->or_like('company_url', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }
}
