<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends AbstractModel {

	var $_table = "users";
    
	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->select("companies.company_name_fr company_name,companies.company_name_fr company_name_fr, companies.logo_url, cities.name city_name, companies.rating, companies.show_home, companies.contact_person, companies.avatar_url, companies.phone, categories.name_en category_name, companies.status");
        $this->db->join("companies","users.company_id = companies.id", "LEFT");
        $this->db->join("cities","companies.city_id = cities.id", "LEFT");
        $this->db->join("categories", "categories.id = companies.category_id", "LEFT");
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.company_name_fr', $filter['search'], 'both');
            $this->db->or_like('companies.contact_person', $filter['search'], 'both');
            $this->db->or_like('cities.name', $filter['search'], 'both');
            $this->db->group_end();
        }
        if(isset($order['phone'])){
            $order['users.phone'] = $order['phone'];
            unset($order['phone']);
        }
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        $this->db->join("companies","users.company_id = companies.id", "LEFT");
        $this->db->join("cities","companies.city_id = cities.id", "LEFT");
        $this->db->join("categories", "categories.id = companies.category_id", "LEFT");
        if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('companies.company_name_fr', $filter['search'], 'both');
            $this->db->or_like('companies.contact_person', $filter['search'], 'both');
            $this->db->or_like('cities.name', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }

}
