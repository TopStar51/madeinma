<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message_model extends AbstractModel {

	var $_table = "messages";

	public function all($filter = NULL, $order = NULL, $direction = 'asc', $fields = "*") {
        $this->db->select('companies.company_name_'.userLang().' as company_name,companies.company_name_fr company_name_fr, companies.logo_url, country.name_'.userLang().' country_name');
        $this->db->join("companies", "companies.id = messages.company_id", "left");
        $this->db->join("country", "country.id = messages.country_id", "left");
        if(isset($filter['is_contact'])) {
            if ($filter['is_contact'] == 1) {
                $this->db->where('messages.company_id = 0');
            } else {
                $this->db->where('messages.company_id > 0');
            }
            unset($filter['is_contact']);
        }
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('messages.name', $filter['search'], 'both');
            $this->db->or_like('messages.content', $filter['search'], 'both');
            $this->db->or_like('companies.company_name_fr', $filter['search'], 'both');
            $this->db->group_end();
        }
		return parent::all($filter, $order);
	}

	public function count($filter = NULL) {
        $this->db->join("companies", "companies.id = messages.company_id", "left");
        $this->db->join("country", "country.id = messages.country_id", "left");
        if(isset($filter['is_contact'])) {
            if ($filter['is_contact'] == 1) {
                $this->db->where('messages.company_id = 0');
            } else {
                $this->db->where('messages.company_id > 0');
            }
            unset($filter['is_contact']);
        }
		if(!empty($filter['search'])) {
            $this->db->group_start();
            $this->db->or_like('messages.name', $filter['search'], 'both');
            $this->db->or_like('messages.content', $filter['search'], 'both');
            $this->db->or_like('companies.company_name_fr', $filter['search'], 'both');
            $this->db->group_end();
        }
        return parent::count($filter);
    }

}
