<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class FrontController extends CI_Controller
{
	// Values and objects to be overrided or accessible from child controllers
	protected $mPageTitlePrefix = '';
	protected $mPageTitle = '';

	// Data to pass into views
	protected $mViewData = array();

	// User Language
	protected $mLang = DEFAULT_LANG;

	function __construct() 
	{
		parent::__construct();

		// initial setup
		$this->_setup();
	}

	private function _setup()
	{
		$user_lang = $this->session->userdata('lang');
		if (!isset($user_lang)) {
			$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
			$acceptLang = ['en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu'];//, 'ir'];
			$this->mLang = in_array($lang, $acceptLang) ? $lang : DEFAULT_LANG;
			$this->session->set_userdata('lang', $this->mLang);
		} else {
			$this->mLang = $user_lang;
		}
		
		switch ($this->mLang) {
			case 'en':
				$this->lang->load('messages', 'english');
				break;
			case 'fr':
				$this->lang->load('messages', 'french');
				break;
			case 'it':
				$this->lang->load('messages', 'italian');
				break;
			case 'es':
				$this->lang->load('messages', 'spanish');
				break;
			case 'de':
				$this->lang->load('messages', 'german');
				break;
			case 'ar':
				$this->lang->load('messages', 'arabic');
				break;
			case 'cn':
				$this->lang->load('messages', 'chinese');
				break;
			case 'ir':
				$this->lang->load('messages', 'farsi');
				break;
			case 'tu':
				$this->lang->load('messages', 'turkish');
				break;
			default:
				break;
		}
	}
	
	protected function render($view_file, $layout = 'default')
	{
		$this->mViewData['title'] = $this->mPageTitlePrefix . $this->mPageTitle;		
        
		$this->mViewData['user_lang'] = $this->mLang;

		$this->load->model('Category_model');
		$this->mViewData['categories'] = $this->Category_model->all(array('status' => 1), array('sort' => 'asc'));

		$date = new DateTime();
		date_sub($date, new DateInterval("P7D"));
		$end_date = date('Y-m-d H:i:s');
		$start_date = $date->format("Y-m-d H:i:s");

		$this->load->model('Company_model');
		$this->load->model('Inquery_model');

		$this->mViewData['total_cnt_companies'] = $this->Company_model->count();

		$where = "TO_DAYS(companies.created_at) BETWEEN TO_DAYS('".$start_date."') AND TO_DAYS('".$end_date."')";       
        $this->db->where($where);
		$this->mViewData['week_cnt_companies'] = $this->Company_model->count();

		$this->mViewData['total_cnt_inqueries'] = $this->Inquery_model->count();

		$where = "TO_DAYS(inqueries.created_at) BETWEEN TO_DAYS('".$start_date."') AND TO_DAYS('".$end_date."')";       
        $this->db->where($where);
		$this->mViewData['week_cnt_inqueries'] = $this->Inquery_model->count();

		$this->load->model('City_model');
		$this->mViewData['cities'] = $this->City_model->all();

		$this->load->model('Country_model');
		$this->mViewData['countries'] = $this->Country_model->all(null, 'name_'.$this->mLang,'asc');
		
		$this->load->library('user_agent');
		
		if(empty($this->mViewData['og_tags'])) $this->mViewData['og_tags'] = array();
		
		if ($this->agent->is_mobile() && !strpos($this->agent->agent, 'iPad')) {
			$this->mViewData['inner_view'] = 'front/mobile/' . $view_file;
			$this->load->view('front/mobile/_layouts/'.$layout, $this->mViewData);
		} else {
			$this->mViewData['inner_view'] = 'front/' . $view_file;
			$this->load->view('front/_layouts/'.$layout, $this->mViewData);
		}

    }

	// Output JSON string
	protected function render_json($data, $code = 200)
	{
		$this->output
			->set_status_header($code)
			->set_content_type('application/json')
			->set_output(json_encode($data));			
		// force output immediately and interrupt other scripts
		global $OUT;
		$OUT->_display();
		exit;
	}

	public function upload_image($uploadDir, $fieldName, $checkExist = true) {

        $retVal = array();
        $retVal['state'] = 'success';

        if (!file_exists('./upload/'.$uploadDir)) {
            mkdir('./upload/'.$uploadDir, 0777, true);
        }

        //upload file
        $config['upload_path'] = './upload/'.$uploadDir;
        $config['allowed_types'] = 'gif|tif|bmp|jpg|png|jpeg';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '10240'; //10 MB
        $this->load->library('upload', $config);
		
		$this->upload->initialize($config);
		
		
        if (isset($_FILES[$fieldName]['name'])) {
            if (0 < $_FILES[$fieldName]['error']) {
                $retVal['msg'] = 'Error during file upload' . $_FILES[$fieldName]['error'];
            } else {
                if (!$this->upload->do_upload($fieldName)) {
                    $retVal['state'] = 'failed';
                    $retVal['msg'] = $this->upload->display_errors();
                } else {
                    $retVal['state'] = 'success';
					$retVal['uploadData'] = $this->upload->data();
                }
            }
        } else {
            if ($checkExist) {
                $retVal['state'] = 'failed';
                $retVal['msg'] = 'Please choose a file';
            }
        }

        return $retVal;
    }

	public function make_thumbnail($filename, $uploadDir, $size, $maintainRatio = true, $targetDir = '', $flag = 'N') {

        $config['image_library'] = 'gd2';
        $config['source_image'] = './upload/'.$uploadDir.'/'.$filename;
        $config['maintain_ratio'] = $maintainRatio;
        switch ($flag) {
            case 'N':
                $config['width'] = $size;
                $config['height'] = $size;
                break;
            case 'W':
                $config['width'] = $size;
                break;
            case 'H':
                $config['height'] = $size;
                break;
        }

        if (empty($targetDir)) {
            $config['new_image'] = './upload/'.$uploadDir.'/'.$size.'-'.$filename;
        } else {
            if (!file_exists('./upload/'.$uploadDir.'/'.$targetDir)) {
                mkdir('./upload/'.$uploadDir.'/'.$targetDir, 0777, true);
            }
            $config['new_image'] = './upload/'.$uploadDir.'/'.$targetDir.'/'.$filename;
        }

        $this->image_lib->initialize($config);
        $this->image_lib->resize(); 
        $this->image_lib->clear();
    }

}

?>
