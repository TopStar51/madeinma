<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class AdminController extends CI_Controller
{
	// Values to be obtained automatically from router
	protected $mModule = '';			// module name (empty = Frontend Website)
	protected $mCtrler = '';		// current controller
	protected $mAction = '';		// controller function being called
	protected $mMethod = '';			// HTTP request method

	// Values and objects to be overrided or accessible from child controllers
	protected $mPageTitlePrefix = '';
	protected $mPageTitle = '';

	// Data to pass into views
	protected $mViewData = array();

	// Login user
	protected $mUser = NULL;

	protected $mVersion = '1.0';

	function __construct() 
	{
		parent::__construct();

		$CI = & get_instance();
		// router info
		$this->mModule = trim($CI->router->directory,"/");
		$this->mCtrler = $CI->router->class;
		$this->mAction = $CI->router->method;
		$this->mMethod = $this->input->server('REQUEST_METHOD');

		// initial setup
		$this->_setup();
	}

	private function _setup()
	{
		$this->lang->load('messages', 'english');

		$user = $this->session->userdata('user');
		if (!isset($user) ) {
			return redirect('admin/login');
		} else if ($user->type != 1) {
			return redirect('admin/login');
		}
		$this->mUser = $user;
	}
	
	protected function render($view_file, $layout = 'default', $view_data = [])
	{
		// automatically generate page title
		if ( empty($this->mPageTitle) )
		{
			if ($this->mAction=='index')
				$this->mPageTitle = humanize($this->mCtrler);
			else
				$this->mPageTitle = humanize($this->mAction);
		}

		$this->mViewData = $view_data;
		$this->mViewData['module'] = $this->mModule;
		$this->mViewData['ctrler'] = $this->mCtrler;
		$this->mViewData['action'] = $this->mAction;
		$this->mViewData['current_uri'] = empty($this->mModule) ? uri_string(): str_replace($this->mModule.'/', '', uri_string());
		$this->mViewData['title'] = $this->mPageTitlePrefix . $this->mPageTitle;
		$this->mViewData['user'] = $this->mUser;
		$this->mViewData['version'] = $this->mVersion;
		// added by simba
		$this->load->model('Review_model');
		$this->mViewData['rating_cnt'] = $this->Review_model->count(array('status'=> 0));
		$this->load->model('Company_model');
		$this->mViewData['update_cnt'] = $this->Company_model->count(array('status'=> 0));
		$this->load->model('Message_model');
		$this->mViewData['contact_cnt'] = $this->Message_model->count(array('company_id'=> 0));

        $this->mViewData['inner_view'] = $view_file;
		$this->load->view('admin/_layouts/'.$layout, $this->mViewData);
    }

	// Output JSON string
	protected function render_json($data, $code = 200)
	{
		$this->output
			->set_status_header($code)
			->set_content_type('application/json')
			->set_output(json_encode($data));			
		// force output immediately and interrupt other scripts
		global $OUT;
		$OUT->_display();
		exit;
	}

	protected function datatable_read ($datatable, $str_model)
	{
		$displayStart = isset($datatable['iDisplayStart']) ? $datatable['iDisplayStart'] : 0;
    	$displayLength = isset($datatable['iDisplayLength']) ? $datatable['iDisplayLength'] : -1;
    	$search = isset($datatable['sSearch']) ? $datatable['sSearch'] : '';
    	$sortingCols = isset($datatable['iSortingCols']) ? $datatable['iSortingCols'] : 0;    	
    	$data = $filter = $order = array();
    	if (isset($datatable['filter']))
    		$filter = $datatable['filter'];
    	$filter['search'] = $search;
		if ($displayLength != -1) {
			$filter['start'] = $displayStart;
    		$filter['limit'] = $displayLength;    		
		}
		for ($i = 0; $i < $sortingCols; $i++) {
			$sortCol = $datatable['iSortCol_' . $i];
			$sortDir = $datatable['sSortDir_' . $i];
			$dataProp = $datatable['mDataProp_' . $sortCol];
			$order[$dataProp] = $sortDir;
		}
		$this->load->model($str_model, 'model');
		$data['data'] = $this->model->all($filter, $order);    		
		$data['iTotalDisplayRecords'] = $this->model->count($filter);
		$data['iTotalRecords'] = $this->model->count($filter);
		$data['sEcho'] = $search;
        return $data;
	}

}

?>