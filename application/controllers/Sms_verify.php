<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/FrontController.php";

class Sms_verify extends FrontController
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
	}

	function index() {
		$this->mViewData['cur_page'] = 'sms_verify';

		$this->render('sms_verify', 'empty');
	}

	function post_code() {
		$config = array(
	        array(
	            'field' => 'sms_code',
	            'label' => 'Sms code',
	            'rules' => 'required',
	            'errors' => array(
	                'required' => lang('validation.user.sms_code.require')
	    		),
	        )
		);

		$this->form_validation->set_rules($config);

		if($this->form_validation->run() == FALSE)
		{
			return $this->render_json(array('success' => FALSE, 'message' => validation_errors()));
		}

		$data = elements(array('sms_code'), $_POST);

		$cur_date = date('Y-m-d H:i:s');
		$user_info = $this->db->from('users')->where('sms_code', $data['sms_code'])->get()->row_array();
		if(empty($user_info)) {
			exit(json_encode(array('success' => false, 'message' => lang('sms.code.error'))));
		}

		if((strtotime($cur_date) - strtotime($user_info['created_at'])) > SMS_VALID_TIME) {
			$this->db->delete('users', array('id' => $user_info['id']));
			exit(json_encode(array('success' => false, 'message' => lang('sms.expire'))));
		}
		$data = array(
			'status' => 1,
			'verified_at' => $cur_date
		);
		$this->db->where('id', $user_info['id']);
		$this->db->update('users', $data);
		$user = $this->User_model->select($user_info['id']);

		$this->session->set_userdata('user', $user);
		$this->session->set_userdata('remember', NULL);

		return $this->render_json(array('success' => TRUE));
	}
}