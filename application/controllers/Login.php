<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/FrontController.php";

class Login extends FrontController
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Token_model');
		$this->load->model('Sms_model');
		$this->load->model('UserLog_model');
		$this->load->model('Session_model');
	}

	public function index()
	{

		$user = $this->session->userdata('user');
		if(!empty($user))
		$this->sessionlog($user);

		if (isset($user) ) {
			return redirect('/');
		}
		$this->mViewData['cur_page'] = 'login';

		$this->render('login', 'empty');
	}

	public function post_login()
	{
		$config = array(
	        array(
	            'field' => 'phone',
	            'label' => 'Phone',
	            'rules' => 'required',
	            'errors' => array(
	            	'required' => lang('validation.user.phone.require')
	    		)
	        ),
	        array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
                'errors' => array(
	            	'required' => lang('validation.user.password.require')
	    		)
	        )
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			return $this->render_json(array('success' => FALSE, 'message' => validation_errors()));
		}

		$phone = $this->input->post('phone', TRUE);
		
		$password = $this->input->post('password', TRUE);

		$user = $this->User_model->one(array('phone' => $phone, 'password' => md5($password), 'type' => 0));
		if (empty($user) || $user->status == 3)
		{
			return $this->render_json(array('success' => FALSE, 'message' => lang('signin.invalid')));
		} else if ($user->status == 0) {
			return $this->render_json(array('success' => FALSE, 'message' => lang('signin.blocked')));
		}

		$remember = $this->input->post('remember');
		if ($remember == 1) {
			$this->session->set_userdata('remember', array('phone' => $phone, 'password' => $password));
		} else {
			$this->session->set_userdata('remember', NULL);
		}

		$this->session->set_userdata('user', $user);
		$this->log("login", $user);
		$this->userlogin($user);
		return $this->render_json(array('success' => TRUE));
	}

	public function post_forgot()
	{
		$config = array(
	        array(
	            'field' => 'phone',
	            'label' => 'Phone',
	            'rules' => 'required'
	        )
		);

		$this->form_validation->set_rules($config);
		
		$phone = $this->input->post('phone');

		if ($this->form_validation->run() == TRUE) {
			// Validate recaptcha response
			$recaptchaResponse = trim($this->input->post('recaptcha_response'));
			$userIp=$this->input->ip_address();

			$url="https://www.google.com/recaptcha/api/siteverify?secret=".RECAPTCHA_SECRET_KEY."&response=".$recaptchaResponse;
 
	        $ch = curl_init(); 
	        curl_setopt($ch, CURLOPT_URL, $url); 
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	        $output = curl_exec($ch); 
	        curl_close($ch);      
	         
			$status= json_decode($output, true);

        	if($status['success'] == true && $status['action'] == 'login' && $status['score'] >= 0.5) {

				$user = $this->User_model->one(array('phone'=>$phone));
				if (!empty($user)) { // ignore verifed user
					$token = $this->get_token();
					$this->Token_model->insert(array("user_id"=>$user->id, "token"=>$token, 'expire_time' => time() + (3 * 60)));
					
					$url = site_url("confirm-user/$token");

					$this->load->helper('sms');
					$result = send_sms($phone, $url);

					if ($result['success']) return $this->render_json(array('success' => TRUE, 'message' => lang('forgot.success_phone')));
					return $this->render_json(array('success' => FALSE, 'message' => $result['error']));
				} else {
					return $this->render_json(array('success' => FALSE, 'message' => lang('forgot.no_phone')));
				}
			} else {
				return $this->render_json(array('success' => FALSE, 'message' => lang('message.recaptacha_error')));
			}
		}
		
		return $this->render_json(array('success' => FALSE, 'message' => validation_errors()));
	}

	function confirm_user($token = '')
	{
		$data = $this->Token_model->one(array('token' => $token));
		if (empty($data)) {
			return redirect('login');
		}

		$this->mViewData['token'] = $token;

		$this->render('confirm_user', 'empty');
	}

	public function post_password()
    {
        $token = $this->input->post("token");

        $data = $this->Token_model->one(array('token' => $token));
        if (empty($data)) {
        	return $this->render_json(array('success' => FALSE, 'message' => lang('forgot.invalid_token')));
        }

        if ($data->expire_time > time()) {
            $password = $this->input->post('password');
            $confirm_password = $this->input->post('confirm_password');
            if($password != $confirm_password){
            	return $this->render_json(array('success' => FALSE, 'message' => lang('validation.user.confirm_password.valid')));
            }

            $user_data['password'] = md5($this->input->post('password'));

            $this->User_model->update($user_data, array('id'=>$data->user_id));

            return $this->render_json(array('success' => TRUE, 'message' => lang('forgot.update_success')));
        } else {
            return $this->render_json(array('success' => FALSE, 'message' => lang('forgot.expire')));
        }
    }

	private function get_token()
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $token = '';
        for ($i = 0; $i < 10; $i++) {
			$token .= $characters[rand(0, $charactersLength - 1)];					
		}

		return $token;
	}

	function send_email($to, $message){

		$req_ip = $_SERVER['REMOTE_ADDR'];
		if ($req_ip == '127.0.0.1' || $req_ip == "::1") return;

		$this->load->library('email');

		//SMTP & mail configuration
		$config = array(
				'protocol'  => 'smtp',
				//'smtp_host' => $SMTPSecure.'://'.$Host,
				'smtp_host' => EMAIL_HOST,
				'smtp_port' => 25,
				'smtp_user' => EMAIL_USER,
				'smtp_pass' => EMAIL_PASSWORD,
				'mailtype'  => 'html',
				'charset'   => 'utf-8'
		);

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$this->email->to($to);
		$this->email->from('_support_email', '_site_name');
		$this->email->cc($to);
		$this->email->subject('_subject');
		$this->email->message($message);

		//Send email
		return $this->email->send();
	}
	public function log($text,$user)
	{
		$slog['text'] = $text;
		$slog['type'] = "Login";
		$slog['phone_number']= $user->phone;
		$slog['ip_addr'] = $this->input->ip_address();
		$slog['created_at'] = date('Y-m-d H:i:s');
		   $slog['session_id'] = session_id();
		$this->Sms_model->save($slog);		
	}

	public function userlogin($user){

		$slog['userid'] = $user->id;
		$slog['ip'] = $this->input->ip_address();
		$slog['created_at'] = date('Y-m-d H:i:s');
		$slog['session_id'] = session_id();
		$slog['status'] = 'login In';
		$this->UserLog_model->save($slog);
	}
	public function userlogout($user){

		$slog['userid'] = $user->id;
		$slog['ip'] = $this->input->ip_address();
		$slog['created_at'] = date('Y-m-d H:i:s');
		$slog['session_id'] = session_id();
		$slog['status'] = 'login out';
		$this->UserLog_model->save($slog);
	}
	public function sessionlog($user){
		$slog['username'] = $user->phone;
		$slog['session_id'] = session_id();
		$this->load->library('user_agent');
		
		if ($this->agent->is_referral())
		{
			$slog['referer'] = $this->agent->referrer();
		}
		$slog['start_lang'] = userlang();
		$slog['browser'] = $_SERVER['HTTP_USER_AGENT'];
		$slog['timestamp'] = date('Y-m-d H:i:s');
		$slog['ip'] = $this->input->ip_address();		
		$this->Session_model->save($slog);
	}
}
