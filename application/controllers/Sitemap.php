<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/FrontController.php";

class Sitemap extends FrontController {
    /**
     * Index Page for this controller.
     *
     */
    public function index()
    {
        $query = $this->db->get_where("companies", array('status' => 1));
        $data['companies'] = $query->result();
        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->load->view('sitemap', $data);
    }
}