<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/FrontController.php";

class Home extends FrontController {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Category_model');
		$this->load->model('City_model');
		$this->load->model('Company_model');
		$this->load->model('User_model');
		$this->load->model('Product_model');
		$this->load->model('Review_model');
		$this->load->model('Inquery_model');
		$this->load->model('Message_model');
		$this->load->model('SText_model');
		$this->load->model('STextLog_model');
		$this->load->model('Country_model');
		$this->load->model('Support_model');
		$this->load->model('Url_shortener_model');
		$this->load->model('Sms_model');
		$this->load->model('UserLog_model');
		$this->load->model('Session_model');
		$this->load->library('User_agent');
	}

	public function index()
	{
		$new_companies = $this->Company_model->all(array('status' => 1, 'start'=>0, 'limit'=>20), array('created_at' => 'desc'));
		foreach ($new_companies as $row) {
			$row->products = $this->Product_model->all(array('company_id' => $row->id));
		}
		$this->mViewData['new_companies'] = $new_companies;
		$week_companies = $this->Company_model->all(array('show_home' => 1, 'status' => 1, 'start' => 0, 'limit => 20'), array('updated_at' => 'asc'));
		foreach ($week_companies as $row) {
			$row->products = $this->Product_model->all(array('company_id' => $row->id));
		}
		$this->mViewData['week_companies'] = $week_companies;
		$this->mViewData['og_tags'] = array(
			'title' => lang('site.title'),
			'image' => base_url('assets/madinma/front/images/logo_ogp.png'),
			'desc' => lang('site.description')
		);
		$this->render('index');
	}
	public function sessionlog_inccount(){
		$slog = $this->db->from('sessions')->where('session_id', session_id())->get()->row();
		$slog1['session_id'] = session_id();
		$slog1['timestamp'] = date('Y-m-d H:i:s');
		$slog1['ip'] = $this->input->ip_address();
		if(isset($slog->count_company_profiles))
		{
			$slog1['count_company_profiles'] = $slog->count_company_profiles + 1;

		}
		 $this->Session_model->update($slog1, array('session_id' => session_id()));
	}
	public function sessionlog($user, $lang){
		$slog['session_id'] = session_id();
		$slog['timestamp'] = date('Y-m-d H:i:s');
		$slog['ip'] = $this->input->ip_address();
		$slog['selected_lang'] = $lang;
		$slog['username'] = $user->phone;
		if ($this->agent->is_referral())
			$slog['referer'] = $this->agent->referrer();
		
		$slog['browser'] = $_SERVER['HTTP_USER_AGENT'];

		// $this->Session_model->save($slog);
		 $this->Session_model->update($slog, array('session_id' => session_id()));
	}
	public function change_lang($lang)
	{	
		$langs = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir');
        if (!in_array($lang, $langs)) {
        	$lang = DEFAULT_LANG;
        }
        $this->session->set_userdata('lang', $lang);
        $user = $this->session->userdata('user');

        if(!empty($user)) {
        	$filter['id'] = $user->id;
        	$this->sessionlog($user, $lang);

        	$data['last_sel_lang'] = $lang;
        	$this->User_model->update($data, $filter);
        }

        if($this->input->is_ajax_request())
			return $this->render_json(array('success' => TRUE));
		else
			return redirect("/");
	}

	public function myaccount($id=NULL)
	{
		$user = $this->session->userdata('user');
		if (!isset($user)) {
			return redirect('login');
		}
		$user_id = $user->id;
		$company_id = $user->company_id;
		if (isset($id) && $user->type == 1) {
			$company_profile = $this->Company_model->select($id);
			$user_id = $company_profile->user_id;
			$company_id = $id;
		} else {
			$company_profile = $this->Company_model->select($user->company_id);
		}

		$this->mViewData['user_id'] = $user_id;
		$this->mViewData['categories'] = $this->Category_model->all(NULL, array('sort' => 'asc'));
		$this->mViewData['user_profile'] = $this->User_model->select($user_id);
		if (!empty($company_profile)) {
			$this->mViewData['company_profile'] = $company_profile;
			$company_products = $this->Product_model->all(array('company_id'=>$company_id));
			$this->mViewData['company_products'] = $company_products;
			$product_ids = [];
			foreach ($company_products as $key => $value) {
				$product_ids[] = $value->img_url;
			}
			$this->mViewData['product_ids'] = $product_ids;
		} else {
			$this->mViewData['company_profile'] = array();
			$this->mViewData['company_products'] = array();
			$this->mViewData['product_ids'] = array();
		}

		$this->mViewData['cur_page'] = 'account';

		$this->render('myaccount', 'empty');
	}

	public function post_company()
	{
		$user = $this->session->userdata('user');
		if (!isset($user)) {
			return redirect('login');
		}
		
		$data = $this->input->post(NULL);
		$config = array(
	        array(
                'field' => 'salutation',
                'label' => humanize(lang('company.salutation')),
                'rules' => 'required'
	        ),
	        array(
                'field' => 'street',
                'label' => humanize(lang('company.street')),
                'rules' => 'required',
                'errors' => array(
	            	'required' => lang('validation.company.street.require')
	    		)
	        ),
	        array(
                'field' => 'contact_person',
                'label' => humanize(lang('company.contact_person')),
                'rules' => 'required',
                'errors' => array(
	            	'required' => lang('validation.company.contact_person.require')
	    		)
	        ),
	        array(
                'field' => 'city_id',
                'label' => humanize(lang('company.city')),
                'rules' => 'required'
	        ),
	        array(
                'field' => 'phone',
                'label' => humanize(lang('company.phone')),
                'rules' => 'required'
	        ),
	        array(
                'field' => 'language',
                'label' => humanize(lang('company.language')),
                'rules' => 'required'
	        ),
	        array(
                'field' => 'employee_count',
                'label' => humanize(lang('company.employee_count')),
                'rules' => 'required'
	        ),
	        array(
                'field' => 'founded_at',
                'label' => humanize(lang('company.founded_at')),
                'rules' => 'required|less_than['.date('Y').']',
                'errors' => array(
	            	'required' => lang('validation.company.founded_at.require')
	    		)
	        ),
	        array(
                'field' => 'category_id',
                'label' => humanize(lang('company.category')),
                'rules' => 'required'
	        ),
	        array(
	        	'field' => 'company_name_fr',
	        	'label' => humanize(lang('company.name').'('.lang('general.lang.fr').')'),
	        	'rules' => 'required'
	        ),
	        array(
	        	'field' => 'desc_fr',
	        	'label' => humanize(lang('company.about_us').'('.lang('general.lang.fr').')'),
	        	'rules' => 'required'
			)
		);
                
		/*foreach (array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu') as $lang) {
			$config[] = array(
				'field' => 'company_name_'.$lang,
				'label' => humanize(lang('company.name').'('.lang('general.lang.'.$lang).')'),
				'rules' => 'required'
			);
		}*/

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			return $this->render_json(array('success' => FALSE, 'message' => validation_errors()));
		}

		if ($user->type == 1) {
			$userdata = $this->User_model->select($data['user_id']);
		} else {
			$userdata = $user;
		}

		// $data['company_name_en'] = preg_replace('#[^a-z0-9\-_ ]#ui', '', $data['company_name_en']);
		
		unset($data['user_id']);

		if (!empty($userdata->company_id)) {
			$data['id'] = $userdata->company_id;
		}
		if($this->Company_model->count_fr($data) > 0){
			return $this->render_json(array('success' => FALSE, 'message' => lang('message.company_error')));
		}

		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['status'] = 0;

		if(isset($_FILES['logo'])) {
			$UploadResp = $this->upload_image('logos', 'logo', false);
	    	if($UploadResp['state'] == 'failed') {
	    		return $this->render_json(array('success'=>FALSE, 'message' => $UploadResp['msg']));
	        }

	        if (!empty($UploadResp['uploadData']['file_name'])) {
		        $data['logo_url'] = $UploadResp['uploadData']['file_name'];
		    }
		}

		if(isset($_FILES['avatar'])) {
	        $UploadResp = $this->upload_image('photos', 'avatar', false);
	    	if($UploadResp['state'] == 'failed') {
	    		return $this->render_json(array('success'=>FALSE, 'message' => $UploadResp['msg']));
	        }

	        if (!empty($UploadResp['uploadData']['file_name'])) {
	        	$data['avatar_url'] = $UploadResp['uploadData']['file_name'];
		    }
		}

	    $products = explode(',', $data['products']);
	    unset($data['products']);

	    if(array_key_exists('reset_logo', $data) && $data['reset_logo'] === true)
	    {
	    	$data['logo_url'] = "";
		}
		
		$userIp = $this->input->ip_address();
		$data['ip_addr'] = $userIp;

		$company = $this->Company_model->save($data);

		if (empty($products)) {
			$this->Product_model->delete(array('company_id'=>$company['id']));
		} else {
			$this->db->where('company_id', $company['id']);
			$this->db->where_not_in('img_url', $products);
			$this->Product_model->delete();

	        foreach($products as $product_url) {
	        	$this->Product_model->update(array('company_id' => $company['id']), array('img_url' =>$product_url));
	        }
		}

		$this->User_model->update(array('company_id'=>$company['id']), array('id'=>$userdata->id));

		if ($user->type != 1) {
			$userdata = $this->User_model->select($userdata->id);
			$this->session->set_userdata('user', $userdata);
		}

		return $this->render_json(array('success' => TRUE, 'message' => lang('message.save_success')));
	}

	public function post_preview()
	{
		$user = $this->session->userdata('user');
		if (!isset($user)) {
			return redirect('login');
		}

		$data = $this->input->post(NULL);

		$company = $this->Company_model->select($user->company_id);
		$data['id'] = $user->company_id;
		
		$data['company_name'] = preg_replace('#[^a-z0-9\-_ ]#ui', '', $data['company_name_'.userLang()]);
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['logo_url'] = isset($data['logo_url']) ? $data['logo_url'] : (empty($company) ? '' : $company->logo_url);
		$data['avatar_url'] = isset($data['avatar_url']) ? $data['avatar_url'] : (empty($company) ? '' : $company->avatar_url);

		$city = $this->City_model->select($data['city_id']);
		$data['city_name'] = empty($city) ? '' : $city->name;

        if (isset($_FILES['logo'])) {
            if (is_uploaded_file($_FILES['logo']['tmp_name']) 
                && in_array(substr($_FILES['logo']['name'], -4), array('.gif','.jpg','.png', '.tif','.bmp','jpeg'))) {
            	$file_ext = substr($_FILES['logo']['name'], -4);
            	if($file_ext == "jpeg") $file_ext == ".jpeg";
                $data['logo_url'] = md5(random_string('alnum', 16)) . $file_ext;
                move_uploaded_file($_FILES['logo']['tmp_name'], './upload/logos/' . $data['logo_url']);
            } else {
                return $this->render_json(array('success'=>FALSE, 'message' => 'Invalid image type'));
            }
        }

        if (isset($_FILES['avatar'])) {
            if (is_uploaded_file($_FILES['avatar']['tmp_name']) 
                && in_array(substr($_FILES['avatar']['name'], -4), array('.gif','.jpg','.png', '.tif','.bmp','jpeg'))) {
            	$file_ext = substr($_FILES['logo']['name'], -4);
            	if($file_ext == "jpeg") $file_ext == ".jpeg";
                $data['avatar_url'] = md5(random_string('alnum', 16)) . $file_ext;
                move_uploaded_file($_FILES['avatar']['tmp_name'], './upload/photos/' . $data['avatar_url']);
            } else {
                return $this->render_json(array('success'=>FALSE, 'message' => 'Invalid image type'));
            }
        }

        $this->session->set_userdata('profile_preview', $data);
		$this->session->set_userdata('preview_lang', array('lang'=>'en'));

		return $this->render_json(array('success' => TRUE));
	}

	public function get_preview($company_id=NULL)
	{
		$user = $this->session->userdata('user');
		if (!isset($user)) {
			return redirect('login');
		}
		$viewData['categories'] = $this->Category_model->all(array('status' => 1), array('sort' => 'asc'));
		if (isset($company_id) && $company_id != 0) {
			$company = $this->Company_model->select($company_id);
			if (!empty($company)) {
				$data = get_object_vars($company);
				$data['products'] = '';
				$products = $this->Product_model->all(array('company_id' => $company_id));
				foreach ($products as $key => $value) {
					$data['products'] .= (empty($data['products']) ? '' : ',') . $value->img_url;
				}
				$this->session->set_userdata('profile_preview', $data);
			}
		}

		$this->render('preview', 'empty');
	}

	public function change_preview_lang($lang=NULL){
		if($lang != NULL)
			$data['lang'] = $lang;
		else
			$data['lang'] = 'fr';
		$this->session->set_userdata('preview_lang', $data);
		$result['success'] = TRUE;
		echo json_encode($result);
	}

	public function product_image_add()
    {
    	$result = array('success'=>TRUE);

    	$uploadResp = $this->upload_image("products", "product");

    	if (!empty($uploadResp['uploadData']['file_name'])) {
            $this->make_thumbnail($uploadResp['uploadData']['file_name'], 'products', 260, false, 'thumb');
			$result['id'] = $this->Product_model->insert(array('img_url'=>$uploadResp['uploadData']['file_name'], 'created_at'=>date('Y-m-d H:i:s')));
			$result['url'] = $uploadResp['uploadData']['file_name'];
           	$result['success'] = TRUE;
        }
        
        return $this->render_json($result);
    }

    public function post_profile()
	{
		$user = $this->session->userdata('user');
		if (!isset($user)) {
			return $this->render_json(array('success' => FALSE, 'message' => lang('signin.invalid')));
		}

		$data = $this->input->post(NULL);

		$config = array(
	        array(
	            'field' => 'phone',
	            'label' => humanize(lang('user.phone')),
	            'rules' => 'required',
	            'errors' => array(
	                'required' => lang('validation.user.phone.require')
				),
	        ),
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			return $this->render_json(array('success' => FALSE, 'message' => validation_errors()));
		}

		if ($user->type == 1) {
			$data['id'] = $data['user_id'];
			if (!empty($data['password'])) {
				$data['password'] = md5($data['password']);
			} else {
				unset($data['password']);
			}
			unset($data['new_password']);
			unset($data['confrim_password']);
		} else {
			if (!empty($data['password'])) {
				if (md5($data['password']) != $user->password) {
					return $this->render_json(array('success' => FALSE, 'message' => lang('signin.invalid')));
				}
				if (empty($data['new_password'])) {
					return $this->render_json(array('success' => FALSE, 'message' => lang('validation.user.new_password.require')));
				}
				if ($data['new_password'] != $data['confirm_password']) {
					return $this->render_json(array('success' => FALSE, 'message' => lang('validation.user.confirm_password.valid')));
				}
				$data['password'] = md5($data['new_password']);
			} else {
				unset($data['password']);
			}
			unset($data['new_password']);
			unset($data['confrim_password']);
			$data['id'] = $user->id;
		}

		unset($data['user_id']);

		$data['created_at'] = date('Y-m-d H:i:s');
		$data['status'] = 1;

		$this->User_model->save($data);

		if ($user->type != 1) {
			$user = $this->User_model->select($user->id);
			$this->session->set_userdata('user', $user);
		}
		$this->log("Password Recovery",$user);
		return $this->render_json(array('success' => TRUE, 'message' => lang('message.save_success')));
	}

	public function post_favorite()
	{
		$company_id = $this->input->post('company_id');
		$favorite = $this->input->post('favorite');

		$favorites = $this->session->userdata('favorites');
		if (empty($favorites)) {
			if ($favorite == 1) {
				$favorites[$company_id] = 1;
			}
		} else {
			if ($favorite == 1) {
				if (!array_key_exists($company_id, $favorites)) {
					$favorites[$company_id] = 1;
				}
			}
			if ($favorite == 0) {
				if (array_key_exists($company_id, $favorites)) {
					unset($favorites[$company_id]);
				}
			}
		}
		$this->session->set_userdata('favorites', $favorites);

		return $this->render_json(array('success' => TRUE));
	}

	function get_category_name_by_lang($category_name){
		$category = $this->Category_model->one_like($category_name);
		if(!empty($category)) {
			$category_name = ($category->name)?($category->name):$category->name_fr;
		}else {
			return redirect('companies');
		}
		return $category_name;
	}

	public function get_companies($category_name=NULL, $is_fav=NULL, $city_name=NULL)
	{
		if(get_cookie('layout_style', TRUE) == 'info')
			set_cookie('layout_style' , 'list' , 86500);
		$category_name = parseSEOStr($category_name);
		if($category_name != "")
			$category_name = $this->get_category_name_by_lang($category_name);
		$filter = array('status' => 1);
		$order_by = array();

		$cat_name = '';

		$this->session->unset_userdata('category');
		if (!empty($category_name)) {
			$cat = $this->Category_model->one(array('name_'.userLang() => $category_name));
			if (!empty($cat)) {
				$filter['companies.category_id'] = $cat->id;
				$cat_name = $category_name;
				$this->session->set_userdata('category', $cat->{'name_'.userLang()});
				$this->session->set_userdata('category_id', $cat->id);
			}
		}

		$user = $this->session->userdata('user');
		$company_ids = array();

		if (!empty($is_fav)) {
			$favorites = $this->session->userdata('favorites');
			if (!empty($favorites)) {
				foreach ($favorites as $key => $value) {
					$company_ids[] = $key;
				}
			}
			$cat_name = 'favorites';
		}

		$q = empty($_POST['q']) ? '' : $_POST['q'];
		if (!empty($q)) {
			$filter['search'] = $q;
		}
		$city = $this->input->post('city');

		if (empty($city)) {
			$city_sel = get_cookie('city_sel', TRUE);
			if (empty($city_sel)) {
				$city = 'all';
			} else {
				$city = $city_sel;
			}
		}
		if ($city != 'all') {
			$row = $this->City_model->one(array('name'=>$city));
			if (!empty($row)) {
				$filter['city_id'] = $row->id;
				set_cookie('city_sel', $city, 86500);
			}
		} else set_cookie('city_sel', '');

		$emp = $this->input->post('emp');
		if (empty($emp)) {
			$emp_sel = get_cookie('emp_sel', TRUE);
			if (empty($emp_sel)) {
				$emp = 'all';
			} else {
				$emp = $emp_sel;
			}
		}
		
		// set sort to cookie
		set_cookie('emp_sel', $emp, 86500);

		if($emp != 'all'){
			$filter['employee_count'] = $emp;
		}
		
		if (!empty($is_fav)) {
			if (!empty($company_ids)) {
				$this->db->where_in('companies.id', $company_ids);
				$total = $this->Company_model->count($filter);
			} else {
				$total = 0;
			}
		} else {
			$total = $this->Company_model->count($filter);
		}

		if($this->input->is_ajax_request()) {
			$start = $this->input->post('start');
			$filter['start'] = empty($start) ? 0 : $start;
		} else {
			$filter['start'] = 0;
		}
		
		$filter['limit'] = 20;

		$sort = $this->input->post('sort');
		if (empty($sort)) {
			$sort_sel = get_cookie('sort_sel', TRUE);
			if (empty($sort_sel)) {
				$sort = 'rating';
			} else {
				$sort = $sort_sel;
			}
		}
		
		// set sort to cookie
		set_cookie('sort_sel', $sort, 86500);

		if ($sort == 'rating') {
			$order_by['companies.rating'] = 'desc';
		} else if ($sort == 'newest') {
			$order_by['companies.created_at'] = 'desc';
		} else if ($sort == 'oldest') {
			$order_by['companies.created_at'] = 'asc';
		}

		if (!empty($is_fav)) {
			if (!empty($company_ids)) {
				$this->db->where_in('companies.id', $company_ids);
				$companies = $this->Company_model->all($filter, $order_by);
			} else {
				$companies = array();
			}
		} else {
			$companies = $this->Company_model->all($filter, $order_by);
		}
		// var_dump($this->db->last_query());exit;

		foreach ($companies as $row) {
			$row->products = $this->Product_model->all(array('company_id' => $row->id));
		}

		if($this->input->is_ajax_request()) {
			$this->load->library('user_agent');
			if ($this->agent->is_mobile() && !strpos($this->agent->agent, 'iPad')) {
				$html = $this->load->view('front/mobile/_partials/company_part', array('companies'=>$companies), TRUE);
			} else {
				$html = $this->load->view('front/_partials/company_part', array('companies'=>$companies), TRUE);
			}
			return $this->render_json(array('count' => count($companies), 'html' => $html));
		} else {
			if (!empty($q)) {
				$stext['text'] = $q;
				$stext['ip_addr'] = $this->input->ip_address();
				$stext['lang'] = userLang();
				$stext['created_at'] = date('Y-m-d H:i:s');
				$stext['results_count'] = $total;
				$stext['session_id'] = session_id();
				$this->STextLog_model->insert($stext);
			}

			$this->session->set_userdata('city_sel', $city);
			$this->session->set_userdata('sort_sel', $sort);
			$this->session->set_userdata('emp_sel', $emp);
			$this->session->set_userdata('company_total', $total);

			$this->mViewData['total'] = $total;
			$this->mViewData['companies'] = $companies;
			$this->mViewData['sEcho'] = $q;
			$this->mViewData['cat_name'] = $cat_name;
			$this->mViewData['city_sel'] = $city;
			$this->mViewData['sort_sel'] = $sort;
			$this->mViewData['emp_sel'] = $emp;
			$this->mViewData['og_tags'] = array(
				'title' => 'made-in.ma - '.$cat_name,
				'image' => base_url('assets/madinma/front/images/logo_ogp.png'),
				'desc' => lang('site.description')
			);
			$this->mViewData['is_fav'] = $is_fav;
			$this->render('companies');
		}
	}
	
	function get_company_name_by_lang($company_name){
		$company = $this->Company_model->one_like($company_name);

		if(!empty($company)) {
			$data['company_name'] = ($company->company_name)?($company->company_name):$company->company_name_fr;
			$data['company_lang'] = ($company->company_name)?'other':'fr';
		}else {
			return redirect('companies');
		}
		return $data;
	}

	public function company_detail($company_name=NULL)
	{	

		$this->sessionlog_inccount();
		if(empty($this->Company_model->one_like(parseSEOStr($company_name))))
			$company_name = parseCompanySEOStr($company_name);
		else
			$company_name = parseSEOStr($company_name);

		$company_data = $this->get_company_name_by_lang($company_name);

		$company_name = $company_data['company_name'];
		if($company_data['company_lang'] == 'fr')
			$company = $this->Company_model->one(array('company_name_fr' => $company_name, 'status' => 1));
		else
			$company = $this->Company_model->one(array('company_name_'.userLang() => $company_name, 'status' => 1));

		if (empty($company)) {
			return redirect('companies');
		}
		$this->Inquery_model->insert(array('company_id' => $company->id, 'created_at' => date('Y-m-d H:i:s')));

		$company_products = $this->Product_model->all(array('company_id' => $company->id));
		$company_reviews = $this->Review_model->all(array('company_id' => $company->id, 'status' => 1));
		$inquery_cnt = $this->Inquery_model->count(array('company_id' => $company->id));

		$this->mViewData['company'] = $company;
		$this->mViewData['company_products'] = $company_products;
		$this->mViewData['company_reviews'] = $company_reviews;
		$this->mViewData['inquery_cnt'] = $inquery_cnt;

		$og_image = '';

		if(empty($company->logo_url)) {
			if(!empty($company->avatar_url)) {
				$og_image = base_url('upload/photos/').$company->avatar_url;
			} else {
				$og_image = base_url('assets/madinma/front/images/logo_ogp.png');
			}
		} else {
			$og_image = base_url('upload/logos/').$company->logo_url;
		}

		$og_desc = '';
		if(empty($company->{'desc_'.userLang()})) {
			$og_desc = lang('site.description');
		} else {
			$og_desc = $company->{'desc_'.userLang()};
		}

		$this->mViewData['og_tags'] = array(
			'title' => 'made-in.ma - '.$company->{'company_name_'.userLang()},
			'image' => $og_image,
			'desc' => $og_desc
		);

		$this->render('detail');
	}

	public function post_rating()
	{
		$config = array(
	        array(
                'field' => 'name',
                'label' => humanize(lang('rating.name')),
                'rules' => 'required'
	        ),
	        array(
                'field' => 'country_id',
                'label' => humanize(lang('rating.country')),
                'rules' => 'required'
	        ),
	        array(
                'field' => 'name',
                'label' => humanize(lang('rating.text')),
                'rules' => 'required'
	        )
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			return $this->render_json(array('success' => FALSE, 'message' => validation_errors()));
		}

		$data = $this->input->post();
		$recaptchaResponse = trim($data['recaptcha_response']);
		$userIp=$this->input->ip_address();

		$url="https://www.google.com/recaptcha/api/siteverify?secret=".RECAPTCHA_SECRET_KEY."&response=".$recaptchaResponse;
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      
         
        $status = json_decode($output, true);

        if($status['success'] == true && $status['action'] == 'detail' && $status['score'] >= 0.5) {
			$data['language'] = userLang();
			$data['created_at'] = date('Y-m-d H:i:s');
			// $data['language'] = userLang();
			unset($data['recaptcha_response']);
			$data['ip_addr'] = $userIp;
			$this->Review_model->insert($data);

			$this->Company_model->update(array('rating' => getCompanyRating($data['company_id'])), array('id' => $data['company_id']));
			return $this->render_json(array('success' => TRUE, 'message' => lang('message.save_success')));
		} else {
			return $this->render_json(array('success' => FALSE, 'message' => lang('message.recaptacha_error')));
		}

	}

	public function post_support()
	{
		$data = $this->input->post();
		$data['ip_addr'] = $this->input->ip_address();
		$data['created_at'] = date('Y-m-d H:i:s');
		$this->Support_model->insert($data);

		return $this->render_json(array('success' => TRUE));
	}

	public function post_mail()
	{

		$user = $this->session->userdata('user');
		$this->log_message("sms sended by fronted",$user);

		$data = $this->input->post();

		$recaptchaResponse = trim($data['recaptcha_response']);
		unset($data['recaptcha_response']);
		
		$userIp = $this->input->ip_address();

		$url="https://www.google.com/recaptcha/api/siteverify?secret=".RECAPTCHA_SECRET_KEY."&response=".$recaptchaResponse;
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      
		$status= json_decode($output, true);
        if($status['success'] == true && ($status['action'] == 'contact' || $status['action'] == 'detail') && $status['score'] >= 0.5) {
        	$data['language'] = userLang();
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['ip_addr'] = $userIp;
			$data['session_id'] = session_id();
			set_cookie('contact_name', $data['name'], 86500);
			set_cookie('contact_email', $data['email'], 86500);
			set_cookie('contact_phone', $data['phone'], 86500);
			set_cookie('contact_country', $data['country_id'], 86500);
			set_cookie('contact_content', $data['content'], 86500);

			$this->Message_model->insert($data);
			if(!empty($data['company_id'])) {
				// Send sms to the manufacturer
				$company = $this->Company_model->select($data['company_id']);
				if(!empty($data['company_id'])) {
					$country = $this->Country_model->select($data['country_id']);
					$sms = $data['name'].','.$data['phone'].','.$data['email'].','.$country->name_fr.','.prefixCutStr($data['content'],160);
				}else{
					$sms = lang('contact.sms');
				}
				$sms_resp = send_sms($company->phone, $sms);			
				if(!$sms_resp['success']) {
					return $this->render_json(array('success' => FALSE, 'message' => lang('contact.sms.error')));
				} else {
					return $this->render_json(array('success' => TRUE, 'message' => lang('contact.sms.success')));
				}
			}
			return $this->render_json(array('success' => TRUE));
        } else {
        	return $this->render_json(array('success' => FALSE, 'message' => lang('message.recaptacha_error')));
        }	

	}
	
	public function post_call()
	{
		$company_id = $this->input->post('company_id');

		$company = $this->Company_model->select($company_id);

		// TODO call

		return $this->render_json(array('success' => TRUE));
	}

	public function get_stexts()
	{
		$q = $this->input->get('q');
		if (empty($q)) {
			return $this->render_json(array('success' => TRUE, 'stexts' => array()));
		}

		$filter['search'] = $q;
		$filter['lang'] = userLang();
		// $filter['allowed'] = 1;
		//autocomplete
		$this->db->group_by('stexts.text');
		$stexts = $this->SText_model->all($filter);

		return $this->render_json(array('success' => TRUE, 'stexts' => $stexts));
	}

	public function change_layout($type)
	{
		set_cookie('layout_style', $type, 86500);
		
		return $this->render_json(array('success' => TRUE));
	}

	public function delete_account()
	{
		$user = $this->session->userdata('user');
		if (!isset($user)) {
			return $this->render_json(array('success' => FALSE));
		}

		$this->Company_model->update(array('status' => 3), array('id' => $user->company_id));
		$this->User_model->update(array('status' => 3), array('id' => $user->id));

		$this->session->set_userdata('user', NULL);

		return $this->render_json(array('success' => TRUE));
	}

	public function get_info()
	{
		$this->load->library('user_agent');

		if ($this->agent->is_mobile()) {
			$this->mViewData['cur_page'] = 'info';
			$this->render('info');
		} else {
			return redirect('/');
		}
	}

	public function get_phone($company_id)
	{
		$company = $this->Company_model->select($company_id);

		if (!empty($company)) {
			return $this->render_json(array('success' => TRUE, 'phone_num' => $company->phone));
		} else {
			return $this->render_json(array('success' => FALSE));
		}
	}

	public function messages_read_ajax()
    {
    	$user = $this->session->userdata('user');
		if (!isset($user)) {
			return $this->render_json(array());
		}

    	$datatable = array_merge([], $_REQUEST);
    	$datatable['filter']['is_contact'] = 0;
    	$datatable['filter']['company_id'] = $user->company_id;

    	$displayStart = isset($datatable['iDisplayStart']) ? $datatable['iDisplayStart'] : 0;
    	$displayLength = isset($datatable['iDisplayLength']) ? $datatable['iDisplayLength'] : -1;
    	$search = isset($datatable['sSearch']) ? $datatable['sSearch'] : '';
    	$sortingCols = isset($datatable['iSortingCols']) ? $datatable['iSortingCols'] : 0;    	
    	$data = $filter = $order = array();
    	if (isset($datatable['filter']))
    		$filter = $datatable['filter'];
    	$filter['search'] = $search;
		if ($displayLength != -1) {
			$filter['start'] = $displayStart;
    		$filter['limit'] = $displayLength;    		
		}
		for ($i = 0; $i < $sortingCols; $i++) {
			$sortCol = $datatable['iSortCol_' . $i];
			$sortDir = $datatable['sSortDir_' . $i];
			$dataProp = $datatable['mDataProp_' . $sortCol];
			$order[$dataProp] = $sortDir;
		}

		$data['data'] = $this->Message_model->all($filter, $order);    		
		$data['iTotalDisplayRecords'] = $this->Message_model->count($filter);
		$data['iTotalRecords'] = $this->Message_model->count($filter);
		$data['sEcho'] = $search;

        return $this->render_json($data);
    }


	public function logout()
	{
		$user = $this->session->userdata('user');
		$this->userlogout($user);
		$this->session->set_userdata('user', NULL);
		return redirect('login');
	}
	public function userlogout($user){

		$slog['userid'] = $user->id;
		$slog['ip'] = $this->input->ip_address();
		$slog['created_at'] = date('Y-m-d H:i:s');
		$slog['session_id'] = session_id();
		$slog['status'] = 'login out';
		$this->UserLog_model->save($slog);
	}

	public function product_watermark($img) {

		$config = array();
                        
		$config['image_library'] = 'GD2';
		$config['source_image']  = './upload/products/'.$img;

		list($width, $height) = getimagesize($config['source_image']);

		$config['wm_type']       = 'overlay';    
		$config['width'] = intval($width/10);
        $config['height'] = intval($width/10);     	      
		$config['wm_overlay_path'] = './assets/madinma/front/images/watermark.png';

		$config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'right';
		                        
		$this->image_lib->initialize($config);
		$this->image_lib->watermark(); 

		$this->output
        ->set_content_type(mime_content_type($config['source_image'])) // You could also use ".jpeg" which will have the full stop removed before looking in config/mimes.php
        ->set_output(file_get_contents($config['source_image']));
	}

	public function go($keyword) {
		$filter['keyword'] = $keyword;
		$data = $this->Url_shortener_model->one($filter);
		redirect($data->company_url);
	}

	public function change_picture_order(){
		$ids = $this->input->post('order');
		
		foreach ($ids as $key => $id) {
			# code...
			$this->db->set('order',$key);
			$this->db->where('id', $id);
			$this->db->update('products');
		}
	}
	public function log($text,$user)
	{
		$slog['text'] = $text;
		$slog['type'] = "password recovery";
		$slog['phone_number']= $user->phone;
		$slog['ip_addr'] = $this->input->ip_address();
		$slog['created_at'] = date('Y-m-d H:i:s');
		   $slog['session_id'] = session_id();
		$this->Sms_model->save($slog);		
	}
	public function log_message($text,$user)
	{
		$slog['text'] = $text;
		$slog['type'] = "message";
		$slog['phone_number']= isset($user->phone)?$user->phone:'';
		$slog['ip_addr'] = $this->input->ip_address();
		$slog['created_at'] = date('Y-m-d H:i:s');
		   $slog['session_id'] = session_id();
		$this->Sms_model->save($slog);		
	}
}
