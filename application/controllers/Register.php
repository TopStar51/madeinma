<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/FrontController.php";

class Register extends FrontController
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Sms_model');
	}

	public function index()
	{
		$promotion_code = $this->input->get('code');
		$user = $this->session->userdata('user');
		if (isset($user) ) {
			return redirect('myaccount');
		}

		$this->mViewData['cur_page'] = 'register';
		$this->mViewData['code'] = $promotion_code;

		$this->render('register', 'empty');
	}

	public function post_register()
	{
		$user = $this->session->userdata('user');

		$this->log("Register", $user);

		$config = array(
	        array(
	            'field' => 'phone',
	            'label' => 'Phone',
	            'rules' => 'required|is_unique[users.phone]',
	            'errors' => array(
	                'required' => lang('validation.user.phone.require'),
	                'is_unique' => lang('validation.user.phone.unique')
	    		),
	        ),
	        array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
                'errors' => array(
	            	'required' => lang('validation.user.password.require')
	    		)
	        ),
	        array(
	        	'field' => 'confirm_password',
	        	'label' => 'Confirm Password',
	        	'rules' => 'required|matches[password]',
	        	'errors' => array(
	        		'required' => lang('validation.user.confirm_password.require'),
	        		'matches' => lang('validation.user.confirm_password.valid')
	        	),
	        )
		);

		$this->form_validation->set_rules($config);

		if($this->form_validation->run() == FALSE)
		{
			return $this->render_json(array('success' => FALSE, 'message' => validation_errors()));
		}

		$data = elements(array('name', 'phone', 'password', 'recaptcha_response', 'code'), $_POST);

		$recaptchaResponse = trim($data['recaptcha_response']);
		unset($data['recaptcha_response']);
		$userIp = $this->input->ip_address();

		$url = "https://www.google.com/recaptcha/api/siteverify?secret=".RECAPTCHA_SECRET_KEY."&response=".$recaptchaResponse;
 
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      
         
		$status = json_decode($output, true);

		// check for duplicate (phone)
		if ($this->User_model->one(array('phone' => $data['phone']))) {
			exit(json_encode(array('success' => false, 'message' => lang('validation.user.phone.unique'))));
		}

		// debuger here
        if($status['success'] == true && $status['action'] == 'register' && $status['score'] >= 0.5) {
        	$redirect_url = '';
        	if(isset($data['code']) && $data['code'] == ADMIN_PROMO_CODE) {
        		$redirect_url = 'myaccount';
        		$data['status'] = 1;
        		$data['input_promo_code'] = $data['code'];
				$code = $data['code'];
				$this->db->set('promotion_count', 'promotion_count +1', FALSE)->where('promotion_code', $code)->update('users');
        	} else {
        		$redirect_url = 'sms_verify';
	        	while(true) {
	        		$sms_code = rand(100000,1000000);
	        		$dupCheck = $this->db->from('users')->where('sms_code', $sms_code)->get()->row();
					if (!$dupCheck) break;
	        	}
				$sms_resp = send_sms($data['phone'], $sms_code);
				if(!$sms_resp['success']) {
					exit(json_encode(array('success' => false, 'message' => $sms_resp['error'])));
				}
				if (isset($data['code'])) {
					$code = $data['code'];
					$data['input_promo_code'] = $code;
					$this->db->set('promotion_count', 'promotion_count +1', FALSE)->where('promotion_code', $code)->update('users');
				}
        	}
			// unique rand string ...
			while (TRUE) {
				$code = random_string('alnum', 4);
				$dupCheck = $this->db->from('users')->where('promotion_code', $code)->get()->row();
				if (!$dupCheck) break;
			}
			unset($data['code']);
			$data['promotion_code'] = $code;
			$data['password'] = md5($data['password']);
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['sms_code'] = $sms_code;
			$data['type'] = 0; // manufacturer		
		    $data['language'] = userLang();

			$user = $this->User_model->save($data);
			$user = $this->User_model->select($user['id']);

			return $this->render_json(array('success' => TRUE, 'redirect_url'=>$redirect_url));

		} else {
			return $this->render_json(array('success' => FALSE, 'message' => lang('message.recaptacha_error')));
		}
	}
	public function log($text,$user)
	{
		$slog['text'] = $text;
		$slog['type'] = "Register";
		$slog['phone_number']= $user->phone;
		$slog['ip_addr'] = $this->input->ip_address();
		$slog['created_at'] = date('Y-m-d H:i:s');
		$slog['session_id'] = session_id();
		$this->Sms_model->save($slog);		
	}

}
