<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/AdminController.php";

class Keyword extends AdminController {

	function __construct() 
	{
		parent::__construct();

		$this->load->model('SText_model');
	}
	public function index()
	{
		$this->mPageTitle = 'Keywords for Autocomplete';
		return $this->render('admin/pages/keywords');
	}

	public function datatable_read_ajax()
    {
		$datatable = array_merge([], $_REQUEST);
		$result = $this->datatable_read($datatable, 'SText_model');
		$i = 0;
		foreach($result['data'] as $key => $value){
			$i++;
			$value->index = $_REQUEST['iDisplayStart'] + $i;
		}
        return $this->render_json($result);
    }

    public function keyword_delete_ajax()
    {
        $ids = $this->input->post('ids', TRUE);
        $result = $this->stexts_model->delete_many($ids);
        $this->render_json(array('success' => $result));
    }

    public function post_keyword() {
        $data = $this->input->post();

        // $data['text'] = preg_replace('#[^a-z0-9\-_ ]#ui', '', $data['text']);

        $data['allowed'] = 1;

        $this->SText_model->save($data);

        $this->render_json(array('success' => TRUE));
    }

}
