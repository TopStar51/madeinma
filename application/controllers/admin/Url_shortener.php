<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/AdminController.php";

class Url_shortener extends AdminController {

	function __construct() 
	{
		parent::__construct();

        $this->load->model('Url_shortener_model');
	}

	public function index()
	{
		$this->mPageTitle = 'URL Shortener';
		return $this->render('admin/pages/url_shortener');
	}

	public function datatable_read_ajax()
    {
    	$datatable = array_merge([], $_REQUEST);
		$result = $this->datatable_read($datatable, 'Url_shortener_model');
		$i = 0;
		foreach($result['data'] as $key => $value){
			$i++;
			$value->index = $_REQUEST['iDisplayStart'] + $i;
		}
        return $this->render_json($result);
    }

    public function post_shorten_url() {
        $data = $this->input->post();
        $data['created_at'] = date('Y-m-d H:i:s');
        $this->Url_shortener_model->save($data);

        $this->render_json(array('success' => TRUE));
    }

	public function delete_ajax()
    {
        $ids = $this->input->post('ids', TRUE);
        $result = $this->Url_shortener_model->delete_many($ids);
        return $this->render_json(array('success' => $result));
    }
}
