<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/AdminController.php";

class Category extends AdminController {

	function __construct() 
	{
		parent::__construct();

		$this->load->model('Category_model');
	}

	public function index()
	{
		$this->mPageTitle = 'Category';
		return $this->render('admin/pages/category');
	}

	public function datatable_read_ajax()
    {
		$datatable = array_merge([], $_REQUEST);
		
		$result = $this->datatable_read($datatable, 'Category_model');
		$i = 0;
		foreach($result['data'] as $key => $value){
			$i++;
			$value->index = $_REQUEST['iDisplayStart'] + $i;
		}
        return $this->render_json($result);
    }

    public function category_delete_ajax()
    {
        $ids = $this->input->post('ids', TRUE);
        $result = $this->Category_model->delete_many($ids);
        $this->render_json(array('success' => $result));
    }

    public function post_category() {
        $data = $this->input->post();

        if (!file_exists("./upload/categories/"))
        {
            mkdir("./upload/categories/", 0777, TRUE);
        }

        $data['name_en'] = preg_replace('#[^a-z0-9\-_ ]#ui', '', $data['name_en']);

        $data['status'] = 1;

        $this->Category_model->save($data);

        $this->render_json(array('success' => TRUE));
    }

    public function change_sort_ajax()
    {
        $id = $this->input->post('id', TRUE);
        $sort = $this->input->post('sort');
        $result = $this->Category_model->update(array('sort' => $sort), array('id' => $id));
        $this->render_json(array('success' => $result));
    }

}
