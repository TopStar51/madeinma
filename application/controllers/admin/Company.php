<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/AdminController.php";

class Company extends AdminController {

	function __construct() 
	{
		parent::__construct();

		$this->load->model('User_model');
        $this->load->model('Company_model');
        $this->load->model('Sms_model');
	}

	public function index()
	{
		$this->mPageTitle = 'Release updates';
		return $this->render('admin/pages/updates');
	}

	public function datatable_read_ajax()
    {
        $datatable = array_merge([], $_REQUEST);
		$result = $this->datatable_read($datatable, 'Company_model');
		$i = 0;
		foreach($result['data'] as $key => $value){
			$i++;
			$value->index = $_REQUEST['iDisplayStart'] + $i;
		}
        return $this->render_json($result);
    }

    public function change_status_ajax()
    {
        $id = $this->input->post('id', TRUE);
        $status = $this->input->post('status');
        $bsms = $this->input->post('sms_status');
        if ($bsms == true){
            $this->log("After release", $this->session->userdata('user'));
        }
        $result = $this->Company_model->update(array('status' => $status), array('id' => $id));
        $this->render_json(array('success' => $result));
    }

    public function log($text,$user)
    {
        $slog['text'] = $text;
        $slog['type'] = "after release sms confirm ask yes or no";
        $slog['phone_number']= $user->phone;
        $slog['ip_addr'] = $this->input->ip_address();
        $slog['created_at'] = date('Y-m-d H:i:s');
        $slog['session_id'] = session_id();
        $this->Sms_model->save($slog);      
    }

    public function company_delete_ajax()
    {
        $ids = $this->input->post('ids', TRUE);
        $result = $this->Company_model->delete_many($ids);
        return $this->render_json(array('success' => $result));
    }

}
