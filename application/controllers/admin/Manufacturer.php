<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/AdminController.php";

class Manufacturer extends AdminController {

	function __construct() 
	{
		parent::__construct();

		$this->load->model('User_model');
        $this->load->model('Company_model');
        $this->load->model('Category_model');
        $this->load->model('City_model');
	}

	public function index()
	{
        $this->mPageTitle = 'Manufacturer';
        $viewData['categories'] = $this->Category_model->all(array('status' => 1), array('sort' => 'asc'));
        $viewData['cities'] = $this->City_model->all();
		return $this->render('admin/pages/manufacturer', 'default', $viewData);
	}

	public function datatable_read_ajax()
    {
    	$datatable = array_merge([], $_REQUEST);

        $datatable['filter']['type'] = 0; // get manufacturers
        $datatable['filter']['users.company_id IS NOT NULL'] = NULL;
		
		$result = $this->datatable_read($datatable, 'User_model');
		$i = 0;
		foreach($result['data'] as $key => $value){
			$i++;
			$value->index = $_REQUEST['iDisplayStart'] + $i;
		}
        return $this->render_json($result);
    }

    public function change_show_ajax()
    {
        $id = $this->input->post('id', TRUE);
        $show_home = $this->input->post('show_home');
        $result = $this->Company_model->update(array('show_home' => $show_home), array('id' => $id));
        $this->render_json(array('success' => $result));
    }

    public function change_status_ajax()
    {
        $id = $this->input->post('id', TRUE);
        $status = $this->input->post('status');
        $result = $this->Company_model->update(array('status' => $status), array('id' => $id));
        $result = $this->User_model->update(array('status' => $status), array('company_id' => $id));
        // var_dump($this->db->last_query());exit;
        $this->render_json(array('success' => $result));
    }

    public function manufacturer_delete_ajax()
    {
        $ids = $this->input->post('ids', TRUE);
        // deleting manafacturer means, deleting companies too --> by Elvis 2019-08-12
        // get company id from manufacturer
        $user = $this->db->from('users')->where('id', $ids)->get()->row();
        if ($user->company_id) {
            $this->db->delete('companies', array('id' => $user->company_id));
        }
        // deleting company end ....
        $result = $this->User_model->delete_many($ids);
        return $this->render_json(array('success' => $result));
    }

    public function api_delete_promotion() {
        $id = $this->input->post('id');
        $delete_count = $this->input->post('count');

        $this->db->set('promotion_count', 'promotion_count - '.$delete_count, FALSE)
                ->where('id', $id)
                ->update('users');

        $this->render_json(array('success' => true));
    }

    public function api_reset_promotion() {
        $id = $this->input->post('id');

        $this->db->update('users', array('promotion_count' => 0), array('id' => $id));

        $this->render_json(array('success' => true));
    }

    public function post_company() {
        $data = $this->input->post();

        // check for duplicate (phone)
		if ($this->User_model->one(array('phone' => $data['phone']))) {
			$this->render_json(array('success' => false, 'message' => lang('validation.user.phone.unique')));
        }
        if (isset($data['code'])) {
            $code = $data['code'];
            $data['input_promo_code'] = $code;
            $this->db->set('promotion_count', 'promotion_count +1', FALSE)->where('promotion_code', $code)->update('users');
        }
        // unique rand string ...
        while (TRUE) {
            $code = random_string('alnum', 4);
            $dupCheck = $this->db->from('users')->where('promotion_code', $code)->get()->row();
            if (!$dupCheck) break;
        }
        unset($data['code']);
        $data['promotion_code'] = $code;
        $data['password'] = md5($data['password']);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['type'] = 0; // manufacturer		
        $data['language'] = userLang();
        $data['company_name_en'] = $data['company_name_fr'];
        $data['status'] = 0;
        $user = $this->User_model->save($data);
        $company = $this->Company_model->save($data);

        $this->User_model->update(array('company_id'=>$company['id']), array('id'=>$user['id']));
        $this->render_json(array('success' => TRUE));
    }
}
