<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Sms_model');
	}

	public function index()
	{
		$user = $this->session->userdata('user');
		if (isset($user) && $user->type == 1) {
			$this->log("Login", $user);

			return redirect('admin/manufacturer/index');
		}

        // $data['text'] = preg_replace('#[^a-z0-9\-_ ]#ui', '', $data['text']);

		$this->load->view('admin/login');
	}

	public function post_login()
	{
		$config = array(
	        array(
	            'field' => 'email',
	            'label' => 'Email',
	            'rules' => 'required|valid_email',
	            'errors' => array(
	            	'required' => 'The Email field is required.',
	                'valid_email' => 'Please enter a valid email address.'
	    		)
	        ),
	        array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
                'errors' => array(
	            	'required' => 'The Password field is required.'
	    		)
	        )
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('message', validation_errors());
			return $this->load->view('admin/login');
		}

		$email = $this->input->post('email', TRUE);
		$password = $this->input->post('password', TRUE);

		$user = $this->User_model->one(array('email' => $email, 'password' => md5($password), 'type' => 1));

		if (empty($user))
		{
			$this->session->set_flashdata('message', 'Invalid credentials');
			return redirect('admin/login');
		}

		$this->session->set_userdata('user', $user);
		$this->log("admin login", $user);
		return redirect('admin/manufacturer/index');
	}
	public function log($text,$user){
		$slog['text'] = $text;
		$slog['type'] = "admin panel";
		$slog['phone_number']= $user->phone;
		$slog['ip_addr'] = $this->input->ip_address();
		$slog['created_at'] = date('Y-m-d H:i:s');
		$slog['session_id'] = session_id();
		$this->Sms_model->save($slog);
	}

}
