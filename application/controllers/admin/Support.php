<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/AdminController.php";

class Support extends AdminController {

	function __construct() 
	{
		parent::__construct();

		$this->load->model('User_model');
        $this->load->model('Support_model');
	}

	public function index()
	{
		$this->mPageTitle = 'Support';
		return $this->render('admin/pages/support');
	}

	public function datatable_read_ajax()
    {
    	$datatable = array_merge([], $_REQUEST);
		$result = $this->datatable_read($datatable, 'Support_model');
		$i = 0;
		foreach($result['data'] as $key => $value){
			$i++;
			$value->index = $_REQUEST['iDisplayStart'] + $i;
		}
        return $this->render_json($result);
    }

    public function support_delete_ajax()
    {
        $ids = $this->input->post('ids', TRUE);
        $result = $this->Support_model->delete_many($ids);
        return $this->render_json(array('success' => $result));
    }

}
