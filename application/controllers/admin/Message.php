<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/AdminController.php";

class Message extends AdminController {

	function __construct() 
	{
		parent::__construct();

		$this->load->model('User_model');
        $this->load->model('Message_model');
	}

	public function index()
	{
		$this->mPageTitle = 'List of all messages';
		return $this->render('admin/pages/messages');
	}

	public function contactus()
	{
		$this->mPageTitle = 'Contact us';
		return $this->render('admin/pages/contactus');
	}

	public function datatable_read_ajax()
    {
    	$datatable = array_merge([], $_REQUEST);
		$result = $this->datatable_read($datatable, 'Message_model');
		$i = 0;
		foreach($result['data'] as $key => $value){
			$i++;
			$value->index = $_REQUEST['iDisplayStart'] + $i;
		}
        return $this->render_json($result);
    }

    public function message_delete_ajax()
    {
        $ids = $this->input->post('ids', TRUE);
        $result = $this->Message_model->delete_many($ids);
        return $this->render_json(array('success' => $result));
    }

}
