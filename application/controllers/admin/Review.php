<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH."/core/AdminController.php";

class Review extends AdminController {

	function __construct() 
	{
		parent::__construct();

		$this->load->model('User_model');
        $this->load->model('Review_model');
	}

	public function index()
	{
		$this->mPageTitle = 'Release ratings';
		return $this->render('admin/pages/reviews');
	}

	public function datatable_read_ajax()
    {
    	$datatable = array_merge([], $_REQUEST);
		$result = $this->datatable_read($datatable, 'Review_model');
		$i = 0;
		foreach($result['data'] as $key => $value){
			$i++;
			$value->index = $_REQUEST['iDisplayStart'] + $i;
		}
        return $this->render_json($result);
    }

    public function change_status_ajax()
    {
        $id = $this->input->post('id', TRUE);
        $status = $this->input->post('status');
        $result = $this->Review_model->update(array('status' => $status), array('id' => $id));
        $this->render_json(array('success' => $result));
    }

    public function review_delete_ajax()
    {
        $ids = $this->input->post('ids', TRUE);
        $result = $this->Review_model->delete_many($ids);
        return $this->render_json(array('success' => $result));
    }

}
