<!DOCTYPE html>
<html>
<?php 
	$this->load->view('front/_base/head'); 
	$preview_lang = $this->session->userdata('preview_lang');
?>
<style type="text/css">
    .navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:hover {
        background-color: transparent !important;
    }
    #map {
        width: 100%;
        height: 350px;
    }
</style>
<!-- Navbar -->
<nav class="navbar navbar-default main-navbar" style="background: #00652D;">
  	<div class="container">
	    <div class="navbar-header">
	      	<a class="navbar-brand" href="<?php echo base_url(); ?>">
                <h2 class="preview_header">Preview</h2>
			    <!-- <img class="logo-mini" src="<?php echo base_url(); ?>assets/madinma/front/images/logo_small_1.png">
			    <img class="logo-lg" src="<?php echo base_url(); ?>assets/madinma/front/images/logo-2.png"> -->
		    </a>
            <!-- <ul class="nav navbar-nav navbar-right visible-xs-1">
		    	<li class="dropdown dropdown-user img-item" style="position:initial">
					<a href="javascript:;" class="dropdown-toggle img-item" data-toggle="dropdown">
					<img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo userLang(); ?>.png">
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default" style="left:auto;right:0;position:absolute;border: 1px solid rgba(0,0,0,.15);z-index: 1000;background-color:white;">
						<?php $lang_arr = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir'); ?>
						<?php foreach ( $lang_arr as $lang): ?>
							<li class="<?php if ($lang == userLang()): ?>disabled<?php endif; ?>">
								<a href="javascript:change_language('<?php echo $lang; ?>');">
									<img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $lang; ?>.png"> <?php echo lang('general.lang.'.$lang); ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</li>
		    </ul> -->
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
		    <ul class="nav navbar-nav navbar-right">
		    	<li class="dropdown dropdown-user img-item">
					<a href="javascript:;" class="dropdown-toggle img-item" data-toggle="dropdown">
    					<img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $preview_lang['lang']; ?>.png">
    					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<?php $lang_arr = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir'); ?>
						<?php foreach ( $lang_arr as $lang): ?>
							<li class="<?php if ($lang == userLang()): ?>disabled<?php endif; ?>">
								<a href="javascript:change_language('<?php echo $lang; ?>');">
									<img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $lang; ?>.png"> <?php echo lang('general.lang.'.$lang); ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</li>
		    </ul>
		    <!-- <ul class="categories visible-xs">
				<?php //foreach ($categories as $category): ?>
				<li class="<?php if (!empty($cat_name) && $cat_name == $category->name_en || !empty($company) && $company->category_id == $category->id): ?>active<?php endif; ?> text-capitalize">
					<a class="visible-lg visible-md  visible-xs" href="<?php echo base_url('category/'.getSEOStr($category->name_en)); ?>" class="text-capitalize">
						<?php echo symbolSVG($category->symbol_url) . $category->{"name_".userLang()}; ?>
					</a>
					<a class="visible-sm" href="<?php echo base_url('category/'.getSEOStr($category->name_en)); ?>" class="text-capitalize">
						<?php echo symbolSVG($category->symbol_url); ?>
					</a>
				</li>
				<?php //endforeach; ?>
			</ul> -->
	    </div>
	    <!-- <div class="site-desc pad-l100 visible-lg visible-md" style="color:white;"><?php echo lang('site.description'); ?></div> -->
	</div>
</nav>

<script>
	$('.login-img').on('mouseover', function() {
		$('.login-img').attr('src', "<?php echo base_url(); ?>assets/madinma/front/images/button_login_small_active.png")
	});
	$('.login-img').on('mouseout', function() {
		$('.login-img').attr('src', "<?php echo base_url(); ?>assets/madinma/front/images/button_login_small<?php echo (!empty($cur_page) && $cur_page == 'login' ? '_active' : ''); ?>.png")
	});
	$('.register-img').on('mouseover', function() {
		$('.register-img').attr('src', "<?php echo base_url(); ?>assets/madinma/front/images/button_register_active.png")
	});
	$('.register-img').on('mouseout', function() {
		$('.register-img').attr('src', "<?php echo base_url(); ?>assets/madinma/front/images/button_register<?php echo (!empty($cur_page) && $cur_page == 'register' ? '_active' : ''); ?>.png")
	});
	var default_avatar = "<?php echo base_url('assets/madinma/front/images/default_avatar.jpg'); ?>";
    var default_avatar_ms = "<?php echo base_url('assets/madinma/front/images/default_wavatar.jpg'); ?>";
</script>

<style>
.preview-footer {
    margin-top: 20px;
}  
</style>

<div class="container main-container">
    <style type="text/css">
        .profile-item a:hover {
            color: red !important;
        }
    </style>
    <div class="col-md-9 col-sm-11" style="float: none; margin: auto; margin-top: 20px;">
        <?php $profile = $this->session->userdata('profile_preview');?>    
        <?php if (!empty($profile)): ?>
        <div class="row company-panel">
            <div class="col-sm-7">
				<div class="row">
					<div class="col-md-12 filter-panel">
						<div class="filter-result pull-left">
							<?php echo $profile['company_name_'.$preview_lang['lang']]; ?>
						</div>
					</div>
				</div>
                <div class="row">
                    <div class="col-md-4">
                        <img id="preview_avatar" src="<?php echo avatarURL($profile['avatar_url'], $profile['salutation']);?>" class="img-thumbnail img-responsive">
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12 company-logo-div">
                                <?php if (!empty($profile['logo_url'])): ?>
                                <img id="preview_logo" src="<?php echo logoURL($profile['logo_url']); ?>" class="img-thumbnail img-responsive">
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="company-profile">
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_person_green.png"/>
                                <span class="contact-person text-capitalize"><?php echo (empty($profile['salutation']) ? '' : $profile['salutation'].'. ').$profile['contact_person']; ?></span>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_website_green.png"/>
                                <?php if (!empty($profile['language'])): ?>
                                <?php $lang_arr = explode(',', $profile['language']); ?>
                                <div class="flag-div">
                                <?php foreach ($lang_arr as $lang): ?>
                                <img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $lang; ?>.png" alt="flag">
                                <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_location_green.png"/>
                                <span class="company-location"><?php echo $profile['city_name'] . ', ' . $profile['zipcode'] . ' ' . $profile['street']; ?></span>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_companyinfo_green.png"/>
                                <span class="company-info"><?php echo lang('detail.since') . " " . $profile['founded_at'] . ", " . employeeCountStr($profile['employee_count']) . lang('detail.employees'); ?></span>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_star_green.png"/>
                                <?php $avg_rating = getCompanyRating($profile['id']); ?>
                                <span class="company-rating"><?php echo getReviewCount($profile['id']); ?></span>
                                <div class="rating-stars">
                                    <?php for ($i = 0; $i < 5; $i++): ?>
                                        <?php if ($i < $avg_rating): ?>
                                        <i class="fa fa-star"></i>
                                        <?php else: ?>
                                        <i class="fa fa-star-o"></i>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </div>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_website_green.png"/>
                                <a href="<?php echo $profile['website']; ?>" target="_blank"><span class="company-website"><?php echo $profile['website']; ?></span></a>
                            </div>
							<div class="row btns-inquery-div">
								<div class="col-md-12">
									<button class="btn btn-bb rightspace15" data-toggle="modal" data-target="#call_modal"><i class="fa fa-phone"></i> <?php echo lang('message.call'); ?></button>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <h4><?php echo humanize(lang('company.about_us')); ?></h4>
                <p><?php echo $profile["desc_".userLang()]; ?></p>
            </div>
        </div>

        <div class="row products-div">
            <?php $company_products = explode(',', $profile['products']); ?>
            <?php if (empty($company_products)): ?>
            <div class="col-xs-12 text-red"><?php echo lang('detail.noproduct'); ?></div>
            <?php endif; ?>
            <?php foreach ($company_products as $product): ?>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="product-wrap">
                    <div class="figure-wrap-only bg-image" data-image-src="<?php echo productUrl($product); ?>">
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <?php if (!empty($profile['youtube'])): ?>
        <div class="row youtube-panel">
            <h4 class="youtube-label"><?php echo lang('detail.youtube'); ?></h4>
            <div class="col-xs-12 col-sm-12 col-md-12" id="youtube_embed">
            </div>
        </div>
        <?php endif; ?>
        <?php if (!empty($profile['youtube1'])): ?>
        <div class="row youtube-panel">
            <h4 class="youtube-label"><?php echo lang('detail.youtube'); ?></h4>
            <div class="col-xs-12 col-sm-12 col-md-12" id="youtube_embed1">
            </div>
        </div>
        <?php endif; ?>
        <?php if (!empty($profile['latitude']) && !empty($profile['longitude'])): ?>
        <div class="row map-panel" id="map-panel">
            <h4 class="map-label"><?php echo lang('detail.map'); ?></h4>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div id="map"></div>
            </div>
        </div>
        <?php endif; ?>
        <?php endif; ?>
    </div>

</div>

<div id="call_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img class="x_btn_img" src="<?php echo base_url('assets/madinma/front/images/x_btn.png'); ?>"/></button>
                <h4 class="modal-title text-uppercase"><?php echo lang('modal.call.title'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <h2 class="phone_text pull-left rightspace15" id="phone_text"></h2>
                        <button class="btn btn-bb-sm pull-left" onclick="copyFunc()"><i class="fa fa-copy"></i></button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p><?php echo lang('modal.call_desc'); ?></p>
                    </div>
                </div>
            </div><!--modal-body-->
            <div class="modal-footer">
                 <a class="btn btn-bb btn-send" href="tel:"><i class="fa fa-phone"></i> <?php echo lang('message.call'); ?></a>
            </div>
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->
<?php if(!empty($profile['latitude']) && !empty($profile['longitude'])): ?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_MAP_KEY?>&callback=initMap" async defer></script>
<?php endif; ?>
<script>
    function getYoutubeId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }

    var map;

    <?php if(!empty($profile['latitude']) && !empty($profile['longitude'])): ?>
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?=floatval($profile['latitude']); ?>, lng: <?=floatval($profile['longitude']); ?>},
          zoom: <?=ZOOM_LEVEL?>
        });
        var icon = {
            url: "<?=base_url('assets/madinma/front/images/marker_red.png')?>", // url
            scaledSize: new google.maps.Size(<?=MARKER_WIDTH?>, <?=MARKER_HEIGHT?>), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(<?=MARKER_WIDTH/2;?>, <?=MARKER_HEIGHT?>) // anchor
        };
        var marker = new google.maps.Marker({
          position:{lat: <?=floatval($profile['latitude']); ?>, lng: <?=floatval($profile['longitude']); ?>},
          icon: icon
        });

        marker.setMap(map);
    }
    <?php endif; ?>
    $('#preview_avatar').attr('src', default_avatar);
        
    $(document).ready(function () {

        var videoId = getYoutubeId('<?=$profile["youtube"];?>');

        if (videoId !='error') {
            $('#youtube_embed').html('<iframe width="60%" height="315" src="https://www.youtube.com/embed/' 
                + videoId + '?rel=0" frameborder="0" allowfullscreen></iframe>')
        }
        var videoId1 = getYoutubeId('<?=$profile["youtube1"];?>');

        if (videoId1 !='error') {
            $('#youtube_embed1').html('<iframe width="60%" height="315" src="https://www.youtube.com/embed/' 
                + videoId1 + '?rel=0" frameborder="0" allowfullscreen></iframe>')
        }
        $(".bg-image").css("background", function() {
            var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
            return a
        });
        
        $('select[name="language"]').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check',
            height: 32,
            dropdownAlignRight: rtl
        });

        $('select[name="language"]').change(function() {
            change_language($(this).val());
        });
		
		$('#call_modal').on('show.bs.modal', function() {
            $.get(base_url + 'phone_num/' + "<?php echo $profile['id']; ?>", function(data) {
				console.log(data)
                if (data.success) {
                    $('#call_modal .phone_text').html(data.phone_num);
                    $('#call_modal .btn-send').attr('href', 'tel:' + data.phone_num);
                }
            });
        });
    });

    function change_language(lang) {
        $.post(base_url + 'change_preview_lang/' + lang, function(data, status){
			location.reload();
        });
    }
</script>

</body>

</html>
