<!DOCTYPE html>
<html>

<?php $this->load->view('front/_base/head'); ?>

<body>

    <?php $this->load->view('front/_partials/header'); ?>

    <section class="pad-tb50">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 tab-content">
                    <div class="m-box row">
                        <div class="col-sm-12 panel-box pad-lr50">
                            <h2 class="text-center text-uppercase"><?php echo lang('message.confirmation'); ?></h2>
                            <p class="bottomspace30"><?php echo lang('sms.desc'); ?></p>
                            <form class="m-form" onsubmit="return validate();">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="form-group">
                                            <label class="text-capitalize"><?php echo lang('user.sms_code'); ?> <span class="text-red">*</span></label>
                                            <input name="sms_code" placeholder="<?php echo humanize(lang('user.sms_code')); ?>" required="" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.sms_code.require'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-3">
                                        <a href='<?php echo base_url("register");?>' class="btn btn-bb text-uppercase"><?php echo lang('signin.back'); ?></a>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-bb text-uppercase pull-right"><?php echo lang('signin.submit'); ?></button>
                                    </div>
                                </div>
                            </form>
                        </div><!--col-12-->
                    </div>
                </div><!--col-9-->
            </div>
        </div><!--container-->
    </section>

<script type="text/javascript">

    function validate() {
        var isValid = true;
        var fields = ['sms_code'];
        var first_err_elem = null;
        $('.form-group').removeClass('has-error');
        for (i = 0; i < fields.length; i++) {
            if ($('input[name="' + fields[i] + '"]').val() == '') {
                isValid = false;
                $('input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').removeClass('hidden');
                $('input[name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
                if (first_err_elem == null) {
                    first_err_elem = $('input[name="' + fields[i] + '"]');
                }
            } else {
                $('input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').addClass('hidden');
                $('input[name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
            }
        }
        if (first_err_elem != null) {
            first_err_elem.focus();
        }

        if (isValid === true) {
            send_code();
        }

        return false;
    }

    function send_code() {
        var formData = new FormData();
        formData.append('sms_code', $('input[name="sms_code"]').val());

        $.ajax({
            'url' : base_url + 'sms_verify/post',
            'type' : 'POST',
            'data' : formData,
            'async' : false,
            'cache' : false,
            'contentType': false,
            'processData' : false,
            'dataType' : 'json',
            'success' : function(response) {
                if (response.success) {
                    window.location = base_url + 'myaccount';
                } else {
                    var dialog = bootbox.dialog({
                        title: message_error,
                        message: response.message,
                        size: 'small',
                        buttons: {
                            ok: {
                                label: message_ok,
                                className: 'btn-danger'
                            }
                        }
                    });
                }
            }
        });
    }

</script>
</body>

</html>