<div class="row footer">
	<div class="col-sm-6 aboutus-div">
		<h4><?php echo lang('footer.about_title'); ?></h4>
		<p><?php echo lang('footer.about_content'); ?></p>
	</div>
	<div class="col-sm-3 contactus-div" style="margin-bottom: 0.5rem;">
		<h4><?php echo lang('footer.contact_title'); ?></h4>
		<p><?php echo lang('footer.contact_content'); ?></p>
		<button class="btn btn-bb text-uppercase" data-toggle="modal" data-target="#contact_modal" style="width: 3.5rem;"><i class="fa fa-envelope" style="margin-right: 0.5rem;"></i> <?php echo lang('footer.mail'); ?></button>
	</div>
	<div class="col-sm-3 statistics-div">
		<h4><?php echo lang('footer.statistics_title'); ?></h4>
		<p style="margin-bottom: 0;"><?php echo $total_cnt_companies; ?> <?php echo lang('footer.statistics_comapny'); ?></p>
		<p><?php echo $week_cnt_companies; ?> <?php echo lang('footer.statistics_comapny_week'); ?></p>
		<p style="margin-bottom: 0;"><?php echo $total_cnt_inqueries; ?> <?php echo lang('footer.statistics_inquery'); ?></p>
		<p><?php echo $week_cnt_inqueries; ?> <?php echo lang('footer.statistics_inquery_week'); ?></p>
	</div>
</div>

<script src="https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_SITE_KEY?>"></script>
<script>
    $(document).ready(function () {
        grecaptcha.ready(function() {
            grecaptcha.execute('<?=RECAPTCHA_SITE_KEY?>', {action: 'contact'}).then(function(token) {
               var recaptchaResponse = document.getElementById('contact_recaptcha');
                recaptchaResponse.value = token;
            });
        });

    	$('#contact_modal').on('show.bs.modal', function() {
            $(this).find('form')[0].reset();
        });

        $('#contact_modal .btn-send').click(function() {

            var isValid = true;
            var fields = ['name', 'email', 'content'];
            
            if ($(".option_type:checked").attr('value') == '0'){
                fields = ['name', 'email', 'content'];
            }else if($(".option_type:checked").attr('value') == '1'){
                fields = ['name', 'phone', 'content'];
            }else{
                fields = ['name', 'email', 'phone', 'content'];
            }
            
            $('#contact_modal .form-group').removeClass('has-error');
            for (i = 0; i < fields.length; i++) {
                if ($('#contact_modal [name="' + fields[i] + '"]').val() == '') {
                    isValid = false;
                    $('#contact_modal [name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
                } else {
                    $('#contact_modal [name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
                }
            }

            if (isValid == true) {
                var params = {
                    company_id: $('#contact_modal [name="company_id"]').val(),
                    name: $('#contact_modal [name="name"]').val(),
                    email: $('#contact_modal [name="email"]').val(),
                    phone: $('#contact_modal [name="phone"]').val(),
                    country_id: $('#contact_modal [name="country_id"]').val(),
                    content: $('#contact_modal [name="content"]').val(),
                    recaptcha_response: $('#contact_modal [name="recaptcha_response"]').val()
                };
                $.post(base_url + 'mail/post', params, function(data, status){
                    if (data.success) {
                        bootbox.dialog({
                            title: message_success,
                            message: message_success,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-success',
                                    callback: function(){
                                        location.reload();
                                    }
                                }
                            }
                        });
                        $('#contact_modal').modal('hide');
                    } else {
                        bootbox.dialog({
                            title: message_error,
                            message: data.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-danger'
                                }
                            }
                        });
                    }
                });
            }
        });
    });
</script>