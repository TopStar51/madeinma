
<style type="text/css">
    .company-profile a:hover {
        color:#D20023;
    }
</style>
<?php 
    $layout_style_cookie = get_cookie('layout_style', TRUE); 
    $layout_style_cookie = empty($layout_style_cookie) ? 'list' : $layout_style_cookie; 
?>

<?php if($this->session->userdata('company_total')!= 0):
	foreach ($companies as $key => $company): ?>
<div class="col-xs-12 col-sm-6 col-md-4">
    <div class="product-wrap">
        <div class="figure-wrap">
            <?php
                if(!isset($company->{'company_name_'.userLang()}) || $company->{'company_name_'.userLang()}==""){
					$company_name = $company->{'company_name_fr'};
				}
				else{
					$company_name = $company->{'company_name_'.userLang()};
				}
            ?>
            <div class="pointer-div"><a href="<?php echo site_url('categories/'.getSEOStr($company->{'name_'.userLang()}).'/'.getSEOStr( $company_name));  ?>"></a></div>
            <div class="favorite">
                <a class="favorite-link" href="javascript:;" data-favorite="<?php echo isFavorite($company->id); ?>" data-company="<?php echo $company->id; ?>">
                    <?php if (isFavorite($company->id)): ?>
                    <img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_on.png"/>
                    <?php else: ?>
                    <img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_off.png"/>
                    <?php endif; ?> 
                </a>
            </div>
            <div class="slide-banner swiper-container figure-wrap">
                <div class="swiper-wrapper">
                    <?php if (empty($company->products)): ?>
                        <div class="figure-wrap bg-image swiper-slide" data-image-src="<?php echo base_url(); ?>assets/madinma/front/images/preview.png">
                        </div>
                    <?php endif; ?>
                    <?php foreach ($company->products as $idx => $product): ?>
                    <div class="figure-wrap bg-image swiper-slide <?php echo ($idx==0 ? 'active' : ''); ?>" data-image-src="<?php echo productThumbURL($product->img_url); ?>">
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <?php if ($layout_style_cookie == 'list'): ?>
        <div class="company-profile">
            <a href="<?php echo  site_url('categories/'.getSEOStr($company->{'name_'.userLang()}).'/'.getSEOStr($company_name));  ?>" style="display: flex;">
                <div class="profile-item">
                    <?php echo symbolSVG($company->symbol_url); ?>
                    <?php
                        if(!isset($company->{'company_name_'.userLang()}) || $company->{'company_name_'.userLang()}=="")
                            $company_name = $company->{'company_name_fr'};
                        else
                            $company_name = $company->{'company_name_'.userLang()};
                    ?>
                    <span class="company-name text-span"><?php echo $company_name; ?></span>
                </div>
            </a>
            <div class="profile-item">
                <img src="<?php echo base_url(); ?>assets/madinma/front/images/location.svg"/>
                <span class="company-location text-span"><a href="javascript:;" class="city_link" data-city="<?php echo $company->city_name; ?>"><?php echo $company->city_name; ?></a><?php echo ', ' . $company->street; ?></span>
            </div>
            <div class="profile-item">
                <img src="<?php echo base_url(); ?>assets/madinma/front/images/analytics.svg"/>
                <span class="company-info text-span"><?php echo lang('profile.since'); ?> <?php echo $company->founded_at; ?>, <?php echo employeeCountStr($company->employee_count); ?>&nbsp;<?php echo lang('profile.employees'); ?></span>
            </div>
            <div class="profile-item">
                <img src="<?php echo base_url(); ?>assets/madinma/front/images/rating.svg"/>
                <?php $avg_rating = getCompanyRating($company->id); ?>
                <?php if(getReviewCount($company->id) > 0) { ?>
                <span class="company-rating"><?php echo getReviewCount($company->id); ?></span>
                <?php } ?>
                <?php if($avg_rating == 0) { ?>
                <span class="company-rating"><?php echo lang('no.ratings'); ?></span>
                <?php  } else { ?>
                <div class="rating-stars">
                    <?php for ($i = 0; $i < 5; $i++): ?>
                        <?php if ($i < $avg_rating): ?>
                        <i class="fa fa-star"></i>
                        <?php else: ?>
                        <i class="fa fa-star-o"></i>
                        <?php endif; ?>
                    <?php endfor; ?>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php endforeach; ?>
<?php endif;?>
<script>
    $(document).ready(function() {
        $('.city_link').click(function() {
            $('#search-form [name="city"]').val($(this).attr('data-city'));
            $('#search-form').attr('action', base_url + 'cities/' + $(this).attr('data-city'));
            $('#search-form')[0].submit();
        });
		
    });
</script>
