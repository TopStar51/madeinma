<style type="text/css">
	.filter-wrapper .dropdown-menu {
		max-height: 500px !important;
		padding-bottom: 10px !important;
	}
</style>
<?php 
    $city_sel = $this->session->userdata('city_sel'); 
    $city_sel = empty($city_sel) ? 'all' : $city_sel;
    $sort_sel = $this->session->userdata('sort_sel'); 
	$sort_sel = empty($sort_sel) ? 'rating' : $sort_sel;
	$emp_sel = $this->session->userdata('emp_sel'); 
	$emp_sel = empty($emp_sel) ? 'all' : $emp_sel;
	$real_action_url = ($this->uri->segment(1,0)=='favorites')? base_url('favorites'):'';
?>
<div class="footer-wrap">
	<div class="footer-item filter-item pull-left">
		<a class="<?php if (!empty($cur_page) && $cur_page == 'info'): ?>active<?php endif; ?>" href="<?php echo base_url('info'); ?>"><i class="icon-info"></i></a>
	</div>
	<div class="footer-item fav-item pull-right">
		<?php if (!empty($cat_name) && $cat_name == 'favorites'): ?>
			<a class="active" href="<?php echo base_url('favorites'); ?>"><i class="fa fa-heart"></i>
		<?php else: ?>
			<a href="<?php echo base_url('favorites'); ?>"><i class="fa fa-heart-o"></i>
		<?php endif; ?>
			<span class="badge badge-success favorite-count"><?php echo favoriteCount(); ?></span>
		</a>
	</div>
	<div class="footer-item emp-item pull-right">
		<a class="<?php if ("all" != $emp_sel): ?>active<?php endif; ?>" href="javascript:;"><i class="fa fa-users"></i></a>
	</div>
	<div class="footer-item sort-item pull-right">
		<a class="<?php if ("rating" != $sort_sel): ?>active<?php endif; ?>" href="javascript:;"><i class="fa fa-sort-amount-asc"></i></a>
	</div>
	<div class="footer-item filter-item pull-right">
		<a class="<?php if ('all' != $city_sel): ?>active<?php endif; ?>" href="javascript:;"><i class="fa fa-filter"></i></a>
	</div>
	<?php 
	    $layout_style_cookie = get_cookie('layout_style', TRUE); 
	    $layout_style_cookie = empty($layout_style_cookie) ? 'list' : $layout_style_cookie;
	?>
	<div class="footer-item layout-item pull-right">
		<a class="<?php if ($layout_style_cookie=='grid'):?>active<?php endif; ?>" href="javascript:;"><i class="fa fa-th-large"></i></a>
	</div>
</div>
<?php
	if (empty($cat_name)) {
		$action_url = base_url('cities/'.getSEOStr($city_sel));
	} else if ($cat_name == 'favorites') {
		$action_url = base_url('favorites');
	} else {
		$action_url = base_url('categories/'.getSEOStr($cat_name));
	}
?>
<!-- Start Top Search -->
<div id="search_modal" class="modal fade top-search">
    <div class="modal-dialog">        
        <form id="search-form" autocomplete="off" action="<?php echo $action_url; ?>" onsubmit="beforeSubmit();" method="POST">
        	<div class="filter-wrapper">
        		<div class="input-group">
        			<img class="close-icon btn" data-dismiss="modal" src="<?php echo base_url('assets/madinma/front/images/close_icon.png'); ?>">
			        <input type="text" name="q" id="top_search_input" class="form-control" old-value="<?php echo empty($sEcho) ? '' : $sEcho; ?>" value="<?php echo empty($sEcho) ? '' : $sEcho; ?>" placeholder="<?php echo lang('topsearch.placeholder'); ?>">
			        <span class="input-group-btn">
			            <button class="btn" type="submit"><img src="<?php echo base_url(); ?>assets/madinma/front/images/search.png"></button>
			        </span>
			    </div> 
        	</div>  
        	<input type="hidden" name="city" value="<?php echo (empty($city_sel) ? '' : $city_sel); ?>">
			<input type="hidden" name="sort" value="<?php echo (empty($sort_sel) ? '' : $sort_sel); ?>">            
			<input type="hidden" name="emp" value="<?php echo (empty($emp_sel) ? '' : $emp_sel); ?>">            
		</form>
    </div>
</div>
<!-- End Top Search -->
<!-- Start Filter -->
<div id="filter_modal" class="modal fade" >
    <div class="modal-dialog">
        <form id="filter-form" action="<?php echo $action_url; ?>" onsubmit="beforeSubmit();" method="POST">
		    <div class="filter-wrapper">
		        <label class="city <?php if ('all' != $city_sel): ?>filtered<?php endif; ?>" style="width: 100%;">
		        	<img class="close-icon btn" data-dismiss="modal" src="<?php echo base_url('assets/madinma/front/images/close_icon.png'); ?>"/>
		            <select id="city_sel" class="pull-right">
		                <option value="all" <?php echo ($city_sel=='all' ? 'selected' :''); ?>><?php echo lang('companies.all'); ?></option>
		                <?php foreach($cities as $city): ?>
		                <option value="<?php echo $city->name; ?>" <?php echo ($city_sel==$city->name ? 'selected' :''); ?>><?php echo $city->name; ?></option>
		                <?php endforeach; ?>
		            </select>
		        </label>
		    </div>           
		</form>
    </div>
</div>
<!-- End Filter -->
<!-- Start Filter -->
<div id="emp_modal" class="modal fade" >
    <div class="modal-dialog">
        <form id="emp-form" action="<?php echo $action_url; ?>" onsubmit="beforeSubmit();" method="POST">
		    <div class="filter-wrapper">
		        <label class="emp <?php if ('all' != $emp_sel): ?>filtered<?php endif; ?>" style="width: 100%;">
		        	<img class="close-icon btn" data-dismiss="modal" src="<?php echo base_url('assets/madinma/front/images/close_icon.png'); ?>"/>
					<?php $emp_count_arr = array('<5', '5+', '10+', '20+', '50+', '100+', '500+', '1000+');?>
					<select id="emp_sel">
						<option value="all" <?php if ('all' == $emp_sel): ?>selected<?php endif; ?>><?php echo lang('companies.all'); ?></option>
						<?php foreach($emp_count_arr as $key => $row): ?>
							<option value="<?php echo $key+1; ?>" <?php if ($key+1 == $emp_sel): ?>selected<?php endif; ?>><?php echo $row; ?></option>
						<?php endforeach; ?>
					</select>
		        </label>
		    </div>           
		</form>
    </div>
</div>
<!-- Start Sort -->
<div id="sort_modal" class="modal fade" >
    <div class="modal-dialog">
        <form id="sort-form" action="<?php echo $action_url; ?>" onsubmit="beforeSubmit();" method="POST">
		    <div class="filter-wrapper">
		        <label class="sort <?php if ("rating" != $sort_sel): ?>filtered<?php endif; ?>" style="width: 100%;">
		        	<img class="close-icon" data-dismiss="modal" src="<?php echo base_url('assets/madinma/front/images/close_icon.png'); ?>"/>
		            <select id="sort_sel" class="pull-right">
		                <option value="rating" <?php echo ($sort_sel=='rating' ? 'selected' :''); ?>><?php echo lang('companies.rating'); ?></option>
		                <option value="newest" <?php echo ($sort_sel=='newest' ? 'selected' :''); ?>><?php echo lang('companies.newest'); ?></option>
		                <option value="oldest" <?php echo ($sort_sel=='oldest' ? 'selected' :''); ?>><?php echo lang('companies.oldest'); ?></option>
		            </select>
		        </label>
		    </div>            
		</form>
    </div>
</div>
<!-- End Sort -->

<script>

	var category = '<?= empty($cat_name)? "" : getSEOStr($cat_name); ?>';
	function beforeSubmit() {
		$('input[name="city"]').val($('#city_sel').val());
		$('input[name="sort"]').val($('#sort_sel').val());
		$('input[name="emp"]').val($('#emp_sel').val());
	}

	$('#search-modal-btn').click(function() {
		$('#filter_modal').modal('hide');
		$('#sort_modal').modal('hide');
		$('#search_modal').modal('show');
		$('#emp_modal').modal('hide');
	});

	$('#search_modal').on('hidden.bs.modal', function (e) {
	  	$(".ui-autocomplete").removeClass('show');
	  	$(".ui-autocomplete li").css('display', 'none');
	})

	$('.footer-wrap .filter-item a').click(function() {
    	$('#filter_modal .city').show();
    	$('#sort_modal').modal('hide');
    	$('#search_modal').modal('hide');
    	$('#filter_modal').modal('show');
    	$('#emp_modal').modal('hide');
    });

    $('.footer-wrap .sort-item a').click(function() {
    	$('#sort_modal .sort').show();
    	$('#filter_modal').modal('hide');
    	$('#search_modal').modal('hide');
		$('#sort_modal').modal('show');
		$('#emp_modal').modal('hide');
	});
	
	$('.footer-wrap .emp-item a').click(function() {
    	$('#sort_modal .sort').show();
    	$('#filter_modal').modal('hide');
    	$('#search_modal').modal('hide');
		$('#sort_modal').modal('hide');
		$('#emp_modal').modal('show');
    });

	$('#sort_sel, #city_sel, #emp_sel').selectpicker({
		width: 'calc(100% - 1.25rem)',
		dropdownAlignRight: rtl
    });

    $('#city_sel, #sort_sel, #emp_sel').change(function() {
    	var city_sel = $('#city_sel').val();
		var action_url = "<?php echo $real_action_url;?>";
        beforeSubmit();
        if(category == '') {
        	city_sel = city_sel.replace(/ /g, "_");
			if(action_url == '')
				$('#search-form').attr('action', base_url + 'cities/' + city_sel);
			else
				$('#search-form').attr('action', action_url);
        } else {
        	category = category.replace(/ /g, "_");
			if(action_url == '')
				$('#search-form').attr('action', base_url + 'categories/' + category);
			else
				$('#search-form').attr('action', action_url);
        }
        $('#search-form')[0].submit();
    });
	$(".reset").click(function(){
		$('input[name="city"]').val('all');
		$('input[name="emp"]').val('all');
		category = category.replace(/ /g, "_");
		var action_url = "<?php echo $action_url;?>";
		if(category != '' && action_url == '')
			$('#search-form').attr('action', base_url + 'categories/' + category);
		else
			$('#search-form').attr('action', action_url);

		$('#search-form')[0].submit();
	})
    $('.layout-item a').click(function() {
    	$(this).toggleClass('active');
        var type = $(this).hasClass('active') ? 'grid' : 'list';
    	$.post(base_url + 'layout/' + type, function(data, status){
            if (data.success) {
                location.reload();
            }
        });
    })

	$('#top_search_input').blur(function(){
	        var q = $('#top_search_input').val();
	        if (q) {
	            if ($('.ui-autocomplete').html()) {
	                $('.ui-autocomplete').addClass('show');
	            	$(".ui-helper-hidden-accessible div").css('display', 'none');
	            }
	        }else{
	            $('.ui-autocomplete').removeClass('show');
	        }
	    });

	    $( '#top_search_input' ).autocomplete({
	        minLength: 0,
	        source: function (request, response) {
	        	$.get(base_url + 'search-texts', {q: request.term}, function(data, status){
		            if (data.success) {
		                $('.ui-autocomplete').removeClass('show');
	                    $('.ui-autocomplete').html('');
	                    var stexts = data.stexts;
	                    response(stexts.map(function (value) {
	                    	return {
		                        'id': value.id,
		                        'label': value.text
		                    };
	                    }));
		            }
		        });
	        },
	    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	        var left = $('.ui-widget-content').css('left');
	        $('.ui-widget-content').css('left', left + 30);
	        var cur_term = $('#top_search_input').val();
	        var new_val = '<span style="font-weight:bold">$1</span>';
	        var patt = new RegExp('(' + cur_term + ')', "gi");
	        return $( "<li style='border-bottom:solid 1px #f1f1f1;'>" )
	            .append( '<a>' + item.label.replace(patt, new_val) + '</a>' )
	            .appendTo( ul );
	    };
        var swiper = new Swiper('.swiper-container', {
			pagination: '.swiper-pagination',
			paginationClickable: true,
			loop:true,
			grabCursor: true,
			createPagination: true
		});
</script>
