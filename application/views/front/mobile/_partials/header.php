<?php $user = $this->session->userdata('user'); ?>
<style type="text/css">
	li.img-item:hover a {
		color: #D20023;
	}
	li.img-item:hover svg path {
		fill: #D20023;
	}
</style>
<nav class="navbar navbar-fixed-top main-header">
  	<div class="container-fluid">
	    <div class="navbar-header">
		    <a class="navbar-menu"  data-toggle="collapse" data-target="#myNavbar"><img class="logo-mini" src="<?php echo base_url(); ?>assets/madinma/front/images/icon-1.png"></a>
	      	<a class="navbar-brand" href="<?php echo base_url(); ?>"><img class="logo-mini" src="<?php echo base_url(); ?>assets/madinma/front/images/logo_small.png"></a>
	      	<a class="navbar-right" id="search-modal-btn" href="javascript:;"><img class="logo-mini" src="<?php echo base_url(); ?>assets/madinma/front/images/search.png"></a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	    	<div class="top-menu-wrap">
	    		<?php if (!isset($user)): ?>
		        	<a class="top-menu-item" href="<?php echo base_url('login'); ?>">
		        		<img class="login-img" src="<?php echo base_url(); ?>assets/madinma/front/images/button_login_small<?php echo (!empty($cur_page) && $cur_page == 'login' ? '_active' : ''); ?>.png"/>
		        	</a>
		        	<a class="top-menu-item" href="<?php echo base_url('register'); ?>">
		        		<img class="register-img <?php echo (!empty($cur_page) && $cur_page == 'register' ? 'active' : ''); ?>" src="<?php echo base_url(); ?>assets/madinma/front/images/button_register<?php echo (!empty($cur_page) && $cur_page == 'register' ? '_active' : ''); ?>.png"/>
		        	</a>
		    	<?php else: ?>
					<?php
					$a_setting = '';
					if ($user->type == 0) {
						$a_setting = '<a class="top-menu-item text-item '.(!empty($cur_page) && $cur_page == 'account' ? 'active' : '').'" href="'.base_url('myaccount').'"><i class="fa fa-cog"></i></a>';
					} else {
						$a_setting = '<a class="top-menu-item text-item '.(!empty($cur_page) && $cur_page == 'account' ? 'active' : '').'" href="'.base_url('admin/manufacturer').'"><i class="fa fa-cog"></i></a>';
					}
					$a_logout = '<a class="top-menu-item text-item" href="'.base_url('logout').'"><i class="fa fa-sign-out"></i></a>';

					if (userLang() == 'ar') {	
						echo $a_setting;
						echo $a_logout;
					} else {
						echo $a_logout;
						echo $a_setting;
					}
					?>
				<?php endif; ?>
		    	<div class="top-menu-item dropdown">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo userLang(); ?>.png">
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<?php $lang_arr = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir'); ?>
						<?php foreach ( $lang_arr as $lang): ?>
							<li class="<?php if ($lang == userLang()): ?>disabled<?php endif; ?>">
								<a href="javascript:change_language('<?php echo $lang; ?>');">
									<img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $lang; ?>.png"> <?php echo lang('general.lang.'.$lang); ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
	        	<a class="top-menu-item" href="<?php echo base_url('favorites'); ?>">
	        		<span class="badge badge-success favorite-count"><?php echo favoriteCount(); ?></span>
	        		<?php if (!empty($cat_name) && $cat_name == 'favorites'): ?>
	        			<img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_on.png">
	        		<?php else: ?>
	        			<img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_off.png">
	        		<?php endif; ?>
	        	</a>
	    	</div>
		    <ul class="nav navbar-nav list-menu-wrap">
		        <?php foreach ($categories as $category): ?>
				<li class="img-item <?php if (!empty($cat_name) && $cat_name == $category->{'name_'.userLang()} || !empty($company) && $company->category_id == $category->id):?>active<?php endif; ?>">
					<a href="<?php echo base_url('categories/'.getSEOStr($category->{'name_'.userLang()})); ?>">
						<?php echo symbolSVG($category->symbol_url) . $category->{"name_".userLang()}; ?>
					</a>
				</li>
				<?php endforeach; ?>
		    </ul>
	    </div>
  	</div>
</nav>
<?php
	if (empty($cat_name)) {
		$action_url = site_url('companies');
	} else {
		$action_url = site_url('categories/'.getSEOStr($cat_name));
	}
?>

<script>
	function change_language(lang) {
		$.post(base_url + 'lang/' + lang, function(data, status){
            if (data.success) {
                location.reload();
            } else {
                bootbox.dialog({
                    title: message_error,
                    message: data.message,
                    size: 'small',
                    buttons: {
                        ok: {
                            label: message_ok,
                            className: 'btn-danger'
                        }
                    }
                });
            }
        });
	}

	function setFocusEnd(id) {
		var node = document.getElementById(id);
		pos = node.value.length;
		node.focus();
		if (!node) {
			return false;
		} else if (node.createTextRange) {
			var textRange = node.createTextRange();
			textRange.collapse(true);
			textRange.moveEnd(pos);
			textRange.moveStart(pos);
			textRange.select();
			return true;
		} else if (node.setSelectionRange) {
			node.setSelectionRange(pos, pos);
			return true;
		}
		return false;
	}
	$(document).ready(function() {
		setFocusEnd('top_search_input');

		$('.navbar-menu').click(function() {
			if ($('.main-header').hasClass('opened')) {
				$('.main-header').removeClass('opened');
				setTimeout(function() {
					$('.main-header').css('height', '');
					$("body").css({"overflow":"initial",'position':'initial'});
				}, 400);
			} else {
				$('.main-header').addClass('opened');
				setTimeout(function() {
					$('.main-header').css('height', 'calc(100% - 1.25rem)');
					// $("body").css({"overflow":"hidden",'position':'fixed'});
				}, 400);
			}
		});

	});
</script>
