<!DOCTYPE html>
<html>

<?php $this->load->view('front/mobile/_base/head'); ?>
<style>
    #flag_phone {
        padding-left: 66px;
    }
</style>
<body>

    <?php $this->load->view('front/mobile/_partials/header'); ?>

    <div class="content-wrap clearfix">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 tab-content">
                <div class="m-box row">
                    <div class="col-sm-12 panel-box">
                        <h2 class="text-center text-uppercase"><?php echo lang('message.signup'); ?></h2>
                        <p class="bottomspace30"><?php echo lang('signup.desc'); ?></p>
                        <div class="row">
                            <form class="m-form" onsubmit="return validate();" id="register_form">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="text-capitalize"><?php echo lang('user.phone'); ?> <span class="text-red">*</span></label>
                                        <input type="number" pattern="[0-9]*" name="phone" id="flag_phone" placeholder="<?php echo lang('user.phone.placeholder'); ?>" required="" class="form-control" autocomplete="off" style="background:url('<?php echo base_url().lang("user.phone.flag");?>') no-repeat !important;">
                                        <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.phone.require'); ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="text-capitalize"><?php echo lang('user.password'); ?></label>
                                        <input name="password" type="password" placeholder="<?php echo humanize(lang('user.password')); ?>" class="form-control" autocomplete="off">
                                        <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.password.require'); ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="text-capitalize"><?php echo lang('user.confirm_password'); ?></label>
                                        <input name="confirm_password" type="password" placeholder="<?php echo humanize(lang('user.confirm_password')); ?>" class="form-control" autocomplete="off">
                                        <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.confirm_password.valid'); ?></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="text-capitalize"><?= lang('user.promotion_code') ?></label>
                                        <input type="text" name="code" value="<?=$code?>" class="form-control" placeholder="<?php echo humanize(lang('promo.placeholder')); ?>">
                                    </div>
                                </div>
                                <input type="hidden" name="recaptcha_response" id="register_recaptcha">
                                <!-- <div class="col-sm-12">
                                     <div class="g-recaptcha recaptScale" data-sitekey="<?=RECAPTCHA_SITE_KEY?>"></div>
                                </div> -->
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-bb text-uppercase"><?php echo lang('message.signup'); ?></button>
                                </div>
                            </form>
                        </div>
                    </div><!--col-12-->
                </div>
            </div><!--col-9-->
        </div>
    </div><!--container-->
    <div id="confirm_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><img class="x_btn_img" src="<?php echo base_url('assets/madinma/front/images/x_btn.png'); ?>"/></button>
                    <h4 class="modal-title text-uppercase"><?php echo lang('modal.confirm.title'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p><?php echo lang('modal.confirm.question'); ?></p><p id="confirm_phone"></p>
                        </div>
                    </div>
                </div><!--modal-body-->
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-bb btn-back" data-dismiss="modal" style="margin-right:10px"><?php echo lang('signin.back'); ?></button>
                        <button class="btn btn-bb btn-send" href=""><?php echo lang('message.ok'); ?></button>
                    </div>
                </div>
            </div><!--modal-content-->
        </div><!--modal-dialog-->
    </div><!--modal-->

    <div id="error_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><img class="x_btn_img" src="<?php echo base_url('assets/madinma/front/images/x_btn.png'); ?>"/></button>
                    <h4 class="modal-title text-uppercase"><?php echo lang('phone.error.title'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p><?php echo lang('phone.invalid'); ?></p>
                        </div>
                    </div>
                </div><!--modal-body-->
                <div class="modal-footer">
                     <button class="btn btn-bb" data-dismiss="modal"><?php echo lang('message.ok'); ?></button>
                </div>
            </div><!--modal-content-->
        </div><!--modal-dialog-->
    </div><!--modal-->

<?php $this->load->view('front/mobile/_partials/footer'); ?>

<script src="https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_SITE_KEY?>"></script>   
<script type="text/javascript">

    grecaptcha.ready(function() {
        grecaptcha.execute('<?=RECAPTCHA_SITE_KEY?>', {action: 'register'}).then(function(token) {
            var registerRecaptcha = document.getElementById('register_recaptcha');
            registerRecaptcha.value = token;
        });
    });

    $(document).ready(function(){
        // phone number box only allow arabic numerals
        // $('#register_form input[name="phone"]').inputmask("699999999");
        
        $(".btn-send").click(function(){
            submit();
        })
    })

    function validate() {
        var isValid = true;
        var inputPhone = $("#flag_phone").val();
        
        if (inputPhone.length != 9 || inputPhone.slice(0, 1) == "0") {
            isValid = false;
            $("#error_modal").modal('show');
            return false;
        }

        var fields = ['phone', 'password', 'confirm_password'];
        var first_err_elem = null;
        $('.form-group').removeClass('has-error');
        for (i = 0; i < fields.length; i++) {
            if ((fields[i] == 'confirm_password' && $('input[name="confirm_password"]').val() != $('input[name="password"]').val()) || 
                ($('input[name="' + fields[i] + '"]').val() == '')) {
                isValid = false;
                $('input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').removeClass('hidden');
                $('input[name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
                if (first_err_elem == null) {
                    first_err_elem = $('input[name="' + fields[i] + '"]');
                }
            } else {
                $('input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').addClass('hidden');
                $('input[name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
            }
        }
        if (first_err_elem != null) {
            first_err_elem.focus();
        }
        
        if (isValid === true) {
            $("#confirm_phone").html('+212' + $("#flag_phone").val());
            $('#confirm_modal').modal('show');
            // register();
        }

        return false;
    }

    function submit() {
        var formData = new FormData();
        formData.append('phone', '+212' + $('input[name="phone"]').val());
        formData.append('password', $('input[name="password"]').val());
        formData.append('confirm_password', $('input[name="confirm_password"]').val());
        formData.append('code', $('input[name="code"]').val());
        formData.append('recaptcha_response', $('input[name="recaptcha_response"]').val());
        
        $.ajax({
            'url' : base_url + 'register/post',
            'type' : 'POST',
            'data' : formData,
            'async' : false,
            'cache' : false,
            'contentType': false,
            'processData' : false,
            'dataType' : 'json',
            'success' : function(response) {
                if (response.success) {
                    window.location = base_url + response.redirect_url;
                } else {
                    var dialog = bootbox.dialog({
                        title: message_error,
                        message: response.message,
                        size: 'small',
                        buttons: {
                            ok: {
                                label: message_ok,
                                className: 'btn-danger'
                            }
                        }
                    });
                }
            }
        });
    }

</script>
</body>

</html>