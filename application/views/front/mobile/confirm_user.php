<!DOCTYPE html>
<html>

<?php $this->load->view('front/mobile/_base/head'); ?>
<body>

    <?php $this->load->view('front/mobile/_partials/header'); ?>
    
    <?php $remember = $this->session->userdata('remember'); ?>
    <div class="content-wrap clearfix">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 tab-content">
                <div class="m-box login-box row">
                    <div class="col-sm-12 panel-box">
                        <h2 class="text-center text-uppercase"><?php echo lang('message.new_password'); ?></h2>
                        <form action="" onsubmit="return validate();">
                            <input type="hidden" name="token" value="<?php echo empty($token) ? '' : $token ?>" >
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-xs-12">
                                    <div class="form-group">
                                        <label class="text-capitalize"><?php echo lang('user.password'); ?></label>
                                        <input name="password" type="password" placeholder="<?php echo humanize(lang('user.password')); ?>" class="form-control">
                                        <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.password.require'); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-3 col-xs-12">
                                    <div class="form-group">
                                        <label class="text-capitalize"><?php echo lang('user.confirm_password'); ?></label>
                                        <input name="confirm_password" type="password" placeholder="<?php echo humanize(lang('user.confirm_password')); ?>" class="form-control">
                                        <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.confirm_password.require'); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn btn-bb text-uppercase pull-left"><?php echo lang('message.save'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div><!--col-12-->
                </div>

            </div><!--col-9-->
        </div>
    </div>

<script type="text/javascript">

    function validate() {
        var isValid = true;
        var fields = ['password', 'confirm_password'];
        var first_err_elem = null;
        $('.form-group').removeClass('has-error');
        for (i = 0; i < fields.length; i++) {
            if ($('input[name="' + fields[i] + '"]').val() == '') {
                isValid = false;
                $('input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').removeClass('hidden');
                $('input[name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
                if (first_err_elem == null) {
                    first_err_elem = $('input[name="' + fields[i] + '"]');
                }
            } else {
                $('input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').addClass('hidden');
                $('input[name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
            }
        }
        if (first_err_elem != null) {
            first_err_elem.focus();
        }
        
        if (isValid === true) {
            submit();
        }

        return false;
    }

    function submit() {
        var formData = new FormData();
        formData.append('token', $('input[name="token"]').val());
        formData.append('password', $('input[name="password"]').val());
        formData.append('confirm_password', $('input[name="confirm_password"]').val());

        $.ajax({
            'url' : base_url + 'password/post',
            'type' : 'POST',
            'data' : formData,
            'async' : false,
            'cache' : false,
            'contentType': false,
            'processData' : false,
            'dataType' : 'json',
            'success' : function(response) {
                if (response.success) {
                    bootbox.dialog({
                        title: message_success,
                        message: message_success,
                        size: 'small',
                        buttons: {
                            ok: {
                                label: message_ok,
                                className: 'btn-success',
                                callback: function(){
                                    window.location = base_url + 'login';
                                }
                            }
                        }
                    });
                } else {
                    var dialog = bootbox.dialog({
                        title: message_error,
                        message: response.message,
                        size: 'small',
                        buttons: {
                            ok: {
                                label: message_ok,
                                className: 'btn-danger'
                            }
                        }
                    });
                }
            }
        });
        return false;
    }

</script>
</body>

</html>