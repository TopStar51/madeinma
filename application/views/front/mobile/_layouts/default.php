<!DOCTYPE html>
<html>

<?php $this->load->view('front/mobile/_base/head', $og_tags); ?>
<style type="text/css">
	.dropdown-menu {
		width: 100px !important;
	}
</style>
<body>

	<?php $this->load->view('front/mobile/_partials/header'); ?>
	<div class="turnDeviceNotification"></div>
	<div class="content-wrap">
		<?php $this->load->view($inner_view); ?>
	</div>

	
<div id="contact_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img class="x_btn_img" src="<?php echo base_url('assets/madinma/front/images/x_btn.png'); ?>"/></button>
                <h4 class="modal-title text-uppercase"><?php echo lang('modal.contact.title'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form>
                            <?php 
                                $contact_name_cookie = get_cookie('contact_name');
                                $contact_email_cookie = get_cookie('contact_email');
                                $contact_phone_cookie = get_cookie('contact_phone');
                                $contact_content_cookie = get_cookie('contact_content');
                                $contact_country_cookie = get_cookie('contact_country');
                            ?>
                            <input type="hidden" name="company_id" value="<?php echo empty($company) ? 0 : $company->id; ?>" ?>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="<?php echo humanize(lang('modal.placeholder.name')); ?>" value="<?php echo empty($contact_name_cookie) ? '' : $contact_name_cookie; ?>">
                            </div>
                            <div class="form-group">
                                <!-- <label class="radio-inline"> -->
                                    <input type="radio" name="optradio" class="option_type" value="0" checked=""><?php echo lang('modal.option.email'); ?>
                                <!-- </label> -->
                                <!-- <label class="radio-inline"> -->
                                    <input type="radio" name="optradio" class="option_type" value="1"><?php echo lang('modal.option.phone'); ?>
                                <!-- </label> -->
                                <!-- <label class="radio-inline"> -->
                                    <input type="radio" name="optradio" class="option_type" value="2"><?php echo lang('modal.option.all'); ?>
                                <!-- </label> -->
                            </div>
                            <div class="form-group email_div">
                                <input type="email" class="form-control" name="email" placeholder="<?php echo humanize(lang('modal.placeholder.email')); ?>" value="<?php echo empty($contact_email_cookie) ? '' : $contact_email_cookie; ?>">
                            </div>
                            <div class="form-group phone_div">
                                <input type="text" class="form-control" style = "direction: ltr !important" name="phone" placeholder="<?php echo humanize(lang('modal.placeholder.phone')); ?>" value="<?php echo empty($contact_phone_cookie) ? '' : $contact_phone_cookie; ?>">
                            </div>
                            <div class="form-group">
                                <select name="country_id" class="select-picker">
                                    <option></option>
                                    <?php foreach ($countries as $country): ?>
                                    <option value="<?php echo $country->id; ?>" <?php echo (!empty($contact_country_cookie) && $contact_country_cookie==$country->id) ? 'selected' : ''; ?>><?php echo $country->{'name_'.userLang()}; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" rows="10" name="content" placeholder="<?php echo humanize(lang('modal.placeholder.text')); ?>"><?php echo empty($contact_content_cookie) ? '' : $contact_content_cookie; ?></textarea>
                            </div>
                            <input type="hidden" name="recaptcha_response" id="contact_recaptcha">
                        </form>
                    </div>
                    <div class="col-sm-12">
                        <!-- <div id="contact_recaptcha" class="g-recaptcha recaptScale"></div> -->
                        <button class="btn btn-bb btn-send"><i class="fa fa-paper-plane"></i> <?php echo lang('modal.btn.send'); ?></button>
                    </div>
                </div>
            </div><!--modal-body-->
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->

	<?php $this->load->view('front/mobile/_partials/footer'); ?>

<script>
	$(document).ready(function() {

		$('.select-picker').selectpicker({
            width: '100%',
            height: 32,
            dropdownAlignRight: rtl,
            noneSelectedText: '<?php echo lang("modal.lang.none.selected")?>'
        });

        // SET OVERFLOW TO AUTO, SO THE SCROLL IT'S ALLOWED
        $("html").css("overflow", "auto");

        /*//HIDE THE NICESCROLL FOR MOBILE DEVICES 
        $("#ascrail2000").css("display", "none");*/
        if ($(".option_type:checked").attr('value') == '0'){
            $(".email_div").show();
            $(".phone_div").hide();
        }else if($(".option_type:checked").attr('value') == '1'){
            $(".email_div").hide();
            $(".phone_div").show();
        }else{
            $(".email_div").show();
            $(".phone_div").show();
        }
            

        $(".option_type").change(function(){
            if ($(this).attr("value") == '0'){
                $(".phone_div").hide();
                $(".email_div").show();
            }else if ($(this).attr("value") == '1'){
                $(".phone_div").show(); 
                $(".email_div").hide();
            }else{
                $(".email_div").show();
                $(".phone_div").show(); 
            }
        })

	});
</script>

</body>

</html>

