<?php 
	$this->load->view('front/mobile/_base/head'); 
	$preview_lang = $this->session->userdata('preview_lang');
?>
<style type="text/css">
    .navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:hover {
        background-color: transparent !important;
    }
    #map {
        width: 100%;
        height: 350px;
    }
</style>
<nav class="navbar navbar-default main-navbar" style="background: #00652D;">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo base_url(); ?>">
                <h2 class="preview_header">Preview</h2>
                <!-- <img class="logo-mini" src="<?php echo base_url(); ?>assets/madinma/front/images/logo_small_1.png"> -->
            </a>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-user img-item" style="position:initial">
                    <a href="javascript:;" class="dropdown-toggle img-item" data-toggle="dropdown">
                    <img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $preview_lang['lang']; ?>.png">
                    <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default" style="left:auto;right:0;position:absolute;border: 1px solid rgba(0,0,0,.15);z-index: 1000;background-color:white;">
                        <?php $lang_arr = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir'); ?>
                        <?php foreach ( $lang_arr as $lang): ?>
                            <li class="<?php if ($lang == userLang()): ?>disabled<?php endif; ?>">
                                <a href="javascript:change_language('<?php echo $lang; ?>');">
                                    <img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $lang; ?>.png"> <?php echo lang('general.lang.'.$lang); ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="site-desc pad-l100 visible-lg visible-md" style="color:white;"><?php echo lang('site.description'); ?></div>
    </div>
</nav>

<div class="content-wrap clearfix">
    <style type="text/css">
        .profile-item a:hover {
            color: #D20023 !important;
        }
        .preview-footer {
            margin-top: 0.5rem;
        }  
    </style>
    <?php $profile = $this->session->userdata('profile_preview'); ?>    
    <?php if (!empty($profile)): ?>
		<div class="row">
			<div class="col-md-12 filter-panel">
				<div class="filter-result pull-left">
					<?php echo $profile['company_name_'.$preview_lang['lang']]; ?>
				</div>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12 company-logo-div">
                <?php if (!empty($profile['logo_url'])): ?>
                <img id="preview_logo" src="<?php echo logoURL($profile['logo_url']); ?>" class="img-thumbnail img-preview">
                <?php endif; ?>
            </div>
        </div>
        <div class="row company-panel">
            <div class="col-sm-7">
                <div class="row imgs-wrapper">
                    <div class="col-md-4 avatar_div">
                        <img id="preview_avatar" src="<?php echo avatarURL($profile['avatar_url'], $profile['salutation']);?>" class="img-thumbnail img-preview">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="company-profile">
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_person_green.png"/>
                                <span class="contact-person text-capitalize text-span"><?php echo (empty($profile['salutation']) ? '' : $profile['salutation'].'. ').$profile['contact_person']; ?></span>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_website_green.png"/>
                                <?php if (!empty($profile['language'])): ?>
                                <?php $lang_arr = explode(',', $profile['language']); ?>
                                <div class="flag-div">
                                <?php foreach ($lang_arr as $lang): ?>
                                <img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $lang; ?>.png" alt="flag">
                                <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_location_green.png"/>
                                <span class="company-location text-span"><?php echo $profile['city_name'] . ', ' . $profile['zipcode'] . ' ' . $profile['street']; ?></span>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_companyinfo_green.png"/>
                                <span class="company-info text-span"><?php echo lang('detail.since') . " " . $profile['founded_at'] . ", " . employeeCountStr($profile['employee_count']) . lang('detail.employees'); ?></span>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_star_green.png"/>
                                <?php $avg_rating = getCompanyRating($profile['id']); ?>
                                <span class="company-rating"><?php echo getReviewCount($profile['id']); ?></span>
                                <div class="rating-stars">
                                    <?php for ($i = 0; $i < 5; $i++): ?>
                                        <?php if ($i < $avg_rating): ?>
                                        <i class="fa fa-star"></i>
                                        <?php else: ?>
                                        <i class="fa fa-star-o"></i>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </div>
                            </div>
                            <div class="profile-item">
                                <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_website_green.png"/>
                                <a href="<?php echo $profile['website']; ?>" target="_blank"><span class="company-website text-span"><?php echo $profile['website']; ?></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <h4><?php echo humanize(lang('company.about_us')); ?></h4>
                <p><?php echo $profile["desc_".userLang()]; ?></p>
            </div>
        </div>

        <div class="row products-div">
            <?php $company_products = explode(',', $profile['products']); ?>
            <?php if (empty($company_products)): ?>
            <div class="col-xs-12 text-red"><?php echo lang('detail.noproduct'); ?></div>
            <?php endif; ?>
            <?php foreach ($company_products as $product): ?>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="product-wrap">
                    <div class="figure-wrap-only bg-image" data-image-src="<?php echo productUrl($product); ?>">
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <?php if (!empty($profile['youtube'])): ?>
        <div class="row youtube-panel">
            <h4 class="youtube-label"><?php echo lang('detail.youtube'); ?></h4>
            <div class="col-xs-12 col-sm-12 col-md-12" id="youtube_embed">
            </div>
        </div>
        <?php endif; ?>
        <?php if (!empty($profile['latitude']) && !empty($profile['longitude'])): ?>
        <div class="row map-panel" id="map-panel">
            <h4 class="map-label"><?php echo lang('detail.map'); ?></h4>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div id="map"></div>
            </div>
        </div>
        <?php endif; ?>
    <?php endif; ?>

        <!-- <div class="row preview-footer">
            <div class="col-md-4 form-group lang-sel">
                <?php $langs = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir'); ?>
                <label><?php echo humanize(lang('company.language')); ?></label>
                <select name="language" class="form-control">
                    <?php foreach($langs as $key => $lang): ?>
                        <option value="<?php echo $lang; ?>" <?php if ($lang == userLang()): ?>selected<?php endif; ?>>
                            <?php echo lang('general.lang.'.$lang); ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div> -->
</div>

<?php $this->load->view('front/mobile/_partials/footer'); ?>
<?php if(!empty($profile['latitude']) && !empty($profile['longitude'])): ?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_MAP_KEY?>&callback=initMap" async defer></script>
<?php endif; ?>
<script>
    function getYoutubeId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }

    var map;

    <?php if(!empty($profile['latitude']) && !empty($profile['longitude'])): ?>
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?=floatval($profile['latitude']); ?>, lng: <?=floatval($profile['longitude']); ?>},
          zoom: <?=ZOOM_LEVEL?>
        });
        var icon = {
            url: "<?=base_url('assets/madinma/front/images/marker_red.png')?>", // url
            scaledSize: new google.maps.Size(<?=MARKER_WIDTH?>, <?=MARKER_HEIGHT?>), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(<?=MARKER_WIDTH/2;?>, <?=MARKER_HEIGHT?>) // anchor
        };
        var marker = new google.maps.Marker({
          position:{lat: <?=floatval($profile['latitude']); ?>, lng: <?=floatval($profile['longitude']); ?>},
          icon: icon
        });

        marker.setMap(map);
    }
    <?php endif; ?>

    $(document).ready(function () {

        var videoId = getYoutubeId('<?=$profile["youtube"];?>');

        if (videoId !='error') {
            $('#youtube_embed').html('<iframe width="100%" height="300" src="https://www.youtube.com/embed/' 
                + videoId + '?rel=0" frameborder="0" allowfullscreen></iframe>')
        }

        $(".bg-image").css("background", function() {
            var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
            return a
        });

        $('select[name="language"]').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check',
            dropdownAlignRight: rtl
        });

        $('select[name="language"]').change(function() {
            change_language($(this).val());
        });

    });
                
    function change_language(lang) {
        $.post(base_url + 'change_preview_lang/' + lang, function(data, status){
			location.reload();
        });
    }
</script>
