<!DOCTYPE html>
<html>

<?php $this->load->view('front/mobile/_base/head'); ?>

<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/iCheck/square/green.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/AdminLTE/bower_components/iCheck/icheck.min.js'); ?>"></script>
<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0.1rem;
    margin-bottom: 0.1rem;
}
.forget-password {
    font-size: 0.35rem;
    float: right;
    display: inline-block;
    margin-top: 0.175rem;
    text-shadow: none;
    color: #5b9bd1;
}
.flag_phone {
    padding-left: 66px;
}
</style>
<body>

    <?php $this->load->view('front/mobile/_partials/header'); ?>
    
    <?php $remember = $this->session->userdata('remember'); ?>
    <div class="content-wrap clearfix">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 tab-content">
                <div class="m-box login-box row">
                    <div class="col-sm-12 panel-box">
                        <h2 class="text-center text-uppercase"><?php echo lang('message.signin'); ?></h2>
                        <form id="login_form" action="" method="post" enctype="multipart/form-data" onsubmit="return validate();">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-xs-12">
                                    <div class="form-group">
                                        <label class="text-capitalize"><?php echo lang('user.phone'); ?></label>
                                        <input type="number" pattern="[0-9]*" name="phone" placeholder="<?php echo lang('user.phone.placeholder'); ?>" required="" class="form-control flag_phone" value="<?php echo empty($remember) ? '' : substr($remember['phone'], 4) ?>" autocomplete="off" style="background:url('<?php echo base_url().lang("user.phone.flag");?>') no-repeat !important;">
                                        <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.phone.require'); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-3 col-xs-12">
                                    <div class="form-group">
                                        <label class="text-capitalize"><?php echo lang('user.password'); ?></label>
                                        <input type="password" name="password" placeholder="<?php echo humanize(lang('user.password')); ?>" class="form-control" value="<?php echo empty($remember) ? '' : $remember['password'] ?>" autocomplete="off">
                                        <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.password.require'); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn btn-bb text-uppercase pull-left"><?php echo lang('message.signin'); ?></button>
                                    <div class="checkbox icheck pull-left">
                                        <label>
                                          <input type="checkbox" <?php echo empty($remember) ? '' : 'checked' ?>> <?php echo lang('signin.remember'); ?>
                                        </label>
                                    </div>
                                    <a href="javascript:;" id="forget-password" class="forget-password"><?php echo lang('signin.forgot'); ?></a>
                                </div>
                            </div>
                        </form>
                    </div><!--col-12-->
                </div>

                <div class="m-box forgot-box row" style="display: none;">
                    <div class="col-sm-12 panel-box">
                        <h2 class="text-center text-uppercase"><?php echo lang('signin.forgot'); ?></h2>
                        <form id="forgot_form" action="" onsubmit="return validate_forgot();">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-xs-12">
                                    <div class="form-group">
                                        <label class="text-capitalize"><?php echo lang('user.phone'); ?></label>
                                        <input type="number" pattern="[0-9]*" name="phone" placeholder="650123456" required="" class="form-control flag_phone" value="<?php echo empty($remember) ? '' : substr($remember['phone'], 4) ?>" autocomplete="off">
                                        <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.phone.require'); ?></span>
                                        <input type="hidden" name="recaptcha_response" id="forgot_recaptcha">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="button" class="btn btn-bb btn-back text-uppercase pull-left"><?php echo lang('signin.back'); ?></button>
                                    <button type="submit" class="btn btn-bb text-uppercase pull-right"><?php echo lang('signin.submit'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div><!--col-12-->
                </div>

            </div><!--col-9-->
        </div>
    </div><!--container-->
<?php $this->load->view('front/mobile/_partials/footer'); ?>
<script src="https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_SITE_KEY?>"></script>
<script type="text/javascript">

    function validate() {
        var isValid = true;
        var fields = ['phone', 'password'];
        var first_err_elem = null;
        $('#login_form .form-group').removeClass('has-error');
        for (i = 0; i < fields.length; i++) {
            if ($('#login_form input[name="' + fields[i] + '"]').val() == '') {
                isValid = false;
                $('#login_form input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').removeClass('hidden');
                $('#login_form input[name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
                if (first_err_elem == null) {
                    first_err_elem = $('input[name="' + fields[i] + '"]');
                }
            } else {
                $('#login_form input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').addClass('hidden');
                $('#login_form input[name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
            }
        }
        if (first_err_elem != null) {
            first_err_elem.focus();
        }
        
        if (isValid === true) {
            submit();
        }

        return false;
    }

    function submit() {
        var formData = new FormData();
        formData.append('phone', '+212' + $('#login_form input[name="phone"]').val());
        formData.append('password', $('#login_form input[name="password"]').val());
        formData.append('remember', $('#login_form input[type=checkbox').is(':checked') ? 1 : 0);
        // formData.append('recaptcha_response', grecaptcha.getResponse());

        $.ajax({
            'url' : base_url + 'login/post',
            'type' : 'POST',
            'data' : formData,
            'async' : false,
            'cache' : false,
            'contentType': false,
            'processData' : false,
            'dataType' : 'json',
            'success' : function(response) {
                if (response.success) {
                    window.location = base_url + 'myaccount';
                } else {
                    var dialog = bootbox.dialog({
                        title: message_error,
                        message: response.message,
                        size: 'small',
                        buttons: {
                            ok: {
                                label: message_ok,
                                className: 'btn-danger'
                            }
                        }
                    });
                }
            }
        });
    }

    function validate_forgot() {
        var isValid = true;
        var fields = ['phone'];
        var first_err_elem = null;
        $('#forgot_form .form-group').removeClass('has-error');
        if ($('#forgot_form input[name="phone"]').val() == '') {
            isValid = false;
            $('#forgot_form input[name="phone"]').parents('.form-group').find('.help-block-error').removeClass('hidden');
            $('#forgot_form input[name="phone"]').parents('.form-group').addClass('has-error');
            $('#forgot_form input[name="phone"]').focus();
        } else {
            $('#forgot_form input[name="phone"]').parents('.form-group').find('.help-block-error').addClass('hidden');
            $('#forgot_form input[name="phone"]').parents('.form-group').removeClass('has-error');
        }
        
        if (isValid === true) {
            submit_forgot();
        }

        return false;
    }

    function submit_forgot() {
        var formData = new FormData();
        formData.append('phone', '+212' + $('#forgot_form input[name="phone"]').val());
        formData.append('recaptcha_response', $('#forgot_form input[name="recaptcha_response"]').val());
        
        $.ajax({
            'url' : base_url + 'forgot/post',
            'type' : 'POST',
            'data' : formData,
            'async' : false,
            'cache' : false,
            'contentType': false,
            'processData' : false,
            'dataType' : 'json',
            'success' : function(response) {
                if (response.success) {
                    var dialog = bootbox.dialog({
                        title: message_success,
                        message: response.message,
                        size: 'small',
                        buttons: {
                            ok: {
                                label: message_ok,
                                className: 'btn-success'
                            }
                        }
                    });
                } else {
                    var dialog = bootbox.dialog({
                        title: message_error,
                        message: response.message,
                        size: 'small',
                        buttons: {
                            ok: {
                                label: message_ok,
                                className: 'btn-danger'
                            }
                        }
                    });
                }
            }
        });
        return false;
    }

    $(function () {
        grecaptcha.ready(function() {
            grecaptcha.execute('<?=RECAPTCHA_SITE_KEY?>', {action: 'login'}).then(function(token) {
                var forgotRecaptcha = document.getElementById('forgot_recaptcha');
                forgotRecaptcha.value = token;
            });
        });

        $('input[type=checkbox]').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
          increaseArea: '20%' /* optional */
        });

        // phone number box only allow arabic numerals
        // $('#login_form input[name="phone"]').inputmask("699999999");
        // $('#forgot_form input[name="phone"]').inputmask("699999999");

        $('.forget-password').click(function() {
            $('.forgot-box').show();
            $('.login-box').hide();
        });

        $('.btn-back').click(function() {
            $('.forgot-box').hide();
            $('.login-box').show();
        });
    });


</script>
</body>

</html>