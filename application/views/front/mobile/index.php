<style>
	/*.content-wrap {	
	    display: flex;
	    flex-wrap: wrap;
	}*/
    .square-item a:hover {
        color: #D20023 !important;
    }
</style>
<?php 
$count = 0;
foreach ($categories as $category): 
    $count += 1;
    if($count % 2) {
?>
<div class="row">
    <div class="col-xs-6 col-sm-6 square-item">
        <a href="<?php echo site_url('categories/'.getSEOStr($category->{'name_en'})); ?>">
            <?php echo symbolSVG($category->symbol_url); ?><?php echo $category->{"name_".userLang()}; ?>
        </a>
    </div>
<?php
    } else { ?>
    <div class="col-xs-6 col-sm-6 square-item">
        <a href="<?php echo site_url('categories/'.getSEOStr($category->{'name_en'})); ?>">
            <?php echo symbolSVG($category->symbol_url); ?><?php echo $category->{"name_".userLang()}; ?>
        </a>
    </div>
</div>  
<?php } endforeach; ?>
