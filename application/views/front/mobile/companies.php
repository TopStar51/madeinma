<style>
    .content-wrap {
        padding: 0;
    }
    .content-wrap .row {
        margin: 0;
    }
    .category-tag {
        margin-left: 5px;
        padding-left: 30px;
        min-width: 80px;
    }
</style>
<?php 
    $action_url = ($this->uri->segment(1,0)=='favorites')? base_url('favorites'):'';
?>
<div class="row filter-panel">
    <div class="col-md-12">
        <?php $category_sess = getSEOStr($this->session->userdata('category')); ?>
        <div class="filter-result pull-left">
            <?php if (empty($category_sess)): ?>
                <span class="result-text"><?php echo $total ?> <?php echo lang('companies.entries'); ?></span>
            <?php else: ?>
                <span class="result-text"><?php echo $total ?> <?php echo lang('companies.entries').' '; ?><?php echo lang('companies.in') . ' '; ?></span>
                <span class="category-tag"><?php echo ($this->session->userdata('category')); ?><img class="tag-icon" onclick="javascript:remove_tag();" src="<?php echo base_url('assets/madinma/front/images/close_icon.png'); ?>"/></span>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="row products-panel">
	<?php if($this->session->userdata('company_total')== 0):?>
		<p style="text-align:center;" class="pad-tb50"> <?php echo lang('topsearch.noresult'); ?>  
			<br><a class="reset" style="color:blue;cursor:pointer"> <?php echo lang('topsearch.reset'); ?> </a>
		</p>
	<?php endif;?>
    <?php $this->load->view('front/mobile/_partials/company_part.php'); ?>
</div>

<script>
    var pre_top = 0;
    var received = true;
    var count = Number("<?php echo count($companies); ?>");
    var is_end = false;

    function getPullingData() {
        pre_top = $(window).scrollTop();
        received = false;
        $.ajax({
            // url: "<?php echo base_url(''); ?><?php echo empty($cat_name) ? 'companies' : '/categories/'.getSEOStr($cat_name); ?>",
            data:  {
                start: count,
                q: $('input[name="q"]').attr('old-value'),
                city: $('[name="city"]').val(),
                sort: $('[name="sort"]').val()
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                count += data.count;
                if (data.count == 0) {
                    is_end = true;
                }
                $('.products-panel').append(data.html);
                $(".bg-image").css("background", function() {
                    var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
                    return a
                });
                $(window).scrollTop(pre_top);
                received = true;
                /*$("html").niceScroll().remove();
                $("html").niceScroll({
                    scrollspeed: 60,
                    mousescrollstep: 40,
                    cursorwidth: 8,
                    cursorheight: 130,
                    cursorborder: 0,
                    cursorcolor: '#D20023',
                    cursorborderradius: 5,
                    styler:"fb",
                    autohidemode: false,
                    horizrailenabled: false
                });*/
            }
        });
    }

     $(window).scroll(function(event) {
        if(!received)
        {
            event.preventDefault();
            event.stopPropagation();
            return;
        }

        if(Math.abs($(window).scrollTop()-$(".products-panel").height()) < 1000 && is_end == false)
        {
            pre_top = $(window).scrollTop();
            getPullingData();
        }
    });
    $(window).scrollTop(0);

    $(document).ready(function () {
        $(".bg-image").css("background", function() {
            var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
            return a
        });
		
    });

    function remove_tag() {
        var city_sel = $('#city_sel').val();
        var action_url = "<?php echo $action_url;?>";
        if(action_url == '')
            $('#search-form').attr('action', base_url + 'cities/' + city_sel);
        else
            $('#search-form').attr('action', action_url);
        $('#search-form')[0].submit();
    }
</script>
