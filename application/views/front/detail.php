<link rel="stylesheet" href="<?= base_url('assets/plugins/fancybox/source/jquery.fancybox.css') ?>">
<script src="<?=base_url('assets/plugins/fancybox/source/jquery.fancybox.js')?>"></script>
<?php $this->load->view('front/_partials/searchbar'); ?>
<style type="text/css">
    .profile-item a:hover {
        color: #D20023 !important;
    }
    .img-responsive {
        border: solid 2px #00652D;
        padding: 5px;
        border-radius: 5px;
    }
    #map {
        width: 100%;
        height: 350px;
	}
	#svg{
		width:25px;
		height:25px;
	}
</style>
<div class="row">
    <div class="col-md-12 col-sm-12 filter-panel">
        <div class="filter-result pull-left">
			<div class="company-profile" style="display:inline-block">
				<?php
					if(!isset($company->{'company_name_'.userLang()}) || $company->{'company_name_'.userLang()}=="")
						$company_name = $company->{'company_name_fr'};
					else
						$company_name = $company->{'company_name_'.userLang()};
				?>
				<div class="profile-item">
					<?php echo symbolSVG($company->symbol_url); ?>
					<span class="contact-person text-capitalize text-span" style="font-size: 16px;">
						<?php echo $company_name; ?>
					</span>
				</div>
				<!-- <?php echo isset($company->company_name)?($company->company_name):($company->company_name_fr); ?> -->
			</div>
        </div>
        <div class="pull-right">
			<?php 
				$url_seg = $this->uri->segment(2,0);
				$back_url = base_url('categories/'.$url_seg); 
			?>
			<a href="<?php echo $back_url;?>"class="btn btn-bb-sm"><i class="fa fa-reply" style="padding-top:5px"></i></a>
        </div>
    </div>
</div>
<div class="row company-panel">
    <div class="col-sm-7">
        <div class="row">
            <div class="col-md-4" style="max-width:200px;">
                <div class="avatar-wrap">
                    <a rel="gallery" class="img-hover-v1" title="<?=humanize(lang('myaccount.contact_avatar'))?>">
                        <span><img id="preview_avatar" class="img-responsive" src="<?php echo avatarURL($company->avatar_url, $company->salutation); ?>" alt=""></span>
                    </a>
                    <div class="favorite">
                        <a class="favorite-link" href="javascript:;" data-favorite="<?php echo isFavorite($company->id); ?>" data-company="<?php echo $company->id; ?>">
                            <?php if (isFavorite($company->id)): ?>
                            <img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_on.png"/>
                            <?php else: ?>
                            <img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_off.png"/>
                            <?php endif; ?> 
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12 company-logo-div">
                        <?php if (!empty($company->logo_url)): ?>
                        <a href="<?php echo logoURL($company->logo_url); ?>" rel="gallery_logo" class="fancybox img-hover-v1" title="<?=humanize(lang('company.logo'))?>">
                            <span><img id="preview_logo" class="img-responsive" src="<?php echo logoURL($company->logo_url); ?>" alt=""></span>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="company-profile">
                    <div class="profile-item">
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/contact.svg"/>
                        <span class="contact-person text-capitalize"><?php echo (empty($company->salutation) ? '' : humanize(lang('company.salutation.placeholder.'.$company->salutation)).'. ').$company->contact_person; ?></span>
                    </div>
                    <div class="profile-item">
                        <!-- <i class="icon-globe"></i> -->
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/translate.svg"/>
                        <!-- <img src="<?php echo base_url(); ?>assets/madinma/front/images/icon_website_green.png"/> -->
                        <?php if (!empty($company->language)): ?>
                        <?php $lang_arr = explode(',', $company->language); ?>
                        <div class="flag-div">
                        <?php foreach ($lang_arr as $lang): ?>
                        <img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $lang; ?>.png" alt="flag">
                        <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="profile-item">
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/location.svg"/>
                        <a href="#map-panel"><span class="company-location"><?php echo $company->city_name . ', ' . $company->street; ?></span></a>
                    </div>
                    <div class="profile-item">
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/analytics.svg"/>
                        <span class="company-info"><?php echo lang('detail.since')?> <?php echo $company->founded_at . ", " . employeeCountStr($company->employee_count) ." ". lang('detail.employees'); ?></span>
                    </div>
                    <div class="profile-item">
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/rating.svg"/>
                        <?php $avg_rating = getCompanyRating($company->id); ?>
                        <?php if(getReviewCount($company->id) > 0) { ?>
                        <a href="#rating-panel"><span class="company-rating"><?php echo getReviewCount($company->id); ?></span></a>
                        <?php } ?>
                        <?php if($avg_rating == 0) { ?>
                        <a href="#rating-panel"><span class="company-rating"><?php echo lang('no.ratings'); ?></span></a>
                        <?php  } else { ?>
                        <a href="#rating-panel"><div class="rating-stars">
                            <?php for ($i = 0; $i < 5; $i++): ?>
                                <?php if ($i < $avg_rating): ?>
                                <i class="fa fa-star"></i>
                                <?php else: ?>
                                <i class="fa fa-star-o"></i>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </div></a>
                        <?php } ?>
                    </div>
                    <div class="profile-item">
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/website.svg"/>
                        <a  style = "direction: ltr !important;" href="<?php echo $company->website; ?>" target="_blank"><span class="company-website" style = "direction: ltr !important;"><?php echo $company->website; ?></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <h4><?php echo humanize(lang('company.about_us')); ?></h4>
        <p><?php echo $company->{"desc_".userLang()}; ?></p>
    </div>
</div>
<div class="row btns-inquery-div">
    <div class="col-md-7">
    <?php if ((!empty($_SESSION['user']) && $company->user_id != $_SESSION['user']->id) || empty($_SESSION['user'])): ?>
        <button class="btn btn-bb rightspace15" data-toggle="modal" data-target="#call_modal"><i class="fa fa-phone"></i> <?php echo lang('message.call'); ?></button>
        <?php if($company->show_msg_btn == 1):?>
            <button class="btn btn-bb rightspace15" data-toggle="modal" data-target="#contact_modal"><i class="fa fa-envelope"></i> <?php echo lang('message.mail'); ?></button>
        <?php endif;?>
    <?php endif; ?>
    </div>
    <div class="col-md-5 bottomspace10">
        <span class="rightspace15"><i class="fa fa-eye"></i> <?php echo $inquery_cnt; ?></span>
        <?php $datetime = date_create($company->updated_at); ?>
        <span><?php echo lang('detail.last.update'); ?>: <?php echo $datetime->format('d.m.Y'); ?></span>
    </div>
</div>

<div class="row products-div">
    <h4 class="product-pic-label"><?php echo lang('detail.product.pics'); ?></h4>
    <?php if (empty($company_products)): ?>
    <div class="col-xs-12 text-red"><?php echo lang('detail.noproduct'); ?></div>
    <?php endif; ?>
    
    <?php foreach ($company_products as $product): ?>
        
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="product-wrap" style="border:none;">
            <a <?= (!empty($product->img_url)) ? 
            'href="'.site_url('home/product_watermark/'.$product->img_url).'" class="fancybox img-hover-v1"' : 'class="img-hover-v1"' ?> rel="gallery" 
            title="<?=humanize(lang('myaccount.company_product'))?>">
                <img class="img-responsive" src="<?php echo productThumbURL($product->img_url); ?>" alt="">
            </a>
        </div>
    </div>
    <?php endforeach; ?>
</div>

<?php if (!empty($company->youtube)): ?>
<div class="row youtube-panel">
    <h4 class="youtube-label"><?php echo lang('detail.youtube'); ?></h4>
    <div class="col-xs-12 col-sm-12 col-md-12" id="youtube_embed">
    </div>
</div>
<?php endif; ?>
<?php if (!empty($company->youtube1)): ?>
<div class="row youtube-panel">
    <h4 class="youtube-label"><?php echo lang('detail.youtube'); ?></h4>
    <div class="col-xs-12 col-sm-12 col-md-12" id="youtube_embed1">
    </div>
</div>
<?php endif; ?>


<?php if ((!empty($_SESSION['user']) && $company->user_id != $_SESSION['user']->id) || empty($_SESSION['user'])): ?>
<div class="row">
    <div class="col-md-12">
        <button class="btn btn-bb pull-right" data-toggle="modal" data-target="#rating_modal"><i class="fa fa-plus-square"></i> <?php echo lang('message.rating'); ?></button>
    </div>
</div>
<?php endif; ?>

<div class="row ratings-panel" id="rating-panel">
    <h4 class="rating-label"><?php echo lang('detail.rating'); ?></h4>
    <?php 
    if(empty($company_reviews)) { ?>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?php echo lang('no.ratings'); ?>
        </div>
    <?php
    } else { 
    foreach ($company_reviews as $review): ?>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="rating-wrap">
            <img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $review->language; ?>.png">
            <p><?php $created_time = date_create($review->created_at); echo $created_time->format('d.m.Y H:i:s'); ?></p>
            <p><?php echo $review->name.', '.$review->country_name; ?></p>
            <p><?php echo $review->content; ?></p>
            <div class="row">
                <div class="col-sm-12">
                    <div class="rating-stars pull-right"> 
                        <?php for ($i = 0; $i < 5; $i++): ?>
                            <?php if ($i < $review->rating): ?>
                            <i class="fa fa-star"></i>
                            <?php else: ?>
                            <i class="fa fa-star-o"></i>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; } ?>
</div>

<?php if (!empty($company->latitude) && !empty($company->longitude)): ?>
<div class="row map-panel" id="map-panel">
    <h4 class="map-label"><?php echo lang('detail.map'); ?></h4>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div id="map"></div>
    </div>
</div>
<?php endif; ?>

<div id="rating_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type = "button" class="close" data-dismiss="modal"><img class="x_btn_img" src="<?php echo base_url('assets/madinma/front/images/x_btn.png'); ?>"/></button>
                <h4 class="modal-title text-uppercase"><?php echo lang('modal.rating.title'); ?></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                    <div class="col-sm-7">
                        <form>
                            <div class="form-group">
                              <input type="text" class="form-control" name="name" placeholder="<?php echo humanize(lang('modal.placeholder.name')); ?>">
                            </div>
                            <div class="form-group">
                              <input type="email" class="form-control" name="email" placeholder="<?php echo humanize(lang('modal.placeholder.email')); ?>">
                            </div>
                            <div class="form-group">
                                <select id="country_sel" name="country">
                                    <option></option>
                                    <?php foreach ($countries as $country): ?>
                                    <option value="<?php echo $country->id; ?>"><?php echo $country->{'name_'.userLang()}; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                              <textarea class="form-control" rows="10" name="content" placeholder="<?php echo humanize(lang('modal.placeholder.text')); ?>"></textarea>
                            </div>
                            <input type="hidden" name="rating" value="1">
                            <input type="hidden" name="company_id" value="<?php echo $company->id; ?>">
                            <input type="hidden" name="recaptcha_response" id="rating_recaptcha">
                        </form>
                    </div>
                    <div class="col-sm-5 right-part" style="height: 382px;">
                        <div class="rating-stars-div">
                            <h4><?php echo humanize(lang('modal.your_rating')); ?>:</h4>
                            <div class="rating-stars"> 
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-star-o"></i></a>
                                <a href="#"><i class="fa fa-star-o"></i></a>
                                <a href="#"><i class="fa fa-star-o"></i></a>
                                <a href="#"><i class="fa fa-star-o"></i></a>
                            </div>
                        </div>
                        <!-- <div id="rating_recaptcha" class="g-recaptcha recaptScale"></div> -->
                        <button class="btn btn-bb btn-send"><i class="fa fa-paper-plane"></i> <?php echo lang('modal.btn.send'); ?></button>
                    </div>
                </div>
            </div><!--modal-body-->
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->

<div id="call_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img class="x_btn_img" src="<?php echo base_url('assets/madinma/front/images/x_btn.png'); ?>"/></button>
                <h4 class="modal-title text-uppercase"><?php echo lang('modal.call.title'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <h2 class="phone_text pull-left rightspace15"  style = "direction: ltr !important;" id="phone_text"></h2>
                        <button class="btn btn-bb-sm pull-left" onclick="copyFunc()"><i class="fa fa-copy"></i></button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p><?php echo lang('modal.call_desc'); ?></p>
                    </div>
                </div>
            </div><!--modal-body-->
            <div class="modal-footer">
                 <a class="btn btn-bb btn-send"><i class="fa fa-phone"></i> <?php echo lang('message.call'); ?></a>
            </div>
        </div><!--modal-content-->
    </div><!--modal-dialog-->
</div><!--modal-->

<script src="https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_SITE_KEY?>"></script>
<script>
    function getYoutubeId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }

    var map;

    <?php if(!empty($company->latitude) && !empty($company->longitude)): ?>
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?=floatval($company->latitude); ?>, lng: <?=floatval($company->longitude); ?>},
          zoom: <?=ZOOM_LEVEL?>
        });
        var icon = {
            url: "<?=base_url('assets/madinma/front/images/marker_red.png')?>", // url
            scaledSize: new google.maps.Size(<?=MARKER_WIDTH?>, <?=MARKER_HEIGHT?>), // scaled size
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(<?=MARKER_WIDTH/2;?>, <?=MARKER_HEIGHT?>) // anchor
        };
        var marker = new google.maps.Marker({
          position:{lat: <?=floatval($company->latitude); ?>, lng: <?=floatval($company->longitude); ?>},
          icon: icon
        });

        marker.setMap(map);
    }
    <?php endif; ?>

    function copyFunc() {
        var text = document.getElementById("phone_text").innerText;
        var elem = document.createElement("input");
        document.body.appendChild(elem);
        elem.value = text;
        elem.select();
        document.execCommand("copy");
        document.body.removeChild(elem);
        alert('Copied: ' + text);
    }

    var FancyBox = function () {
        return {
            //Fancybox
            initFancybox: function () {
                jQuery(".fancybox").fancybox({
                    groupAttr: 'data-rel',
                    prevEffect: 'fade',
                    nextEffect: 'fade',
                    openEffect  : 'elastic',
                    closeEffect  : 'fade',
                    closeBtn: true,
                    helpers: {
                        title: {
                                type: 'float'
                            }
                        },
                        overlay : {
                            showEarly : false
                        }
                    });

                $(".fbox-modal").fancybox({
                    maxWidth    : 800,
                    maxHeight   : 600,
                    fitToView   : false,
                    width       : '70%',
                    height      : '70%',
                    autoSize    : false,
                    closeClick  : false,
                    closeEffect : 'fade',
                    openEffect  : 'elastic'
                });
            }

        };

    }();

    $(document).ready(function() {

        FancyBox.initFancybox();

        var videoId = getYoutubeId('<?=$company->youtube?>');
        var videoId1 = getYoutubeId('<?=$company->youtube1?>');
        if (videoId !='error') {
            $('#youtube_embed').html('<iframe width="60%" height="315" src="https://www.youtube.com/embed/' 
                + videoId + '?rel=0" frameborder="0" allowfullscreen></iframe>')
        }
        if (videoId1 !='error') {
            $('#youtube_embed1').html('<iframe width="60%" height="315" src="https://www.youtube.com/embed/' 
                + videoId1 + '?rel=0" frameborder="0" allowfullscreen></iframe>')
        }
        grecaptcha.ready(function() {
            grecaptcha.execute('<?=RECAPTCHA_SITE_KEY?>', {action: 'detail'}).then(function(token) {
                var ratingRecaptcha = document.getElementById('rating_recaptcha');
                var contactRecaptcha = document.getElementById('contact_recaptcha');
                ratingRecaptcha.value = token;
                contactRecaptcha.value = token;
            });
        });

        $('#country_sel').selectpicker({
            width: '100%',
            height: 32,
            dropdownAlignRight: rtl,
            noneSelectedText: '<?php echo lang("modal.lang.none.selected")?>'
        });

        $('#call_modal').on('show.bs.modal', function() {
            $.get(base_url + 'phone_num/' + "<?php echo $company->id; ?>", function(data) {
                if (data.success) {
                    $('#call_modal .phone_text').html(data.phone_num);
                    $('#call_modal .btn-send').attr('href', 'tel:' + data.phone_num);
                }
            });
        });

        $('#call_modal').on('hidden.bs.modal', function() {
            $('#call_modal .phone_text').html('');
            $('#call_modal .btn-send').removeAttr('href');  
        });

        $('#rating_modal').on('show.bs.modal', function() {
            $(this).find('form')[0].reset();
            $('#rating_modal .rating-stars i').addClass('fa-star-o');
            $('#rating_modal .rating-stars i').removeClass('fa-star');
            $($('#rating_modal .rating-stars i')[0]).addClass('fa-star');
            $($('#rating_modal .rating-stars i')[0]).removeClass('fa-star-o');
            var obj = $(this).find('textarea')[0];
            $(obj).attr('placeholder',"<?php echo lang('modal.placeholder.rating_detail')?>");
        });

        $('#rating_modal .btn-send').click(function() {

            var isValid = true;
            var fields = ['name', 'country', 'email', 'content'];
            $('#rating_modal .form-group').removeClass('has-error');
            for (i = 0; i < fields.length; i++) {
                if ($('#rating_modal [name="' + fields[i] + '"]').val() == '') {
                    isValid = false;
                    $('#rating_modal [name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
                } else {
                    $('#rating_modal [name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
                }
            }

            if (isValid == true) {
                var params = {
                    name: $('#rating_modal [name="name"]').val(),
                    email: $('#rating_modal [name="email"]').val(),
                    country_id: $('#country_sel').selectpicker('val').toString(),
                    content: $('#rating_modal [name="content"]').val(),
                    rating: $('#rating_modal [name="rating"]').val(),
                    company_id: $('#rating_modal [name="company_id"]').val(),
                    recaptcha_response: $('#rating_modal [name="recaptcha_response"]').val()
                };
                $.post(base_url + 'rating/post', params, function(data, status){
                    if (data.success) {
                        bootbox.dialog({
                            title: message_success,
                            message: data.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-success',
                                    callback: function(){
                                        location.reload();
                                    }
                                }
                            }
                        });
                        $('#rating_modal').modal('hide');
                    } else {
                        bootbox.dialog({
                            title: message_error,
                            message: data.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-danger'
                                }
                            }
                        });
                    }
                });
            }
        });

        $('#rating_modal .rating-stars a').click(function() {
            $('#rating_modal .rating-stars i').removeClass('fa-star');
            $('#rating_modal .rating-stars i').addClass('fa-star-o');
            var pre_stars = $(this).prevAll('a');
            for (i = 0; i < pre_stars.length; i++) {
                $(pre_stars[i]).find('i').removeClass('fa-star-o');
                $(pre_stars[i]).find('i').addClass('fa-star');
            }
            $(this).find('i').removeClass('fa-star-o');
            $(this).find('i').addClass('fa-star');

            $('#rating_modal [name="rating"]').val(Number(pre_stars.length + 1));
        });

        $('#contact_modal').on('show.bs.modal', function() {
            $(this).find('form')[0].reset();
            var obj = $(this).find('textarea')[0];
            $(obj).attr('placeholder',"<?php echo lang('modal.placeholder.company_detail')?>");
        });

        $('#contact_modal .btn-send').click(function() {
            
            var isValid = true;
            var fields = ['name', 'email', 'content'];

            if ($(".option_type:checked").attr('value') == '0'){
                fields = ['name', 'email', 'content'];
            }else if($(".option_type:checked").attr('value') == '1'){
                fields = ['name', 'phone', 'content'];
            }else{
                fields = ['name', 'email', 'phone', 'content'];
            }
            
            $('#contact_modal .form-group').removeClass('has-error');
            for (i = 0; i < fields.length; i++) {
                if ($('#contact_modal [name="' + fields[i] + '"]').val() == '') {
                    isValid = false;
                    $('#contact_modal [name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
                } else {
                    $('#contact_modal [name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
                }
            }

            if (isValid == true) {
                var params = {
                    company_id: $('#contact_modal [name="company_id"]').val(),
                    name: $('#contact_modal [name="name"]').val(),
                    email: $('#contact_modal [name="email"]').val(),
                    phone: $('#contact_modal [name="phone"]').val(),
                    country_id: $('#contact_modal [name="country_id"]').val(),
                    content: $('#contact_modal [name="content"]').val(),
                    recaptcha_response: $('#contact_modal [name="recaptcha_response"]').val()
                };
                $.post(base_url + 'mail/post', params, function(data, status){
                    if (data.success) { 
                        bootbox.dialog({
                            title: message_success,
                            message: message_success,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-success',
                                    callback: function(){
                                        location.reload();
                                    }
                                }
                            }
                        });
                        $('#contact_modal').modal('hide');
                    } else {
                        bootbox.dialog({
                            title: message_error,
                            message: data.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-danger'
                                }
                            }
                        });
                    }
                });
            }
        });

        $(".bg-image").css("background", function() {
            var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
            return a
        });

    });
    
</script>
<?php if(!empty($company->latitude) && !empty($company->longitude)): ?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_MAP_KEY?>&callback=initMap" async defer></script>
<?php endif; ?>
