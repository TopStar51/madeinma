<head>
	<title><?php echo lang('site.title'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="msapplication-TileColor" content="#d20023">
	<meta name="theme-color" content="#d20023">

	<meta name="google" content="notranslate" />
	<meta name="google" content="nositelinkssearchbox" />

	<meta property="og:title" content="<?php echo isset($title) ? $title : '' ; ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo current_url()?>" />
	<meta property="og:image" content="<?php echo isset($image)? $image : '' ; ?>" />
	<meta property="og:description" content="<?php echo isset($desc)? $desc : '' ; ?>" />
	<meta property="og:site_name" content="made-in.ma" />

	<link rel="shortcut icon" href="<?php echo base_url('assets/favicon/favicon.ico'); ?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/favicon/apple-touch-icon.png'); ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/favicon/favicon-32x32.png'); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/favicon/favicon-16x16.png'); ?>">
	<link rel="manifest" href="<?php echo base_url('assets/favicon/site.webmanifest'); ?>">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#d20023">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/css/bootstrap.min.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/AdminLTE/bower_components/jquery-ui/themes/base/jquery-ui.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/plugins/bootstrap-select/bootstrap-select.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/plugins/owlcarousel/assets/owl.carousel.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/plugins/owlcarousel/assets/owl.theme.default.min.css'); ?>" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/css/style.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/css/responsive.css'); ?>" />

    <?php if (userLang() == 'ar') :// || userLang() == 'ir'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/css/style_rtl.css'); ?>" />
	<?php endif; ?>

	<script type="text/javascript" src="<?php echo base_url('assets/madinma/front/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/madinma/front/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/madinma/front/plugins/owlcarousel/owl.carousel.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/madinma/front/js/nicescroll.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/madinma/front/plugins/bootbox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/madinma/front/plugins/bootstrap-select/bootstrap-select.js"></script>
	<script src="<?php echo base_url(); ?>assets/AdminLTE/bower_components/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/AdminLTE/bower_components/jquery-ui/ui/autocomplete.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/madinma/front/js/jquery.inputmask.js')?>"></script>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143709250-1"></script>
</head>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-143709250-1');

	var user_lang = "<?php echo userLang(); ?>";
	var base_url = "<?php echo base_url(); ?>";
	var message_success = "<?php echo lang('message.success'); ?>";
	var message_error = "<?php echo lang('message.error'); ?>";
	var message_warning = "<?php echo lang('message.warning'); ?>";
	var message_ok = "<?php echo lang('message.ok'); ?>";
	var message_cancel = "<?php echo lang('message.cancel'); ?>";
	var rtl = (user_lang == 'ar' ? true : false);

	function change_language(lang) {
		$.post(base_url + 'lang/' + lang, function(data, status){
            if (data.success) {
                location.reload();
            } else {
                bootbox.dialog({
                    title: message_error,
                    message: data.message,
                    size: 'small',
                    buttons: {
                        ok: {
                            label: message_ok,
                            className: 'btn-danger'
                        }
                    }
                });
            }
        });
	}

	$(document).ready(function() {

	    $("html").niceScroll({
			scrollspeed: 60,
			mousescrollstep: 40,
			cursorwidth: 8,
			cursorheight: 130,
			cursorborder: 0,
			cursorcolor: '#D20023',
			cursorborderradius: 5,
			styler:"fb",
			autohidemode: false,
			horizrailenabled: false
		});

		$('.favorite-link').click(function(e) {
			var me = $(this);

			var favorite = me.attr('data-favorite');
			var company_id  = me.attr('data-company');
			var on_img_path = "<?php echo base_url(); ?>assets/madinma/front/images/heart_on.png";
			var off_img_path = "<?php echo base_url(); ?>assets/madinma/front/images/heart_off.png";

			$.post(base_url + 'favorite/post', {favorite: (favorite == 1 ? 0 : 1), company_id: company_id}, function(data, status){
              	if (data.success) {
              		me.attr('data-favorite', (favorite == 1 ? 0 : 1));
              		me.find('img').attr('src', favorite == 1 ? off_img_path : on_img_path);
              		var fav_cnt = Number($('.favorite-count').html());
              		if (fav_cnt.toString() == "NaN") {
              			fav_cnt = 0;
              		}
              		if (me.attr('data-favorite') == 1) {
              			fav_cnt += 1;
              		} else {
              			fav_cnt -= 1;
              		}
              		$('.favorite-count').html(fav_cnt);
              	}
          	});
		});

	});
</script>
