<div class="row footer">
	<div class="col-sm-6 aboutus-div">
		<h4><?php echo lang('footer.about_title'); ?></h4>
		<p><?php echo lang('footer.about_content'); ?></p>
	</div>
	<div class="col-sm-3 contactus-div">
		<h4><?php echo lang('footer.contact_title'); ?></h4>
		<p><?php echo lang('footer.contact_content'); ?></p>
		<button class="btn btn-bb text-uppercase" data-toggle="modal" data-target="#contact_modal"><i class="fa fa-envelope"></i> <?php echo lang('footer.mail'); ?></button>
	</div>
	<div class="col-sm-3 statistics-div">
		<h4><?php echo lang('footer.statistics_title'); ?></h4>
		<p style="margin-bottom: 0;"><?php echo $total_cnt_companies; ?> <?php echo lang('footer.statistics_comapny'); ?></p>
		<p><?php echo $week_cnt_companies; ?> <?php echo lang('footer.statistics_comapny_week'); ?></p>
		<p style="margin-bottom: 0;"><?php echo $total_cnt_inqueries; ?> <?php echo lang('footer.statistics_inquery'); ?></p>
		<p><?php echo $week_cnt_inqueries; ?> <?php echo lang('footer.statistics_inquery_week'); ?></p>
	</div>
</div>