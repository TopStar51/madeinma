<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-9">
		<?php
    		$city_sel = $this->session->userdata('city_sel'); 
		    $city_sel = empty($city_sel) ? 'all' : $city_sel;
		    $sort_sel = $this->session->userdata('sort_sel'); 
			$sort_sel = empty($sort_sel) ? 'rating' : $sort_sel;
			$emp_sel = $this->session->userdata('emp_sel'); 
    		$emp_sel = empty($emp_sel) ? 'all' : $emp_sel;

		    if (empty($cat_name)) {
				$action_url = base_url('cities/'.getSEOStr($city_sel));
			} else if ($cat_name == 'favorites') {
				$action_url = base_url('favorites');
			} else {
				$action_url = base_url('categories/'.getSEOStr($cat_name));
			}
		?>
		<form action="<?php echo $action_url; ?>" onsubmit="beforeSubmit();" method="POST" class="top-search">
		    <div class="input-group" >
		        <input z-index = "100000" type="text" name="q" id="top_search_input" class="form-control" old-value="<?php echo empty($sEcho) ? '' : $sEcho; ?>" value="<?php echo empty($sEcho) ? '' : $sEcho; ?>" placeholder="<?php echo lang('topsearch.placeholder'); ?>">
		        <input z-index = "100000"  type="hidden" name="city" value="<?php echo $city_sel; ?>">
		        <input  z-index = "100000" type="hidden" name="sort" value="<?php echo $sort_sel; ?>">
		        <input z-index = "100000"  type="hidden" name="emp" value="<?php echo $emp_sel; ?>">
		        <span class="input-group-btn">
		            <button class="btn" type="submit"><i class="fa fa-search"></i> <?php echo lang('topsearch.search'); ?></button>
		        </span>
		    </div>                  
		</form>
	</div>
</div>

<script>
	function beforeSubmit(id) {
		$('input[name="city"]').val($('#city_sel').val());
		$('input[name="sort"]').val($('#sort_sel').val());
		$('input[name="emp"]').val($('#emp_sel').val());
	}
	function setFocusEnd(id) {
		var node = document.getElementById(id);
		pos = node.value.length;
		node.focus();
		if (!node) {
			return false;
		} else if (node.createTextRange) {
			var textRange = node.createTextRange();
			textRange.collapse(true);
			textRange.moveEnd(pos);
			textRange.moveStart(pos);
			textRange.select();
			return true;
		} else if (node.setSelectionRange) {
			node.setSelectionRange(pos, pos);
			return true;
		}
		return false;
	}
	$(document).ready(function() {
		setFocusEnd('top_search_input');

		$('#top_search_input').blur(function(){
	        var q = $('#top_search_input').val();
	        if (q) {
	            if ($('.ui-autocomplete').html())
	                $('.ui-autocomplete').addClass('show');
	        }else{
	            $('.ui-autocomplete').removeClass('show');
	        }
	    });

	    $( '#top_search_input' ).autocomplete({
	        minLength: 0,
	        source: function (request, response) {
	        	$.get(base_url + 'search-texts', {q: request.term}, function(data, status){
		            if (data.success) {
		                $('.ui-autocomplete').removeClass('show');
	                    $('.ui-autocomplete').html('');
	                    var stexts = data.stexts;
	                    response(stexts.map(function (value) {
	                    	return {
		                        'id': value.id,
		                        'label': value.text
		                    };
	                    }));
		            }
		        });
	        },
	    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	        var left = $('.ui-widget-content').css('left');
	        $('.ui-widget-content').css('left', left + 30);
	        var cur_term = $('#top_search_input').val();
	        var new_val = '<span style="font-weight:bold">$1</span>';
	        var patt = new RegExp('(' + cur_term + ')', "gi");
	        return $( "<li z-index = \"10000000\"  style='border-bottom:solid 1px #f1f1f1;'>" )
	            .append( '<a>' + item.label.replace(patt, new_val) + '</a>' )
	            .appendTo( ul );
	    };

	});

</script>
