<?php $user = $this->session->userdata('user'); 
// var_dump($_SERVER['HTTP_USER_AGENT']);
?>
<!-- Navbar -->
<nav class="navbar navbar-default main-navbar">
  	<div class="container">
	    <div class="navbar-header">
	    	<a class="navbar-toggle"  data-toggle="collapse" data-target="#myNavbar">
	    		<img class="logo-mini" src="<?php echo base_url(); ?>assets/madinma/front/images/menu.png">
	    	</a>
	      	<a class="navbar-brand" href="<?php echo base_url(); ?>">
			    <img class="logo-mini" src="<?php echo base_url(); ?>assets/madinma/front/images/logo_small.png">
			    <img class="logo-lg" src="<?php echo base_url(); ?>assets/madinma/front/images/logo-1.png">
		    </a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
		    <ul class="nav navbar-nav navbar-right">
		    	<li class="dropdown dropdown-user img-item">
					<a href="javascript:;" class="dropdown-toggle img-item" data-toggle="dropdown">
					<img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo userLang(); ?>.png">
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<?php $lang_arr = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir'); ?>
						<?php foreach ( $lang_arr as $lang): ?>
							<li class="<?php if ($lang == userLang()): ?>disabled<?php endif; ?>">
								<a href="javascript:change_language('<?php echo $lang; ?>');">
									<img class="img-circle" src="<?php echo base_url(); ?>assets/madinma/front/images/flag_<?php echo $lang; ?>.png"> <?php echo lang('general.lang.'.$lang); ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</li>
		        <li class="img-item favorite-item">
		        	<a href="<?php echo base_url('favorites'); ?>">
		        		<span class="badge badge-success favorite-count"><?php echo favoriteCount(); ?></span>
		        		<?php if (!empty($cat_name) && $cat_name == 'favorites'): ?>
		        			<img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_on.png">
		        		<?php else: ?>
		        			<img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_off.png">
		        		<?php endif; ?>
		        	</a>
		        </li>

		        <?php if (!isset($user)): ?>
		        	<li class="img-item">
			        	<a href="<?php echo base_url('login'); ?>">
			        		<img class="login-img" src="<?php echo base_url(); ?>assets/madinma/front/images/button_login_small<?php echo (!empty($cur_page) && $cur_page == 'login' ? '_active' : ''); ?>.png"/>
			        	</a>
			        </li>
			        <li class="img-item register">
			        	<a href="<?php echo base_url('register'); ?>" class="<?php echo (!empty($cur_page) && $cur_page == 'register' ? 'active' : ''); ?>">
							<img class="register-img" src="<?php echo base_url(); ?>assets/madinma/front/images/button_register<?php echo (!empty($cur_page) && $cur_page == 'register' ? '_active' : ''); ?>.png"/>
							<span class="uppercase text-bold"><?=lang('header.register')?></span>
			        	</a>
			       	</li>
		    	<?php else: ?>
		    		<?php if ($user->type == 0): ?>
				    	<li class="img-item text-item <?php echo (!empty($cur_page) && $cur_page == 'account' ? 'active' : ''); ?>" class="img-item"><a href="<?php echo base_url('myaccount'); ?>"><i class="fa fa-cog"></i></a></li>
				    <?php else: ?>
				    	<li class="img-item text-item <?php echo (!empty($cur_page) && $cur_page == 'account' ? 'active' : ''); ?>" class="img-item"><a href="<?php echo base_url('admin/manufacturer'); ?>"><i class="fa fa-cog"></i></a></li>
			       	<?php endif; ?>
				    	<li class="img-item text-item"><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-sign-out"></i></a></li>
		    	<?php endif; ?>
		    </ul>
		    <ul class="categories visible-xs">
				<?php foreach ($categories as $category): ?>
				<li class="<?php if (!empty($cat_name) && $cat_name == $category->name_en || !empty($company) && $company->category_id == $category->id): ?>active<?php endif; ?> text-capitalize">
					<a class="visible-lg visible-md  visible-xs" href="<?php echo base_url('category/'.getSEOStr($category->name_en)); ?>" class="text-capitalize">
						<?php echo symbolSVG($category->symbol_url) . $category->{"name_".userLang()}; ?>
					</a>
					<a class="visible-sm" href="<?php echo base_url('category/'.getSEOStr($category->name_en)); ?>" class="text-capitalize">
						<?php echo symbolSVG($category->symbol_url); ?>
					</a>
				</li>
				<?php endforeach; ?>
			</ul>
	    </div>
	    <div class="site-desc pad-l100 visible-lg visible-md visible-sm"><?php echo lang('site.description'); ?></div>
	</div>
</nav>

<script>
	$('.login-img').on('mouseover', function() {
		$('.login-img').attr('src', "<?php echo base_url(); ?>assets/madinma/front/images/button_login_small_active.png")
	});
	$('.login-img').on('mouseout', function() {
		$('.login-img').attr('src', "<?php echo base_url(); ?>assets/madinma/front/images/button_login_small<?php echo (!empty($cur_page) && $cur_page == 'login' ? '_active' : ''); ?>.png")
	});
	$('li.register').on('mouseover', function() {
		$('.register-img').attr('src', "<?php echo base_url(); ?>assets/madinma/front/images/button_register_active.png")
	});
	$('li.register').on('mouseout', function() {
		$('.register-img').attr('src', "<?php echo base_url(); ?>assets/madinma/front/images/button_register<?php echo (!empty($cur_page) && $cur_page == 'register' ? '_active' : ''); ?>.png")
	});
</script>
