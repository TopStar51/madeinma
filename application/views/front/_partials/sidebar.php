<style type="text/css">
	.text-capitalize a:hover {
		color:red;
	}
</style>
<aside class="sidebar topspace30 pad-r30 visible-lg visible-md visible-sm">
	<div class="wowwidget">
		<h4 class="visible-md"><?php echo lang('sidebar.title'); ?></h4>
		<ul class="categories">
			<?php foreach ($categories as $category): ?>
			
			<li class="<?php if (!empty($cat_name) && $cat_name == $category->{'name_'.userLang()} || !empty($company) && $company->category_id == $category->id): ?>active<?php endif; ?>">
				<a class="visible-lg visible-md  visible-xs " href="<?php echo base_url('categories/'.getSEOStr($category->{'name_'.userLang()})); ?>">
					<?php echo symbolSVG($category->symbol_url) . $category->{"name_".userLang()}; ?>
				</a>
				<a class="visible-sm" href="<?php echo base_url('categories/'.getSEOStr($category->{'name_'.userLang()})); ?>">
					<?php echo symbolSVG($category->symbol_url); ?>
				</a>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
</aside>
