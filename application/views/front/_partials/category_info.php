<div class="row">
    <div class="col-xs-12 filter-panel">
        <div class="filter-result pull-left" style="padding-left:15px;">
			<?php echo ($this->session->userdata('category')); ?>
        </div>
        <div class="pull-right" style="padding-right:15px;">
            <button class="btn btn-bb-sm" onclick="location.href=document.referrer;"><i class="fa fa-reply"></i></button>
        </div>
    </div>
</div>

<div class="col-xs-12" style="padding:15px;">
	<div class="category-wrap list">
		<?php $category_id = $this->session->userdata('category_id');?>
		<div class="col-sm-6 com-md-6">
			<div class="category-detail">
				<p class="company-name text-span">
					<?php echo lang('categoryInfo.detail.'.$category_id)?>
				</p>
			</div>
		</div>
		<div class="category-img col-sm-6 com-md-6">
			<img src="<?php echo base_url().'assets/madinma/front/images/info/'.lang('categoryInfo.imgURL.'.$category_id); ?>"/>
		</div>
	</div>
</div>
