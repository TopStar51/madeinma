<!DOCTYPE html>
<html>

<?php $this->load->view('front/_base/head'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/plugins/jquery-tags-input/jquery.tagsinput.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/madinma/front/plugins/bootstrap-select/bootstrap-select.min.css'); ?>" />
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/madinma/front/plugins/jquery-tags-input/jquery.tagsinput.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/madinma/front/plugins/bootstrap-select/bootstrap-select.min.js'); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<style>
.progress-bar-mask{
    background: rgba(0,0,0,0.4);
    position: fixed;
    z-index: 1;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    display: -webkit-box;
    -webkit-box-align: center;
    display: -moz-box;
    -moz-box-align: center;
}
.my-progress-bar{
    width: 20vw;
    margin-left: 40vw;
    height: 5px;
    outline: 20px solid rgba(100,100,100,0.7);
    background: white;
}
.my-progress-bar div{
    width: 100%;
    height: 100%;
    background-color: rgba(250, 0, 0, 0.7);
    -webkit-animation-name: progress; /* Safari 4.0 - 8.0 */
    -webkit-animation-duration: 2s; /* Safari 4.0 - 8.0 */
    animation-name: progress;
    animation-duration: 2s;
    animation-iteration-count: infinite;
}

/* Safari 4.0 - 8.0 */
@-webkit-keyframes progress {
    from {width: 0px;}
    to {width: 100%;}
}

/* Standard syntax */
@keyframes progress {
    from {width: 0px;}
    to {width: 100%;}
}
</style>
<body>
    <div class="progress-bar-mask hidden">
        <div class="my-progress-bar">
            <div></div>
        </div>
    </div>

    <?php $this->load->view('front/_partials/header'); ?>

    <?php $langs = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir'); ?>

    <section class="pad-tb50">
        <div class="container">
            <div class="row profile-wrap">
                <div class="col-md-3">
                    <div class="row account-profile m-box">
                        <div class="profile-pic text-center">
                            <img src="<?php if (!empty($company_profile)) { echo avatarURL($company_profile->avatar_url, $company_profile->salutation); } else { echo avatarURL(''); } ?>" class="img-circle img-preview">
                        </div>
                        <div class="profile-details text-center">
                            <span class="profile-name"><?php echo empty($company_profile->contact_person) ? '' : $company_profile->contact_person; ?></span>
                            <a class="profile-phone"><?php echo empty($user_profile) ? '' : $user_profile->phone; ?></a>
                        </div>
                        <ul class="nav nav-pills nav-stacked account-sidebar">
                            <li class="m-separator"></li>
                            <li class="active">
                                <a href="#tab_company" data-toggle="tab"><i class="fa fa-id-card"></i> <?php echo humanize(lang('myaccount.company_profile')); ?></a>
                            </li>
                            <li>
                                <a href="#tab_messages" data-toggle="tab"><i class="fa fa-envelope"></i> <?php echo humanize(lang('myaccount.messages')); ?></a>
                            </li>
                            <li>
                                <a href="#tab_account" data-toggle="tab"><i class="fa fa-user"></i> <?php echo humanize(lang('myaccount.title')); ?></a>
                            </li>
                            <li>
                                <a href="#tab_support" data-toggle="tab"><i class="fa fa-question-circle"></i> <?php echo humanize(lang('myaccount.support')); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('logout'); ?>"><i class="fa fa-sign-out"></i> <?php echo humanize(lang('message.signout')); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 tab-content">
                    <div class="m-box row tab-pane fade active in" id="tab_company">
                        <div class="col-md-12 panel-box">
                            <h2><?php echo humanize(lang('myaccount.company_profile')); ?></h2>
                            <h4 class="underlined"><?php echo humanize(lang('myaccount.company_information')); ?></h4>
                            <form class="m-company-form">
                                <input name="user_id" type="hidden" value="<?php echo $user_id; ?>">
                                <?php
                                    $logo_url       = empty($company_profile) ? '' : $company_profile->logo_url;
                                    // $company_name   = empty($company_profile) ? '' : $company_profile->{'company_name_'.userLang()};
                                    $avatar_url     = empty($company_profile) ? '' : $company_profile->avatar_url;
                                    $user_lang      = empty($company_profile) ? '' : $company_profile->language;
                                    $salutation     = empty($company_profile) ? '' : $company_profile->salutation;
                                    $contact_person = empty($company_profile) ? '' : $company_profile->contact_person;
                                    $street         = empty($company_profile) ? '' : $company_profile->street;
                                    $city_id        = empty($company_profile) ? '' : $company_profile->city_id;
                                    $latitude       = empty($company_profile) ? '' : $company_profile->latitude;
                                    $longitude      = empty($company_profile) ? '' : $company_profile->longitude;
                                    $founded_at     = empty($company_profile) ? '' : $company_profile->founded_at;
                                    $company_email  = empty($company_profile) ? '' : $company_profile->email;
                                    $zipcode        = empty($company_profile) ? '' : $company_profile->zipcode;
                                    $website        = empty($company_profile) ? '' : $company_profile->website;
                                    $youtube        = empty($company_profile) ? '' : $company_profile->youtube;
                                    $youtube1        = empty($company_profile) ? '' : $company_profile->youtube1;
                                    $keywords       = empty($company_profile) ? '' : $company_profile->keywords;
                                    $employee_count = empty($company_profile) ? 0  : $company_profile->employee_count;
                                    $phone          = empty($company_profile) ? $user_profile->phone : empty($company_profile->phone)?$user_profile->phone:$company_profile->phone;
                                    $category_id    = empty($company_profile) ? '' : $company_profile->category_id;
                                ?>
                                <div class="row">
                                    <div class="col-sm-6 img-preview-div">
                                        <a href="javascript:;" style="position: relative;">
                                            <img id="preview_logo" src="<?php echo logoURL($logo_url); ?>" class="img-thumbnail" style="width: 250px; height: 180px;">
                                        </a>
                                        <span style="white-space: nowrap;"><?php echo humanize(lang('myaccount.logo')); ?></span>
                                        <input type="file" name="logo" accept="image/*" onchange="previewImage(this)" style="display:none;">
                                    </div>
                                    <div class="col-sm-6 img-preview-div">
                                        <a href="javascript:;" style="position: relative;">
                                            <img id="preview_avatar" src="<?php echo avatarURL($avatar_url, $salutation); ?>" class="img-thumbnail" style="width: 150px; height: 150px;">
                                        </a>
                                        <span style="white-space: nowrap;"><?php echo humanize(lang('myaccount.contact_avatar')); ?></span>
                                        <input type="file" name="avatar" accept="image/*" onchange="previewImage(this)" style="display:none;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.salutation')); ?> <span class="text-red">*</span></label>
                                            <select class="form-control" name="salutation">
                                                <option value="mr" <?php echo ($salutation == 'mr' ? 'selected' : ''); ?>><?php echo humanize(lang('company.salutation.placeholder.mr')); ?></option>
                                                <option value="ms" <?php echo ($salutation == 'ms' ? 'selected' : ''); ?>><?php echo humanize(lang('company.salutation.placeholder.ms')); ?></option>
                                            </select>
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.salutation.require'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.contact_person')); ?> <span class="text-red">*</span></label>
                                            <input name="contact_person" type="text" value="<?php echo $contact_person; ?>" placeholder="<?php echo humanize(lang('company.contact_person.placeholder')); ?>" required="" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.contact_person.require'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.street')); ?> <span class="text-red">*</span></label>
                                            <input name="street" type="text" value="<?php echo $street; ?>" placeholder="<?php echo humanize(lang('company.street.placeholder')); ?>" required="" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.street.require'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.city')); ?> <span class="text-red">*</span></label>
                                            <select name="city_id" class="form-control">
                                                <option value="" <?php if (empty($city_id)): ?>selected<?php endif; ?>><?php echo humanize(lang('myaccount.select')); ?> <?php echo humanize(lang('company.city')); ?></option>
                                                <?php foreach($cities as $city): ?>
                                                <option value="<?php echo $city->id; ?>" <?php if ($city->id == $city_id): ?>selected<?php endif; ?>><?php echo $city->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.city.require'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.latitude')); ?></label>
                                            <input name="latitude" type="number" step="0.000001" value="<?php echo $latitude; ?>" placeholder="<?php echo lang('company.latitude.placeholder'); ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.longitude')); ?></label>
                                            <input name="longitude" type="number" step="0.000001" value="<?php echo $longitude; ?>" placeholder="<?php echo lang('company.longitude.placeholder'); ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.zipcode')); ?></label>
                                            <input name="zipcode" type="number" value="<?php echo $zipcode; ?>" placeholder="<?php echo lang('company.zipcode.placeholder'); ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.email')); ?></label>
                                            <input name="email" type="email" value="<?php echo $company_email; ?>" placeholder="<?php echo lang('company.email.placeholder'); ?>" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.email.require'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.phone')); ?> <span class="text-red">*</span></label>
                                            <input name="phone" type="text" value="<?php echo $phone; ?>" placeholder="<?php echo lang('company.phone.placeholder'); ?>" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.phone.require'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.website')); ?></label>
                                            <input name="website" type="text" value="<?php echo $website; ?>" placeholder="<?php echo lang('company.website.placeholder'); ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.youtube')); ?></label>
                                            <input name="youtube" type="text" value="<?php echo $youtube; ?>" placeholder="<?php echo lang('company.youtube.placeholder'); ?>" class="form-control">
                                            <br>
                                            <input name="youtube1" type="text" value="<?php echo $youtube1; ?>" placeholder="<?php echo lang('company.youtube.placeholder'); ?>" class="form-control">

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group lang-sel">
                                            <label><?php echo humanize(lang('company.language')); ?> <span class="text-red">*</span></label>
                                            <select name="language" class="form-control" multiple>
                                                <?php foreach($langs as $key => $lang): ?>
                                                    <option value="<?php echo $lang; ?>" <?php if (!empty($user_lang) && in_array($lang, explode(',', $user_lang))): ?>selected<?php endif; ?>>
                                                        <?php echo lang('general.lang.'.$lang); ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.lang.require'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><?php echo humanize(lang('company.employee_count')); ?> <span class="text-red">*</span></label>
                                                    <?php $emp_count_arr = array('<5', '5+', '10+', '20+', '50+', '100+', '500+', '1000+'); ?>
                                                    <select name="employee_count" class="form-control">
                                                        <option value="" <?php if (empty($employee_count)): ?>selected<?php endif; ?>><?php echo humanize(lang('myaccount.select')); ?> <?php echo humanize(lang('company.employee_count')); ?></option>
                                                        <?php foreach($emp_count_arr as $key => $row): ?>
                                                        <option value="<?php echo $key+1; ?>" <?php if ($key+1 == $employee_count): ?>selected<?php endif; ?>><?php echo $row; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.employee_count.require'); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.founded_at')); ?> <span class="text-red">*</span></label>
                                            <input name="founded_at" type="number" value="<?php echo $founded_at; ?>" placeholder="<?php echo humanize(lang('company.founded_at.placeholder')); ?>" required="" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.founded_at.require'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><?php echo humanize(lang('company.category')); ?> <span class="text-red">*</span></label>
                                                    <select name="category_id" class="form-control">
                                                        <option value="" <?php if (empty($category_id)): ?>selected<?php endif; ?>><?php echo humanize(lang('myaccount.select')); ?> <?php echo humanize(lang('company.category')); ?></option>
                                                        <?php foreach($categories as $cat): ?>
                                                        <option value="<?php echo $cat->id; ?>" <?php if ($cat->id == $category_id): ?>selected<?php endif; ?>><?php echo $cat->{'name_'.userLang()}; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.category.require'); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.keywords')); ?></label>
                                            <input name="keywords" type="text" value="<?php echo $keywords; ?>" class="form-control tags">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.name')); ?>(<?php echo lang('general.lang.fr') ?>) <span class="text-red">*</span></label>
                                            <input name="company_name_fr" type="text" value="<?php echo empty($company_profile) ? '' : $company_profile->company_name_fr; ?>" placeholder="<?php echo lang('company.name.placeholder'); ?>(<?php echo lang('general.lang.fr') ?>)" required="" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.name.require'); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.name')); ?> (<?php echo lang('general.lang.ar') ?>)</label>
                                            <input name="company_name_ar" type="text" value="<?php echo empty($company_profile) ? '' : $company_profile->company_name_ar; ?>" placeholder="<?php echo lang('company.name.placeholder'); ?>(<?php echo lang('general.lang.ar') ?>)" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.name.require'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php foreach ($langs as $key => $lang): if ($lang != 'fr' && $lang != 'ar'): ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.name')); ?>(<?php echo lang('general.lang.'.$lang) ?>)</label>

                                            <input name="company_name_<?php echo $lang;?>" type="text" value="<?php echo empty($company_profile) ? '' : $company_profile->{'company_name_'.$lang}; ?>" placeholder="<?php echo lang('company.name.placeholder'); ?>(<?php echo lang('general.lang.'.$lang); ?>)" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.company.name.require'); ?></span>
                                        </div>
                                    </div>
                                    <?php endif; endforeach; ?>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.about_us')); ?> <span class="about_lg">(<?php echo lang('general.lang.fr') ?>)</span><span class="text-red">*</span></label>
                                            <textarea name="desc_fr" rows="5" placeholder="<?php echo lang('company.about_us.placeholder'); ?> (<?php echo lang('general.lang.fr') ?>)" required="" class="form-control"><?php echo empty($company_profile) ? '' : $company_profile->{"desc_fr"}; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.about_us')); ?> <span class="about_lg">(<?php echo lang('general.lang.ar') ?>)</span></label>
                                            <textarea name="desc_ar" rows="5" placeholder="<?php echo lang('company.about_us.placeholder'); ?> (<?php echo lang('general.lang.ar') ?>)" class="form-control"><?php echo empty($company_profile) ? '' : $company_profile->{"desc_ar"}; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php foreach ($langs as $key => $lang): if ($lang != 'fr' && $lang != 'ar'): ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('company.about_us')); ?> <span class="about_lg">(<?php echo lang('general.lang.'.$lang) ?>)</span></label>
                                            <textarea name="desc_<?php echo $lang; ?>" rows="5" placeholder="<?php echo lang('company.about_us.placeholder'); ?> (<?php echo lang('general.lang.'.$lang) ?>)" class="form-control"><?php echo empty($company_profile) ? '' : $company_profile->{"desc_$lang"}; ?></textarea>
                                        </div>
                                    </div>
                                    <?php endif; endforeach; ?>
                                </div>
                                <div class="row topspace30">
                                    <div class="col-md-12">
                                        <h4 class="underlined"><?php echo humanize(lang('myaccount.company_product')); ?></h4>
                                    </div>
                                </div>
                                <input type="text" class="hidden" name="products" 
                                        value="<?php echo (empty($product_ids) ? '' : implode(',', $product_ids)); ?>">
                            </form>
                                <div class="row">
                                <div data-force="30" class="layer block">
                                    <ul id="foo" class="block__list block__list_words">
                                        <?php foreach ($company_products as $product): ?>
                                            <li data-id="<?php echo $product->id; ?>"><div class= "hidden"><?php echo $product->img_url; ?></div>
                                            <div class="col-xs-12 col-md-6 col-md-4 product-item " >
                                                        <div class="product-wrap" >
                                                            <div class="figure-wrap-only bg-image" data-image-src = "<?php echo productURL($product->img_url); ?>">
                                                                <button type="button" class="btn del-product" data-id="<?php echo $product->img_url; ?>"><span aria-hidden="true">×</span></button>
                                                            </div>
                                                        </div>
                                                    </div>  
                                            </li>                                           
                                        <?php endforeach; ?>
                                        </ul>
                                    </div>

                                    <div class="col-xs-12 col-md-6 col-md-4 product-item">
                                        <div class="preview-wrap">
                                            <div class="figure-wrap-only bg-image" data-image-src="<?php echo base_url(); ?>assets/madinma/front/images/preview.png">
                                                <img src="<?php echo base_url();?>assets/madinma/front/images/add_img.png" class="add_img_icon">
                                            </div>
                                            <form class="product-form">
                                                <input type="file" class="hidden" accept="image/*">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                             <div class="row topspace15">
                                <div class="col-md-12">
                                    <button onclick="javascript:submit_company_profile();" class="btn btn-bb">
                                        <i class="fa fa-paper-plane"></i> <?php echo humanize(lang('message.save')); ?>
                                    </button>
                                    <button onclick="javascript:preview_profile();" class="btn btn-bb">
                                        <i class="fa fa-eye"></i> <?php echo humanize(lang('myaccount.preview')); ?>
                                    </button>
                                </div>
                            </div>
                        </div><!--col-12-->
                    </div>
                    <div class="m-box row tab-pane fade" id="tab_messages">
                        <div class="col-md-12 panel-box">
                            <h2><?php echo lang('myaccount.messages'); ?></h2>
                            <table id="m_table" class="table table-bordered table-hover"></table>
                        </div>
                    </div>
                    <div class="m-box row tab-pane fade" id="tab_account">
                        <div class="col-md-12 panel-box">
                            <h2><?php echo humanize(lang('myaccount.title')); ?></h2>
                            <h4 class="underlined"><?php echo humanize(lang('myaccount.general_information')); ?></h4>
                            <form class="m-user-form">
                                <input name="user_id" type="hidden" value="<?php echo $user_id; ?>">
                                <?php
                                    $user_phone = empty($user_profile) ? '' : $user_profile->phone;
                                ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('user.phone')); ?> <span class="text-red">*</span></label>
                                            <input name="phone" type="text" value="<?php echo $user_phone; ?>" placeholder="<?php echo humanize(lang('user.phone')); ?>" required="" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.phone.require'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row topspace30">
                                    <div class="col-md-12">
                                        <h4 class="underlined"><?php echo humanize(lang('myaccount.change_password')); ?></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('myaccount.current_password')); ?></label>
                                            <input name="password" type="password" placeholder="<?php echo humanize(lang('myaccount.current_password')); ?>" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.current_password.require'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <?php $logined_user = $this->session->userdata('user'); ?>
                                <?php if ($logined_user->type == 0): ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('myaccount.new_password')); ?></label>
                                            <input name="new_password" type="password" placeholder="<?php echo humanize(lang('myaccount.new_password')); ?>" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.new_password.require'); ?></span>
                                        </div>
                                    </div>
                                 </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo humanize(lang('user.confirm_password')); ?></label>
                                            <input name="confirm_password" type="password" placeholder="<?php echo humanize(lang('user.confirm_password')); ?>" class="form-control">
                                            <span class="help-block-error text-danger hidden"><?php echo lang('validation.user.confirm_password.valid'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </form>
                            <div class="row">
                                <div class="col-md-12">
                                    <button onclick="javascript:submit_user_profile();" class="btn btn-bb">
                                        <i class="fa fa-paper-plane"></i> <?php echo humanize(lang('message.save')); ?>
                                    </button>
                                </div>
                            </div>
                            <div class="row topspace30">
                                <div class="col-md-12">
                                    <h4 class="underlined"><?php echo humanize(lang('myaccount.promotion')); ?></h4>
                                </div>
                            </div>
                            <div>
                                <label style="margin-bottom: 15px;"><?=lang('myaccount.promotion.desc')?></label>
                                <div class="row">
                                    <label class="col-md-3" style="float: none;"><?=lang('myaccount.promotion.promotion_code')?>:</label>
                                    <input class="form-control" name="promotion_code" value="<?=$user_profile->promotion_code?>" style="text-align: center; width: 100px; display: inline-block;" readonly>
                                    <input type="text" style="display: none;" value="<?=$user_profile->promotion_code?>" id="promotion_code">
                                    <button onclick="copy_to_clipboard('promotion_code')" class="btn"><i class="glyphicon glyphicon-copy"></i></button>
                                </div>
                                <div class="row">
                                    <label class="col-md-3" style="float: none;"><?=lang('myaccount.promotion.promotion_link')?>:</label>
                                    <label class="col-md-6 col-sm-12"><?= base_url('register?code='.$user_profile->promotion_code) ?></label>
                                    <input type="text" style="display: none;" value="<?= base_url('register?code='.$user_profile->promotion_code) ?>" id="invite_link">
                                    <button class="btn" style="margin-left: 10px;"><i class="glyphicon glyphicon-share"></i></button>
                                    <button onclick="copy_to_clipboard('invite_link')" class="btn"><i class="glyphicon glyphicon-copy"></i></button>
                                </div>
                                <div class="row">
                                    <label class="col-md-3" style="float: none;"><?=lang('myaccount.promotion.current_promotions')?>:</label>
                                    <input class="form-control" value="<?=$user_profile->promotion_count?>" name="promotion_count" style="text-align: center; width: 100px; display: inline-block;" readonly>
                                    <button class="btn"><i class="glyphicon glyphicon-pay"></i>Payout</button>
                                </div>
                            </div>
                            <div class="row topspace30">
                                <div class="col-md-12">
                                    <h4 class="underlined"><?php echo humanize(lang('myaccount.del_account')); ?></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button onclick="javascript:delete_account();" class="btn btn-bb btn-red">
                                        <i class="fa fa-trash"></i> <?php echo humanize(lang('myaccount.del_account')); ?>
                                    </button>
                                </div>
                            </div>
                        </div><!--col-12-->
                    </div>
                    <div class="m-box row tab-pane fade" id="tab_support">
                        <div class="col-md-12 panel-box">
                            <h2><?php echo lang('myaccount.support'); ?></h2>
                            <div class="row">
                                <div class="col-sm-12">
                                    <form>
                                        <?php
                                            $company_id = empty($company_profile) ? 0 : $company_profile->id;
                                            $company_name_fr = empty($company_profile) ? '' : $company_profile->company_name_fr;
                                            $user_phone = empty($user_profile) ? '' : $user_profile->phone;
                                        ?>
                                        <input type="hidden" name="company_id" value="<?=$company_id?>" />
                                        <input type="hidden" name="company_email" value="<?=$company_email?>" />
                                        <input type="hidden" name="company_name_fr" value="<?=$company_name_fr?>" />
                                        <input type="hidden" name="city_id" value="<?=$city_id?>" />
                                        <input type="hidden" name="phone" value="<?=$user_phone?>" />

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="<?php echo humanize(lang('modal.placeholder.name')); ?>">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="10" name="content" placeholder="<?php echo humanize(lang('modal.placeholder.text')); ?>"></textarea>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-bb btn-contact"><i class="fa fa-paper-plane"></i> <?php echo lang('modal.btn.send'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--col-9-->
            </div>
        </div><!--container-->
    </section>
<script>
    var default_avatar = "<?php echo base_url('assets/madinma/front/images/default_avatar.jpg'); ?>";
    var default_avatar_ms = "<?php echo base_url('assets/madinma/front/images/default_wavatar.jpg'); ?>";
    var default_logo = "<?php echo base_url();?>assets/madinma/front/images/company_avatar.png";
    var default_preview = "<?php echo base_url();?>assets/madinma/front/images/preview.png";
    var confirm_pwd = <?php $logined_user->type; ?>

    $('#preview_avatar').click(function (argument) {    
        $("input[name=avatar]").click();
    });

    $('#preview_logo').click(function (argument) {
        $("input[name=logo]").click();
    })

    $('[name="salutation"]').change(function() {
        if (!($("input[name=avatar]")[0].files && $("input[name=avatar]")[0].files[0])) {
            if ($(this).val() == 'ms') {
                $('.profile-pic img, #preview_avatar').attr('src', default_avatar_ms);
            } else {
                $('.profile-pic img, #preview_avatar').attr('src', default_avatar);
            }
        }
    });
    
    function previewImage(input) {
        var name = $(input).attr('name');
        if(name == "logo")
        {
            to_reset_logo = false;
            $("#preview_logo").parent().append("<button type='button' class='btn del-product' style='position: absolute;right:5px;top:5px;' onclick='remove_preview_logo(this)'><span aria-hidden='true'>×</span></button>");
        }
		if(name == "avatar")
        {
            to_reset_avatar = false;
            $("#preview_avatar").parent().append("<button type='button' class='btn del-product' style='position: absolute;right:5px;top:5px;' onclick='remove_preview_avatar(this)'><span aria-hidden='true'>×</span></button>");
        }
        var preview = document.getElementById('preview_' + name);
        var profile_img = $('.profile-pic img')[0];
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                preview.setAttribute('src', e.target.result);
                if (name == 'avatar') {
                    profile_img.setAttribute('src', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            var salutation = $('[name="salutation"]').val();
            preview.setAttribute('src', name == 'avatar' ? (salutation == 'ms' ? default_avatar_ms : default_avatar) : default_logo);
            if (name == 'avatar') {
                profile_img.setAttribute('src', (salutation == 'ms' ? default_avatar_ms : default_avatar));
            }
        }
    }

    $('.preview-wrap .bg-image').click(function() {
        $(this).parents('.preview-wrap').find('input[type=file]').click();
    });

    $('.preview-wrap input').change(function() {
        var input = $(this)[0];
        if (!(input.files && input.files[0])) {
            return;
        }

        var formData = new FormData();
        formData.append('product', this.files[0]);
        $.ajax({
            'beforeSend': function () {
                $(".progress-bar-mask").addClass('hidden').removeClass('hidden');
            },
            // 'complete' : function () {
            //     $(".progress-bar-mask").removeClass('hidden').addClass('hidden');
            // },
            'url' : base_url + 'product/add',
            'type' : 'POST',
            'data' : formData,
            'async' : false,
            'cache' : false,
            'contentType': false,
            'processData' : false,
            'dataType' : 'json',
            'success' : function(response) {

                if(response.success == true) {
                    var product_url = base_url + 'upload/products/' + response.url;
                    $('.preview-wrap').parent('.product-item').before('<div class="col-xs-12 col-md-6 col-md-4 product-item">\
                                                                            <div class="product-wrap">\
                                                                                <div class="figure-wrap-only bg-image" data-image-src="' + product_url + '">\
                                                                                    <button type="button" class="btn del-product" data-id=' + response.url + '><span aria-hidden="true">×</span></button>\
                                                                                </div>\
                                                                            </div>\
                                                                        </div>');
                    $(".bg-image").css("background", function() {
                        var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
                        return a
                    });

                    var product_ids = $('input[name="products"]').val() == '' ? [] : $('input[name="products"]').val().split(',');
                    product_ids.push(response.url);
                    
                    $('input[name="products"]').val(product_ids.toString());
                    
                    init_del_btns();

                    // refresh scroll
                    $("html").niceScroll().remove();
                    $("html").niceScroll({
                        scrollspeed: 60,
                        mousescrollstep: 40,
                        cursorwidth: 8,
                        cursorheight: 130,
                        cursorborder: 0,
                        cursorcolor: '#D20023',
                        cursorborderradius: 5,
                        styler:"fb",
                        autohidemode: false,
                        horizrailenabled: false
                    });
                    setTimeout(function(){
                        $(".progress-bar-mask").removeClass('hidden').addClass('hidden');}, 1500);
                }
                
            },
			'error' : function(){
				$(".progress-bar-mask").removeClass('hidden').addClass('hidden');
				bootbox.dialog({
					title: message_error,
					message: response.message,
					size: 'small',
					buttons: {
						ok: {
							label: message_ok,
							className: 'btn-danger'
						}
					}
				});
			}
        });
    });

    function init_del_btns() {
        $('.del-product').unbind('click');
        $('.del-product').click(function() {
            var product_ids = $('input[name="products"]').val() == '' ? [] : $('input[name="products"]').val().split(',');
            var del_id = $(this).attr('data-id');
            var del_index = product_ids.indexOf(del_id);
            var product_item = $(this).parents('.product-item');
            product_ids.splice(product_ids.indexOf(del_id), 1);
            $('input[name="products"]').val(product_ids.toString());
            $('.product-form')[0].reset();
            product_item.remove();
        });
    }


    function validate_comapny() {
        var isValid = true;
        var fields = ['company_name', 'contact_person', 'street', 'city_id', 'founded_at', 'employee_count', 'phone', 'language', 'category_id', 'company_name_fr'];
        $('.m-company-form .form-group').removeClass('has-error');
        for (i = 0; i < fields.length; i++) {
            if ($('.m-company-form').find('[name="' + fields[i] + '"]').val() == '') {
                isValid = false;
                $('.m-company-form').find('[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').removeClass('hidden');
                $('.m-company-form').find('[name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
            } else {
                $('.m-company-form').find('[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').addClass('hidden');
                $('.m-company-form').find('[name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
            }
        }
        var product_ids = $('input[name="products"]').val() == '' ? [] : $('input[name="products"]').val().split(',');
        if (product_ids.length == 0) {
            var dialog = bootbox.dialog({
                title: message_error,
                message: "<?php echo lang('myaccount.product_warning'); ?>",
                size: 'small',
                buttons: {
                    ok: {
                        label: message_ok,
                        className: 'btn-danger'
                    }
                }
            });
            isValid = false;
        }

        return isValid;
    }

    function validate_user() {
        var isValid = true;
        var fields = ['name', 'email', 'password', 'confirm_password'];
        if (confirm_pwd == 1) {
            fields = ['name', 'email'];
        }
        $('.m-user-form .form-group').removeClass('has-error');
        for (i = 0; i < fields.length; i++) {
            if ((fields[i] == 'password' && $('input[name="password"]').val() == '' && ($('input[name="confirm_password"]').val() != '' || $('input[name="new_password"]').val())) ||
                (fields[i] == 'confirm_password' && $('input[name="confirm_password"]').val() != $('input[name="new_password"]').val()) ||
                (!(fields[i] == 'password' || fields[i] == 'confirm_password') && $('.m-user-form').find('[name="' + fields[i] + '"]').val() == '')) {
                isValid = false;
                $('.m-user-form').find('input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').removeClass('hidden');
                $('.m-user-form').find('input[name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
            } else {
                $('.m-user-form').find('input[name="' + fields[i] + '"]').parents('.form-group').find('.help-block-error').addClass('hidden');
                $('.m-user-form').find('input[name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
            }
        }
        return isValid;
    }

    function submit_company_profile() {
        if (validate_comapny() === true) {
            var formData = new FormData();

            var form = $(".m-company-form")[0];
            for (i = 0; i < form.length; i++) {
                if (form[i].name == 'avatar') {
                    if ($('input[name=avatar]')[0].files[0]) {
                        formData.append('avatar', $('input[name=avatar]')[0].files[0]);
                    } else if ($('#preview_avatar').attr('src') == default_avatar || $('input[name=avatar]').attr('src') == default_avatar_ms) {
                        formData.append('avatar_url', '');
                    }
                } else if (form[i].name == 'language') {
                    if ($(form[i]).selectpicker('val') == null) {
                        formData.append(form[i].name,  '');
                    } else {
                        formData.append(form[i].name, $(form[i]).selectpicker('val'));
                    }
                } else if (form[i].name == 'logo') {
                    if ($('input[name=logo]')[0].files[0]) {
                        formData.append('logo', $('input[name=logo]')[0].files[0]);
                    } else if ($('#preview_logo').attr('src') == default_logo) {
                        formData.append('logo_url', '');
                    }
                } else if (form[i].name != '') {
                    formData.append(form[i].name, form[i].value);
                }
            }
            $(".progress-bar-mask").removeClass("hidden");
            formData.append("reset_logo",to_reset_logo);
            $.ajax({
                'url' : base_url + 'company/post',
                'type' : 'POST',
                'data' : formData,
                'async' : false,
                'cache' : false,
                'contentType': false,
                'processData' : false,
                'dataType' : 'json',
                'success' : function(response) {
                    $(".progress-bar-mask").addClass("hidden");
                    if (response.success) {
                        bootbox.dialog({
                            title: message_success,
                            message: response.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-success'
                                }
                            }
                        });
                    } else {
                        bootbox.dialog({
                            title: message_error,
                            message: response.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-danger'
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    function submit_user_profile() {
        if (validate_user() === true) {
            var formData = new FormData();

            var form = $(".m-user-form")[0];
            for (i = 0; i < form.length; i++) {
                if (form[i].name != '') {
                    formData.append(form[i].name, form[i].value);
                }
            }

            $.ajax({
                'url' : base_url + 'profile/post',
                'type' : 'POST',
                'data' : formData,
                'async' : false,
                'cache' : false,
                'contentType': false,
                'processData' : false,
                'dataType' : 'json',
                'success' : function(response) {
                    if (response.success) {
                        bootbox.dialog({
                            title: message_success,
                            message: response.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-success'
                                }
                            }
                        });
                    } else {
                        var dialog = bootbox.dialog({
                            title: message_error,
                            message: response.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-danger'
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    var to_reset_logo = false;
    $(document).ready(function () {
        if($("#preview_logo").attr("src") && $("#preview_logo").attr("src").indexOf("company_avatar") < 0)
        {
            $("#preview_logo").parent().append("<button type='button' class='btn del-product' style='position: absolute;right:5px;top:5px;' onclick='remove_preview_logo(this)'><span aria-hidden='true'>×</span></button>");
        }

		if($("#preview_avatar").attr("src") && $("#preview_avatar").attr("src").indexOf("company_avatar") < 0)
        {
            $("#preview_avatar").parent().append("<button type='button' class='btn del-product' style='position: absolute;right:5px;top:5px;' onclick='remove_preview_avatar(this)'><span aria-hidden='true'>×</span></button>");
        }

        $('input[name="keywords"]').tagsInput({
            width: 'auto',
            defaultText:'<?php echo lang("company.keywords.placeholder"); ?>',
        });

        // Can input only 9 or 10 digits
        $('input[name="phone"]').inputmask("+9{11,12}");
        
        $('select[name="language"]').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check',
            dropdownAlignRight: rtl,
            noneSelectedText: '<?php echo humanize(lang("company.lang.non.selected.placeholder")); ?>'
        });

        $(".bg-image").css("background", function() {
            var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
            return a
        });

        init_del_btns();

    });

    function remove_preview_logo(btn)
    {
        $(btn).remove();
        $("#preview_logo").attr("src",default_logo);
        to_reset_logo = true;
    }
	
    function remove_preview_avatar(btn)
    {
        $(btn).remove();
        $("#preview_avatar").attr("src",default_avatar);
        to_reset_avatar = true;
    }

    function preview_profile(e) {
        var formData = new FormData();

        var form = $(".m-company-form")[0];
        for (i = 0; i < form.length; i++) {
            if (form[i].name == 'avatar') {
                if ($('input[name=avatar]')[0].files[0]) {
                    formData.append('avatar', $('input[name=avatar]')[0].files[0]);
                } else if ($('#preview_avatar').attr('src') == default_avatar || $('input[name=avatar]').attr('src') == default_avatar_ms) {
                    formData.append('avatar_url', '');
                }
            } else if (form[i].name == 'language') {
                if ($(form[i]).selectpicker('val') == null) {
                    formData.append(form[i].name,  '');
                } else {
                    formData.append(form[i].name, $(form[i]).selectpicker('val'));
                }
            } else if (form[i].name == 'logo') {
                if ($('input[name=logo]')[0].files[0]) {
                    formData.append('logo', $('input[name=logo]')[0].files[0]);
                } else if ($('#preview_logo').attr('src') == default_logo) {
                    formData.append('logo_url', '');
                }
            } else if (form[i].name != '') {
                formData.append(form[i].name, form[i].value);
            }
        }

        $.ajax({
            'url' : base_url + 'preview/post',
            'type' : 'POST',
            'data' : formData,
            'async' : false,
            'cache' : false,
            'contentType': false,
            'processData' : false,
            'dataType' : 'json',
            'success' : function(response) {
                if (response.success) {
                    window.open(base_url + 'preview');
                }
            }
        });
    }

    function delete_account() {
        var dialog = bootbox.dialog({
            title: "<?php echo lang('message.confirmation'); ?>",
            message: "<h4><?php echo lang('myaccount.del_account_msg'); ?></h4>",
            size: 'small',
            buttons: {
                cancel: {
                    label: "<?php echo lang('message.cancel'); ?>",
                    className: 'btn-danger',
                    callback: function(){
                        dialog.modal('hide');
                    }
                },
                ok: {
                    label: "<?php echo lang('message.ok'); ?>",
                    className: 'btn-success',
                    callback: function() {
                        $.get(base_url + 'delete-account', function(data, status) {
                            if (data.success) {
                                location.href = base_url;
                            }
                        });
                    }
                }
            }
        });
    }

    var oTable;

    $(function () {

        var handleTable = function () {

            var table = $('#m_table');

            oTable = table.dataTable({
                "bServerSide": true,
                "bProcessing": true,
                "bDeferRender": true,
                "bAutoWidth": false,
                "aoColumns": [
                  {
                    "sTitle" : "<?php echo humanize(lang('message.label.no'));?>",
                    "mData": "",
                    mRender: function (data, type, row, pos) {
                      return Number(pos.row)+1;
                    },
                    "sWidth" : 30
                  },
                  {
                    "sTitle" : "<?php echo humanize(lang('message.label.name'));?>",
                    "mData": "name",
                    "sWidth" : 120
                  },
                  {
                    "sTitle" : "<?php echo humanize(lang('message.label.email'));?>",
                    "mData": "email",
                    "sWidth" : 150
                  },
                  {
                    "sTitle" : "<?php echo humanize(lang('message.label.phone'));?>",
                    "mData": "phone",
                    "sWidth" : 120
                  },
                  {
                    "sTitle" : "<?php echo humanize(lang('message.label.country'));?>",
                    "mData": "country_name",
                    "sWidth" : 80
                  },
                  {
                    "sTitle" : "<?php echo humanize(lang('message.label.language'));?>",
                    "mData": "language",
                    mRender: function (data, type, row, pos) {
                      var langs = [];
                      if (data != '' && data != null) {
                        langs = data.split(',');
                      }
                      var html = '';
                      for (i =0 ; i < langs.length; i++ ) {
                        html += '<img src="' + base_url + 'assets/madinma/front/images/flag_' + langs[i] + '.png" class="img-circle" width="25" height="25" style="margin: 0 2px;"/>';
                      }
                      return html;
                    },
                    "sWidth" : 80
                  },
                  {
                    "sTitle" : "<?php echo humanize(lang('message.label.content'));?>",
                    "mData": "content",
                    mRender: function (data, type, row, pos) {
                      return '<textarea rows="3" style="width: 100%; border: none;" readonly>' + data + '</textarea>';
                    },
                    "sWidth" : 300
                  },
                  {
                    "sTitle" : "<?php echo humanize(lang('message.label.created_at'));?>",
                    "mData": "created_at",
                    "sWidth" : 100
                  },
                ],
                "sAjaxSource": base_url + 'home/messages_read_ajax',
                "sAjaxDataProp": "data",
                "scrollX": true,
                "lengthMenu": [
                    [10, 20, 50, -1],
                    [10, 20, 50, "ALL"] // change per page values here
                ],
                "pageLength": 10, // set the initial value,
                columnDefs: [{
                    orderable: true,
                    targets: [1]
                }],
                "order": [
                    [7, "desc"]
                ]
            });

            $(window).resize(function() {
                oTable.fnAdjustColumnSizing();
            });

            $('[href="#tab_messages"]').on('shown.bs.tab', function(event){
                oTable.fnAdjustColumnSizing();
            });

        }();

        $('.select-picker').selectpicker({
            width: '100%',
            height: 32,
            dropdownAlignRight: rtl
        });

        $('.btn-contact').click(function() {
            var isValid = true;
            var fields = ['phone', 'name', 'content'];
            $('#tab_support .form-group').removeClass('has-error');
            for (i = 0; i < fields.length; i++) {
                if ($('#tab_support [name="' + fields[i] + '"]').val() == '') {
                    isValid = false;
                    $('#tab_support [name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
                } else {
                    $('#tab_support [name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
                }
            }

            if (isValid == true) {
                var params = {
                    company_id: $('#tab_support [name="company_id"]').val(),
                    company_name_fr: $('#tab_support [name="company_name_fr"]').val(),
                    company_email: $('#tab_support [name="company_email"]').val(),
                    city_id: $('#tab_support [name="city_id"]').val(),
                    phone: $('#tab_support [name="phone"]').val(),
                    name: $('#tab_support [name="name"]').val(),
                    content: $('#tab_support [name="content"]').val()

                };
                $.post(base_url + 'support/post', params, function(data, status){
                    if (data.success) {
                        bootbox.dialog({
                            title: message_success,
                            message: message_success,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-success'
                                }
                            }
                        });
                    } else {
                        bootbox.dialog({
                            title: message_error,
                            message: data.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-danger'
                                }
                            }
                        });
                    }
                });
            }
        });

    });

    function copy_to_clipboard(element) {
        $('#' + element).show();
        $('#' + element).select();
        document.execCommand('copy');
        $('#' + element).hide();
    }

</script>

<script src="<?php echo base_url('assets/Sortable/Sortable.js'); ?>"></script>
<script src="<?php echo base_url('assets/Sortable/Sortable_list.js'); ?>"></script>
</body>

</html>
