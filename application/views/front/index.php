<style>
    .owl-nav .owl-prev,
    .owl-nav .owl-next {
        position: absolute;
        top: 45%;
        transform: translate(0, -45%);
    }
    .owl-nav .owl-prev {
        left: -45px;
    }
    .owl-nav .owl-next {
        right: -45px;
    }
    .owl-theme .owl-nav [class*=owl-]:hover {
        background: #D20023;
    }
    .owl-theme .owl-nav [class*=owl-] {
        background: #00652D;
    }
    .product-wrap {
        margin-bottom: 0;
    }
    
    .company-profile a:hover {
        color: #D20023 !important;
    }
</style>

<div class="row" style="padding: 0 40px;">

<?php $this->load->view('front/_partials/searchbar'); ?>
<?php if (!empty($week_companies)): ?>
<h4><?php echo lang('startpage.week'); ?></h4>
<div class="owl-carousel owl-theme bottomspace30 first">
    <?php foreach ($week_companies as $key => $company): ?>
    <?php
        if(!isset($company->{'company_name_'.userLang()}) || $company->{'company_name_'.userLang()}=="")
            $company_name = $company->{'company_name_fr'};
        else
            $company_name = $company->{'company_name_'.userLang()};
    ?>
        <div class="item product-wrap">
            <div class="figure-wrap">
                <div class="pointer-div"><a href="<?php echo base_url('categories/'.getSEOStr($company->{'name_'.userLang()}).'/'.getSEOStr($company_name)); ?>"></a></div>
                <div class="favorite">
                    <a class="favorite-link" href="javascript:;" data-favorite="<?php echo isFavorite($company->id); ?>" data-company="<?php echo $company->id; ?>">
                        <?php if (isFavorite($company->id)): ?>
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_on.png"/>
                        <?php else: ?>
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_off.png"/>
                        <?php endif; ?> 
                    </a>
                </div>
                <div id="myCarousel<?php echo $key; ?>" class="figure-wrap carousel slide carousel-v1">
                    <div class="carousel-inner">
                        <?php if (empty($company->products)): ?>
                            <div class="figure-wrap bg-image item active" data-image-src="<?php echo base_url(); ?>assets/madinma/front/images/preview.png">
                            </div>
                        <?php endif; ?>
                        <?php foreach ($company->products as $idx => $product): ?>
                        <div class="figure-wrap bg-image item <?php echo ($idx==0 ? 'active' : ''); ?>" data-image-src="<?php echo productThumbURL($product->img_url); ?>">
                            <div class="carousel-caption">
                                <p><?php echo ($idx+1).'/'.count($company->products); ?></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php if (!empty($company->products)): ?>
                    <div class="carousel-arrow">
                        <a class="left carousel-control" href="#myCarousel<?php echo $key; ?>" data-slide="prev">
                            <i class="fa fa-angle-left fade"></i>
                        </a>
                        <a class="right carousel-control" href="#myCarousel<?php echo $key; ?>" data-slide="next">
                            <i class="fa fa-angle-right fade"></i>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="company-profile">
                <a href="<?php echo base_url('categories/'.getSEOStr($company->{'name_'.userLang()}).'/'.getSEOStr($company_name)); ?>" style="display: flex;">
                    <div class="profile-item">
                        <?php echo symbolSVG($company->symbol_url); ?>
                        <span class="company-name text-span"><?php echo $company_name; ?></span>
                    </div>
                </a>
                <div class="profile-item">
                    <img src="<?php echo base_url(); ?>assets/madinma/front/images/location.svg"/>
                    <span class="company-location text-span"><a href="javascript:;" class="city_link" data-city="<?php echo $company->city_name; ?>"><?php echo $company->city_name; ?></a><?php echo ', ' . $company->street; ?></span>
                </div>
                <div class="profile-item">
                    <img src="<?php echo base_url(); ?>assets/madinma/front/images/analytics.svg"/>
                    <span class="company-info text-span"><?php echo lang('profile.since'); ?> <?php echo $company->founded_at; ?>, <?php echo employeeCountStr($company->employee_count); ?> <?php echo lang('profile.employees'); ?></span>
                </div>
                <div class="profile-item">
                    <img src="<?php echo base_url(); ?>assets/madinma/front/images/rating.svg"/>
                    <?php $avg_rating = getCompanyRating($company->id); ?>
                    <?php if(getReviewCount($company->id) > 0) { ?>
                    <span class="company-rating"><?php echo getReviewCount($company->id); ?></span>
                    <?php } ?>
                    <?php if($avg_rating == 0) { ?>
                    <span class="company-rating"><?php echo lang('no.ratings'); ?></span>
                    <?php  } else { ?>
                    <div class="rating-stars">
                        <?php for ($i = 0; $i < 5; $i++): ?>
                            <?php if ($i < $avg_rating): ?>
                            <i class="fa fa-star"></i>
                            <?php else: ?>
                            <i class="fa fa-star-o"></i>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<?php if (!empty($new_companies)): ?>
<h4><?php echo lang('startpage.new'); ?></h4>
<div class="owl-carousel owl-theme bottomspace30">
    <?php foreach ($new_companies as $key => $company): ?>
    <?php
        if(!isset($company->{'company_name_'.userLang()}) || $company->{'company_name_'.userLang()}=="")
            $company_name = $company->{'company_name_fr'};
        else
            $company_name = $company->{'company_name_'.userLang()};
    ?>
        <div class="item product-wrap">
            <div class="figure-wrap">
                <div class="pointer-div"><a href="<?php echo site_url('categories/'.getSEOStr($company->{'name_'.userLang()}).'/'.getSEOStr($company_name)); ?>"></a></div>
                <div class="favorite">
                    <a class="favorite-link" href="javascript:;" data-favorite="<?php echo isFavorite($company->id); ?>" data-company="<?php echo $company->id; ?>">
                        <?php if (isFavorite($company->id)): ?>
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_on.png"/>
                        <?php else: ?>
                        <img src="<?php echo base_url(); ?>assets/madinma/front/images/heart_off.png"/>
                        <?php endif; ?> 
                    </a>
                </div>
                <div id="myCarousel2<?php echo $key; ?>" class="figure-wrap carousel slide carousel-v1">
                    <div class="carousel-inner">
                        <?php if (empty($company->products)): ?>
                            <div class="figure-wrap bg-image item active" data-image-src="<?php echo base_url(); ?>assets/madinma/front/images/preview.png">
                            </div>
                        <?php endif; ?>
                        <?php foreach ($company->products as $idx => $product): ?>
                        <div class="figure-wrap bg-image item <?php echo ($idx==0 ? 'active' : ''); ?>" data-image-src="<?php echo productThumbURL($product->img_url); ?>">
                            <div class="carousel-caption">
                                <p><?php echo ($idx+1).'/'.count($company->products); ?></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php if (!empty($company->products)): ?>
                    <div class="carousel-arrow">
                        <a class="left carousel-control" href="#myCarousel2<?php echo $key; ?>" data-slide="prev">
                            <i class="fa fa-angle-left fade"></i>
                        </a>
                        <a class="right carousel-control" href="#myCarousel2<?php echo $key; ?>" data-slide="next">
                            <i class="fa fa-angle-right fade"></i>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="company-profile">
                <a href="<?php echo base_url('categories/'.getSEOStr($company->{'name_'.userLang()}).'/'.getSEOStr($company_name)); ?>" style="display: flex;">
                    <div class="profile-item">
                        <?php echo symbolSVG($company->symbol_url); ?>
                        <span class="company-name text-span"><?php echo $company_name; ?></span>
                    </div>
                </a>
                <div class="profile-item">
                    <img src="<?php echo base_url(); ?>assets/madinma/front/images/location.svg"/>
                    <span class="company-location text-span"><a href="javascript:;" class="city_link" data-city="<?php echo $company->city_name; ?>"><?php echo $company->city_name; ?></a><?php echo ', ' . $company->street; ?></span>
                </div>
                <div class="profile-item">
                    <img src="<?php echo base_url(); ?>assets/madinma/front/images/analytics.svg"/>
                    <span class="company-info text-span"><?php echo lang('profile.since'); ?> <?php echo $company->founded_at; ?>, <?php echo employeeCountStr($company->employee_count); ?> <?php echo lang('profile.employees'); ?></span>
                </div>
                <div class="profile-item">
                    <img src="<?php echo base_url(); ?>assets/madinma/front/images/rating.svg"/>
                    <?php $avg_rating = getCompanyRating($company->id); ?>
                    <?php if(getReviewCount($company->id) > 0) { ?>
                    <span class="company-rating"><?php echo getReviewCount($company->id); ?></span>
                    <?php } ?>
                    <?php if($avg_rating == 0) { ?>
                    <span class="company-rating"><?php echo lang('no.ratings'); ?></span>
                    <?php  } else { ?>
                    <div class="rating-stars">
                        <?php for ($i = 0; $i < 5; $i++): ?>
                            <?php if ($i < $avg_rating): ?>
                            <i class="fa fa-star"></i>
                            <?php else: ?>
                            <i class="fa fa-star-o"></i>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<?php $this->load->view('front/_partials/footer'); ?>

</div>

<script src="https://www.google.com/recaptcha/api.js?render=<?=RECAPTCHA_SITE_KEY?>"></script>

<script>
    $(document).ready(function () {
        grecaptcha.ready(function() {
            grecaptcha.execute('<?=RECAPTCHA_SITE_KEY?>', {action: 'contact'}).then(function(token) {
               var recaptchaResponse = document.getElementById('contact_recaptcha');
                recaptchaResponse.value = token;
            });
        });

        $(".bg-image").css("background", function() {
            var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
            return a
		});
		
        $('.owl-carousel').owlCarousel({
            loop:true,
            nav:true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            dots: false,
            rtl: rtl,
            margin:10,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:false
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:false
                }
            }
		});

        $('#contact_modal').on('show.bs.modal', function() {
            $(this).find('form')[0].reset();
        });

        $('#contact_modal .btn-send').click(function() {
            var isValid = true;
            var fields = ['name', 'email', 'content'];
            $('#contact_modal .form-group').removeClass('has-error');
            for (i = 0; i < fields.length; i++) {
                if ($('#contact_modal [name="' + fields[i] + '"]').val() == '') {
                    isValid = false;
                    $('#contact_modal [name="' + fields[i] + '"]').parents('.form-group').addClass('has-error');
                } else {
                    $('#contact_modal [name="' + fields[i] + '"]').parents('.form-group').removeClass('has-error');
                }
            }

            if (isValid == true) {
                var params = {
                    company_id: $('#contact_modal [name="company_id"]').val(),
                    name: $('#contact_modal [name="name"]').val(),
                    email: $('#contact_modal [name="email"]').val(),
                    phone: $('#contact_modal [name="phone"]').val(),
                    country_id: $('#contact_modal [name="country_id"]').val(),
                    content: $('#contact_modal [name="content"]').val(),
                    recaptcha_response: $('#contact_modal [name="recaptcha_response"]').val()
                };
                $.post(base_url + 'mail/post', params, function(data, status){
                    if (data.success) {
                        bootbox.dialog({
                            title: message_success,
                            message: message_success,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-success',
                                    callback: function(){
                                        location.reload();
                                    }
                                }
                            }
                        });
                        $('#contact_modal').modal('hide');
                    } else {
                        bootbox.dialog({
                            title: message_error,
                            message: data.message,
                            size: 'small',
                            buttons: {
                                ok: {
                                    label: message_ok,
                                    className: 'btn-danger'
                                }
                            }
                        });
                    }
                });
            }
        });

    });

</script>
