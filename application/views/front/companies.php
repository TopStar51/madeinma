<?php $this->load->view('front/_partials/searchbar'); ?>
<style>
    .bootstrap-select.btn-group .btn .filter-option {
        max-width: 100px;
        text-overflow: ellipsis;
    }
</style>
  <?php $category_id = $this->session->userdata('category_id');?>

<?php 
    $city_sel = $this->session->userdata('city_sel'); 
    $city_sel = empty($city_sel) ? 'all' : $city_sel;
    $sort_sel = $this->session->userdata('sort_sel'); 
    $sort_sel = empty($sort_sel) ? 'rating' : $sort_sel;

    $emp_sel = $this->session->userdata('emp_sel'); 
    $emp_sel = empty($emp_sel) ? 'all' : $emp_sel;

    $action_url = ($this->uri->segment(1,0)=='favorites')? base_url('favorites'):'';
?> 
<div class="row filter-panel">
    <div class="col-md-12">
        <?php $category_sess = getSEOStr($this->session->userdata('category'));?>
        <div class="filter-result pull-left" style="margin-bottom: 10px;">
            <?php if (empty($category_sess)): ?>
                <span class="result-text"><?php echo $total ?> <?php echo lang('companies.entries'); ?></span>
            <?php else: ?>
                <span class="result-text"><?php echo $total ?> <?php echo lang('companies.entries').' '; ?><?php echo lang('companies.in') . ' '; ?></span>
                <span class="category-tag"><?php echo ($this->session->userdata('category')); ?><img class="tag-icon" onclick="javascript:remove_tag();" src="<?php echo base_url('assets/madinma/front/images/close_icon.png'); ?>"/></span>
            <?php endif; ?>
        </div>
        <div class="filter-wrapper pull-right">
            <label class="city <?php if ('all' != $city_sel): ?>filtered<?php endif; ?>">
                <i class="fa fa-filter" style="font-size:20px;color:#00652d;padding-right: 7px;"></i>
                <?php //echo humanize(lang('company.city')); ?> 
                <select id="city_sel">
                    <option value="all" <?php if ('all' == $city_sel): ?>selected<?php endif; ?>><?php echo lang('companies.all'); ?></option>
                    <?php foreach($cities as $city): ?>
                    <option value="<?php echo $city->name; ?>" <?php if ($city->name == $city_sel): ?>selected<?php endif; ?>><?php echo $city->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </label>
            <label class="sort <?php if ("rating" != $sort_sel): ?>filtered<?php endif; ?>">
                <?php //echo lang('companies.sortby'); ?> 
                <i class="fa fa-sort-amount-asc" style="font-size:20px;color:#00652d;padding-right: 7px;"></i>
                <select id="sort_sel">
                    <option value="rating" <?php if ("rating" == $sort_sel): ?>selected<?php endif; ?>><?php echo lang('companies.rating'); ?></option>
                    <option value="newest" <?php if ("newest" == $sort_sel): ?>selected<?php endif; ?>><?php echo lang('companies.newest'); ?></option>
                    <option value="oldest" <?php if ("oldest" == $sort_sel): ?>selected<?php endif; ?>><?php echo lang('companies.oldest'); ?></option>
                </select>
            </label>
            <label class="emp <?php if ("all" != $emp_sel): ?>filtered<?php endif; ?>">
                <i class="fa fa-users" style="font-size:20px;color:#00652d;padding-right: 7px;"></i>
                <?php $emp_count_arr = array('<5', '5+', '10+', '20+', '50+', '100+', '500+', '1000+');?>
                <select id="emp_sel">
                    <option value="all" <?php if ('all' == $emp_sel): ?>selected<?php endif; ?>><?php echo lang('companies.all'); ?></option>
                    <?php foreach($emp_count_arr as $key => $row): ?>
                        <option value="<?php echo $key+1; ?>" <?php if ($key+1 == $emp_sel): ?>selected<?php endif; ?>><?php echo $row; ?></option>
                    <?php endforeach; ?>
                </select>
            </label>
            <?php 
                $layout_style_cookie = get_cookie('layout_style', TRUE); 
                $layout_style_cookie = empty($layout_style_cookie) ? 'list' : $layout_style_cookie;
            ?>
            <?php if ($this->session->has_userdata('category')):?>
                <?php  if (lang('categoryInfo.detail.'.$category_id)):?>
                <a class="btn btn-bb-sm view-btn <?php if ($layout_style_cookie == 'info'): ?>selected<?php endif; ?>" href="#" onclick="change_layout('info');return false;">
                <i class="icon-info"></i></a>
                <?php endif;?>
            <?php endif;?>
			<a class="btn btn-bb-sm view-btn <?php if ($layout_style_cookie == 'list'): ?>selected<?php endif; ?>" href="#" onclick="change_layout('list');return false;"><i class="fa fa-th-list"></i></a>
            <a class="btn btn-bb-sm view-btn <?php if ($layout_style_cookie == 'grid'): ?>selected<?php endif; ?>" href="#" onclick="change_layout('grid');return false;"><i class="fa fa-th-large"></i></a>
        </div>
    </div>
</div>

<div class="row products-panel">
  
    <?php 
        if($layout_style_cookie == 'info')
            $this->load->view('front/_partials/category_info.php');
        else 
            $this->load->view('front/_partials/company_part.php');
    ?>
</div>

<script>
    var category_info = "<?= lang('categoryInfo.detail.'.$category_id); ?>";
    // alert(category_info);
    function change_layout(type) {
        $.post(base_url + 'layout/' + type, function(data, status){
            if (data.success) {
                location.reload();
            }
        });
    }

    var pre_top = 0;
    var received = true;
    var count = Number("<?php echo count($companies); ?>");
    var is_end = false;
    var company_total = '<?= $this->session->userdata("company_total"); ?>';
    if (company_total == 0)
        is_end = true;
    var category = '<?= empty($category_sess)? "" : $category_sess; ?>';
    
    function getPullingData() {
        pre_top = $(window).scrollTop();
        received = false;
        $.ajax({
            // url: "<?php echo base_url(); ?><?php echo empty($cat_name) ? 'companies' : 'categories/'.$cat_name; ?><?= $is_fav ? '/TRUE' : '' ?>",
            data:  {
                start: count,
                q: $('input[name="q"]').attr('old-value'),
                city: $('[name="city"]').val(),
                sort: $('[name="sort"]').val(),
                emp: $('[name="emp"]').val()
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                count += data.count;
                if (data.count == 0) {
                    is_end = true;
                }
                $('.products-panel').append(data.html);
                $(".bg-image").css("background", function() {
                    var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
                    return a
                });
                $(window).scrollTop(pre_top);
                received = true;
                $("html").niceScroll().remove();
                $("html").niceScroll({
                    scrollspeed: 60,
                    mousescrollstep: 40,
                    cursorwidth: 8,
                    cursorheight: 130,
                    cursorborder: 0,
                    cursorcolor: '#D20023',
                    cursorborderradius: 5,
                    styler:"fb",
                    autohidemode: false,
                    horizrailenabled: false
                });
            }
        });
    }   

     $(window).scroll(function(event) {
        if(!received)
        {
            event.preventDefault();
            event.stopPropagation();
            return;
        }

        if(Math.abs($(window).scrollTop()-$(".products-panel").height()) < 1000 && is_end == false)
        {
            pre_top = $(window).scrollTop();
            getPullingData();
        }
    });
    $(window).scrollTop(0);

    $(document).ready(function () {
        $(".bg-image").css("background", function() {
            var a = "url(" + $(this).data("image-src") + ") no-repeat center center";
            return a
        });

        $('#sort_sel').selectpicker({
            width: 150,
            height: 32,
            dropdownAlignRight: rtl
        });
        
        $('#city_sel').selectpicker({
            width: 150,
            height: 32,
            dropdownAlignRight: rtl
        });

        $('#emp_sel').selectpicker({
            width: 122,
            height: 32,
            dropdownAlignRight: rtl
        });

        $('#sort_sel, #city_sel, #emp_sel').change(function() {
            var city_sel = $('#city_sel').val();
            $('input[name="city"]').val(city_sel);
            $('input[name="sort"]').val($('#sort_sel').val());
            $('input[name="emp"]').val($('#emp_sel').val());
            var action_url = "<?php echo $action_url;?>";
            if(category == '') {
                city_sel = city_sel.replace(/ /g, "_");
                if(action_url == '')
                    $('.top-search').attr('action', base_url + 'cities/' + city_sel);
                else
                    $('.top-search').attr('action', action_url);
            } else {
                category = category.replace(/ /g, "_");
                if(action_url == '')
                    $('.top-search').attr('action', base_url + 'categories/' + category);
                else
                    $('.top-search').attr('action', action_url);
            }
            $('.top-search')[0].submit();
        })

		$(".reset").click(function(){
            $('input[name="city"]').val('all');
            $('input[name="sort"]').val($('#sort_sel').val());
            $('input[name="emp"]').val('all');
            var action_url = "<?php echo $action_url;?>";
			category = category.replace(/ /g, "_");
			if(category != '' && action_url == '')
				$('.top-search').attr('action', base_url + 'categories/' + category);
			else
				$('.top-search').attr('action', action_url);
            
            $('.top-search')[0].submit();
		})

    });

    function remove_tag() {
        var city_sel = $('#city_sel').val();
        var action_url = "<?php echo $action_url;?>";
        city_sel = city_sel.replace(/ /g, "_");
        if(action_url == '')
            $('.top-search').attr('action', base_url + 'cities/' + city_sel);
        else
            $('.top-search').attr('action', action_url);
        $('.top-search')[0].submit();
    }

</script>
