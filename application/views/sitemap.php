<?php echo'<?xml version="1.0" encoding="UTF-8" ?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo base_url();?></loc>
        <lastmod>2019-07-31</lastmod>
        <priority>1.0</priority>
        <changefreq>monthly</changefreq>
    </url>

    <!-- Sitemap -->
    <?php foreach($companies as $company) { ?>
    <url>
        <?php
            if(!isset($company->{'company_name_'.userLang()}) || $company->{'company_name_'.userLang()}=="")
                $company_name = $company->{'company_name_fr'};
            else
                $company_name = $company->{'company_name_'.userLang()};
        ?>
        <loc><?php echo base_url().getSEOStr($company_name); ?></loc>
        <lastmod><?php echo date('Y-m-d', strtotime($company->updated_at)); ?></lastmod>
        <priority>0.5</priority>
        <changefreq>monthly</changefreq>
    </url>
    <?php } ?>

</urlset>