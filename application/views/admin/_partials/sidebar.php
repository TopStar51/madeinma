 <aside class="main-sidebar">
    <!-- sidebar -->
    <section class="sidebar">
    	<!-- Sidebar user panel -->
		<div class="user-panel">
	        <div class="pull-left image">
	          <img src="<?php echo base_url('assets/madinma/back/images/default_avatar.jpg') ?>" class="img-circle" alt="">
	        </div>
	        <div class="pull-left info">
	          	<p><?php echo $user->email ?></p>
	        </div>
      	</div>
       	<!-- sidebar menu -->
       <ul class="sidebar-menu" data-widget="tree">
	        <li class="header">MAIN MENU</li>	        
			<li class="<?php if ($ctrler == 'manufacturer'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/manufacturer'); ?>"> 
					<i class="fa fa-users text-red"></i>
					<span>Manufacturer</span>
				</a>						
			</li>
			<li class="<?php if ($ctrler == 'category'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/category'); ?>"> 
					<i class="fa fa-bell text-yellow"></i>
					<span>Category</span>
				</a>						
			</li>
			<li class="<?php if ($ctrler == 'message' && $action == 'index'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/message'); ?>"> 
					<i class="fa fa-commenting text-green"></i>
					<span>List of all messages</span>
				</a>						
			</li>
			<li class="<?php if ($ctrler == 'review'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/review'); ?>"> 
					<i class="fa fa-star text-aqua"></i>
					<?php if ($rating_cnt>0):?>
						<span class="badge" style="float:right;background:red;"><?php echo $rating_cnt;?></span>
					<?php endif;?>
					<span>Release ratings</span>
				</a>						
			</li>
			<li class="<?php if ($ctrler == 'company'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/company'); ?>"> 
					<i class="fa fa-send text-red"></i>
					<?php if ($update_cnt>0):?>
						<span class="badge" style="float:right;background:red;"><?php echo $update_cnt;?></span>
					<?php endif;?>
					<span>Release updates</span>
				</a>						
			</li>
			<li class="<?php if ($ctrler == 'stext' && $action == 'swords'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/stext/swords'); ?>"> 
					<i class="fa fa-search text-light"></i>
					<span>Search words</span>
				</a>						
			</li>
			<li class="<?php if ($ctrler == 'stext' && $action == 'keywords'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/stext/keywords'); ?>"> 
					<i class="fa fa-tag text-yellow"></i>
					<span>Keywords for Autocomplete</span>
				</a>						
			</li>
			<li class="<?php if ($ctrler == 'message' && $action == 'contactus'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/message/contactus'); ?>"> 
					<i class="fa fa-envelope text-green"></i>
					<?php if ($contact_cnt>0):?>
						<span class="badge" style="float:right;background:red;"><?php echo $contact_cnt;?></span>
					<?php endif;?>
					<span>Contact us</span>
				</a>						
			</li>
			<li class="<?php if ($ctrler == 'support' && $action == 'support'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/support'); ?>"> 
					<i class="fa fa-support text-green"></i>
					<span>Support</span>
				</a>						
			</li>
			<li class="<?php if ($ctrler == 'url_shortener' && $action == 'url_shortener'): ?>active<?php endif; ?>">
				<a href="<?php echo base_url('admin/url_shortener'); ?>"> 
					<i class="fa fa-link text-green"></i>
					<span>URL Shortener</span>
				</a>						
			</li>
			<li>
				<a href="<?php echo base_url('logout'); ?>"> 
					<i class="fa fa-sign-out text-blue"></i>
					<span>Sign Out</span>
				</a>						
			</li>
      	</ul>
    </section>
    <!-- /.sidebar -->
</aside>