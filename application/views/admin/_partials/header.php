<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">made-in.ma</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php echo base_url(); ?>assets/madinma/front/images/icon-1.png" style="width: 35px; height: 35px; margin-right: 10px;"/><b>made-in.ma</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      	<!-- Sidebar toggle button-->
      	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        	<span class="sr-only">Toggle navigation</span>
      	</a>

      	<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- DATOS DEL USUARIO -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
						<img src="<?php echo base_url('assets/madinma/back/images/default_avatar.jpg'); ?>" class="user-image" alt="">
						<span class="hidden-xs"><?php echo $user->email; ?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-header">
							<img src="<?php echo base_url('assets/madinma/back/images/default_avatar.jpg'); ?>" class="img-circle" alt="">
							<p>
								<?php echo $user->email; ?>
							</p>
						</li>
						<li class="user-footer">
							<div class="pull-right">
								<a href="<?php echo base_url('logout'); ?>" class="btn btn-default btn-flat">Sign Out</a>
							</div>
						</li>
					</ul>
				</li>					
			</ul>
		</div>

    </nav>
</header>