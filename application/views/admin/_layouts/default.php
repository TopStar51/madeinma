<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('admin/_base/head'); ?>

<body class="hold-transition skin-blue sidebar-mini">		
	
	<div class="wrapper">
		
		<?php $this->load->view('admin/_partials/header'); ?>

		<?php $this->load->view('admin/_partials/sidebar'); ?>
		
		<!-- DIV PRINCIPAL -->
		<div class="content-wrapper">

			<section class="content-header">
			    <h1>
			        <?php echo $title; ?>
			    </h1>
		    </section>

			<?php $this->load->view($inner_view); ?>
			
		</div>
		
		<?php $this->load->view('admin/_partials/footer'); ?>
		
	</div>
	
</body>
</html>

