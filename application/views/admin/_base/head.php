<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/favicon/apple-touch-icon.png'); ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/favicon/favicon-32x32.png'); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/favicon/favicon-16x16.png'); ?>">
	<link rel="manifest" href="<?php echo base_url('assets/favicon/site.webmanifest'); ?>">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#d20023">

	<title><?php echo WEBSITE_TITLE; ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/Ionicons/css/ionicons.min.css'); ?> ">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/dist/css/AdminLTE.min.css'); ?>">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	     folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/dist/css/skins/_all-skins.min.css'); ?>">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/toastr/jquery.toast.css'); ?>">

	<!-- iCheck for checkboxes and radio inputs -->
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/iCheck/all.css'); ?>">
	<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/timepicker/bootstrap-timepicker.min.css'); ?>">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	<link rel="stylesheet" href="<?php echo base_url('assets/madinma/back/css/common.css'); ?>">
	
	<!-- jQuery 3 -->
	<script src="<?php echo base_url('assets/AdminLTE/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url('assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
	<!-- datepicker -->
	<script src="<?php echo base_url('assets/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>
	<!-- Slimscroll -->
	<script src="<?php echo base_url('assets/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
	<!-- FastClick -->
	<script src="<?php echo base_url('assets/AdminLTE/bower_components/fastclick/lib/fastclick.js'); ?>"></script>
	
	<script src="<?php echo base_url('assets/AdminLTE/bower_components/toastr/jquery.toast.js'); ?>"></script>
	<!-- bootstrap time picker -->
	<script src="<?php echo base_url('assets/AdminLTE/bower_components/timepicker/bootstrap-timepicker.min.js'); ?>"></script>
	<!-- iCheck 1.0.1 -->
	<script src="<?php echo base_url('assets/AdminLTE/bower_components/iCheck/icheck.min.js'); ?>"></script>

	<script src="<?php echo base_url('assets/AdminLTE/bower_components/bootbox.min.js'); ?>"></script>
	
	<!-- AdminLTE App -->
	<script src="<?php echo base_url('assets/AdminLTE/dist/js/adminlte.min.js'); ?>"></script>

	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';

		var general_error = "<?php echo lang('general.error'); ?>";
		var general_warning = "<?php echo lang('general.warning'); ?>";
		var general_success = "<?php echo lang('general.success'); ?>";
	</script>

	<script src="<?php echo base_url('assets/madinma/back/scripts/common.js'); ?>"></script>

</head>