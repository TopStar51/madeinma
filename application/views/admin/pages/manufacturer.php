<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">
<style>
  .rating-stars i {
      color: #ffd953 !important;
  }
</style>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <div class="col-md-2">
              <button type="button" id="btn_add" class="btn btn-block btn-primary" data-toggle="modal" data-target="#m_modal">ADD</button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="m_table" class="table table-bordered table-hover"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->


<!-- m_modal -->
<div id="promotion_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Set promotion count</h5>
            </div>
            <form id="m_form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id">
                <div class="modal-body">
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="form-group">
                              <label>Current Number of Promotions</label>
                              <input type="text" value="" class="form-control" readonly id="promotion_count">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="form-group">
                              <label>Number of Promotions to Delete</label>
                              <input type="text" name="delete_count" value="" class="form-control" required>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="modal-footer" id="btns">
                    <button type="button" onclick="save_promotion();" class="btn btn-warning btn-sm">Delete</button>
                    <button type="button" onclick="reset_promotion()" class="btn btn-danger btn-sm">Reset</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- m_modal -->
<div id="m_modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Company</h5>
            </div>
            <form id="m_company_form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Phone</label>
                                <input type="text" name="phone" placeholder="+222222222222" class="form-control"/>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" placeholder="MichaelGross@migro.net" class="form-control" autocomplete="off"/>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" placeholder="" class="form-control" autocomplete="off"/>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Promotion Code (Optional)</label>
                                <input type="text" name="promotion_code" placeholder="" class="form-control"/>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <?php 
                              // $langs = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');
                              $langs = array('fr');
                             ?>
                            <?php foreach ($langs as $key => $lang): ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Companyname or Title(<?php echo lang('general.lang.'.$lang) ?>)</label>
                                    <input type="text" name="company_name_<?php echo $lang; ?>" placeholder="Name (<?php echo lang('general.lang.'.$lang) ?>)" class="form-control"/>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Contact Person</label>
                                <input type="text" name="contact_person" placeholder="Ismail Moukafih" class="form-control"/>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>City</label>
                                <select name="city_id" class="form-control">
                                  <?php foreach($cities as $key => $city): ?>
                                    <option value="<?php echo $city->id;?>"><?php echo $city->name;?></option>
                                  <?php endforeach;?>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Category</label>
                                <select name="category_id" class="form-control">
                                  <?php foreach($categories as $key => $category): ?>
                                    <option value="<?php echo $category->id;?>"><?php echo $category->name_en;?></option>
                                  <?php endforeach;?>
                                </select>
                              </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="modal-footer" id="btns">
                    <button type="button" onclick="javascript:save();" class="btn btn-primary btn-sm">Save</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">
  var oTable;

  function change_show(id, show_home=1) {
    var dialog = bootbox.dialog({
        title: 'Confirmation',
        message: "<h4>Are You Sure Want to " + (show_home == 1 ? 'show' : 'hide') + "?</h4>",
        size: 'small',
        buttons: {
          cancel: {
            label: "Cancel",
            className: 'btn-danger',
            callback: function(){
              dialog.modal('hide');
            }
          },
          ok: {
            label: "OK",
            className: 'btn-success',
            callback: function() {
              $.post(base_url + 'admin/manufacturer/change_show_ajax', {id: id, show_home: show_home}, function(data, status){
                if (data.success) {
                    success_message('Successfully saved !');
                    oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                } else {
                    error_message('Failed');
                }
              });
            }
          }
        }
    });
  }
  function change_status(id, status = 0) {
    var dialog = bootbox.dialog({
        title: 'Confirmation',
        message: "<h4>Are You Sure Want to " + (status == 0 ? 'Block' : 'Allow') + "?</h4>",
        size: 'small',
        buttons: {
          cancel: {
            label: "Cancel",
            className: 'btn-danger',
            callback: function(){
              dialog.modal('hide');
            }
          },
          ok: {
            label: "OK",
            className: 'btn-success',
            callback: function() {
              $.post(base_url + 'admin/manufacturer/change_status_ajax', {id: id, status: status}, function(data, status){
                if (data.success) {
                    success_message('Successfully saved !');
                    oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                } else {
                    error_message('Failed');
                }
              });
            }
          }
        }
    });
  }

  $(function () {

    var handleTable = function () {
        var nEditing = null;

        var table = $('#m_table');

        oTable = table.dataTable({
            "bServerSide": true,
            "bProcessing": true,
            "bDeferRender": true,
            "bAutoWidth": false,
            "aoColumns": [
              {
                "sTitle" : "No", 
                "mData": "index", 
                // mRender: function (data, type, row, pos) {
								// 	console.log(pos)
                //   return Number(pos.row)+1;
                // },
                "sWidth" : 30
              },
              {
                "sTitle" : "Company", 
                "mData": "company_name_fr",
                "sWidth" : 120
              },
              {
                "sTitle" : "Contact Person", 
                "mData": "contact_person",
                "sWidth" : 120
              },
              {
                "sTitle" : "City", 
                "mData": "city_name",
                "sWidth" : 100
              },
              {
                "sTitle" : "Category", 
                "mData": "category_name",
                "sWidth" : 100
              },
              {
                "sTitle" : "Phone", 
                "mData": "phone",
                "sWidth" : 100
              },
              {
                "sTitle" : "Email", 
                "mData": "email",
                "sWidth" : 150
              },
              {
                "sTitle" : "Rating", 
                "mData": "rating",
                mRender: function (data, type, row, pos) {
                  var html = '<div class="rating-stars">';
                  for (i = 0; i < data; i++) {
                    html += '<i class="fa fa-star"></i>';
                  }
                  return html;
                },
                "sWidth" : 100
              },
              {
                sTitle: 'Promotion Code',
                mData: 'promotion_code',
                sWidth: 50
              },
              {
                sTitle: 'Promotion Count',
                mData: 'promotion_count',
                sWidth: 50
              },
              {
                "sTitle" : "Status", 
                "mData": "status",
                mRender: function (data, type, row, pos) {
                  return data == 1 ? '<span class="label label-success">Allowed</span>' : '<span class="label label-warning">Bloked</span>';
                },
                "sWidth" : 100
              },
              {
                "sTitle" : "Action",
                "bSearchable": false,
                "bSortable": false,
                mRender: function (data, type, row) {
                  return '<a href="' + base_url + 'home/myaccount/' + row.company_id + '" class="fa fa-edit" title="Edit" style="cursor: pointer;" target="_blank"></a> \
                          <a class="fa fa-trash delete" title="Delete" style="cursor: pointer;"></a> \
                          <a class="fa ' + (row.status == 1 ? 'fa-circle-o' : 'fa-check-square-o') + ' ' + (row.status == 1 ? 'allow' : 'block') + '" title="' + (row.status == 1 ? 'Block' : 'Allow') + '" style="cursor: pointer;"></a> \
                          <a class="fa ' + (row.show_home == 1 ? 'fa-bullseye' : 'fa-eye') + ' ' + (row.show_home == 1 ? 'hide_home' : 'show_home') + '" title="' + (row.show_home == 1 ? 'Hide' : 'Show') + '" style="cursor: pointer;"></a> \
                          <a class="fa fa-dollar payout" title="Payout" style="cursor: pointer;"></a>';
                },
                "sWidth" : 200
              }
            ],
            "sAjaxSource": base_url + 'admin/manufacturer/datatable_read_ajax',
            "sAjaxDataProp": "data",
            "fnServerParams": function (aoData) {
            },
            "fnServerData": function (sSource, aoData, fnCallback){
                $.ajax({
                    "dataType": "json", 
                    "type": "POST", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                });
            },
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "ALL"] // change per page values here
            ],
            "pageLength": 10, // set the initial value,
            columnDefs: [{
                orderable: true,
                targets: [1]
            }],
            "order": [
                [1, "asc"]
            ]
        });

        $(window).resize(function() {
            oTable.fnAdjustColumnSizing();
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();
    
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var dialog = bootbox.dialog({
                title: 'Confirmation',
                message: "<h4>Are You Sure Want to delete?</h4>",
                size: 'small',
                buttons: {
                  cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function(){
                      dialog.modal('hide');
                    }
                  },
                  ok: {
                    label: "OK",
                    className: 'btn-success',
                    callback: function() {
                      $.post(base_url + 'admin/manufacturer/manufacturer_delete_ajax', {ids: aData.id}, function(data, status){
                          if (data.success) {
                              success_message('Successfully removed!');
                              oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                          } else {
                              error_message('Failed');
                          }
                      });
                    }
                  }
                }
            });
        });

        table.on('click', '.hide_home', function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            change_show(aData.company_id, 0);
        });

        table.on('click', '.show_home', function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            change_show(aData.company_id);
        });

        table.on('click', '.block', function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            change_status(aData.company_id, 1);
        });

        table.on('click', '.allow', function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            change_status(aData.company_id);
        });

        table.on('click', '.payout', function (e) {
          e.preventDefault();

          var nRow = $(this).parents('tr')[0];
          var aData = oTable.fnGetData(nRow);

          $('#promotion_modal').find('input[name="id"]').val(aData.id);
          $('#promotion_modal').find('#promotion_count').val(aData.promotion_count);
          $('#promotion_modal').find('input[name="delete_count"]').val('');
          $('#promotion_modal').modal('show');
        });

    }();

  });

  function save_promotion() {
    // check if delete count is bigger than current promotion count
    if ($('#promotion_count').val() < $('input[name="delete_count"]').val()) {
      alert('Can\'t delete more than current promotion count.');
      return;
    }

    $.ajax({
      url: '<?= base_url('admin/manufacturer/api_delete_promotion') ?>',
      dataType: 'json',
      type: 'POST',
      data: {
        id: $('#promotion_modal').find('input[name="id"]').val(),
        count: $('#promotion_modal').find('input[name="delete_count"]').val()
      },
      success: function(ret) {
        if (ret.success) {
          $('#promotion_modal').modal('hide');
          success_message('Successfully updated!');
          oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
        } else {

        }
      }
    })
  }

  function reset_promotion() {
    if (!confirm('Are you sure to reset?')) {
      return;
    }
    $.ajax({
      url: '<?= base_url('admin/manufacturer/api_reset_promotion') ?>',
      dataType: 'json',
      type: 'POST',
      data: {
        id: $('#promotion_modal').find('input[name="id"]').val()
      },
      success: function(ret) {
        if (ret.success) {
          $('#promotion_modal').modal('hide');
          success_message('Successfully reset!');
          oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
        } else {
          alert(ret.message);
        }
      }
    });
  }
  function save() {
    var A = new FormData();

    if ($('[name=company_name_fr]').val() == '' || $('[name=phone]').val() == '' || $('[name=contact_person]').val() == '') {
        bootbox.dialog({
            title: "Warning",
            message: "Please input company, phone, contact person fields.",
            size: 'small',
            buttons: {
                ok: {
                    label: "OK",
                    className: 'btn-success',
                }
            }
        });
        return;
    }

    var form = $("#m_company_form")[0];
    for (i=0; i<form.length; i++) {
        if (form[i].name != '') {
            A.append(form[i].name, form[i].value);
        }
    }
    
    var C = new XMLHttpRequest();
    C.open("POST", base_url + 'admin/manufacturer/post_company');
    C.onload = function() {
        var data = JSON.parse(C.response);
        if (data.success) {
            bootbox.dialog({
                title: "Success",
                message: "Successfully saved!",
                size: 'small',
                buttons: {
                    ok: {
                        label: "OK",
                        className: 'btn-success',
                        callback: function() {
                            $("#m_form")[0].reset();
                            oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                            $('#m_modal').modal('hide');
                        }
                    }
                }
            });
        } else {
            bootbox.dialog({
                title: "Error",
                message: "Failed",
                size: 'small',
                buttons: {
                    ok: {
                        label: "OK",
                        className: 'btn-error'
                    }
                }
            });
        }
    };
    C.send(A);
}
</script>
