<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="m_table" class="table table-bordered table-hover"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<!-- DataTables -->
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">
  var oTable;

  $(function () {

    var handleTable = function () {
        var nEditing = null;

        var table = $('#m_table');

        oTable = table.dataTable({
            "bServerSide": true,
            "bProcessing": true,
            "bDeferRender": true,
            "bAutoWidth": false,
            "aoColumns": [
              {
                "sTitle" : "No", 
                "mData": "index", 
                // mRender: function (data, type, row, pos) {
                //   return Number(pos.row)+1;
                // },
                "sWidth" : 30
              },
              {
                "sTitle" : "Name", 
                "mData": "name",
                "sWidth" : 120
              },
              {
                "sTitle" : "Email",
                "mData": "email",
                "sWidth" : 150
              },
              {
                "sTitle" : "Phone", 
                "mData": "phone",
                "sWidth" : 120
              },
              {
                "sTitle" : "Content", 
                "mData": "content",
                mRender: function (data, type, row, pos) {
                  return '<textarea rows="3" style="width: 100%;" readonly>' + data + '</textarea>';
                },
                "sWidth" : 300
              }, {
                'sTitle': 'IpAddress',
                'mData': 'ip_addr',
                'sWidth': 100
              }, {
                "sTitle" : "Created At", 
                "mData": "created_at",
                "sWidth" : 100
              },
              {
                "sTitle" : "Action",
                "bSearchable": false,
                "bSortable": false,
                mRender: function (data, type, row) {
                  return '<a class="fa fa-trash delete" title="Delete" style="cursor: pointer;"></a>';
                },
                "sWidth" : 200
              }
            ],
            "sAjaxSource": base_url + 'admin/message/datatable_read_ajax',
            "sAjaxDataProp": "data",
            "fnServerParams": function (aoData) {
              aoData.push({"name": "filter[is_contact]", "value": 1});
            },
            "fnServerData": function (sSource, aoData, fnCallback){
                $.ajax({
                    "dataType": "json", 
                    "type": "POST", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                });
            },
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "ALL"] // change per page values here
            ],
            "pageLength": 10, // set the initial value,
            columnDefs: [{
                orderable: true,
                targets: [1]
            }],
            "order": [
                [6, "desc"]
            ]
        });

        $(window).resize(function() {
            oTable.fnAdjustColumnSizing();
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();
    
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var dialog = bootbox.dialog({
                title: 'Confirmation',
                message: "<h4>Are You Sure Want to delete?</h4>",
                size: 'small',
                buttons: {
                  cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function(){
                      dialog.modal('hide');
                    }
                  },
                  ok: {
                    label: "OK",
                    className: 'btn-success',
                    callback: function() {
                      $.post(base_url + 'admin/message/message_delete_ajax', {ids: aData.id}, function(data, status){
                          if (data.success) {
                              success_message('Successfully removed!');
                              oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                          } else {
                              error_message('Failed');
                          }
                      });
                    }
                  }
                }
            });
        });

    }();

  });
</script>
