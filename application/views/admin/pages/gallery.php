<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <label>
              <input type="checkbox" id="pendding_filter" checked=""> Pendding
            </label>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="m_table" class="table table-bordered table-hover"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<!-- m_modal -->
<div id="m_modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Gallery</h5>
            </div>

            <form id="m_form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="first_name">
                            </div>
                            <div class="col-sm-6">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="last_name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Phone Number</label>
                                <input type="text" class="form-control" name="phone">
                            </div>
                            <div class="col-sm-6">
                                <label>Street</label>
                                <input type="text" class="form-control" name="street">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>House Number</label>
                                <input type="text" class="form-control" name="house_number">
                            </div>
                            <div class="col-sm-3">
                                <label>Postal Code</label>
                                <input type="text" class="form-control" name="postal_code">
                            </div>
                            <div class="col-sm-6">
                                <label>Address</label>
                                <input type="text" class="form-control" name="address">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Photo or Video</label>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <img id="photo" class="img-responsive center-block" src="<?php echo base_url('assets/lsw/back/images/default_photo.png'); ?>" alt=""/>
                                  </div>
                                  <div class="col-sm-12">
                                    <video id="video" style="width:100%;" controls autoplay></video>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" id="btns">
                    <button type="button" class="btn btn-primary btn-sm btn_accept">Accept</button>
                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Video dialog -->
<div id="video-dialog" class="modal fade" role="dialog" data-backdrop=true>
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body" style="min-height:300px;">
          <div class="row">
            <div class="col-sm-12">
              <img id="photo-show" class="img-responsive center-block" src="" alt=""/>
            </div>
            <div class="col-sm-12">
              <video id="video-play" style="width:100%;" controls autoplay>
              </video>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>

<!-- DataTables -->
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">
  var oTable;

  function change_status(id, status=1) {
    var dialog = bootbox.dialog({
        title: 'Confirmation',
        message: "<h4>Are You Sure Want to " + (status == 1 ? 'accept' : 'block') + "?</h4>",
        size: 'small',
        buttons: {
          cancel: {
            label: "Cancel",
            className: 'btn-danger',
            callback: function(){
              dialog.modal('hide');
            }
          },
          ok: {
            label: "OK",
            className: 'btn-success',
            callback: function() {
              $.post(base_url + 'admin/gallery/change_status_ajax', {id: id, status: status}, function(data, status){
                if (data.success) {
                    success_message('Successfully ' + (status == 1 ? 'accept' : 'block') + 'ed !');
                    oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                } else {
                    error_message('Failed');
                }
              });
            }
          }
        }
    });
  }

  $(function () {
    $('input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_minimal-blue'
    })

    $('input[type="checkbox"]').on('ifChanged', function(e) {
        $(this).val(this.checked ? 1 : 0);
        $(this).trigger('change');
    });

    var handleTable = function () {
        var nEditing = null;
          
        function editRow(oTable, nRow) {

            $("#m_form")[0].reset();

            if (oTable != null && nRow != null) {
                var aData = oTable.fnGetData(nRow);
                $('#m_form input[name="id"]').val(aData.id);
                $('#m_form input[name="first_name"]').val(aData.first_name);
                $('#m_form input[name="last_name"]').val(aData.last_name);
                $('#m_form input[name="phone"]').val(aData.phone);
                $('#m_form input[name="street"]').val(aData.street);
                $('#m_form input[name="house_number"]').val(aData.house_number);
                $('#m_form input[name="postal_code"]').val(aData.postal_code);
                $('#m_form input[name="address"]').val(aData.address);
                $('#m_form input[name="email"]').val(aData.email);
                $('#m_form input[name="title"]').val(aData.title);
                if (Number(aData.video_url) != 0) {
                  $('#m_form #photo').addClass('hidden');
                  $('#m_form #video').removeClass('hidden');
                  $("#m_form #video").attr("src", base_url + 'upload/videos/' + aData.video_url);
                } else {
                  $('#m_form #photo').removeClass('hidden');
                  $('#m_form #video').addClass('hidden');
                  $('#m_form #photo').attr("src", base_url + 'upload/photos/' + aData.img_url);
                }
            }
           
            $("#m_modal").modal('show');
        }

        var table = $('#m_table');

        oTable = table.dataTable({
            "bServerSide": true,
            "bProcessing": true,
            "bDeferRender": true,
            "bAutoWidth": false,
            "aoColumns": [
              {
                "sTitle" : "No", 
                "mData": "", 
                mRender: function (data, type, row, pos) {
                  return Number(pos.row)+1;
                },
                "sWidth" : 30
              },
              {
                "sTitle" : "Uploaded by", 
                "mData": "first_name",
                "sWidth" : 150
              },
              {
                "sTitle" : "Title", 
                "mData": "title",
                "sWidth" : 150
              },
              {
                "sTitle" : "Type", 
                "mData": "",
                mRender: function (data, type, row, pos) {
                  if (Number(row.video_url) != 0) {
                    return '<span class="badge bg-green">Video</span>';
                  } else if (Number(row.img_url) != 0) {
                    return '<span class="badge bg-red">Photo</span>';
                  }
                  return "";
                },
                "sWidth" : 80
              },
              {
                "sTitle" : "Email",
                "mData": "email",
                "sWidth" : 150
              },
              {
                "sTitle" : "Status", 
                "mData": "status",
                mRender: function (data, type, row, pos) {
                  return data == 1 ? '<span class="label label-success">Accepted</span>' : '<span class="label label-warning">Pending</span>';
                },
                "sWidth" : 100
              },
              {
                "sTitle" : "Action",
                "bSearchable": false,
                "bSortable": false,
                mRender: function (data, type, row) {
                  return '<a class="fa fa-eye edit" title="Detail" style="cursor: pointer;"></a> \
                          <a class="fa fa-trash delete" title="Delete" style="cursor: pointer;"></a> \
                          <a class="fa fa-play-circle-o play" title="Play" style="cursor: pointer;"></a> \
                          <a class="fa ' + (row.status == 0 ? 'fa-check-circle-o' : 'fa-lock') + ' ' + (row.status == 0 ? 'accept' : 'block') + '" title="' + (row.status == 0 ? 'Accept' : 'Block') + '" style="cursor: pointer;"></a>';
                },
                "sWidth" : 200
              }
            ],
            "sAjaxSource": base_url + 'admin/gallery/datatable_read_request_ajax',
            "sAjaxDataProp": "data",
            "fnServerParams": function (aoData) {
              if ($('#pendding_filter').is(':checked')) {
                aoData.push({"name": "filter[status]", "value": 0});
              }
            },
            "fnServerData": function (sSource, aoData, fnCallback){
                $.ajax({
                    "dataType": "json", 
                    "type": "POST", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                });
            },
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "ALL"] // change per page values here
            ],
            "pageLength": 10, // set the initial value,
            columnDefs: [{
                orderable: true,
                targets: [1]
            }],
            "order": [
                [1, "asc"]
            ]
        });

        $(window).resize(function() {
            oTable.fnAdjustColumnSizing();
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();
    
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var dialog = bootbox.dialog({
                title: 'Confirmation',
                message: "<h4>Are You Sure Want to delete?</h4>",
                size: 'small',
                buttons: {
                  cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function(){
                      dialog.modal('hide');
                    }
                  },
                  ok: {
                    label: "OK",
                    className: 'btn-success',
                    callback: function() {
                      $.post(base_url + 'admin/gallery/gallery_delete_ajax', {ids: aData.id}, function(data, status){
                          if (data.success) {
                              success_message('Successfully removed!');
                              oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                          } else {
                              error_message('Failed');
                          }
                      });
                    }
                  }
                }
            });
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            editRow(oTable, nRow);
        });

        table.on('click', '.play', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $("#video-dialog .modal-title").html(aData.title);
            if (Number(aData.video_url) != 0) {
              $('#photo-show').addClass('hidden');
              $('#video-play').removeClass('hidden');
              $("#video-play").attr("src", base_url + 'upload/videos/' + aData.video_url);
            } else {
              $('#photo-show').removeClass('hidden');
              $('#video-play').addClass('hidden');
              $('#photo-show').attr("src", base_url + 'upload/photos/' + aData.img_url);
            }
            $("#video-dialog").modal("show");

        });

        table.on('click', '.block', function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            change_status(aData.id, 0);
        });

        table.on('click', '.accept', function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            change_status(aData.id);
        });

        $('.btn_accept').click(function() {      
            var id = $('#m_form input[name="id"]').val()
            change_status(id);
        });

        $('#pendding_filter').change(function() {      
            oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
        });

    }();

  });
</script>