<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <label>
            <input type="checkbox" id="pendding_filter" checked=""> PENDING UPDATES
          </label>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="m_table" class="table table-bordered table-hover"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<!-- DataTables -->
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">
  var oTable;

  function change_status(id, status=1) {
    var dialog = bootbox.dialog({
        title: 'Confirmation',
        message: "<h4>Are You Sure Want to " + (status == 1 ? 'release' : 'block') + "?</h4>" + "<label><input type='checkbox' id='sms_confirm' checked>   After release sms confirm</label>",
        size: 'small',
        buttons: {
          cancel: {
            label: "Cancel",
            className: 'btn-danger',
            callback: function(){
              dialog.modal('hide');
            }
          },
          ok: {
            label: "OK",
            className: 'btn-success',
            callback: function() {
              $.post(base_url + 'admin/company/change_status_ajax', {id: id, status: status, sms_status: $('#sms_confirm')[0].checked}, function(data, sts){
                if (data.success) {
                    success_message('Successfully ' + (status == 1 ? 'release' : 'block') + 'ed !');
                    oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                } else {
                    error_message('Failed');
                }
              });
            }
          }
        }
    });
  }

  $(function () {

    $('input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_minimal-blue'
    })

    $('input[type="checkbox"]').on('ifChanged', function(e) {
        $(this).val(this.checked ? 1 : 0);
        $(this).trigger('change');
    });

    var handleTable = function () {
        var nEditing = null;

        var table = $('#m_table');

        oTable = table.dataTable({
            "bServerSide": true,
            "bProcessing": true,
            "bDeferRender": true,
            "bAutoWidth": false,
            "aoColumns": [
              {
                "sTitle" : "No", 
                "mData": "index", 
                // mRender: function (data, type, row, pos) {
                //   return Number(pos.row)+1;
                // },
                "sWidth" : 30
              },
              {
                "sTitle" : "Company", 
                "mData": "company_name_fr",
                "sWidth" : 120
              },
              {
                "sTitle" : "Contact Person", 
                "mData": "contact_person",
                "sWidth" : 120
              },
              {
                "sTitle" : "City", 
                "mData": "city_name",
                "sWidth" : 120
              },
              {
                "sTitle" : "Category", 
                "mData": "name_en",
                "sWidth" : 120
              },
              {
                "sTitle" : "Email", 
                "mData": "email",
                "sWidth" : 150
              },
              {
                "sTitle" : "Language", 
                "mData": "language",
                mRender: function (data, type, row, pos) {
                  var langs = [];
                  if (data != '' && data != null) {
                    langs = data.split(',');
                  }
                  var html = '';
                  for (i =0 ; i < langs.length; i++ ) {
                    html += '<img src="' + base_url + 'assets/madinma/front/images/flag_' + langs[i] + '.png" class="img-circle" width="25" height="25" style="margin: 0 2px;"/>';
                  }
                  return html;
                },
                "sWidth" : 150
              }, {
                'sTitle': 'IpAddress',
                'mData': 'ip_addr',
                'sWidth': 100
              }, {
                "sTitle" : "Updated At", 
                "mData": "updated_at",
                "sWidth" : 100
              },
              {
                "sTitle" : "Action",
                "bSearchable": false,
                "bSortable": false,
                mRender: function (data, type, row) {
                  return '<a href="' + base_url + 'home/myaccount/' + row.id + '" class="fa fa-edit" title="Edit" style="cursor: pointer;" target="_blank"></a> \
                          <a href="' + base_url + 'preview/' + row.id + '" class="fa fa-eye" title="Preview" style="cursor: pointer;" target="_blank"></a> \
                          <a class="fa fa-trash delete" title="Delete" style="cursor: pointer;"></a> \
                          <a class="fa ' + (row.status == 0 ? 'fa-check-circle-o' : 'fa-lock') + ' ' + (row.status == 0 ? 'release' : 'block') + '" title="' + (row.status == 0 ? 'Release' : 'Block') + '" style="cursor: pointer;"></a>';
                },
                "sWidth" : 200
              }
            ],
            "sAjaxSource": base_url + 'admin/company/datatable_read_ajax',
            "sAjaxDataProp": "data",
            "fnServerParams": function (aoData) {
              if ($('#pendding_filter').is(':checked')) {
                aoData.push({"name": "filter[status]", "value": 0});
              }
            },
            "fnServerData": function (sSource, aoData, fnCallback){
                $.ajax({
                    "dataType": "json", 
                    "type": "POST", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                });
            },
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "ALL"] // change per page values here
            ],
            "pageLength": 10, // set the initial value,
            columnDefs: [{
                orderable: true,
                targets: [1]
            }],
            "order": [
                [6, "desc"]
            ]
        });

        $(window).resize(function() {
            oTable.fnAdjustColumnSizing();
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();
    
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var dialog = bootbox.dialog({
                title: 'Confirmation',
                message: "<h4>Are You Sure Want to delete?</h4>",
                size: 'small',
                buttons: {
                  cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function(){
                      dialog.modal('hide');
                    }
                  },
                  ok: {
                    label: "OK",
                    className: 'btn-success',
                    callback: function() {
                      $.post(base_url + 'admin/company/company_delete_ajax', {ids: aData.id}, function(data, status){
                          if (data.success) {
                              success_message('Successfully removed!');
                              oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                          } else {
                              error_message('Failed');
                          }
                      });
                    }
                  }
                }
            });
        });

        table.on('click', '.block', function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            change_status(aData.id, 0);
        });

        table.on('click', '.release', function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            change_status(aData.id);
        });

        $('#pendding_filter').change(function() {      
            oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
        });

    }();

  });
</script>
