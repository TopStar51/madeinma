<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">

<style>
    .icon-panel {
        max-height: 300px;
        overflow-y: scroll;
    }
    .icon-panel .svg-wrap {
        width: 100px;
        height: 100px;
        float: left;
        border: solid 2px #00652d;
        margin: 5px;
        cursor: pointer;
    }
    .icon-panel .svg-wrap svg {
        width: 100px;
        height: 100px;
    }
    .icon-panel .svg-wrap svg path {
        fill: #00652d;
    }
    .icon-panel .svg-wrap:hover, .icon-panel .svg-wrap.active {
        border: solid 2px #D20023;
    }
    .icon-panel .svg-wrap:hover svg path, .icon-panel .svg-wrap.active svg path {
        fill: #D20023;
    }
</style>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <div class="col-md-2">
                <button type="button" id="btn_add" class="btn btn-block btn-primary" data-toggle="modal" data-target="#m_modal">ADD</button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="m_table" class="table table-bordered table-hover"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<!-- m_modal -->
<div id="m_modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Category</h5>
            </div>
            <form id="m_form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <?php $langs = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu'); ?>
                            <?php foreach ($langs as $key => $lang): ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Name (<?php echo lang('general.lang.'.$lang) ?>)</label>
                                    <input type="text" name="name_<?php echo $lang; ?>" placeholder="Name (<?php echo lang('general.lang.'.$lang) ?>)" class="form-control"/>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Image</label>
                            </div>
                            <div class="col-sm-12 icon-panel">
                                <?php
                                    $d = dir("./assets/icons_svg");
                                    while (false !== ($entry = $d->read())) {
                                        if (is_file($d->path . '/' . $entry)) {
                                            echo '<div class="svg-wrap" data-url="' . $entry . '">' . file_get_contents($d->path . '/' . $entry) . "</div>";
                                        }
                                    }
                                    $d->close();
                                ?> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" id="btns">
                    <button type="button" onclick="javascript:save();" class="btn btn-primary btn-sm">Save</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">
  var oTable;

  $(function () {

    $('.svg-wrap').click(function() {
        $('.svg-wrap').removeClass('active');
        $(this).addClass('active');
    })

    $('#btn_add').click(function() {
        $("#m_form")[0].reset();
        $('.svg-wrap').removeClass('active');
    })

    var handleTable = function () {
          
        function editRow(oTable, nRow) {

            $("#m_form")[0].reset();
            $('.svg-wrap').removeClass('active');

            if (oTable != null && nRow != null) {
                var aData = oTable.fnGetData(nRow);
                $('#m_form input[name="id"]').val(aData.id);
                $('#m_form input[name="name_en"]').val(aData.name_en);
                $('#m_form input[name="name_fr"]').val(aData.name_fr);
                $('#m_form input[name="name_it"]').val(aData.name_it);
                $('#m_form input[name="name_es"]').val(aData.name_es);
                $('#m_form input[name="name_de"]').val(aData.name_de);
                $('#m_form input[name="name_ar"]').val(aData.name_ar);
                $('#m_form input[name="name_cn"]').val(aData.name_cn);
                if (aData.symbol_url == '' || aData.symbol_url == null) {
                    $('#m_form #preview_img').attr('src', base_url + 'assets/madinma/back/images/default_photo.png');
                } else {
                    $('.svg-wrap[data-url="' + aData.symbol_url + '"]').addClass('active');
                }
            }
           
            $("#m_modal").modal('show');
        }

        var table = $('#m_table');

        oTable = table.dataTable({
            "bServerSide": true,
            "bProcessing": true,
            "bDeferRender": true,
            "bAutoWidth": false,
            "aoColumns": [
              {
                "sTitle" : "No", 
                "mData": "index", 
                // mRender: function (data, type, row, pos) {
                //   return Number(pos.row)+1;
                // },
                "sWidth" : 30
              },
              {
                "sTitle" : "Image", 
                "mData": "symbol_url",
                mRender: function (data, type, row, pos) {
                    if (data == '' || data == null) {
                        return '<img src="' + base_url + 'assets/madinma/back/images/default_photo.png' + '" class="img-thumbnail" style="height:40px !important;"/>';   
                    }
                    return '<img src="' + base_url + 'assets/icons_svg/' + data + '" class="img-thumbnail" style="height:40px !important;"/>';
                },
                "sWidth" : 80
              },
              {
                "sTitle" : "Name (English)", 
                "mData": "name_en",
                "sWidth" : 150
              },
              {
                "sTitle" : "Name (French)", 
                "mData": "name_fr",
                "sWidth" : 150
              },
              {
                "sTitle" : "Name (Italian)", 
                "mData": "name_it",
                "sWidth" : 150
              },
              {
                "sTitle" : "Name (Spanish)", 
                "mData": "name_es",
                "sWidth" : 150
              },
              {
                "sTitle" : "Name (Germen)", 
                "mData": "name_de",
                "sWidth" : 150
              },
              {
                "sTitle" : "Name (Arabic)", 
                "mData": "name_ar",
                "sWidth" : 150
              },
              {
                "sTitle" : "Sort", 
                "mData": "sort",
                mRender: function (data, type, row) {
                  return '<input type="number" class="sort_input" value="' + (data==null ? '' : data) + '"/>';
                },
                "sWidth" : 80
              },
              {
                "sTitle" : "Action",
                "bSearchable": false,
                "bSortable": false,
                mRender: function (data, type, row) {
                  return '<a class="fa fa-edit edit" title="Edit" style="cursor: pointer;"></a> \
                          <a class="fa fa-trash delete" title="Delete" style="cursor: pointer;"></a>';
                },
                "sWidth" : 200
              }
            ],
            "sAjaxSource": base_url + 'admin/category/datatable_read_ajax',
            "sAjaxDataProp": "data",
            "fnServerParams": function (aoData) {
            },
            "fnServerData": function (sSource, aoData, fnCallback){
                $.ajax({
                    "dataType": "json", 
                    "type": "POST", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                });
            },
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "ALL"] // change per page values here
            ],
            "pageLength": 10, // set the initial value,
            columnDefs: [{
                orderable: true,
                targets: [1]
            }],
            "order": [
                [3, "asc"]
            ]
        });

        $(window).resize(function() {
            oTable.fnAdjustColumnSizing();
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();
    
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var dialog = bootbox.dialog({
                title: 'Confirmation',
                message: "<h4>Are You Sure Want to delete?</h4>",
                size: 'small',
                buttons: {
                  cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function(){
                      dialog.modal('hide');
                    }
                  },
                  ok: {
                    label: "OK",
                    className: 'btn-success',
                    callback: function() {
                      $.post(base_url + 'admin/category/category_delete_ajax', {ids: aData.id}, function(data, status){
                          if (data.success) {
                              success_message('Successfully removed!');
                              oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                          } else {
                              error_message('Failed');
                          }
                      });
                    }
                  }
                }
            });
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            editRow(oTable, nRow);
        });

        table.on('change', '.sort_input', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            $.post(base_url + 'admin/category/change_sort_ajax', {id: aData.id, sort: $(this).val()}, function(data, sts){

            });
        });

    }();

  });

function save() {
    var A = new FormData();

    if ($('[name=name_en]').val() == '' && $('[name=name_fr]').val() == '' && $('[name=name_it]').val() == '' && 
        $('[name=name_es]').val() == '' && $('[name=name_de]').val() == '' && $('[name=name_ar]').val() == '') {
        bootbox.dialog({
            title: "Warning",
            message: "Please input name.",
            size: 'small',
            buttons: {
                ok: {
                    label: "OK",
                    className: 'btn-success',
                }
            }
        });
        return;
    }

    if ($('.svg-wrap.active').length == 1) {
        A.append('symbol_url', $('.svg-wrap.active').attr('data-url'));
    } else {
        bootbox.dialog({
            title: "Warning",
            message: "Please select an image.",
            size: 'small',
            buttons: {
                ok: {
                    label: "OK",
                    className: 'btn-success',
                }
            }
        });
        return;
    }

    var form = $("#m_form")[0];
    for (i=0; i<form.length; i++) {
        if (form[i].name != '' && form[i].name != "image") {
            A.append(form[i].name, form[i].value);
        }
    }
    
    var C = new XMLHttpRequest();
    C.open("POST", base_url + 'admin/category/post_category');
    C.onload = function() {
        var data = JSON.parse(C.response);
        if (data.success) {
            bootbox.dialog({
                title: "Success",
                message: "Successfully saved!",
                size: 'small',
                buttons: {
                    ok: {
                        label: "OK",
                        className: 'btn-success',
                        callback: function() {
                            $("#m_form")[0].reset();
                            oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                            $('#m_modal').modal('hide');
                        }
                    }
                }
            });
        } else {
            bootbox.dialog({
                title: "Error",
                message: "Failed",
                size: 'small',
                buttons: {
                    ok: {
                        label: "OK",
                        className: 'btn-error'
                    }
                }
            });
        }
    };
    C.send(A);
}
</script>
