<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">
<style>
.lang-text {
  min-width: 100px;
}
.lang-items li.active a {
  background-color: #3c8dbc !important;
  color: white;
}  
</style>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class = "col-xs-12">
      <div class = "box">
        <div class = "box-header">
          <div class = "col-md-4 pull-left">
            <div class = "btn-group">
              <button type = "button" class="btn btn-default lang-text">Language</button>
              <button type = "button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu lang-items" role="menu">
                <li data-lang="*" class="active"><a href="javascript:;">All</a></li>
                <?php $lang_arr = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir'); ?>
                <?php foreach ($lang_arr as $lang): ?>
                  <li data-lang = "<?php echo $lang; ?>"><a href="javascript:;"><?php echo lang('general.lang.'.$lang); ?></a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
          <div class="col-md-2 pull-right">
              <button type="button" id="btn_add" class="btn btn-block btn-primary" data-toggle="modal" data-target="#m_modal">ADD</button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="m_table" class="table table-bordered table-hover"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<!-- m_modal -->
<div id="m_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Keyword</h5>
            </div>
            <form id="m_form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id">
                <div class="modal-body">
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="form-group">
                              <label>Text</label>
                              <input type="text" name="text" class="form-control">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <?php $langs = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn', 'tu');//, 'ir'); ?>
                      <div class="col-sm-12">
                          <div class="form-group">
                              <label>Language</label>
                              <select class="form-control" name="lang">
                                <?php foreach ($langs as $key => $lang): ?>
                                <option value="<?php echo $lang; ?>"><?php echo lang('general.lang.'.$lang) ?></option>
                                <?php endforeach; ?>
                              </select>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="modal-footer" id="btns">
                    <button type="button" onclick="javascript:save();" class="btn btn-primary btn-sm">Save</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">

  $('#btn_add').click(function() {
      $("#m_form")[0].reset();
      var lang = $('.lang-items li.active').attr('data-lang');
      if (lang != '*') {
        $('#m_modal [name="lang"]').val(lang);
      }
  });

  var oTable;
  var oTable_group;

  $(function () {

    var handleTable = function () {

        function editRow(oTable, nRow) {

            $("#m_form")[0].reset();

            if (oTable != null && nRow != null) {
                var aData = oTable.fnGetData(nRow);
                $('#m_form [name="id"]').val(aData.id);
                $('#m_form [name="text"]').val(aData.text);
                $('#m_form [name="lang"]').val(aData.lang);
            }
           
            $("#m_modal").modal('show');
        }

        var table = $('#m_table');

        oTable = table.dataTable({
            "bServerSide": true,
            "bProcessing": true,
            "bDeferRender": true,
            "bAutoWidth": false,
            "aoColumns": [
              {
                "sTitle" : "No", 
                "mData": "index", 
                // mRender: function (data, type, row, pos) {
                //   return Number(pos.row)+1;
                // },
                "sWidth" : 30
              },
              {
                "sTitle" : "Keywords", 
                "mData": "text",
                "sWidth" : 200
              },
              {
                "sTitle" : "Language",
                "mData": "lang",
                mRender: function (data, type, row, pos) {
                  if (data == '' || data == null) {
                      return '';
                  }
                  return '<img src="' + base_url + 'assets/madinma/front/images/flag_' + data + '.png" class="img-circle" width="25" height="25" style="margin: 0 2px;"/>';
                },
                "sWidth" : 150
              },
              {
                "sTitle" : "Action",
                "bSearchable": false,
                "bSortable": false,
                mRender: function (data, type, row) {
                  return '<a class="fa fa-edit edit" title="Edit" style="cursor: pointer;"></a> \
                          <a class="fa fa-trash delete" title="Delete" style="cursor: pointer;"></a>';
                },
                "sWidth" : 200
              }
            ],
            "sAjaxSource": base_url + 'admin/keyword/datatable_read_ajax',
            "sAjaxDataProp": "data",
            "fnServerParams": function (aoData) {
              aoData.push({"name": "filter[allowed]", "value": 1});
              var lang = $('.lang-items li.active').attr('data-lang');
              if (lang != '*') {
                aoData.push({"name": "filter[lang]", "value": lang});
              }
            },
            "fnServerData": function (sSource, aoData, fnCallback){
                $.ajax({
                    "dataType": "json", 
                    "type": "POST", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                });
            },
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "ALL"] // change per page values here
            ],
            "pageLength": 10, // set the initial value,
            columnDefs: [{
                orderable: true,
                targets: [1]
            }],
            "order": [
                [1, "desc"]
            ]
        });

        $(window).resize(function() {
            oTable.fnAdjustColumnSizing();
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();
    
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var dialog = bootbox.dialog({
                title: 'Confirmation',
                message: "<h4>Are You Sure Want to delete?</h4>",
                size: 'small',
                buttons: {
                  cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function(){
                      dialog.modal('hide');
                    }
                  },
                  ok: {
                    label: "OK",
                    className: 'btn-success',
                    callback: function() {
                      $.post(base_url + 'admin/keyword/keyword_delete_ajax', {ids: aData.id}, function(data, status){
                          if (data.success) {
                              success_message('Successfully removed!');
                              oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                          } else {
                              error_message('Failed');
                          }
                      });
                    }
                  }
                }
            });
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            editRow(oTable, nRow);
        });

    }();

    $('.lang-items li').click(function() {
      $('.lang-items li').removeClass('active');
      $(this).addClass('active');
      $('.lang-text').html($(this).find('a').html());
      var lang = $(this).attr('data-lang');
      oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
    });

  });

function save() {
    var A = new FormData();

    if ($('[name=text]').val() == '') {
        bootbox.dialog({
            title: "Warning",
            message: "Please input text.",
            size: 'small',
            buttons: {
                ok: {
                    label: "OK",
                    className: 'btn-success',
                }
            }
        });
        return;
    }
    var form = $("#m_form")[0];
    for (i=0; i<form.length; i++) {
        if (form[i].name != '' && form[i].name != "image") {
            A.append(form[i].name, form[i].value);
        }
    }
    
    var C = new XMLHttpRequest();
    C.open("POST", base_url + 'admin/keyword/post_keyword');
    C.onload = function() {
        var data = JSON.parse(C.response);
        if (data.success) {
            bootbox.dialog({
                title: "Success",
                message: "Successfully saved!",
                size: 'small',
                buttons: {
                    ok: {
                        label: "OK",
                        className: 'btn-success',
                        callback: function() {
                            $("#m_form")[0].reset();
                            oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                            $('#m_modal').modal('hide');
                        }
                    }
                }
            });
        } else {
            bootbox.dialog({
                title: "Error",
                message: "Failed",
                size: 'small',
                buttons: {
                    ok: {
                        label: "OK",
                        className: 'btn-error'
                    }
                }
            });
        }
    };
    C.send(A);
}
</script>
