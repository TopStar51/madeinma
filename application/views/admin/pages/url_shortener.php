<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <div class="col-md-2 pull-right">
              <button type="button" id="btn_add" class="btn btn-block btn-primary" data-toggle="modal" data-target="#m_modal">ADD</button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="m_table" class="table table-bordered table-hover"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<!-- m_modal -->
<div id="m_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">URL Shortener</h5>
            </div>
            <form id="m_form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id">
                <div class="modal-body">
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="form-group">
                              <label>Keyword</label>
                              <input type="text" name="keyword" class="form-control">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="form-group">
                              <label>URL</label>
                              <input type="text" name="company_url" class="form-control" placeholder="made-in.ma/categories/{name of category}/{title of company}" value="<?=base_url()?>">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="form-group">
                              <label>Shorten URL</label>
                              <input type="text" name="shorten_url" class="form-control" readonly value="<?=SHORTEN_URL_PREFIX?>">
                          </div>
                      </div>
                  </div>
                </div>
                <div class="modal-footer" id="btns">
                    <button type="button" onclick="javascript:save();" class="btn btn-primary btn-sm">Save</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">

  $('#btn_add').click(function() {
      $("#m_form input[name='id']").val('');
      $("#m_form")[0].reset();
  });

  var oTable;
  var shorten_url_prefix = "<?=SHORTEN_URL_PREFIX; ?>";

  $(function () {

    $('#m_form [name="keyword"]').keyup(function(){
      $('#m_form [name="shorten_url"]').val(shorten_url_prefix + $(this).val());
    });

    $('#m_form [name="keyword"]').keydown(function(){
      $('#m_form [name="shorten_url"]').val(shorten_url_prefix + $(this).val());
    });

    var handleTable = function () {

        function editRow(oTable, nRow) {

            $("#m_form")[0].reset();

            if (oTable != null && nRow != null) {
                var aData = oTable.fnGetData(nRow);
                $('#m_form [name="id"]').val(aData.id);
                $('#m_form [name="keyword"]').val(aData.keyword);
                $('#m_form [name="company_url"]').val(aData.company_url);
                $('#m_form [name="shorten_url"]').val(aData.shorten_url);
            }
           
            $("#m_modal").modal('show');
        }

        var table = $('#m_table');

        oTable = table.dataTable({
            "bServerSide": true,
            "bProcessing": true,
            "bDeferRender": true,
            "bAutoWidth": false,
            "aoColumns": [
              {
                "sTitle" : "No", 
                "mData": "index", 
                // mRender: function (data, type, row, pos) {
                //   return Number(pos.row)+1;
                // },
                "sWidth" : 30
              },
              {
                "sTitle" : "Keyword", 
                "mData": "keyword",
                "sWidth" : 60
              },
              {
                "sTitle" : "Company URL", 
                "mData": "company_url",
                 mRender: function (data, type, row, pos) {
                  if(data)
                  {
                    return '<a href="' + data + '" target="_blank">' + data + '</a>';
                  }
                  else
                  {
                    return '';
                  }
                },
                "sWidth" : 140
              },
              {
                "sTitle" : "Shorten URL", 
                "mData": "shorten_url",
                 mRender: function (data, type, row, pos) {
                  if(data)
                  {
                    return '<a href="' + data + '" target="_blank">' + data + '</a>';
                  }
                  else
                  {
                    return '';
                  }
                },
                "sWidth" : 100
              },
              {
                "sTitle" : "Created At", 
                "mData": "created_at",
                "sWidth" : 80
              },
              {
                "sTitle" : "Action",
                "bSearchable": false,
                "bSortable": false,
                mRender: function (data, type, row) {
                  return '<a class="fa fa-edit edit" title="Edit" style="cursor: pointer;"></a> \
                          <a class="fa fa-trash delete" title="Delete" style="cursor: pointer;"></a>';
                },
                "sWidth" : 80
              }
            ],
            "sAjaxSource": base_url + 'admin/url_shortener/datatable_read_ajax',
            "sAjaxDataProp": "data",
            "fnServerData": function (sSource, aoData, fnCallback){
                $.ajax({
                    "dataType": "json", 
                    "type": "POST", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                });
            },
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "ALL"] // change per page values here
            ],
            "pageLength": 10, // set the initial value,
            columnDefs: [{
                orderable: true,
                targets: [1]
            }],
            "order": [
                [3, "desc"]
            ]
        });

        $(window).resize(function() {
            oTable.fnAdjustColumnSizing();
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();
    
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var dialog = bootbox.dialog({
                title: 'Confirmation',
                message: "<h4>Are you sure you want to delete?</h4>",
                size: 'small',
                buttons: {
                  cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function(){
                      dialog.modal('hide');
                    }
                  },
                  ok: {
                    label: "OK",
                    className: 'btn-success',
                    callback: function() {
                      $.post(base_url + 'admin/url_shortener/delete_ajax', {ids: aData.id}, function(data, status){
                          if (data.success) {
                              success_message('Successfully removed!');
                              oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                          } else {
                              error_message('Failed');
                          }
                      });
                    }
                  }
                }
            });
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();
            var nRow = $(this).parents('tr')[0];
            editRow(oTable, nRow);
        });

    }();

  });

  function save() {
      var A = new FormData();

      if ($('[name=keyword]').val() == '') {
          bootbox.dialog({
              title: "Warning",
              message: "Please input keyword.",
              size: 'small',
              buttons: {
                  ok: {
                      label: "OK",
                      className: 'btn-success',
                  }
              }
          });
          return;
      }

      if ($('[name=company_url]').val() == '') {
          bootbox.dialog({
              title: "Warning",
              message: "Please input company profile url.",
              size: 'small',
              buttons: {
                  ok: {
                      label: "OK",
                      className: 'btn-success',
                  }
              }
          });
          return;
      }

      var form = $("#m_form")[0];
      for (i=0; i<form.length; i++) {
          if (form[i].name != '' && form[i].name != "image") {
              A.append(form[i].name, form[i].value);
          }
      }
      
      var C = new XMLHttpRequest();
      C.open("POST", base_url + 'admin/url_shortener/post_shorten_url');
      C.onload = function() {
          var data = JSON.parse(C.response);
          if (data.success) {
              bootbox.dialog({
                  title: "Success",
                  message: "Successfully saved!",
                  size: 'small',
                  buttons: {
                      ok: {
                          label: "OK",
                          className: 'btn-success',
                          callback: function() {
                              $("#m_form")[0].reset();
                              oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                              $('#m_modal').modal('hide');
                          }
                      }
                  }
              });
          } else {
              bootbox.dialog({
                  title: "Error",
                  message: "Failed",
                  size: 'small',
                  buttons: {
                      ok: {
                          label: "OK",
                          className: 'btn-error'
                      }
                  }
              });
          }
      };
      C.send(A);
  }
</script>
