<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">
<style>
.lang-text {
  min-width: 100px;
}
.lang-items li.active a {
  background-color: #3c8dbc !important;
  color: white;
}  
</style>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <div class="btn-group">
            <button type="button" class="btn btn-default lang-text">Language</button>
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <span class="caret"></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu lang-items" role="menu">
              <li data-lang="*" class="active"><a href="javascript:;">All</a></li>
              <?php $lang_arr = array('en', 'fr', 'it', 'es', 'de', 'ar', 'cn','tu');//, 'ir'); ?>
              <?php foreach ($lang_arr as $lang): ?>
                <li data-lang="<?php echo $lang; ?>"><a href="javascript:;"><?php echo lang('general.lang.'.$lang); ?></a></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="m_table" class="table table-bordered table-hover"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<!-- DataTables -->
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">

  var oTable;
  var oTable_group;

  $(function () {

    var handleTable = function () {

        var table = $('#m_table');

        oTable = table.dataTable({
            "bServerSide": true,
            "bProcessing": true,
            "bDeferRender": true,
            "bAutoWidth": false,
            "aoColumns": [
              {
                "sTitle" : "No", 
                "mData": "index", 
                // mRender: function (data, type, row, pos) {
                //   return Number(pos.row)+1;
                // },
                "sWidth" : 30
              },
              {
                "sTitle" : "Search Word", 
                "mData": "text",
                "sWidth" : 200
              },
              {
                "sTitle" : "IP Address", 
                "mData": "ip_addr",
                "sWidth" : 120
              },
              {
                "sTitle" : "Language",
                "mData": "lang",
                mRender: function (data, type, row, pos) {
                  if (data == '' || data == null) {
                      return '';
                  }
                  return '<img src="' + base_url + 'assets/madinma/front/images/flag_' + data + '.png" class="img-circle" width="25" height="25" style="margin: 0 2px;"/>';
                },
                "sWidth" : 150
              },
              {
                "sTitle" : "Created At", 
                "mData": "created_at",
                "sWidth" : 100
              },
              {
                "sTitle" : "Results Count", 
                "mData": "results_count",
                "sWidth" : 100
              },
              {
                "sTitle" : "Action",
                "bSearchable": false,
                "bSortable": false,
                mRender: function (data, type, row) {
                  return '<a class="fa fa-trash delete" title="Delete" style="cursor: pointer;"></a>';
                },
                "sWidth" : 200
              }
            ],
            "sAjaxSource": base_url + 'admin/stext/datatable_read_ajax',
            "sAjaxDataProp": "data",
            "fnServerParams": function (aoData) {
              aoData.push({"name": "filter[allowed]", "value": 0});
              var lang = $('.lang-items li.active').attr('data-lang');
              if (lang != '*') {
                aoData.push({"name": "filter[lang]", "value": lang});
              }
            },
            "fnServerData": function (sSource, aoData, fnCallback){
                $.ajax({
                    "dataType": "json", 
                    "type": "POST", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                });
            },
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "ALL"] // change per page values here
            ],
            "pageLength": 10, // set the initial value,
            columnDefs: [{
                orderable: true,
                targets: [1]
            }],
            "order": [
                [4, "desc"]
            ]
        });

        $(window).resize(function() {
            oTable.fnAdjustColumnSizing();
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();
    
            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            var dialog = bootbox.dialog({
                title: 'Confirmation',
                message: "<h4>Are You Sure Want to delete?</h4>",
                size: 'small',
                buttons: {
                  cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                    callback: function(){
                      dialog.modal('hide');
                    }
                  },
                  ok: {
                    label: "OK",
                    className: 'btn-success',
                    callback: function() {
                      $.post(base_url + 'admin/stext/stext_delete_ajax', {ids: aData.id}, function(data, status){
                          if (data.success) {
                              success_message('Successfully removed!');
                              oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
                          } else {
                              error_message('Failed');
                          }
                      });
                    }
                  }
                }
            });
        });

    }();

    $('.lang-items li').click(function() {
      $('.lang-items li').removeClass('active');
      $(this).addClass('active');
      $('.lang-text').html($(this).find('a').html());
      var lang = $(this).attr('data-lang');
      oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load(null, false);
    });

  });
</script>
