<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['/'] = "home/index";
$route['login'] = "login/index";
$route['login/post'] = "login/post_login";
$route['register'] = "register/index";
$route['register/post'] = "register/post_register";
$route['sms_verify/post'] = "sms_verify/post_code";
$route['logout'] = "home/logout";
$route['myaccount'] = "home/myaccount";
$route['company/post'] = "home/post_company";
$route['product/add'] = "home/product_image_add";
$route['product/remove'] = "home/product_image_remove";
$route['profile/post'] = "home/post_profile";
$route['favorites'] = 'home/get_companies//TRUE';
$route['favorite/post'] = "home/post_favorite";
$route['companies'] = "home/get_companies";
$route['mail/post'] = 'home/post_mail';
$route['support/post'] = 'home/post_support';
$route['rating/post'] = 'home/post_rating';
$route['call/post'] = "home/post_call";
$route['search-texts'] = "home/get_stexts";

$route['preview/post'] = "home/post_preview";
$route['preview'] = "home/get_preview";
$route['delete-account'] = "home/delete_account";
$route['forgot/post'] = "login/post_forgot";
$route['password/post'] = "login/post_password";
$route['info'] = "home/get_info";

$route['admin'] = "admin/login";
$route['admin/login/post'] = "admin/login/post_login";

$route['sitemap\.xml'] = "sitemap/index";

$route['categories/favorites/(:any)'] = 'home/get_companies/favorites/$1';
$route['categories/(:any)'] = 'home/get_companies/$1';
$route['categories/(:any)/(:any)'] = 'home/company_detail/$2';
// $route['company_detail/(:any)'] = 'home/company_detail/$1';
$route['cities/(:any)'] = "home/get_companies///$1";
$route['companies/(:any)'] = 'home/company_detail/$1';

$route['lang/(:any)'] = 'home/change_lang/$1';
$route['layout/(:any)'] = "home/change_layout/$1";
$route['preview/(:any)'] = "home/get_preview/$1";
$route['confirm-user/(:any)'] = "login/confirm_user/$1";
$route['phone_num/(:any)'] = "home/get_phone/$1";
$route['go/(:any)'] = "home/go/$1";
$route['change_preview_lang/(:any)'] = "home/change_preview_lang/$1";
