<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['site.description'] = "Find manufacturer, supplier and service companies in Morocco";
$lang['site.title'] = "made-in.ma - The online marketplace for Moroccan companies";
$lang['site.keywords'] = "INETTECH, INETTECH.IO, made-in.ma, Made in Morocco, Manufacturer, Suppliers, Service Companies, B2B, B2B-Plattform, Export, Import, Products";

#register
$lang['header.register'] = 'Register';

#message
$lang['general.lang.en'] = "English";
$lang['general.lang.fr'] = "Français";
$lang['general.lang.it'] = "Italiano";
$lang['general.lang.es'] = "Español";
$lang['general.lang.de'] = "Deutsch";
$lang['general.lang.ar'] = "العربية";
$lang['general.lang.cn'] = "中國";
$lang['general.lang.tu'] = "Türkçe";
// $lang['general.lang.ir'] = "Farsi";

#message
$lang['message.success'] = "Success";
$lang['message.error'] = "Error";
$lang['message.warning'] = "Warning";
$lang['message.ok'] = "OK";
$lang['message.cancel'] = "Cancel";
$lang['message.signin'] = "Sign In";
$lang['message.signup'] = "Sign Up";
$lang['message.signout'] = "Sign out";
$lang['message.save'] = "Save";
$lang['message.mail'] = "Mail";
$lang['message.rating'] = "Rating";
$lang['message.call'] = "Call";
$lang['message.save_success'] = "Need check before release please wait...";
$lang['message.confirmation'] = "Confirmation";
$lang['message.new_password'] = "Please input new password";
$lang['message.recaptacha_error'] = "Sorry. Google Recaptcha Unsuccessful!";
$lang['message.phone_duplicate'] = 'Phone no already registered.';
$lang['message.sms_error'] = 'Phone number wrong';
$lang['message.label.name'] = "name";
$lang['message.label.email'] = "email";
$lang['message.label.phone'] = 'phone';
$lang['message.label.phone'] = "phone";
$lang['message.label.country'] = "country";
$lang['message.label.language'] = "language";
$lang['message.label.content'] = "content";
$lang['message.label.created_at'] = "created at";
$lang['message.company_error'] = "Company name is already registered.";

#validation
$lang['validation.user.name.salutation'] = "Please enter salutation.";
$lang['validation.user.name.require'] = "Please enter your name.";
$lang['validation.user.email.require'] = "Please enter your email.";
$lang['validation.user.phone.require'] = 'Please enter your phone number.';
$lang['validation.user.email.valid'] = "Please enter a valid email address.";
$lang['validation.user.email.unique'] = "This email address is already registered. <br>Please use a different email address.";
$lang['validation.user.phone.unique'] = "This phone number is already registered. <br>Please use a different phone number.";
$lang['validation.user.password.require'] = "Please enter your password.";
$lang['validation.user.confirm_password.require'] = "Please enter a your confirm password.";
$lang['validation.user.confirm_password.valid'] = "Your password and confirmation password do not match.";
$lang['validation.user.new_password.require'] = "Please enter your new password.";
$lang['validation.user.current_password.require'] = "Please enter your current password.";
$lang['validation.user.sms_code.require'] = "Please input your sms code.";

$lang['validation.company.name.require'] = "Please enter your company name.";
$lang['validation.company.salutation.require'] = "Please enter salutation.";
$lang['validation.company.contact_person.require'] = "Please enter contact person.";
$lang['validation.company.street.require'] = "Please enter street.";
$lang['validation.company.city.require'] = "Please select city.";
$lang['validation.company.founded_at.require'] = "Please enter founded year.";
$lang['validation.company.email.require'] = "Please enter email.";
$lang['validation.company.zipcode.require'] = "Please enter zipcode.";
$lang['validation.company.phone.require'] = "Please enter phone.";
$lang['validation.company.website.require'] = "Please enter website.";
$lang['validation.company.keywords.require'] = "Please enter keywords.";
$lang['validation.company.lang.require'] = "Please select language.";
$lang['validation.company.employee_count.require'] = "Please select number of employees.";
$lang['validation.company.category.require'] = "Please select category.";

#user
$lang['user.name'] = "Name";
$lang['user.email'] = "Email";
$lang['user.phone'] = 'Mobile Phone';
$lang['user.password'] = "Password";
$lang['user.confirm_password'] = "Confirm password";
$lang['user.sms_code'] = 'Code';
$lang['user.promotion_code'] = 'Promotion code (optional)';

#placeholder
$lang['user.phone.flag'] = 'assets/madinma/front/images/1.png';
$lang['user.phone.placeholder'] = '650123456';
$lang['phone.placeholder'] = '+212...';
$lang['promo.placeholder'] = 'A12B';

#company setting labels
$lang['company.logo'] = "Company Logo or Shop Image";
$lang['company.name'] = "Companyname or Title";
$lang['company.salutation'] = "Salutation";
$lang['company.contact_person'] = "Contact person";
$lang['company.street'] = "Street";
$lang['company.city'] = "City";
$lang['company.founded_at'] = "Founded at";
$lang['company.employee_count'] = "Number of employees";
$lang['company.zipcode'] = "Zip code";
$lang['company.phone'] = "Phone";
$lang['company.email'] = "Email";
$lang['company.website'] = "Website or Facebooksite";
$lang['company.youtube'] = "Youtube Video";
$lang['company.keywords'] = "Keywords";
$lang['company.about_us'] = "About us";
$lang['company.category'] = "Category";
$lang['company.language'] = "Spoken Languages";
$lang['company.latitude'] = "Latitude";
$lang['company.longitude'] = "Longitude";

#company setting placeholders
$lang['company.logo.placeholder'] = "Company Logo or Shop Image";
$lang['company.name.placeholder'] = "Ameublement el Bahja";
$lang['company.salutation.placeholder.mr'] = "Mr";
$lang['company.salutation.placeholder.ms'] = "Ms";
$lang['company.contact_person.placeholder'] = "Mohamed Kamal";
$lang['company.street.placeholder'] = "138 Boulevard Moulay Youssef, Angle Rue Tiznit";
$lang['company.founded_at.placeholder'] = "2008";
$lang['company.zipcode.placeholder'] = "20240";
$lang['company.phone.placeholder'] = "+212...";
$lang['company.email.placeholder'] = "your@email.com";
$lang['company.website.placeholder'] = "https://www.yourwebsite.com";
$lang['company.youtube.placeholder'] = "https://www.youtube.com/watch?v=iwa7_SHf114";
$lang['company.keywords.placeholder'] = "Arganoil, Argan...";
$lang['company.about_us.placeholder'] = "Please write here a little text about your company...";
$lang['company.lang.non.selected.placeholder'] = "Nothing selected";
$lang['company.latitude.placeholder'] = "33.333333";
$lang['company.longitude.placeholder'] = "43.333333";

#top-search
$lang['topsearch.search'] = "SEARCH";
$lang['topsearch.placeholder'] = "What are you looking for...";
$lang['topsearch.noresult'] = "No result please remove some filters and try again...";
$lang['topsearch.reset'] = "Reset Filters";

#sidebar
$lang['sidebar.title'] = ""; // Categories

#footer
$lang['footer.about_title'] = "About made-in.ma";
$lang['footer.about_content'] = 'made-in.ma is an online marketplace that aims to strengthen the export and domestic trade of smaller companies in Morocco. Manufacturers, suppliers and service providers can present themselves here to international and national customers. By creating a free company profile.<br><br>This service is provided by <a href="https://www.inettech.io" title="">INETTECH.io</a><br><br><a href="https://www.youtube.com/channel/UCAqJdNAA1onUmm7KZo4qvqg" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_youtube.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/madein.ma/" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_instagram.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://fb.me/wwwmadeinma" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_facebook.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href=" https://wa.me/212688217862" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_whatsapp.png" alt="" height="35" width="35"></a>';
$lang['footer.contact_title'] = "Contact and Cooperation";
$lang['footer.contact_content'] = "Please contact us by mail";
$lang['footer.mail'] = "Mail";
$lang['footer.statistics_title'] = "Statistics";
$lang['footer.statistics_comapny'] = "Companies listed";
$lang['footer.statistics_comapny_week'] = "New this week";
$lang['footer.statistics_inquery'] = "Inquery total";
$lang['footer.statistics_inquery_week'] = "Inquery this week";

#modals
$lang['modal.contact.title'] = "Contact";
$lang['modal.rating.title'] = "Rating";
$lang['modal.call.title'] = "Call";
$lang['modal.btn.send'] = "Send";
$lang['modal.placeholder.name'] = "Your name";
$lang['modal.placeholder.email'] = "Your email";
$lang['modal.placeholder.phone'] = "+212XXXXXXXX";
$lang['modal.placeholder.text'] = "Please write here the text for us";
$lang['modal.placeholder.company_detail'] = "Please write here your text to the company";
$lang['modal.placeholder.rating_detail'] = "Please write here your comment to this company";
$lang['modal.your_rating'] = "Your rating";
$lang['modal.call_desc'] = "Tell this company you found him on made-in.ma. Thank you!";
$lang['modal.confirm.title'] = "Confirm";
$lang['modal.confirm.question'] = "Confirm your phone number!";
$lang['modal.lang.none.selected'] = 'Nothing selected';
$lang['modal.option.email'] = "Email";
$lang['modal.option.phone'] = "Phone";
$lang['modal.option.all'] = "All";

#startpage
$lang['startpage.new'] = "New Manufacturer";
$lang['startpage.week'] = "Manufacturer of the week";

#signin
$lang['signin.remember'] = "Remember Me";
$lang['signin.invalid'] = "Invalid credentials";
$lang['signin.blocked'] = "Your account was blocked.";
$lang['signin.back'] = "Back";
$lang['signin.submit'] = "Submit";
$lang['signin.forgot'] = "Forget Password ?";

#signup
$lang['signup.desc'] = "Register here on made-in.ma as manufacturers, suppliers and service providers free of charge and without obligation. You can advertise your products and services nationally and internationally.";

#sms verification
$lang['sms.desc'] = "Please enter your sms verification code.";
$lang['sms.code.error'] = "Invalid code";
$lang['sms.expire'] = "Your code was expired. Please try to register again.";

#myacount
$lang['myaccount.company_profile'] = "company profile";
$lang['myaccount.title'] = "My account";
$lang['myaccount.company_information'] = "company information";
$lang['myaccount.logo_note'] = "Your company logo. Pictures larger than pixels will be scaled down.";
$lang['myaccount.avatar_note'] = "Your virtual face or picture. Pictures larger than pixels will be scaled down.";
$lang['myaccount.company_product'] = "company product";
$lang['myaccount.general_information'] = "general information";
$lang['myaccount.change_password'] = "change password";
$lang['myaccount.current_password'] = "current password";
$lang['myaccount.promotion'] = 'promotion';
$lang['myaccount.promotion.desc'] = 'Recommended made-in.ma friends and acquaintances. You will receive 20 dirhams for each registered company registration that you enter using your promotion code. A payout is possible rom 100 registrations. The payment is made via Western Union.';
$lang['myaccount.promotion.promotion_code'] = 'Your Promotion Code';
$lang['myaccount.promotion.promotion_link'] = 'Promotion Link';
$lang['myaccount.promotion.current_promotions'] = 'Current Promotions';
$lang['myaccount.new_password'] = "New password";
$lang['myaccount.select'] = "select";
$lang['myaccount.logo'] = "Logo";
$lang['myaccount.contact_avatar'] = "Photo of contact person";
$lang['myaccount.product_warning'] = "Please add a product.";
$lang['myaccount.preview'] = "Preview";
$lang['myaccount.del_account'] = "DELETE ACCOUNT";
$lang['myaccount.del_account_msg'] = "Are You Sure Want to delete?";
$lang['myaccount.messages'] = "Messages";
$lang['myaccount.support'] = "Support";

#companies
$lang['companies.all'] = "All";
$lang['companies.sortby'] = "Sort By";
$lang['companies.rating'] = "Rating";
$lang['companies.newest'] = "Newest";
$lang['companies.oldest'] = "Oldest";
$lang['companies.entries'] = "entries";
$lang['companies.in'] = " in ";

#company detail
$lang['detail.since'] = "Since";
$lang['detail.employees'] = "employees";
$lang['detail.date'] = "Date";
$lang['detail.name'] = "Name";
$lang['detail.description'] = "Description";
$lang['detail.noproduct'] = "There is no product.";
$lang['detail.product.pics'] = 'Product Pictures';
$lang['detail.last.update'] = 'Last Update';
$lang['detail.rating'] = 'Rating';
$lang['detail.youtube'] = 'Video';
$lang['detail.map'] = 'Location';

#rating modal
$lang['rating.name'] = "Name";
$lang['rating.country'] = "Country";
$lang['rating.text'] = "Text";
$lang['no.ratings'] = "No ratings";

#company profile
$lang['profile.since'] = "Since";
$lang['profile.employees'] = "employees";

#forgot
$lang['forgot.no_email'] = "No such email address exists.";
$lang['forgot.no_phone'] = "No such phone exists.";
$lang['forgot.success_email'] = "Reset link sent your email address. Please check your junk mail folder.";
$lang['forgot.success_phone'] = "Reset link sent to your phone";
$lang['forgot.invalid_token'] = "Invalid token.";
$lang['forgot.update_success'] = "Password is successfully updated.";
$lang['forgot.expire'] = "Your expire time is passed.";

#contact sms message
$lang['contact.sms'] = 'Please sign in made-in.ma. You have a new message.';
$lang['contact.sms.success'] = 'Your message was sent to the manufacturer successfully.';
$lang['contact.sms.error'] = 'Your message was not sent because of sms problem';

#phone number invalid message
$lang['phone.error.title'] = "Error";
$lang['phone.invalid'] = "Your phone number is invalid. The count of digits should be 9 and do not start with '0' at first";

#category info
$lang['categoryInfo.imgURL.26'] = "10.jpg";
$lang['categoryInfo.detail.26'] = "";

$lang['categoryInfo.imgURL.25'] = "10.jpg";
$lang['categoryInfo.detail.25'] = "";

$lang['categoryInfo.imgURL.24'] = "10.jpg";
$lang['categoryInfo.detail.24'] = "";

$lang['categoryInfo.imgURL.23'] = "10.jpg";
$lang['categoryInfo.detail.23'] = "";

$lang['categoryInfo.imgURL.22'] = "10.jpg";
$lang['categoryInfo.detail.22'] = "";

$lang['categoryInfo.imgURL.21'] = "10.jpg";
$lang['categoryInfo.detail.21'] = "";

$lang['categoryInfo.imgURL.20'] = "10.jpg";
$lang['categoryInfo.detail.20'] = "";

$lang['categoryInfo.imgURL.19'] = "10.jpg";
$lang['categoryInfo.detail.19'] = "";

$lang['categoryInfo.imgURL.18'] = "10.jpg";
$lang['categoryInfo.detail.18'] = "";

$lang['categoryInfo.imgURL.17'] = "10.jpg";
$lang['categoryInfo.detail.17'] = "";

$lang['categoryInfo.imgURL.16'] = "10.jpg";
$lang['categoryInfo.detail.16'] = "";

$lang['categoryInfo.imgURL.15'] = "10.jpg";
$lang['categoryInfo.detail.15'] = "";

$lang['categoryInfo.imgURL.14'] = "10.jpg";
$lang['categoryInfo.detail.14'] = "";

$lang['categoryInfo.imgURL.13'] = "10.jpg";
$lang['categoryInfo.detail.13'] = "";

$lang['categoryInfo.imgURL.12'] = "10.jpg";
$lang['categoryInfo.detail.12'] = "";

$lang['categoryInfo.imgURL.11'] = "10.jpg";
$lang['categoryInfo.detail.11'] = "";

$lang['categoryInfo.imgURL.10'] = "10.jpg";
$lang['categoryInfo.detail.10'] = "";

$lang['categoryInfo.imgURL.9'] = "10.jpg";
$lang['categoryInfo.detail.9'] = "";

$lang['categoryInfo.imgURL.8'] = "10.jpg";
$lang['categoryInfo.detail.8'] = "";

$lang['categoryInfo.imgURL.7'] = "10.jpg";
$lang['categoryInfo.detail.7'] = "";

$lang['categoryInfo.imgURL.6'] = "10.jpg";
$lang['categoryInfo.detail.6'] = "";

$lang['categoryInfo.imgURL.5'] = "10.jpg";
$lang['categoryInfo.detail.5'] = "";

$lang['categoryInfo.imgURL.4'] = "10.jpg";
$lang['categoryInfo.detail.4'] = "";

$lang['categoryInfo.imgURL.3'] = "10.jpg";
$lang['categoryInfo.detail.3'] = "";

$lang['categoryInfo.imgURL.2'] = "10.jpg";
$lang['categoryInfo.detail.2'] = "";

$lang['categoryInfo.imgURL.1'] = "10.jpg";
$lang['categoryInfo.detail.1'] = "";
