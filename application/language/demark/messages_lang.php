<?php
defined('BASEPATH') OR exit('No direct script access allowed');

#general
$lang['general.gallery'] = "Gallery";
$lang['general.appointment'] = "Appointment";
$lang['general.person'] = "Person";
$lang['general.it'] = "ITALIAN";
$lang['general.fr'] = "FRENCH";
$lang['general.de'] = "GERMAN";
$lang['general.error'] = "Error";
$lang['general.warning'] = "Warning";
$lang['general.success'] = "Success";
$lang['general.unsupported_language'] = "Unsupported Language";
$lang['general.ok'] = "OK";
$lang['general.cancel'] = "cancel";
$lang['general.validation.prefix'] = "Please enter your ";

#person
$lang['person.gender'] = "Geschlecht";
$lang['person.female'] = "WEIBLICH";
$lang['person.male'] = "MÄNNLICH";
$lang['person.first_name'] = "Vorname";
$lang['person.last_name'] = "Name";
$lang['person.phone'] = "Telefonnummer";
$lang['person.street'] = "Strasse";
$lang['person.house_number'] = "Nummer";
$lang['person.postal_code'] = "PLZ";
$lang['person.address'] = "Wohnort";
$lang['person.email'] = "Email";

#person placeholder
$lang['person.placeholder.firstname'] = "z.B. Meier";
$lang['person.placeholder.lastname'] = "z.B. Lorin";
$lang['person.placeholder.street'] = "z.B. Musterstrasse";
$lang['person.placeholder.house_number'] = "z.B. 42";
$lang['person.placeholder.postal_code'] = "z.B. 42";
$lang['person.placeholder.address'] = "z.B. Musterort";
$lang['person.placeholder.phone'] = "z.B. 079 123 45 67";

#buttons
$lang['btn.modal1'] = "SPAREN LEICHT GEMACHT";
$lang['btn.go_home'] = "ZUM BERATUNGSTERMIN";
$lang['btn.send'] = "ABSCHICKEN";
$lang['btn.join'] = "JETZT TEILNEHMEN";
$lang['btn.go_upload'] = "JETZT MITMACHEN";

#index
$lang['index.title1'] = "Bis zu CHF 2000.– sparen bei <br>den Gesundheitskosten";
$lang['index.content1'] = "Über die Hälfte der Schweizer Bevölkerung findet ihre Krankenkasse zu teuer. Aber weniger als 10% tun etwas dagegen. Darum gibt es den Wechselservice der AXA. Wer eine Kranken-Zusatzversicherung bei der AXA abschliesst, bekommt ihn gratis mit dazu. Und kann jedes Jahr ohne Aufwand zur günstigsten Grundversicherung wechseln. Unsere Kunden sparen so im Durchschnitt CHF 500.–, Familien sogar bis zu CHF 2000.– pro Jahr!";
$lang['index.title2'] = "Beratung vereinbaren";
$lang['index.content2'] = "Du möchtest mehr erfahren über den Wechselservice und die attraktiven Kranken-Zusatzversicherungen der AXA? Gerne beraten wir dich unverbindlich. Teile uns mit, wann es dir am besten passt.";
$lang['index.title3'] = "Mach mit beim Wake-up-Wettbewerb";
$lang['index.content3'] = "Gewinne das ultimative Aufwach-Erlebnis für bis zu 5 Personen: eine Reise in den Siam-Wasserpark auf Teneriffa im Wert von CHF 5000.–! Inklusive Flug, Hotel und VIP-Tagespass. Zusätzlich verlosen wir 10 Mal CHF 1000.–.";
$lang['index.title4'] = "Wake-up-Galerie";
$lang['index.content4'] = "Es gibt viele Möglichkeiten, um aufzuwachen. Lustige, schräge und solche, auf die wir nie gekommen wären. Zeig uns, wie DU garantiert hellwach wirst!";

#index modal
$lang['modal.title'] = "Die Vorteile auf einen Blick";
$lang['modal.text1'] = "Vom kostenlosen Wechselservice profitieren alle, die eine Kranken-Zusatzversicherung bei der AXA haben. Dank Wechselservice sparen unsere Kunden jedes Jahr rund CHF 500.– pro Person, Familien sogar bis zu CHF 2000.–!";
$lang['modal.section.title1'] = "Der Wechselservice kurz erklärt";
$lang['modal.section.content1'] = "Die Grundversicherung beinhaltet immer die gleichen Leistungen – bei jeder Krankenkasse. Der einzige Unterschied liegt im Preis. Darum lohnt es sich, regelmässig zum individuell günstigsten Anbieter zu wechseln. Nur: Prämien vergleichen, Vertrag kündigen und neu anmelden, das kostet Zeit und Nerven. Der Wechselservice der AXA nimmt dir all das ab.Wir liefern die nötigen Infos, du entscheidest – wir erledigen den Rest.";
$lang['modal.section.title2'] = "Einzigartiger Rechnungsservice";
$lang['modal.section.content2'] = "Viele schrecken davor zurück, Grund- und Zusatzversicherungen bei verschiedenen Anbietern zu haben («Splitting»). Dank unserem Rechnungsservice gibt es trotz Splitting nur einen Ansprechpartner: Reiche einfach alle medizinischen Belege bei uns ein! Wir sortieren sie und leiten sie an die zuständige Krankenkasse weiter. Natürlich gratis.";
$lang['modal.section.title3'] = "Attraktive Kranken-Zusatzversicherungen";
$lang['modal.section.content3'] = "Sichere dich und deine Familie optimal ab! Ob Prävention, Komplementärmedizin, Spital, Zähne oder Kapitalabsicherung – bei uns findest du bestimmt das Produkt, das zu dir passt.";
$lang['modal.section.title4'] = "Beratung vereinbaren";
$lang['modal.section.content4'] = "Du möchtest mehr erfahren über den Wechselservice und die attraktiven Kranken-Zusatzversicherungen der AXA? Gerne beraten wir dich unverbindlich. Teile uns mit, wann es dir am besten passt.";

#video3
$lang['video3.title'] = "Wecke deine Freunde";
$lang['video3.content'] = "Könnte es sein, dass deine Freunde den Krankenkassenwechsel verschlafen? Weck sie jetzt auf, damit auch sie in Zukunft sparen können – mit dem bequemen Wechselservice der AXA.";
$lang['video3.links'] = "Dieses Video teilen";

#appointment
$lang['appointment.title'] = "Beratung vereinbaren";
$lang['appointment.text'] = "Gemeinsam prüfen wir, ob sich deine Versicherungssituation optimieren lässt. <br>Teile uns mit, wann es dir am besten passt. Wir melden uns.";
$lang['appointment.find_date'] = "Passende Zeit finden";
$lang['appointment.mon'] = "Montag";
$lang['appointment.tue'] = "Dienstag";
$lang['appointment.wed'] = "Mittwoch";
$lang['appointment.thur'] = "Donnerstag";
$lang['appointment.fri'] = "Freitag";
$lang['appointment.sat'] = "Wochenende";
$lang['appointment.time_range'] = "Zeitraum";
$lang['appointment.plz.invalid'] = "Invalid PLZ";

#appointment success
$lang['appoint_success.title'] = "Vielen Dank für den Terminwunsch";
$lang['appoint_success.content'] = "Wir melden uns! Ob du konkrete Fragen hast oder eine umfassende Standortbestimmung wünschst –&#xA;<br />der Berater kann dir bestimmt weiterhelfen.";

#upload
$lang['upload.title'] = "Wake-up-Wettbewerb";
$lang['upload.content'] = "Fulle einfach das Formular aus, um an der grossen Verlosung des ultimativen <br>aufwach-Erlebnisses im Wert von CHF 5000.- und von 10 Mal CHF 1000.- teilzunehmen!";
$lang['upload.slide.btn'] = "Aufwach-Tipp hochladen";
$lang['upload.slide.title'] = "Foto/Video hochladen";
$lang['upload.slide.content'] = "Hast du einen Aufwach-Tipp, den die Welt sehen muss? Dann hilf auch anderen beim Aufwachen: Lade jetzt dein Foto oder Video hoch! Die Teilnahme am Wettbewerb ist auch ohne Aufwach-Tipp möglich.";
$lang['upload.slide.title'] = "Titel (nur bei Upload)";
$lang['upload.slide.title.placeholder'] = "z.B. Wake Up";
$lang['upload.slide.file'] = "Foto oder Video (nur bei Upload)";
$lang['upload.slide.file.placeholder'] = "Optionales Bild oder Video hochladen";
$lang['upload.slide.file.help'] = "Bilder (.jpg, .png, .gif) oder Videos (.mp4, .mov, .mkv). Max. 64MB.";
$lang['upload.slide.email'] = "E-Mail (nur bei Upload)";
$lang['upload.slide.email.placeholder'] = "z.B. name@mail.com";
$lang['upload.accept_label'] = "Mit meiner Anmeldung akzeptiere ich die";
$lang['upload.accept_link'] = "Teilnahmebedingungen";

#upload success
$lang['upload_success.title'] = "Du bist dabei";
$lang['upload_success.content'] = "Vielen Dank für die Teilnahme am Wake-up-Wettbewerb! Teilnahmeschluss ist der 31.12.2019.&#xA;Dann kommt aus, wer zu den glücklichen Gewinnern gehört. Wir drücken dir auf jeden Fall die Daumen! Und mit etwas Glück erwachst du schon bald in Teneriffa. Oder du schlägst die Augen auf und bist plötzlich CHF 1000.– reicher.";

#gallery
$lang['gallery.title'] = "Gleich bist du hellwach";
$lang['gallery.content'] = "Du denkst, du kennst bereits die verrücktesten Aufwachmethoden? Lass dich überraschen! <br>Oder hast du eine noch bessere Idee? Dann mach unbedingt mit und teile deinen Aufwach-Tipp in der Galerie!";
$lang['gallery.search.placeholder'] = "Suchen";
