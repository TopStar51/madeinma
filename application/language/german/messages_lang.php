<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['site.description'] = "Finde Hersteller, Zulieferer und Dienstleister in Marokko";
$lang['site.title'] = "made-in.ma - Der Online Marktplatz für Marokkanische Unternehmen";
$lang['site.keywords'] = "INETTECH, INETTECH.IO, made-in.ma, Made in Morocco, Manufacturer, Suppliers, Service Companies, B2B, B2B-Plattform, Export, Import, Products";

#register
$lang['header.register'] = 'Registrieren';

#message
$lang['general.lang.en'] = "English";
$lang['general.lang.fr'] = "Français";
$lang['general.lang.it'] = "Italiano";
$lang['general.lang.es'] = "Español";
$lang['general.lang.de'] = "Deutsch";
$lang['general.lang.ar'] = "العربية";
$lang['general.lang.cn'] = "中國";
$lang['general.lang.tu'] = "Türkçe";
// $lang['general.lang.ir'] = "Farsi";

#message
$lang['message.success'] = "Erfolgreich";
$lang['message.error'] = "Fehler";
$lang['message.warning'] = "Warnung";
$lang['message.ok'] = "OK";
$lang['message.cancel'] = "Abbrechen";
$lang['message.signin'] = "Einloggen";
$lang['message.signup'] = "Anmelden";
$lang['message.signout'] = "Ausloggen";
$lang['message.save'] = "Speichern";
$lang['message.mail'] = "Nachricht";
$lang['message.rating'] = "Bewertung";
$lang['message.call'] = "Anrufen";
$lang['message.save_success'] = "Änderungen werden geprüft und in kürze freigeschalten...";
$lang['message.confirmation'] = "Bestätigung";
$lang['message.new_password'] = "Bitte neues Passwort eingeben";
$lang['message.recaptacha_error'] = "Sorry. Google Recaptcha Unsuccessful!";
$lang['message.phone_duplicate'] = 'Google Recaptcha Fehlerhaft!';
$lang['message.sms_error'] = 'Telefonnummer bereits vergeben';
$lang['message.label.name'] = "Name";
$lang['message.label.email'] = "E-Mail";
$lang['message.label.phone'] = 'Telefon';
$lang['message.label.phone'] = "Telefon";
$lang['message.label.country'] = "Land";
$lang['message.label.language'] = "Sprache";
$lang['message.label.content'] = "Text";
$lang['message.label.created_at'] = "Erstellt am";
$lang['message.company_error'] = "Company name is already registered.";

#validation
$lang['validation.user.name.salutation'] = "Please enter salutation.";
$lang['validation.user.name.require'] = "Please enter your name.";
$lang['validation.user.email.require'] = "Please enter your email.";
$lang['validation.user.phone.require'] = 'Please enter your phone number.';
$lang['validation.user.email.valid'] = "Please enter a valid email address.";
$lang['validation.user.email.unique'] = "This email address is already registered. <br>Please use a different email address.";
$lang['validation.user.phone.unique'] = "This phone number is already registered. <br>Please use a different phone number.";
$lang['validation.user.password.require'] = "Please enter your password.";
$lang['validation.user.confirm_password.require'] = "Please enter a your confirm password.";
$lang['validation.user.confirm_password.valid'] = "Your password and confirmation password do not match.";
$lang['validation.user.new_password.require'] = "Please enter your new password.";
$lang['validation.user.current_password.require'] = "Please enter your current password.";
$lang['validation.user.sms_code.require'] = "Please input your sms code.";

$lang['validation.company.name.require'] = "Please enter your company name.";
$lang['validation.company.salutation.require'] = "Please enter salutation.";
$lang['validation.company.contact_person.require'] = "Please enter contact person.";
$lang['validation.company.street.require'] = "Please enter street.";
$lang['validation.company.city.require'] = "Please select city.";
$lang['validation.company.founded_at.require'] = "Please enter founded year.";
$lang['validation.company.email.require'] = "Please enter email.";
$lang['validation.company.zipcode.require'] = "Please enter zipcode.";
$lang['validation.company.phone.require'] = "Please enter phone.";
$lang['validation.company.website.require'] = "Please enter website.";
$lang['validation.company.keywords.require'] = "Please enter keywords.";
$lang['validation.company.lang.require'] = "Please select language.";
$lang['validation.company.employee_count.require'] = "Please select number of employees.";
$lang['validation.company.category.require'] = "Please select category.";

#user
$lang['user.name'] = "Name";
$lang['user.email'] = "E-Mail";
$lang['user.phone'] = 'Handy Phone';
$lang['user.password'] = "Passwort";
$lang['user.confirm_password'] = "Passwort Bestätigen";
$lang['user.sms_code'] = 'Code';
$lang['user.promotion_code'] = 'Promotion code (optional)';

#placeholder
$lang['user.phone.flag'] = 'assets/madinma/front/images/1.png';
$lang['user.phone.placeholder'] = '650123456';
$lang['phone.placeholder'] = '+212...';
$lang['promo.placeholder'] = 'A12B';

#company setting labels
$lang['company.logo'] = "Company Logo or Shop Image";
$lang['company.name'] = "Firmenname oder Title";
$lang['company.salutation'] = "Anrede";
$lang['company.contact_person'] = "Kontaktperson";
$lang['company.street'] = "Strasse";
$lang['company.city'] = "Stadt";
$lang['company.founded_at'] = "Gegründet seit";
$lang['company.employee_count'] = "Anzahl der Mitarbeiter";
$lang['company.zipcode'] = "PLZ";
$lang['company.phone'] = "Handy Nummer";
$lang['company.email'] = "E-Mail";
$lang['company.website'] = "Website or Facebooksite";
$lang['company.youtube'] = "Youtube Video";
$lang['company.keywords'] = "Suchbegriffe";
$lang['company.about_us'] = "Über uns";
$lang['company.category'] = "Kategorie";
$lang['company.language'] = "Gesprochene Sprachen";
$lang['company.latitude'] = "Latitude";
$lang['company.longitude'] = "Longitude";

#company setting placeholders
$lang['company.logo.placeholder'] = "Company Logo or Shop Image";
$lang['company.name.placeholder'] = "Ameublement el Bahja";
$lang['company.salutation.placeholder.mr'] = "Mr";
$lang['company.salutation.placeholder.ms'] = "Ms";
$lang['company.contact_person.placeholder'] = "Mohamed Kamal";
$lang['company.street.placeholder'] = "138 Boulevard Moulay Youssef, Angle Rue Tiznit";
$lang['company.founded_at.placeholder'] = "2008";
$lang['company.zipcode.placeholder'] = "20240";
$lang['company.phone.placeholder'] = "+212...";
$lang['company.email.placeholder'] = "your@email.ma";
$lang['company.website.placeholder'] = "https://www.yourwebsite.com";
$lang['company.youtube.placeholder'] = "https://www.youtube.com/watch?v=iwa7_SHf114";
$lang['company.keywords.placeholder'] = "Arganöl, Argan...";
$lang['company.about_us.placeholder'] = "Bitte schreiben Sie hier etwas über Ihre Firma...";
$lang['company.lang.non.selected.placeholder'] = "Bitte auswählen";
$lang['company.latitude.placeholder'] = "33.333333";
$lang['company.longitude.placeholder'] = "43.333333";

#top-search
$lang['topsearch.search'] = "SUCHE";
$lang['topsearch.placeholder'] = "Nach was möchten Sie suchen...";

#sidebar
$lang['sidebar.title'] = ""; // Kategorie

#footer
$lang['footer.about_title'] = "Über made-in.ma";
$lang['footer.about_content'] = 'made-in.ma ist ein Online Marktplatz der den Export- und Binnenhandel kleinerer Unternehmen in Marokko stärken möchte. Hersteller, Zulieferer und Dienstleister können sich hier kostenlos internationalen und nationalen Kunden präsentieren.<br><br>Dieser Dienst wird von <a href="https://www.inettech.io" title="">INETTECH.io</a> entwickelt und betrieben.<br><br><a href="https://www.youtube.com/channel/UCAqJdNAA1onUmm7KZo4qvqg" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_youtube.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/madein.ma/" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_instagram.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://fb.me/wwwmadeinma" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_facebook.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href=" https://wa.me/212688217862" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_whatsapp.png" alt="" height="35" width="35"></a>';
$lang['footer.contact_title'] = "Kontakt und Kooperation";
$lang['footer.contact_content'] = "Bitte kontaktieren Sie uns per E-Mail";
$lang['footer.mail'] = "E-Mail";
$lang['footer.statistics_title'] = "Statistik";
$lang['footer.statistics_comapny'] = "Gelistete Firmen";
$lang['footer.statistics_comapny_week'] = "Diese Woche neu gelistet";
$lang['footer.statistics_inquery'] = "Abfragen insgesamt";
$lang['footer.statistics_inquery_week'] = "Abfragen diese Woche";

#modals
$lang['modal.contact.title'] = "Kontakt";
$lang['modal.rating.title'] = "Bewertung";
$lang['modal.call.title'] = "Anruf";
$lang['modal.btn.send'] = "Senden";
$lang['modal.placeholder.name'] = "Name";
$lang['modal.placeholder.email'] = "E-Mail";
$lang['modal.placeholder.phone'] = "+49XXXXXXXXX";
$lang['modal.placeholder.text'] = "Text";
$lang['modal.placeholder.company_detail'] = "Please write here your text to the company";
$lang['modal.placeholder.rating_detail'] = "Please write here your comment to this company";
$lang['modal.your_rating'] = "Ihre Bewertung";
$lang['modal.call_desc'] = "Sagen Sie dem Unternehmen Sie haben seine Anzeige auf made-in.ma gefunden. Vielen Dank!";
$lang['modal.confirm.title'] = "Bestätigen";
$lang['modal.confirm.question'] = "Bestätigen Sie Ihre Telefonnummer!";
$lang['modal.lang.none.selected'] = 'Bitte auswählen';
# Translate this
$lang['modal.option.email'] = "Email";
$lang['modal.option.phone'] = "Phone";
$lang['modal.option.all'] = "All";

#startpage
$lang['startpage.new'] = "Neue Firmen";
$lang['startpage.week'] = "Firma der Woche";

#signin
$lang['signin.remember'] = "Zugangsdaten merken";
$lang['signin.invalid'] = "Zugangsdaten ungültig";
$lang['signin.blocked'] = "Ihr Account ist gesperrt";
$lang['signin.back'] = "Zurück";
$lang['signin.submit'] = "Weiter";
$lang['signin.forgot'] = "Passwort vergessen?";


#signup
$lang['signup.desc'] = "Registrieren Sie sich hier kostenlos und unverbindlich auf made-in.ma als Hersteller, Zulieferer oder Dienstleister.<br><br>Sie können hier Ihre Produkte und Dienstleistungen kostenlos national und international bewerben.";

#sms verification
$lang['sms.desc'] = "Bitte geben Sie den Code der Verifizierungs SMS ein";
$lang['sms.code.error'] = "Code ungültig";
$lang['sms.expire'] = "Code ist abgelaufen. Bitte nochmal versuchen";

#myacount
$lang['myaccount.company_profile'] = "Firmen Profil";
$lang['myaccount.title'] = "Mein Account";
$lang['myaccount.company_information'] = "Firmen Informationen";
$lang['myaccount.logo_note'] = "Your company logo. Pictures larger than pixels will be scaled down.";
$lang['myaccount.avatar_note'] = "Your virtual face or picture. Pictures larger than pixels will be scaled down.";
$lang['myaccount.company_product'] = "Produkte/Referenzen";
$lang['myaccount.general_information'] = "Allgemeine Informationen";
$lang['myaccount.change_password'] = "Ändere Passwort";
$lang['myaccount.current_password'] = "Aktuelles Passwort";
$lang['myaccount.promotion'] = "Partnerprogramm";
$lang['myaccount.promotion.desc'] = 'Empfohlene made-in.ma Freunde und Bekannte die ebenfalls eine Firma haben. Nach Prüfung erhalten Sie 20 Dirham für jede registrierte Firma, die bei der Registrierung Ihren Partner-Code eingegeben hat. Eine Auszahlung ist ab 100 Anmeldungen möglich. Die Zahlung erfolgt über Western Union. Das Partnerprogramm ist zeitlich begrenzt.';
$lang['myaccount.promotion.promotion_code'] = "Ihr Partner Code";
$lang['myaccount.promotion.promotion_link'] = "Ihr Partner Link";
$lang['myaccount.promotion.current_promotions'] = "Anzahl aktuelle Registrierungen";
$lang['myaccount.new_password'] = "Neues Passwort";
$lang['myaccount.select'] = "Auswahl";
$lang['myaccount.logo'] = "Shop/Firmen Bild";
$lang['myaccount.contact_avatar'] = "Foto der Kontaktperson";
$lang['myaccount.product_warning'] = "Bitte füge ein Produkt oder Referenzbild hinzu";
$lang['myaccount.preview'] = "Vorschau";
$lang['myaccount.del_account'] = "ACCOUNT LÖSCHEN";
$lang['myaccount.del_account_msg'] = "Sind Sie sicher dass Sie den Account unwiderruflich löschen möchten?";
$lang['myaccount.messages'] = "Nachrichten";
$lang['myaccount.support'] = "Support";

#companies
$lang['companies.all'] = "Alle";
$lang['companies.sortby'] = "Reihenfolge";
$lang['companies.rating'] = "Bewertung";
$lang['companies.newest'] = "Neuste";
$lang['companies.oldest'] = "Älteste";
$lang['companies.entries'] = "Einträge";
$lang['companies.in'] = " in ";

#company detail
$lang['detail.employees'] = "Angestellte";
$lang['detail.date'] = "Datum";
$lang['detail.name'] = "Name";
$lang['detail.description'] = "Beschreibung";
$lang['detail.noproduct'] = "Keine Produkte";
$lang['detail.product.pics'] = "Produkt Bilder";
$lang['detail.last.update'] = 'Letzte Aktualisierung';
$lang['detail.rating'] = 'Bewertung';
$lang['detail.youtube'] = 'Video';
$lang['detail.map'] = 'Standort';

#rating modal
$lang['rating.name'] = "Name";
$lang['rating.country'] = "Land";
$lang['rating.text'] = "Bewertungstext";
$lang['no.ratings'] = "Keine Bewertungen";

#company profile
$lang['profile.since'] = "Seit";
$lang['profile.employees'] = "Angestellte";

#forgot
$lang['forgot.no_email'] = "No such email address exists.";
$lang['forgot.no_phone'] = "No such phone exists.";
$lang['forgot.success_email'] = "Reset link sent your email address. Please check your junk mail folder.";
$lang['forgot.success_phone'] = "Reset link sent to your phone";
$lang['forgot.invalid_token'] = "Invalid token.";
$lang['forgot.update_success'] = "Password is successfully updated.";
$lang['forgot.expire'] = "Your expire time is passed.";

#contact sms message
$lang['contact.sms'] = 'Please sign in made-in.ma. You have a new message.';
$lang['contact.sms.success'] = 'Your message was sent to the manufacturer successfully.';
$lang['contact.sms.error'] = 'Your message was not sent because of sms problem';

#phone number invalid message
$lang['phone.error.title'] = "Error";
$lang['phone.invalid'] = "Your phone number is invalid. The count of digits should be 9 and do not start with '0' at first";

#category info
$lang['categoryInfo.imgURL.26'] = "10.jpg";
$lang['categoryInfo.detail.26'] = "";

$lang['categoryInfo.imgURL.25'] = "10.jpg";
$lang['categoryInfo.detail.25'] = "";

$lang['categoryInfo.imgURL.24'] = "10.jpg";
$lang['categoryInfo.detail.24'] = "";

$lang['categoryInfo.imgURL.23'] = "10.jpg";
$lang['categoryInfo.detail.23'] = "";

$lang['categoryInfo.imgURL.22'] = "10.jpg";
$lang['categoryInfo.detail.22'] = "";

$lang['categoryInfo.imgURL.21'] = "10.jpg";
$lang['categoryInfo.detail.21'] = "";

$lang['categoryInfo.imgURL.20'] = "10.jpg";
$lang['categoryInfo.detail.20'] = "";

$lang['categoryInfo.imgURL.19'] = "10.jpg";
$lang['categoryInfo.detail.19'] = "";

$lang['categoryInfo.imgURL.18'] = "10.jpg";
$lang['categoryInfo.detail.18'] = "";

$lang['categoryInfo.imgURL.17'] = "10.jpg";
$lang['categoryInfo.detail.17'] = "";

$lang['categoryInfo.imgURL.16'] = "10.jpg";
$lang['categoryInfo.detail.16'] = "";

$lang['categoryInfo.imgURL.15'] = "10.jpg";
$lang['categoryInfo.detail.15'] = "";

$lang['categoryInfo.imgURL.14'] = "10.jpg";
$lang['categoryInfo.detail.14'] = "";

$lang['categoryInfo.imgURL.13'] = "10.jpg";
$lang['categoryInfo.detail.13'] = "";

$lang['categoryInfo.imgURL.12'] = "10.jpg";
$lang['categoryInfo.detail.12'] = "";

$lang['categoryInfo.imgURL.11'] = "10.jpg";
$lang['categoryInfo.detail.11'] = "";

$lang['categoryInfo.imgURL.10'] = "10.jpg";
$lang['categoryInfo.detail.10'] = "";

$lang['categoryInfo.imgURL.9'] = "10.jpg";
$lang['categoryInfo.detail.9'] = "";

$lang['categoryInfo.imgURL.8'] = "10.jpg";
$lang['categoryInfo.detail.8'] = "";

$lang['categoryInfo.imgURL.7'] = "10.jpg";
$lang['categoryInfo.detail.7'] = "";

$lang['categoryInfo.imgURL.6'] = "10.jpg";
$lang['categoryInfo.detail.6'] = "";

$lang['categoryInfo.imgURL.5'] = "10.jpg";
$lang['categoryInfo.detail.5'] = "";

$lang['categoryInfo.imgURL.4'] = "10.jpg";
$lang['categoryInfo.detail.4'] = "";

$lang['categoryInfo.imgURL.3'] = "10.jpg";
$lang['categoryInfo.detail.3'] = "";

$lang['categoryInfo.imgURL.2'] = "10.jpg";
$lang['categoryInfo.detail.2'] = "";

$lang['categoryInfo.imgURL.1'] = "10.jpg";
$lang['categoryInfo.detail.1'] = "";
