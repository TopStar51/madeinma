<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['site.description'] = "Made in Morocco - find manufacturer, supplier and service provider";
$lang['site.title'] = "Made in Morocco - find manufacturer, supplier and service provider";

#message
$lang['general.lang.en'] = "English";
$lang['general.lang.fr'] = "Français";
$lang['general.lang.it'] = "Italiano";
$lang['general.lang.es'] = "Español";
$lang['general.lang.de'] = "Deutsch";
$lang['general.lang.ar'] = "العربية";
$lang['general.lang.cn'] = "中國";
$lang['general.lang.tu'] = "Türkçe";
$lang['general.lang.ir'] = "فارسی";

#message
$lang['message.success'] = "Success";
$lang['message.error'] = "Error";
$lang['message.warning'] = "Warning";
$lang['message.ok'] = "OK";
$lang['message.cancel'] = "Cancel";
$lang['message.signin'] = "Sign In";
$lang['message.signup'] = "Sign Up";
$lang['message.signout'] = "Sign Out";
$lang['message.save'] = "Save";
$lang['message.mail'] = "Mail";
$lang['message.rating'] = "Rating";
$lang['message.call'] = "Call";
$lang['message.save_success'] = "Changes are being reviewed and will soon be unlocked ...";
$lang['message.confirmation'] = "Confirmation";
$lang['message.new_password'] = "Please input new password";
$lang['message.recaptacha_error'] = "Google Recaptcha Unsuccessful!";
#validation
$lang['validation.user.name.salutation'] = "Please enter salutation";
$lang['validation.user.name.require'] = "Please enter your name";
$lang['validation.user.email.require'] = "Please enter your email";
$lang['validation.user.email.valid'] = "Please enter a valid email address";
$lang['validation.user.email.unique'] = "This email address is already registered. <br>Please use a different email address.";
$lang['validation.user.password.require'] = "Please enter your password";
$lang['validation.user.confirm_password.require'] = "Please enter a your confirm password";
$lang['validation.user.confirm_password.valid'] = "Your password and confirmation password do not match";
$lang['validation.user.new_password.require'] = "Please enter your new password";
$lang['validation.user.current_password.require'] = "Please enter your current password";

$lang['validation.company.name.require'] = "Please enter your company name";
$lang['validation.company.salutation.require'] = "Please enter salutation";
$lang['validation.company.contact_person.require'] = "Please enter contact person";
$lang['validation.company.street.require'] = "Please enter street";
$lang['validation.company.founded_at.require'] = "Please enter founded year";
$lang['validation.company.email.require'] = "Please enter email";
$lang['validation.company.zipcode.require'] = "Please enter zipcode";
$lang['validation.company.phone.require'] = "Please enter phone";
$lang['validation.company.website.require'] = "Please enter website";
$lang['validation.company.keywords.require'] = "Please enter keywords";

#user
$lang['user.name'] = "name";
$lang['user.email'] = "email";
$lang['user.phone'] = "phone";
$lang['user.password'] = "password";
$lang['user.confirm_password'] = "confirm password";

#user
$lang['company.logo'] = "Shop/Company image";
$lang['company.name'] = "title";
$lang['company.salutation'] = "salutation";
$lang['company.contact_person'] = "contact person";
$lang['company.street'] = "area/street";
$lang['company.city'] = "city";
$lang['company.founded_at'] = "founded at";
$lang['company.employee_count'] = "number of employees";
$lang['company.zipcode'] = "zip code";
$lang['company.phone'] = "mobile";
$lang['company.email'] = "email";
$lang['company.website'] = "website";
$lang['company.keywords'] = "keywords";
$lang['company.about_us'] = "about us";
$lang['company.category'] = "category";
$lang['company.language'] = "spoken languages";

#top-search
$lang['topsearch.search'] = "Search";
$lang['topsearch.placeholder'] = "What are you looking for...";

#sidebar
$lang['sidebar.title'] = ""; // Categories

#footer
$lang['footer.about_title'] = "About made-in.ma";
$lang['footer.about_content'] = 'made-in.ma is a non-profit organization that aims to strengthen the export and internal trade of smaller companies in Morocco. Manufacturers, suppliers and service providers can present here free international and national customers.<br><br>This service is developed and operated by <a href="https://www.iNetTech.io" title="">iNetTech.io</a>';
$lang['footer.contact_title'] = "Contact and Cooperation";
$lang['footer.contact_content'] = "Please contact us by mail";
$lang['footer.mail'] = "mail";
$lang['footer.statistics_title'] = "Statistics";
$lang['footer.statistics_comapny'] = "Total companies listed";
$lang['footer.statistics_comapny_week'] = "new this week";
$lang['footer.statistics_inquery'] = "inquery total";
$lang['footer.statistics_inquery_week'] = "inquery this week";

#modals
$lang['modal.contact.title'] = "contact";
$lang['modal.rating.title'] = "rating";
$lang['modal.call.title'] = "call";
$lang['modal.btn.send'] = "send";
$lang['modal.placeholder.name'] = "name";
$lang['modal.placeholder.email'] = "email";
$lang['modal.placeholder.phone'] = "+212XXXXXXXXX";
$lang['modal.placeholder.text'] = "Please write here the text for us";
$lang['modal.placeholder.company_detail'] = "Please write here your text to the company";
$lang['modal.placeholder.rating_detail'] = "Please write here your comment to this company";
$lang['modal.your_rating'] = "Your rating";
$lang['modal.call_desc'] = "Tell me you found my on made-in.ma. Thank you!";
$lang['modal.option.email'] = "Email";
$lang['modal.option.phone'] = "Phone";
$lang['modal.option.all'] = "All";

#startpage
$lang['startpage.new'] = "New company";
$lang['startpage.week'] = "company of the week";

#signin
$lang['signin.remember'] = "Remember Me";
$lang['signin.invalid'] = "Invalid credentials";
$lang['signin.blocked'] = "Your account was blocked.";
$lang['signin.back'] = "Back";
$lang['signin.submit'] = "Submit";
$lang['signin.forgot'] = "Forget Password?";

#signup
$lang['signup.desc'] = "Register here on made-in.ma as manufacturer, supplier or service provider free of charge and without obligation.<br><br>You can advertise your products and services nationally and internationally.";

#myacount
$lang['myaccount.company_profile'] = "company profile";
$lang['myaccount.title'] = "My Account";
$lang['myaccount.company_information'] = "company information";
$lang['myaccount.logo_note'] = "Your company logo. Pictures larger than pixels will be scaled down.";
$lang['myaccount.avatar_note'] = "Your virtual face or picture. Pictures larger than pixels will be scaled down.";
$lang['myaccount.company_product'] = "product/references";
$lang['myaccount.general_information'] = "general information";
$lang['myaccount.change_password'] = "change password";
$lang['myaccount.current_password'] = "current password";
$lang['myaccount.new_password'] = "new password";
$lang['myaccount.select'] = "select";
$lang['myaccount.logo'] = "Shop/Company image";
$lang['myaccount.contact_avatar'] = "Photo of contact person";
$lang['myaccount.product_warning'] = "Please add a product or references image";
$lang['myaccount.preview'] = "Preview";
$lang['myaccount.del_account'] = "Delete Account";
$lang['myaccount.del_account_msg'] = "Are you sure you want to permanently delete the account?";
$lang['myaccount.messages'] = "Messages";
$lang['myaccount.support'] = "Support";

#companies
$lang['companies.all'] = "All";
$lang['companies.sortby'] = "Sort By";
$lang['companies.rating'] = "rating";
$lang['companies.newest'] = "newest";
$lang['companies.oldest'] = "oldest";
$lang['companies.entries'] = "entries";
$lang['companies.in'] = " in ";

#company detail
$lang['detail.employees'] = "employees";
$lang['detail.date'] = "date";
$lang['detail.name'] = "name";
$lang['detail.description'] = "description";
$lang['detail.noproduct'] = "There is no product.";

#rating modal
$lang['rating.name'] = "Name";
$lang['rating.country'] = "Country";
$lang['rating.text'] = "Your Message";

#company profile
$lang['profile.since'] = "Since";
$lang['profile.employees'] = "employees";

#forgot
$lang['forgot.no_email'] = "No such email address exists.";
$lang['forgot.success_email'] = "Reset link sent your email address. Please check your junk mail folder.";
$lang['forgot.invalid_token'] = "Invalid token.";
$lang['forgot.update_success'] = "Password is successfully updated.";
$lang['forgot.expire'] = "Your expire time is passed.";
