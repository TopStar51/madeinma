<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['site.description'] = "Trouver des fabricants, des fournisseurs et des sociétés de services au Maroc";
$lang['site.title'] = "made-in.ma - La place de marché en ligne pour les entreprises marocaines";
$lang['site.keywords'] = "INETTECH, INETTECH.IO, made-in.ma, Made in Morocco, Manufacturer, Suppliers, Service Companies, B2B, B2B-Plattform, Export, Import, Products";

#register
$lang['header.register'] = 'Enregistrement';

#message
$lang['general.lang.en'] = "English";
$lang['general.lang.fr'] = "Français";
$lang['general.lang.it'] = "Italiano";
$lang['general.lang.es'] = "Español";
$lang['general.lang.de'] = "Deutsch";
$lang['general.lang.ar'] = "العربية";
$lang['general.lang.cn'] = "中國";
$lang['general.lang.tu'] = "Türkçe";
// $lang['general.lang.ir'] = "Farsi";

#message
$lang['message.success'] = "Succès";
$lang['message.error'] = "Erreur";
$lang['message.warning'] = "Attention";
$lang['message.ok'] = "OK";
$lang['message.cancel'] = "Annuler";
$lang['message.signin'] = "Se connecter";
$lang['message.signup'] = "S'inscrire";
$lang['message.signout'] = "Déconnexion";
$lang['message.save'] = "Sauvegarder";
$lang['message.mail'] = "Courrier";
$lang['message.rating'] = "Évaluation";
$lang['message.call'] = "Appel";
$lang['message.save_success'] = "Les modifications sont en cours de révision et seront bientôt débloquées ...";
$lang['message.confirmation'] = "Confirmation";
$lang['message.new_password'] = "Veuillez saisir un nouveau mot de passe";
$lang['message.recaptacha_error'] = "Google Recaptcha non réussi!";
$lang['message.phone_duplicate'] = 'Numéro de téléphone déjà enregistré.';
$lang['message.sms_error'] = 'Numéro de téléphone faux';
$lang['message.label.name'] = "Prénom";
$lang['message.label.email'] = "Email";
$lang['message.label.phone'] = 'Téléphone';
$lang['message.label.phone'] = "Téléphone";
$lang['message.label.country'] = "Pays";
$lang['message.label.language'] = "La langue";
$lang['message.label.content'] = "Contenu";
$lang['message.label.created_at'] = "Créé à";
$lang['message.company_error'] = "Company name is already registered.";

#validation
$lang['validation.user.name.salutation'] = "S'il vous plaît entrer une salutation.";
$lang['validation.user.name.require'] = "S'il vous plaît entrez votre nom.";
$lang['validation.user.email.require'] = "S'il vous plaît entrer votre email.";
$lang['validation.user.phone.require'] = "S'il vous plaît entrez votre numéro de téléphone.";
$lang['validation.user.email.valid'] = "S'il vous plaît, mettez une adresse email valide.";
$lang['validation.user.email.unique'] = "Cette adresse email est déjà enregistrée. <br> Veuillez utiliser une adresse email différente.";
$lang['validation.user.phone.unique'] = "Ce numéro de téléphone est déjà enregistré. <br> Veuillez utiliser un numéro de téléphone différent.";
$lang['validation.user.password.require'] = "S'il vous plait entrez votre mot de passe.";
$lang['validation.user.confirm_password.require'] = "Veuillez entrer votre mot de passe de confirmation.";
$lang['validation.user.confirm_password.valid'] = "Votre mot de passe et votre mot de passe de confirmation ne correspondent pas.";
$lang['validation.user.new_password.require'] = "Veuillez entrer votre nouveau mot de passe.";
$lang['validation.user.current_password.require'] = "Veuillez saisir votre mot de passe actuel.";
$lang['validation.user.sms_code.require'] = "S'il vous plaît entrer votre code SMS.";

$lang['validation.company.name.require'] = "Veuillez saisir le nom de votre entreprise.";
$lang['validation.company.salutation.require'] = "S'il vous plaît entrer une salutation.";
$lang['validation.company.contact_person.require'] = "S'il vous plaît entrer la personne de contact.";
$lang['validation.company.street.require'] = "S'il vous plaît entrer la rue.";
$lang['validation.company.city.require'] = "Veuillez sélectionner une ville.";
$lang['validation.company.founded_at.require'] = "S'il vous plaît entrer l'année fondée.";
$lang['validation.company.email.require'] = "S'il vous plaît entrer email.";
$lang['validation.company.zipcode.require'] = "S'il vous plaît entrer le code postal.";
$lang['validation.company.phone.require'] = "S'il vous plaît entrer le téléphone.";
$lang['validation.company.website.require'] = "S'il vous plaît entrer site web.";
$lang['validation.company.keywords.require'] = "Veuillez entrer des mots-clés.";
$lang['validation.company.lang.require'] = "S'il vous plaît sélectionner la langue.";
$lang['validation.company.employee_count.require'] = "Veuillez sélectionner le nombre d'employés.";
$lang['validation.company.category.require'] = "Veuillez sélectionner une catégorie.";

#user
$lang['user.name'] = "Prénom";
$lang['user.email'] = "Email";
$lang['user.phone'] = 'Téléphone';
$lang['user.password'] = "Mot de passe";
$lang['user.confirm_password'] = "Confirmez le mot de passe";
$lang['user.sms_code'] = 'Code';
$lang['user.promotion_code'] = 'Code promotionnel (si disponible)';

#placeholder
$lang['user.phone.flag'] = 'assets/madinma/front/images/1.png';
$lang['user.phone.placeholder'] = '650123456';
$lang['phone.placeholder'] = '+212...';
$lang['promo.placeholder'] = 'A12B';

#company setting labels
$lang['company.logo'] = "Logo d'entreprise";
$lang['company.name'] = "Titre";
$lang['company.salutation'] = "Salutation";
$lang['company.contact_person'] = "Contact";
$lang['company.street'] = "Rue";
$lang['company.city'] = "Ville";
$lang['company.founded_at'] = "Fondée à";
$lang['company.employee_count'] = "Nombre d'employés";
$lang['company.zipcode'] = "Code postal";
$lang['company.phone'] = "Téléphone";
$lang['company.email'] = "Email";
$lang['company.website'] = "Site Internet";
$lang['company.youtube'] = "Youtube";
$lang['company.keywords'] = "Mots clés";
$lang['company.about_us'] = "à propos de nous";
$lang['company.category'] = "Catégorie";
$lang['company.language'] = "Langues";
$lang['company.latitude'] = "Latitude";
$lang['company.longitude'] = "Longitude";

#company setting placeholders
$lang['company.logo.placeholder'] = "Logo d'entreprise";
$lang['company.name.placeholder'] = "Ameublement el Bahja";
$lang['company.salutation.placeholder.mr'] = "Mr";
$lang['company.salutation.placeholder.ms'] = "Ms";
$lang['company.contact_person.placeholder'] = "Mohamed Kamal";
$lang['company.street.placeholder'] = "138 Boulevard Moulay Youssef, Angle Rue Tiznit";
$lang['company.founded_at.placeholder'] = "2008";
$lang['company.zipcode.placeholder'] = "47963";
$lang['company.phone.placeholder'] = "+212...";
$lang['company.email.placeholder'] = "your@mail.ma";
$lang['company.website.placeholder'] = "https://yourwebsite.ma";
$lang['company.youtube.placeholder'] = "https://www.youtube.com/watch?v=iwa7_SHf114";
$lang['company.keywords.placeholder'] = "Huile d\'Argan, Argan ...";
$lang['company.about_us.placeholder'] = "à propos de nous";
$lang['company.lang.non.selected.placeholder'] = "Rien de sélectionné";
$lang['company.latitude.placeholder'] = "33.333333";
$lang['company.longitude.placeholder'] = "43.333333";

#top-search
$lang['topsearch.search'] = "CHERCHER";
$lang['topsearch.placeholder'] = "Que cherchez-vous...";

#sidebar
$lang['sidebar.title'] = ""; // Les catégories

#footer
$lang['footer.about_title'] = "À propos de made-in.ma";
$lang['footer.about_content'] = 'made-in.ma est une organisation à but non lucratif qui souhaite renforcer l’économie d’exportation des petites entreprises marocaines. Les fabricants, les fournisseurs de services et les fournisseurs peuvent se présenter à des clients internationaux.<br><br>Ce service est fourni par <a href="https://www.inettech.io" title="">INETTECH.io</a><br><br><a href="https://www.youtube.com/channel/UCAqJdNAA1onUmm7KZo4qvqg" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_youtube.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/madein.ma/" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_instagram.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://fb.me/wwwmadeinma" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_facebook.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href=" https://wa.me/212688217862" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_whatsapp.png" alt="" height="35" width="35"></a>';
$lang['footer.contact_title'] = "Contact et coopération";
$lang['footer.contact_content'] = "Merci de nous contacter par mail";
$lang['footer.mail'] = "Courrier";
$lang['footer.statistics_title'] = "Statistiques";
$lang['footer.statistics_comapny'] = "Sociétés cotées";
$lang['footer.statistics_comapny_week'] = "Nouveau cette semaine";
$lang['footer.statistics_inquery'] = "Total de la demande";
$lang['footer.statistics_inquery_week'] = "Enquête cette semaine";

#modals
$lang['modal.contact.title'] = "Contact";
$lang['modal.rating.title'] = "Évaluation";
$lang['modal.call.title'] = "Appel";
$lang['modal.btn.send'] = "Envoyer";
$lang['modal.placeholder.name'] = "Prénom";
$lang['modal.placeholder.email'] = "Email";
$lang['modal.placeholder.phone'] = "+212XXXXXXXX";
$lang['modal.placeholder.text'] = "Texte";
$lang['modal.placeholder.company_detail'] = "Merci d'écrire ici votre texte à l'entreprise";
$lang['modal.placeholder.rating_detail'] = "Veuillez écrire ici votre commentaire à cette entreprise";
$lang['modal.your_rating'] = "Votre note";
$lang['modal.call_desc'] = "Dis-moi que tu as trouvé mon annonce sur made-in.ma. Merci!";
$lang['modal.confirm.title'] = "Confirmer";
$lang['modal.confirm.question'] = "Confirmez votre numéro de téléphone!";
$lang['modal.lang.none.selected'] = "rien de sélectionné";
$lang['modal.option.email'] = "Email";
$lang['modal.option.phone'] = "Téléphone";
$lang['modal.option.all'] = "All";

#startpage
$lang['startpage.new'] = "Nouveau fabricant";
$lang['startpage.week'] = "Fabricant de la semaine";

#signin
$lang['signin.remember'] = "Souviens-toi de moi";
$lang['signin.invalid'] = "Les informations d'identification invalides";
$lang['signin.blocked'] = "Votre compte a été bloqué.";
$lang['signin.back'] = "Retour";
$lang['signin.submit'] = "Soumettre";
$lang['signin.forgot'] = "Mot de passe oublié ?";

#signup
$lang['signup.desc'] = "Enregistrez-vous ici sur made-in.ma en tant que fabricant ou prestataire de services, gratuitement et sans obligation. Vous pouvez annoncer vos produits et services aux niveaux national et international.";

#sms verification
$lang['sms.desc'] = "S'il vous plaît entrer votre code de vérification SMS.";
$lang['sms.code.error'] = "Code invalide";
$lang['sms.expire'] = "Votre code a expiré. S'il vous plaît essayez de vous inscrire à nouveau.";

#myacount
$lang['myaccount.company_profile'] = "Profil de la société";
$lang['myaccount.title'] = "Mon compte";
$lang['myaccount.company_information'] = "Informations sur la société";
$lang['myaccount.logo_note'] = "Le logo de votre entreprise. Les images plus grandes que les pixels seront réduites.";
$lang['myaccount.avatar_note'] = "Votre visage ou image virtuelle. Les images plus grandes que les pixels seront réduites.";
$lang['myaccount.company_product'] = "Produit de l'entreprise";
$lang['myaccount.general_information'] = "Informations générales";
$lang['myaccount.change_password'] = "Changer le mot de passe";
$lang['myaccount.current_password'] = "Mot de passe actuel";
$lang['myaccount.promotion'] = 'Promotion';
$lang['myaccount.promotion.desc'] = 'Recommandé fait-en.ma amis et connaissances. Vous recevrez 20 dirhams pour chaque enregistrement de société enregistré que vous entrez en utilisant votre code de promotion. Un paiement est possible à partir de 100 inscriptions. Le paiement est effectué via Western Union.';
$lang['myaccount.promotion.promotion_code'] = 'Votre code de promotion';
$lang['myaccount.promotion.promotion_link'] = "Lien de promotion";
$lang['myaccount.promotion.current_promotions'] = 'Promotions en cours';
$lang['myaccount.new_password'] = "Nouveau mot de passe";
$lang['myaccount.select'] = "Sélectionner";
$lang['myaccount.logo'] = "Logo";
$lang['myaccount.contact_avatar'] = "Photo de la personne de contact";
$lang['myaccount.product_warning'] = "S'il vous plaît ajouter un produit.";
$lang['myaccount.preview'] = "Aperçu";
$lang['myaccount.del_account'] = "Supprimer le compte";
$lang['myaccount.del_account_msg'] = "Êtes-vous sûr de vouloir supprimer?";
$lang['myaccount.messages'] = "Messages";
$lang['myaccount.support'] = "Soutien";

#companies
$lang['companies.all'] = "Tout";
$lang['companies.sortby'] = "Trier par";
$lang['companies.rating'] = "Évaluation";
$lang['companies.newest'] = "Plus récent";
$lang['companies.oldest'] = "Le plus vieux";
$lang['companies.entries'] = "Les entrées";
$lang['companies.in'] = "Dans ";

#company detail
$lang['detail.since'] = "Puisque";
$lang['detail.employees'] = "Des employés";
$lang['detail.date'] = "Rendez-vous amoureux";
$lang['detail.name'] = "Prénom";
$lang['detail.description'] = "La description";
$lang['detail.noproduct'] = "Il n'y a pas de produit.";
$lang['detail.product.pics'] = 'Produit Photos';
$lang['detail.last.update'] = 'Dernière mise à jour';
$lang['detail.rating'] = 'Évaluation';
$lang['detail.youtube'] = 'Youtube';
$lang['detail.map'] = 'Emplacement';

#rating modal
$lang['rating.name'] = "Prénom";
$lang['rating.country'] = "Pays";
$lang['rating.text'] = "Texte";
$lang['no.ratings'] = "Pas de notes";

#company profile
$lang['profile.since'] = "Puisque";
$lang['profile.employees'] = "Des employés";

#forgot
$lang['forgot.no_email'] = "Aucune adresse e-mail de ce type n'existe.";
$lang['forgot.no_phone'] = "Un tel téléphone n'existe pas.";
$lang['forgot.success_email'] = "Lien de réinitialisation envoyé votre adresse e-mail. Veuillez vérifier votre dossier de courrier indésirable.";
$lang['forgot.success_phone'] = "Réinitialiser le lien envoyé à votre téléphone";
$lang['forgot.invalid_token'] = "Jeton invalide.";
$lang['forgot.update_success'] = "Le mot de passe est correctement mis à jour.";
$lang['forgot.expire'] = "Votre temps d'expiration est passé.";

#contact sms message
$lang['contact.sms'] = 'Veuillez vous connecter à made-in.ma. Vous avez un nouveau message.';
$lang['contact.sms.success'] = 'Votre message a été envoyé au fabricant avec succès.';
$lang['contact.sms.error'] = "Votre message n'a pas été envoyé à cause du problème sms";

#phone number invalid message
$lang['phone.error.title'] = "Error";
$lang['phone.invalid'] = "Your phone number is invalid. The count of digits should be 9 and do not start with '0' at first";

#category info
$lang['categoryInfo.imgURL.26'] = "10.jpg";
$lang['categoryInfo.detail.26'] = "";

$lang['categoryInfo.imgURL.25'] = "10.jpg";
$lang['categoryInfo.detail.25'] = "";

$lang['categoryInfo.imgURL.24'] = "10.jpg";
$lang['categoryInfo.detail.24'] = "";

$lang['categoryInfo.imgURL.23'] = "10.jpg";
$lang['categoryInfo.detail.23'] = "";

$lang['categoryInfo.imgURL.22'] = "10.jpg";
$lang['categoryInfo.detail.22'] = "";

$lang['categoryInfo.imgURL.21'] = "10.jpg";
$lang['categoryInfo.detail.21'] = "";

$lang['categoryInfo.imgURL.20'] = "10.jpg";
$lang['categoryInfo.detail.20'] = "";

$lang['categoryInfo.imgURL.19'] = "10.jpg";
$lang['categoryInfo.detail.19'] = "";

$lang['categoryInfo.imgURL.18'] = "10.jpg";
$lang['categoryInfo.detail.18'] = "";

$lang['categoryInfo.imgURL.17'] = "10.jpg";
$lang['categoryInfo.detail.17'] = "";

$lang['categoryInfo.imgURL.16'] = "10.jpg";
$lang['categoryInfo.detail.16'] = "";

$lang['categoryInfo.imgURL.15'] = "10.jpg";
$lang['categoryInfo.detail.15'] = "";

$lang['categoryInfo.imgURL.14'] = "10.jpg";
$lang['categoryInfo.detail.14'] = "";

$lang['categoryInfo.imgURL.13'] = "10.jpg";
$lang['categoryInfo.detail.13'] = "";

$lang['categoryInfo.imgURL.12'] = "10.jpg";
$lang['categoryInfo.detail.12'] = "";

$lang['categoryInfo.imgURL.11'] = "10.jpg";
$lang['categoryInfo.detail.11'] = "";

$lang['categoryInfo.imgURL.10'] = "10.jpg";
$lang['categoryInfo.detail.10'] = "";

$lang['categoryInfo.imgURL.9'] = "10.jpg";
$lang['categoryInfo.detail.9'] = "";

$lang['categoryInfo.imgURL.8'] = "10.jpg";
$lang['categoryInfo.detail.8'] = "";

$lang['categoryInfo.imgURL.7'] = "10.jpg";
$lang['categoryInfo.detail.7'] = "";

$lang['categoryInfo.imgURL.6'] = "10.jpg";
$lang['categoryInfo.detail.6'] = "";

$lang['categoryInfo.imgURL.5'] = "10.jpg";
$lang['categoryInfo.detail.5'] = "";

$lang['categoryInfo.imgURL.4'] = "10.jpg";
$lang['categoryInfo.detail.4'] = "";

$lang['categoryInfo.imgURL.3'] = "10.jpg";
$lang['categoryInfo.detail.3'] = "";

$lang['categoryInfo.imgURL.2'] = "10.jpg";
$lang['categoryInfo.detail.2'] = "";

$lang['categoryInfo.imgURL.1'] = "10.jpg";
$lang['categoryInfo.detail.1'] = "";
