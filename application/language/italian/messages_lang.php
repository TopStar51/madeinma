<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['site.description'] = "Trova produttori, fornitori e servizi in Marocco";
$lang['site.title'] = "made-in.ma - Il mercato online per le aziende marocchine";

$lang['site.keywords'] = "INETTECH, INETTECH.IO, made-in.ma, Made in Morocco, Manufacturer, Suppliers, Service Companies, B2B, B2B-Plattform, Export, Import, Products";

#register
$lang['header.register'] = 'Registrare';

#message
$lang['general.lang.en'] = "English";
$lang['general.lang.fr'] = "Français";
$lang['general.lang.it'] = "Italiano";
$lang['general.lang.es'] = "Español";
$lang['general.lang.de'] = "Deutsch";
$lang['general.lang.ar'] = "العربية";
$lang['general.lang.cn'] = "中國";
$lang['general.lang.tu'] = "Türkçe";
// $lang['general.lang.ir'] = "Farsi";

#message
$lang['message.success'] = "Successo";
$lang['message.error'] = "Errore";
$lang['message.warning'] = "Avvertimento";
$lang['message.ok'] = "OK";
$lang['message.cancel'] = "Annulla";
$lang['message.signin'] = "Registrati";
$lang['message.signup'] = "Iscriviti";
$lang['message.signout'] = "Disconnessione";
$lang['message.save'] = "Salvare";
$lang['message.mail'] = "Posta";
$lang['message.rating'] = "Valutazione";
$lang['message.call'] = "Chiamata";
$lang['message.save_success'] = "È necessario controllare prima del rilascio attendere ...";
$lang['message.confirmation'] = "Conferma";
$lang['message.new_password'] = "Si prega di inserire una nuova password";
$lang['message.recaptacha_error'] = "Scusate. Google Recaptcha non riuscito!";
$lang['message.phone_duplicate'] = 'Telefono non già registrato.';
$lang['message.sms_error'] = 'Numero di telefono errato';
$lang['message.label.name'] = "Nome";
$lang['message.label.email'] = "E-mail";
$lang['message.label.phone'] = 'Telefono';
$lang['message.label.phone'] = "Telefono";
$lang['message.label.country'] = "Nazione";
$lang['message.label.language'] = "Linguaggio";
$lang['message.label.content'] = "Soddisfare";
$lang['message.label.created_at'] = "Creato a";
$lang['message.company_error'] = "Company name is already registered.";

#validation
$lang['validation.user.name.salutation'] = "Per favore, inserisci il saluto.";
$lang['validation.user.name.require'] = "Per favore inserisci il tuo nome.";
$lang['validation.user.email.require'] = "Per favore inserisci la tua email";
$lang['validation.user.phone.require'] = 'Per favore, inserisca il suo numero di telefono.';
$lang['validation.user.email.valid'] = "Si prega di inserire un indirizzo email valido.";
$lang['validation.user.email.unique'] = "Questo indirizzo email è già stato registrato. <br> Si prega di utilizzare un indirizzo e-mail diverso.";
$lang['validation.user.phone.unique'] = "Questo numero di telefono è già registrato. <br> Utilizzare un numero di telefono diverso.";
$lang['validation.user.password.require'] = "Per favore inserisci LA TUA password.";
$lang['validation.user.confirm_password.require'] = "Inserisci una password di conferma.";
$lang['validation.user.confirm_password.valid'] = "La password e la password di conferma non corrispondono.";
$lang['validation.user.new_password.require'] = "Per favore inserisci la tua nuova password";
$lang['validation.user.current_password.require'] = "Per favore inserisci la tua password attuale.";
$lang['validation.user.sms_code.require'] = "Inserisci il tuo codice sms.";

$lang['validation.company.name.require'] = "Per favore inserisci il nome della tua azienda.";
$lang['validation.company.salutation.require'] = "Per favore, inserisci il saluto.";
$lang['validation.company.contact_person.require'] = "Please enter contact person.";
$lang['validation.company.street.require'] = "Per favore, inserisci la strada.";
$lang['validation.company.city.require'] = "Seleziona la città";
$lang['validation.company.founded_at.require'] = "Inserisci l'anno di fondazione.";
$lang['validation.company.email.require'] = "Per favore, inserisci l'e-mail.";
$lang['validation.company.zipcode.require'] = "Si prega di inserire il codice postale.";
$lang['validation.company.phone.require'] = "Per favore inserisci il telefono";
$lang['validation.company.website.require'] = "Per favore, inserisci il sito web.";
$lang['validation.company.keywords.require'] = "Per favore, inserisci le parole chiave.";
$lang['validation.company.lang.require'] = "Seleziona la lingua";
$lang['validation.company.employee_count.require'] = "Seleziona il numero di dipendenti.";
$lang['validation.company.category.require'] = "Seleziona la categoria";

#user
$lang['user.name'] = "Nome";
$lang['user.email'] = "E-mail";
$lang['user.phone'] = 'Telefono';
$lang['user.password'] = "Parola d'ordine";
$lang['user.confirm_password'] = "Conferma password";
$lang['user.sms_code'] = 'Codice';
$lang['user.promotion_code'] = 'Codice promozionale';

#placeholder
$lang['user.phone.flag'] = 'assets/madinma/front/images/1.png';
$lang['user.phone.placeholder'] = '650123456';
$lang['phone.placeholder'] = '+212...';
$lang['promo.placeholder'] = 'A12b';

#company setting labels
$lang['company.logo'] = "Logo della compagnia";
$lang['company.name'] = "Titolo";
$lang['company.salutation'] = "Saluto";
$lang['company.contact_person'] = "Referente";
$lang['company.street'] = "Strada";
$lang['company.city'] = "Città";
$lang['company.founded_at'] = "Fondata a";
$lang['company.employee_count'] = "Numero di dipendenti";
$lang['company.zipcode'] = "Cap";
$lang['company.phone'] = "Telefono";
$lang['company.email'] = "E-mail";
$lang['company.website'] = "Sito web";
$lang['company.youtube'] = "Video";
$lang['company.keywords'] = "Parole chiave";
$lang['company.about_us'] = "Riguardo a noi";
$lang['company.category'] = "Categoria";
$lang['company.language'] = "Le lingue";
$lang['company.latitude'] = "Latitudine";
$lang['company.longitude'] = "Longitudine";

#company setting placeholders
$lang['company.logo.placeholder'] = "Logo della compagnia";
$lang['company.name.placeholder'] = "Ameublement el Bahja";
$lang['company.salutation.placeholder.mr'] = "Sig";
$lang['company.salutation.placeholder.ms'] = "Signorina";
$lang['company.contact_person.placeholder'] = "Mohamed Kamal";
$lang['company.street.placeholder'] = "138 Boulevard Moulay Youssef, Angle Rue Tiznit";
+
$lang['company.founded_at.placeholder'] = "2008";
$lang['company.zipcode.placeholder'] = "47963";
$lang['company.phone.placeholder'] = "+212...";
$lang['company.email.placeholder'] = "your@mail.ma";
$lang['company.website.placeholder'] = "https://yourwebsite.ma";
$lang['company.youtube.placeholder'] = "https://www.youtube.com/watch?v=iwa7_SHf114";
$lang['company.keywords.placeholder'] = "Olio di Argan, Argan ...";
$lang['company.about_us.placeholder'] = "Riguardo a noi";
$lang['company.lang.non.selected.placeholder'] = "Niente di selezionato";
$lang['company.latitude.placeholder'] = "33.333333";
$lang['company.longitude.placeholder'] = "43.333333";

#top-search
$lang['topsearch.search'] = "Ricerca";
$lang['topsearch.placeholder'] = "Che cosa sta cercando...";

#sidebar
$lang['sidebar.title'] = ""; // Category

#footer
$lang['footer.about_title'] = "Informazioni su made-in.ma";
$lang['footer.about_content'] = 'made-in.ma è un organizzazione senza scopo di lucro che vuole rafforzare l economia delle esportazioni delle società più piccole in Marocco. Produttori, fornitori di servizi e fornitori possono presentarsi ai clienti internazionali.<br><br>Questo servizio è fornito da <a href="https://www.inettech.io" title="">INETTECH.io</a><br><br><a href="https://www.youtube.com/channel/UCAqJdNAA1onUmm7KZo4qvqg" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_youtube.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/madein.ma/" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_instagram.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://fb.me/wwwmadeinma" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_facebook.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href=" https://wa.me/212688217862" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_whatsapp.png" alt="" height="35" width="35"></a>';
$lang['footer.contact_title'] = "Contatto e cooperazione";
$lang['footer.contact_content'] = "Vi preghiamo di contattarci via mail";
$lang['footer.mail'] = "Posta";
$lang['footer.statistics_title'] = "Statistica";
$lang['footer.statistics_comapny'] = "Società quotate";
$lang['footer.statistics_comapny_week'] = "Nuovo questa settimana";
$lang['footer.statistics_inquery'] = "Totale richiesta";
$lang['footer.statistics_inquery_week'] = "Inchiesta questa settimana";

#modals
$lang['modal.contact.title'] = "Contatto";
$lang['modal.rating.title'] = "Valutazione";
$lang['modal.call.title'] = "Chiamata";
$lang['modal.btn.send'] = "Inviare";
$lang['modal.placeholder.name'] = "Nome";
$lang['modal.placeholder.email'] = "E-mail";
$lang['modal.placeholder.phone'] = "+2125XXXXXXXX";
$lang['modal.placeholder.text'] = "Testo";
$lang['modal.placeholder.company_detail'] = "Please write here your text to the company";
$lang['modal.placeholder.rating_detail'] = "Please write here your comment to this company";
$lang['modal.your_rating'] = "Il tuo punteggio";
$lang['modal.call_desc'] = "Dimmi che hai trovato il mio annuncio su made-in.ma. Grazie!";
$lang['modal.confirm.title'] = "Confermare";
$lang['modal.confirm.question'] = "Conferma il tuo numero di telefono!";

$lang['modal.lang.none.selected'] = 'Nothing selected';
$lang['modal.option.email'] = "Email";
$lang['modal.option.phone'] = "Phone";
$lang['modal.option.all'] = "All";
#startpage
$lang['startpage.new'] = "Nuovo produttore";
$lang['startpage.week'] = "Produttore della settimana";

#signin
$lang['signin.remember'] = "Ricordati di me";
$lang['signin.invalid'] = "Credenziali non valide";
$lang['signin.blocked'] = "Il tuo account è stato bloccato.";
$lang['signin.back'] = "Indietro";
$lang['signin.submit'] = "Sottoscrivi";
$lang['signin.forgot'] = "Dimenticare la password?";

#signup
$lang['signup.desc'] = "Registrati qui su made-in.ma come produttore o fornitore di servizi gratuitamente e senza impegno. Puoi pubblicizzare i tuoi prodotti e servizi a livello nazionale e internazionale.";

#sms verification
$lang['sms.desc'] = "Inserisci il tuo codice di verifica sms.";
$lang['sms.code.error'] = "Codice non VALIDO";
$lang['sms.expire'] = "Il tuo codice è scaduto Prova a registrarti di nuovo.";

#myacount
$lang['myaccount.company_profile'] = "Profilo Aziendale";
$lang['myaccount.title'] = "Il mio account";
$lang['myaccount.company_information'] = "Informazioni sull'azienda";
$lang['myaccount.logo_note'] = "Il logo della tua azienda. Le immagini più grandi dei pixel verranno ridimensionate.";
$lang['myaccount.avatar_note'] = "La tua faccia o foto virtuale. Le immagini più grandi dei pixel verranno ridimensionate.";
$lang['myaccount.company_product'] = "Prodotto aziendale";
$lang['myaccount.general_information'] = "Informazione generale";
$lang['myaccount.change_password'] = "Cambia la password";
$lang['myaccount.current_password'] = "Password attuale";
$lang['myaccount.promotion'] = 'Promozione';
$lang['myaccount.promotion.desc'] = 'Consigliati amici e conoscenti made-in.ma. Riceverai 20 dirham per ciascuna registrazione della società registrata che inserisci utilizzando il tuo codice promozionale. Un pagamento è possibile da 100 registrazioni. Il pagamento viene effettuato tramite Western Union.';
$lang['myaccount.promotion.promotion_code'] = 'Il tuo codice promozionale';
$lang['myaccount.promotion.promotion_link'] = 'Link promozionale';
$lang['myaccount.promotion.current_promotions'] = 'Promozioni attuali';
$lang['myaccount.new_password'] = "Nuova password";
$lang['myaccount.select'] = "Selezionare";
$lang['myaccount.logo'] = "Logo";
$lang['myaccount.contact_avatar'] = "Foto della persona di contatto";
$lang['myaccount.product_warning'] = "Si prega di aggiungere un prodotto.";
$lang['myaccount.preview'] = "Anteprima";
$lang['myaccount.del_account'] = "Eliminare l'account";
$lang['myaccount.del_account_msg'] = "Sei sicuro di voler eliminare?";
$lang['myaccount.messages'] = "Messaggi";
$lang['myaccount.support'] = "Supporto";

#companies
$lang['companies.all'] = "Tutti";
$lang['companies.sortby'] = "Ordina per";
$lang['companies.rating'] = "Valutazione";
$lang['companies.newest'] = "Più recente";
$lang['companies.oldest'] = "Il più vecchio";
$lang['companies.entries'] = "Inserimenti";
$lang['companies.in'] = "Nel";

#company detail
$lang['detail.employees'] = "Dipendenti";
$lang['detail.date'] = "Data";
$lang['detail.name'] = "Nome";
$lang['detail.description'] = "Descrizione";
$lang['detail.noproduct'] = "Non ci sono prodotti";
$lang['detail.product.pics'] = 'Immagini del prodotto';
$lang['detail.last.update'] = 'Ultimo aggiornamento';
$lang['detail.rating'] = 'Valutazione';
$lang['detail.youtube'] = 'Youtube';
$lang['detail.map'] = 'Posizione';

#rating modal
$lang['rating.name'] = "Nome";
$lang['rating.country'] = "Nazione";
$lang['rating.text'] = "Testo";
$lang['no.ratings'] = "Nessuna valutazione";

#company profile
$lang['profile.since'] = "Da";
$lang['profile.employees'] = "Dipendenti";

#forgot
$lang['forgot.no_email'] = "Nessun indirizzo e-mail esiste.";
$lang['forgot.no_phone'] = "Non esiste un telefono simile.";
$lang['forgot.success_email'] = "Reimposta link ha inviato il tuo indirizzo email. Controlla la cartella della posta indesiderata.";
$lang['forgot.success_phone'] = "Reimposta collegamento inviato al telefono";
$lang['forgot.invalid_token'] = "Gettone non valido.";
$lang['forgot.update_success'] = "La password è stata aggiornata con successo";
$lang['forgot.expire'] = "Il tempo di scadenza è trascorso.";

#contact sms message
$lang['contact.sms'] = 'Accedi made-in.ma. Hai un nuovo messaggio';
$lang['contact.sms.success'] = 'Il tuo messaggio è stato inviato correttamente al produttore.';
$lang['contact.sms.error'] = 'Il tuo messaggio non è stato inviato a causa di un problema sms';

#phone number invalid message
$lang['phone.error.title'] = "Error";
$lang['phone.invalid'] = "Your phone number is invalid. The count of digits should be 9 and do not start with '0' at first";

#category info
$lang['categoryInfo.imgURL.26'] = "10.jpg";
$lang['categoryInfo.detail.26'] = "";

$lang['categoryInfo.imgURL.25'] = "10.jpg";
$lang['categoryInfo.detail.25'] = "";

$lang['categoryInfo.imgURL.24'] = "10.jpg";
$lang['categoryInfo.detail.24'] = "";

$lang['categoryInfo.imgURL.23'] = "10.jpg";
$lang['categoryInfo.detail.23'] = "";

$lang['categoryInfo.imgURL.22'] = "10.jpg";
$lang['categoryInfo.detail.22'] = "";

$lang['categoryInfo.imgURL.21'] = "10.jpg";
$lang['categoryInfo.detail.21'] = "";

$lang['categoryInfo.imgURL.20'] = "10.jpg";
$lang['categoryInfo.detail.20'] = "";

$lang['categoryInfo.imgURL.19'] = "10.jpg";
$lang['categoryInfo.detail.19'] = "";

$lang['categoryInfo.imgURL.18'] = "10.jpg";
$lang['categoryInfo.detail.18'] = "";

$lang['categoryInfo.imgURL.17'] = "10.jpg";
$lang['categoryInfo.detail.17'] = "";

$lang['categoryInfo.imgURL.16'] = "10.jpg";
$lang['categoryInfo.detail.16'] = "";

$lang['categoryInfo.imgURL.15'] = "10.jpg";
$lang['categoryInfo.detail.15'] = "";

$lang['categoryInfo.imgURL.14'] = "10.jpg";
$lang['categoryInfo.detail.14'] = "";

$lang['categoryInfo.imgURL.13'] = "10.jpg";
$lang['categoryInfo.detail.13'] = "";

$lang['categoryInfo.imgURL.12'] = "10.jpg";
$lang['categoryInfo.detail.12'] = "";

$lang['categoryInfo.imgURL.11'] = "10.jpg";
$lang['categoryInfo.detail.11'] = "";

$lang['categoryInfo.imgURL.10'] = "10.jpg";
$lang['categoryInfo.detail.10'] = "";

$lang['categoryInfo.imgURL.9'] = "10.jpg";
$lang['categoryInfo.detail.9'] = "";

$lang['categoryInfo.imgURL.8'] = "10.jpg";
$lang['categoryInfo.detail.8'] = "";

$lang['categoryInfo.imgURL.7'] = "10.jpg";
$lang['categoryInfo.detail.7'] = "";

$lang['categoryInfo.imgURL.6'] = "10.jpg";
$lang['categoryInfo.detail.6'] = "";

$lang['categoryInfo.imgURL.5'] = "10.jpg";
$lang['categoryInfo.detail.5'] = "";

$lang['categoryInfo.imgURL.4'] = "10.jpg";
$lang['categoryInfo.detail.4'] = "";

$lang['categoryInfo.imgURL.3'] = "10.jpg";
$lang['categoryInfo.detail.3'] = "";

$lang['categoryInfo.imgURL.2'] = "10.jpg";
$lang['categoryInfo.detail.2'] = "";

$lang['categoryInfo.imgURL.1'] = "10.jpg";
$lang['categoryInfo.detail.1'] = "";
