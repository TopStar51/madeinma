<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['site.description'] = "查找制造商，供应商和服务公司";
$lang['site.title'] = "made-in.ma - 摩洛哥公司的在线市场";
$lang['site.keywords'] = "INETTECH, INETTECH.IO, made-in.ma, Made in Morocco, Manufacturer, Suppliers, Service Companies, B2B, B2B-Plattform, Export, Import, Products";

#register
$lang['header.register'] = '寄存器';

#message
$lang['general.lang.en'] = "English";
$lang['general.lang.fr'] = "Français";
$lang['general.lang.it'] = "Italiano";
$lang['general.lang.es'] = "Español";
$lang['general.lang.de'] = "Deutsch";
$lang['general.lang.ar'] = "العربية";
$lang['general.lang.cn'] = "中国";
$lang['general.lang.tu'] = "Türkçe";
// $lang['general.lang.ir'] = "Farsi";

#message
$lang['message.success'] = "成功";
$lang['message.error'] = "错误";
$lang['message.warning'] = "Warning";
$lang['message.ok'] = "好";
$lang['message.cancel'] = "取消";
$lang['message.signin'] = "登入";
$lang['message.signup'] = "注册";
$lang['message.signout'] = "退出";
$lang['message.save'] = "保存";
$lang['message.mail'] = "邮件";
$lang['message.rating'] = "评分";
$lang['message.call'] = "呼叫";
$lang['message.save_success'] = "发布前需要检查，请稍候...";
$lang['message.confirmation'] = "确认书";
$lang['message.new_password'] = "请输入新密码";
$lang['message.recaptacha_error'] = "抱歉。 Google Recaptcha不成功！";
$lang['message.phone_duplicate'] = '电话未注册。';
$lang['message.sms_error'] = '电话号码错误';
$lang['message.label.name'] = '姓名';
$lang['message.label.email'] = "电子邮件";
$lang['message.label.phone'] = '电话';
$lang['message.label.phone'] = "电话";
$lang['message.label.country'] = "国家";
$lang['message.label.language'] = "语言";
$lang['message.label.content'] = "內容";
$lang['message.label.created_at'] = "创建于";
$lang['message.company_error'] = "Company name is already registered.";

#validation
$lang['validation.user.name.salutation'] = "请输入称呼。";
$lang['validation.user.name.require'] = "请输入你的名字。";
$lang['validation.user.email.require'] = "请输入您的电子邮件。";
$lang['validation.user.phone.require'] = '请输入您的电话号码。';
$lang['validation.user.email.valid'] = "请输入有效的电子邮件地址。";
$lang['validation.user.email.unique'] = "该邮箱地址已被注册。 <br>请使用其他电子邮件地址。";
$lang['validation.user.phone.unique'] = "该电话号码已被注册。 <br>请使用其他电话号码。";
$lang['validation.user.password.require'] = "请输入您的密码。";
$lang['validation.user.confirm_password.require'] = "请输入您的确认密码。";
$lang['validation.user.confirm_password.valid'] = "您的密码和确认密码不同。";
$lang['validation.user.new_password.require'] = "请输入新密码。";
$lang['validation.user.current_password.require'] = "请输入您的当前密码。";
$lang['validation.user.sms_code.require'] = "请输入您的短信代码。";

$lang['validation.company.name.require'] = "请输入您的公司名。";
$lang['validation.company.salutation.require'] = "请输入称呼。";
$lang['validation.company.contact_person.require'] = "请输入联系人。";
$lang['validation.company.street.require'] = "请输入街道。";
$lang['validation.company.city.require'] = "请选择城市。";
$lang['validation.company.founded_at.require'] = "请输入成立年份。";
$lang['validation.company.email.require'] = "请输入电子邮件。";
$lang['validation.company.zipcode.require'] = "请输入邮政编码。";
$lang['validation.company.phone.require'] = "请输入电话号码。";
$lang['validation.company.website.require'] = "请输入网站。";
$lang['validation.company.keywords.require'] = "请输入关键词。";
$lang['validation.company.lang.require'] = "请选择语言。";
$lang['validation.company.employee_count.require'] = "请选择员工人数。";
$lang['validation.company.category.require'] = "请选择类别。";

#user
$lang['user.name'] = "姓名";
$lang['user.email'] = "电子邮件";
$lang['user.phone'] = '电话';
$lang['user.password'] = "密码";
$lang['user.confirm_password'] = "确认密码";
$lang['user.sms_code'] = '短信编码';
$lang['user.promotion_code'] = '促销代码';

#placeholder
$lang['user.phone.flag'] = 'assets/madinma/front/images/1.png';
$lang['user.phone.placeholder'] = '650123456';
$lang['phone.placeholder'] = '+212...';
$lang['promo.placeholder'] = 'A12B';

#company setting labels
$lang['company.logo'] = "公司标志";
$lang['company.name'] = "公司名";
$lang['company.salutation'] = "称呼";
$lang['company.contact_person'] = "联系人";
$lang['company.street'] = "街";
$lang['company.city'] = "市";
$lang['company.founded_at'] = "成立于";
$lang['company.employee_count'] = "在职员工人数";
$lang['company.zipcode'] = "邮政编码";
$lang['company.phone'] = "电话";
$lang['company.email'] = "电子邮件";
$lang['company.website'] = "网站";
$lang['company.youtube'] = "Youtube";
$lang['company.keywords'] = "关键词";
$lang['company.about_us'] = "于于我们";
$lang['company.category'] = "类别";
$lang['company.language'] = "语言";
$lang['company.latitude'] = "纬度";
$lang['company.longitude'] = "经度";

#company setting placeholders
$lang['company.logo.placeholder'] = "公司标志";
$lang['company.name.placeholder'] = "标题";
$lang['company.salutation.placeholder.mr'] = "先生";
$lang['company.salutation.placeholder.ms'] = "女士";
$lang['company.contact_person.placeholder'] = "Mohamed Kamal";
$lang['company.street.placeholder'] = "138 Boulevard Moulay Youssef, Angle Rue Tiznit";
$lang['company.founded_at.placeholder'] = "2008";
$lang['company.zipcode.placeholder'] = "47963";
$lang['company.phone.placeholder'] = "+212...";
$lang['company.email.placeholder'] = "your@mail.ma";
$lang['company.website.placeholder'] = "https://yourwebsite.ma";
$lang['company.youtube.placeholder'] = "https://www.youtube.com/watch?v=iwa7_SHf114";
$lang['company.keywords.placeholder'] = "摩洛哥坚果油，摩洛哥坚果...";
$lang['company.about_us.placeholder'] = "于于我们";
$lang['company.lang.non.selected.placeholder'] = "没有选择";
$lang['company.latitude.placeholder'] = "33.333333";
$lang['company.longitude.placeholder'] = "43.333333";

#top-search
$lang['topsearch.search'] = "搜索";
$lang['topsearch.placeholder'] = "你在找什麼...";

#sidebar
$lang['sidebar.title'] = ""; // Category

#footer
$lang['footer.about_title'] = "于于 made-in.ma";
$lang['footer.about_content'] = 'made-in.ma 是一家非營利组织，希望加強摩洛哥较小公司的出口经济。 制造商，服务提供商和供应商可以向国际客户展示自己<br><br>此服务由 <a href="https://www.inettech.io" title="">INETTECH.io</a> 提供<br><br><a href="https://www.youtube.com/channel/UCAqJdNAA1onUmm7KZo4qvqg" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_youtube.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/madein.ma/" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_instagram.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://fb.me/wwwmadeinma" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_facebook.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href=" https://wa.me/212688217862" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_whatsapp.png" alt="" height="35" width="35"></a>';
$lang['footer.contact_title'] = "联系与合作";
$lang['footer.contact_content'] = "请通过邮件联系我们";
$lang['footer.mail'] = "邮件";
$lang['footer.statistics_title'] = "统计";
$lang['footer.statistics_comapny'] = "上市公司";
$lang['footer.statistics_comapny_week'] = "本周新";
$lang['footer.statistics_inquery'] = "询问总数";
$lang['footer.statistics_inquery_week'] = "本周查询";

#modals
$lang['modal.contact.title'] = "联系";
$lang['modal.rating.title'] = "评分";
$lang['modal.call.title'] = "呼叫";
$lang['modal.btn.send'] = "发送";
$lang['modal.placeholder.name'] = "姓名";
$lang['modal.placeholder.email'] = "电子邮件";
$lang['modal.placeholder.phone'] = "+2125XXXXXXXX";
$lang['modal.placeholder.text'] = "文本";
$lang['modal.placeholder.company_detail'] = "请把你的短信写在这里给公司";
$lang['modal.placeholder.rating_detail'] = "请在这里写下你对这家公司的意见";
$lang['modal.your_rating'] = "你的评分";
$lang['modal.call_desc'] = "告訴我您在made-in.ma上找到了我的廣告。 謝謝！";
$lang['modal.confirm.title'] = "确认";
$lang['modal.confirm.question'] = "确认您的电话号码！";

$lang['modal.lang.none.selected'] = '未选择任何内容';
$lang['modal.option.email'] = "电子邮件";
$lang['modal.option.phone'] = "电话";
$lang['modal.option.all'] = "全部";

#startpage
$lang['startpage.new'] = "新制造商";
$lang['startpage.week'] = "本周制造商";

#signin
$lang['signin.remember'] = "记住帐号";
$lang['signin.invalid'] = "无效证件";
$lang['signin.blocked'] = "您的帐户已被封锁。";
$lang['signin.back'] = "換回";
$lang['signin.submit'] = "提交";
$lang['signin.forgot'] = "忘记密码？";

#signup
$lang['signup.desc'] = "在此处免费在制造商或服务提供商处注册，没有任何义务。 您可以在国內和国际上宣传您的产品和服务。";

#sms verification
$lang['sms.desc'] = "请输入您的短信驗证码。";
$lang['sms.code.error'] = "无效的代码";
$lang['sms.expire'] = "您的代码已过期。 请尝试重新注册。";

#myacount
$lang['myaccount.company_profile'] = "公司简介";
$lang['myaccount.title'] = "我的帐户";
$lang['myaccount.company_information'] = "公司信息";
$lang['myaccount.logo_note'] = "您的公司徽标。 大于像素的图片將按比例缩小。";
$lang['myaccount.avatar_note'] = "您的虛擬面孔或图片。 大于像素的图片將按比例缩小。";
$lang['myaccount.company_product'] = "公司产品";
$lang['myaccount.general_information'] = "一般信息";
$lang['myaccount.change_password'] = "更改密码";
$lang['myaccount.current_password'] = "当前密码";
$lang['myaccount.promotion'] = '晋升';
$lang['myaccount.promotion.desc'] = '推荐made in.ma朋友和熟人。您将收到20迪拉姆为每个注册公司注册，您输入使用您的推广代码。一个支付是可能的rom 100注册。付款是通过西联银行支付的。';
$lang['myaccount.promotion.promotion_code'] = '您的促销代码';
$lang['myaccount.promotion.promotion_link'] = '促销链接';
$lang['myaccount.promotion.current_promotions'] = '最新促销';
$lang['myaccount.new_password'] = "新密码";
$lang['myaccount.select'] = "选择";
$lang['myaccount.logo'] = "商标";
$lang['myaccount.contact_avatar'] = "联系人的照片";
$lang['myaccount.product_warning'] = "请添加产品。";
$lang['myaccount.preview'] = "预览";
$lang['myaccount.del_account'] = "刪除帐户";
$lang['myaccount.del_account_msg'] = "您确定要刪除嗎？";
$lang['myaccount.messages'] = "留言內容";
$lang['myaccount.support'] = "支持";

#companies
$lang['companies.all'] = "所有";
$lang['companies.sortby'] = "排序方式";
$lang['companies.rating'] = "评分";
$lang['companies.newest'] = "最新的";
$lang['companies.oldest'] = "最久的";
$lang['companies.entries'] = "参赛作品";
$lang['companies.in'] = " 在 ";

#company detail
$lang['detail.employees'] = "雇员";
$lang['detail.date'] = "日期";
$lang['detail.name'] = "姓名";
$lang['detail.description'] = "描述";
$lang['detail.noproduct'] = "没有商品";
$lang['detail.product.pics'] = '产品图片';
$lang['detail.last.update'] = '最后更新';
$lang['detail.rating'] = '评分';
$lang['detail.youtube'] = 'Youtube';
$lang['detail.map'] = '地图';

#rating modal
$lang['rating.name'] = "姓名";
$lang['rating.country'] = "国家";
$lang['rating.text'] = "文本";
$lang['no.ratings'] = "无评分";

#company profile
$lang['profile.since'] = "以来";
$lang['profile.employees'] = "雇员";

#forgot
$lang['forgot.no_email'] = "没有这样的电子邮件地址。";
$lang['forgot.no_phone'] = "没有这样的电话。";
$lang['forgot.success_email'] = "重置链接已发送您的电子邮件地址。 请检查您的垃圾邮件文件夹。";
$lang['forgot.success_phone'] = "重置发送到您手機的链接";
$lang['forgot.invalid_token'] = "令牌无效。";
$lang['forgot.update_success'] = "密码已成功更新。";
$lang['forgot.expire'] = "您的过期时间已过。";

#contact sms message
$lang['contact.sms'] = '请登錄made-in.ma。 您有新讯息。';
$lang['contact.sms.success'] = '您的消息已成功发送到制造商。';
$lang['contact.sms.error'] = '由于短信问题，您的消息未发送';

#phone number invalid message
$lang['phone.error.title'] = "错误";
$lang['phone.invalid'] = "你的电话号码无效。数字的计数应为9，开始时不以“0”开头";

#category info
$lang['categoryInfo.imgURL.26'] = "10.jpg";
$lang['categoryInfo.detail.26'] = "";

$lang['categoryInfo.imgURL.25'] = "10.jpg";
$lang['categoryInfo.detail.25'] = "";

$lang['categoryInfo.imgURL.24'] = "10.jpg";
$lang['categoryInfo.detail.24'] = "";

$lang['categoryInfo.imgURL.23'] = "10.jpg";
$lang['categoryInfo.detail.23'] = "";

$lang['categoryInfo.imgURL.22'] = "10.jpg";
$lang['categoryInfo.detail.22'] = "";

$lang['categoryInfo.imgURL.21'] = "10.jpg";
$lang['categoryInfo.detail.21'] = "";

$lang['categoryInfo.imgURL.20'] = "10.jpg";
$lang['categoryInfo.detail.20'] = "";

$lang['categoryInfo.imgURL.19'] = "10.jpg";
$lang['categoryInfo.detail.19'] = "";

$lang['categoryInfo.imgURL.18'] = "10.jpg";
$lang['categoryInfo.detail.18'] = "";

$lang['categoryInfo.imgURL.17'] = "10.jpg";
$lang['categoryInfo.detail.17'] = "";

$lang['categoryInfo.imgURL.16'] = "10.jpg";
$lang['categoryInfo.detail.16'] = "";

$lang['categoryInfo.imgURL.15'] = "10.jpg";
$lang['categoryInfo.detail.15'] = "";

$lang['categoryInfo.imgURL.14'] = "10.jpg";
$lang['categoryInfo.detail.14'] = "";

$lang['categoryInfo.imgURL.13'] = "10.jpg";
$lang['categoryInfo.detail.13'] = "";

$lang['categoryInfo.imgURL.12'] = "10.jpg";
$lang['categoryInfo.detail.12'] = "";

$lang['categoryInfo.imgURL.11'] = "10.jpg";
$lang['categoryInfo.detail.11'] = "";

$lang['categoryInfo.imgURL.10'] = "10.jpg";
$lang['categoryInfo.detail.10'] = "";

$lang['categoryInfo.imgURL.9'] = "10.jpg";
$lang['categoryInfo.detail.9'] = "";

$lang['categoryInfo.imgURL.8'] = "10.jpg";
$lang['categoryInfo.detail.8'] = "";

$lang['categoryInfo.imgURL.7'] = "10.jpg";
$lang['categoryInfo.detail.7'] = "";

$lang['categoryInfo.imgURL.6'] = "10.jpg";
$lang['categoryInfo.detail.6'] = "";

$lang['categoryInfo.imgURL.5'] = "10.jpg";
$lang['categoryInfo.detail.5'] = "";

$lang['categoryInfo.imgURL.4'] = "10.jpg";
$lang['categoryInfo.detail.4'] = "";

$lang['categoryInfo.imgURL.3'] = "10.jpg";
$lang['categoryInfo.detail.3'] = "";

$lang['categoryInfo.imgURL.2'] = "10.jpg";
$lang['categoryInfo.detail.2'] = "";

$lang['categoryInfo.imgURL.1'] = "10.jpg";
$lang['categoryInfo.detail.1'] = "";
