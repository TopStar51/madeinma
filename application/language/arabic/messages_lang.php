<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['site.description'] = "العثور على الشركات المصنعة والموردين وشركات الخدمات في المغرب";
$lang['site.title'] = "made-in.ma - السوق الإلكتروني للشركات المغربية";
$lang['site.keywords'] = "INETTECH, INETTECH.IO, made-in.ma, Made in Morocco, Manufacturer, Suppliers, Service Companies, B2B, B2B-Plattform, Export, Import, Products";

#register
$lang['header.register'] = 'تسجيل';

#message
$lang['general.lang.en'] = "English";
$lang['general.lang.fr'] = "Français";
$lang['general.lang.it'] = "Italiano";
$lang['general.lang.es'] = "Español";
$lang['general.lang.de'] = "Deutsch";
$lang['general.lang.ar'] = "العربية";
$lang['general.lang.cn'] = "中國";
$lang['general.lang.tu'] = "Türkçe";
// $lang['general.lang.ir'] = "Farsi";

#message
$lang['message.success'] = "نجاح";
$lang['message.error'] = "خطأ";
$lang['message.warning'] = "تحذير";
$lang['message.ok'] = "موافق";
$lang['message.cancel'] = "إلغاء";
$lang['message.signin'] = "تسجيل الدخول";
$lang['message.signup'] = "سجل";
$lang['message.signout'] = "خروج";
$lang['message.save'] = "حفظ";
$lang['message.mail'] = "بريد";
$lang['message.rating'] = "تصنيف";
$lang['message.call'] = "دعوة";
$lang['message.save_success'] = "تتم مراجعة التغييرات وسيتم إلغاء قفلها قريبًا ...";
$lang['message.confirmation'] = "التأكيد";
$lang['message.new_password'] = "يرجى إدخال كلمة مرور جديدة";
$lang['message.recaptacha_error'] = "اختبار جوجل ري كابتشا غير ناجح";
$lang['message.phone_duplicate'] = 'الهاتف غير مسجل بالفعل';
$lang['message.sms_error'] = 'رقم الهاتف خاطئ';
$lang['message.label.name'] = "الاسم";
$lang['message.label.email'] = "البريد الإلكتروني";
$lang['message.label.phone'] = 'الهاتف';
$lang['message.label.phone'] = "الهاتف";
$lang['message.label.country'] = "البلد";
$lang['message.label.language'] = "الغة";
$lang['message.label.content'] = "يحتوى";
$lang['message.label.created_at'] = "أنشئت في";
$lang['message.company_error'] = "Company name is already registered.";

#validation
$lang['validation.user.name.salutation'] = "الرجاء تحديد جنسك";
$lang['validation.user.name.require'] = "أدخل إسمك من فضلك";
$lang['validation.user.email.require'] = "رجاءا أدخل بريدك الإلكتروني";
$lang['validation.user.phone.require'] = 'الرجاء ادخل رقم الهاتف';
$lang['validation.user.email.valid'] = "من فضلك أدخل بريد إلكتروني صحيح";
$lang['validation.user.email.unique'] = "هذا البريد الالكتروني مسجل سابقا. <br> الرجاء استخدام بريد إلكتروني مختلف";
$lang['validation.user.phone.unique'] = "رقم الهاتف مسجل بالفعل. <br> الرجاء استخدام رقم هاتف مختلف";
$lang['validation.user.password.require'] = ".من فضلك أدخل رقمك السري";
$lang['validation.user.confirm_password.require'] = "الرجاء إدخال كلمة المرور للتأكيد";
$lang['validation.user.confirm_password.valid'] = "كلمة المرور وكلمة المرور للتأكيد غير متطابقتين";
$lang['validation.user.new_password.require'] = ".من فضلك أدخل كلمة المرور الجديدة";
$lang['validation.user.current_password.require'] = "الرجاء إدخال كلمة المرور الحالية";
$lang['validation.user.sms_code.require'] = ".يرجى إدخال رمز الرسائل القصيرة الخاص بك";

$lang['validation.company.name.require'] = "الرجاء إدخال عنوان لملف التعريف الخاص بك";
$lang['validation.company.salutation.require'] = "الرجاء إدخال التحية";
$lang['validation.company.contact_person.require'] = "الرجاء إدخال اسمك للتواصل معك";
$lang['validation.company.street.require'] = "الرجاء إدخال عنوان المحل";
$lang['validation.company.city.require'] = "يرجى اختيار المدينة";
$lang['validation.company.founded_at.require'] = "الرجاء إدخال سنة الشروع في المجال";
$lang['validation.company.email.require'] = "الرجاء إدخال البريد الإلكتروني";
$lang['validation.company.zipcode.require'] = "الرجاء إدخال الرمز البريدي";
$lang['validation.company.phone.require'] = "يرجى إدخال رقم هاتفك";
$lang['validation.company.website.require'] = "الرجاء إدخال موقع الويب";
$lang['validation.company.keywords.require'] = "الرجاء إدخال الكلمات الأساسية لتسهيل العثور عليك";
$lang['validation.company.lang.require'] = ".يرجى اختيار اللغة";
$lang['validation.company.employee_count.require'] = ".يرجى اختيار عدد الموظفين";
$lang['validation.company.category.require'] = ".يرجى اختيار الفئة.";

#user
$lang['user.name'] = "الاسم";
$lang['user.email'] = "البريد الالكتروني";
$lang['user.phone'] = 'الهاتف';
$lang['user.password'] = "كلمه المرور";
$lang['user.confirm_password'] = "تأكيد كلمة المرور";
$lang['user.sms_code'] = 'الرمز';
$lang['user.promotion_code'] = 'تاكيد الرمز ، ليس ضروري';

#placeholder
$lang['user.phone.flag'] = 'assets/madinma/front/images/1.png';
$lang['user.phone.placeholder'] = '650123456';
$lang['phone.placeholder'] = '+212...';
$lang['promo.placeholder'] = 'A12B';

#company setting labels
$lang['company.logo'] = "شعار الشركة";
$lang['company.name'] = "العنوان";
$lang['company.salutation'] = "الرجاء تحديد جنسك";
$lang['company.contact_person'] = "الاسم الكامل";
$lang['company.street'] = "الشارع او الحي";
$lang['company.city'] = "المدينة";
$lang['company.founded_at'] = "تأسست في";
$lang['company.employee_count'] = "عدد الموظفين";
$lang['company.zipcode'] = "الرمز البريدي";
$lang['company.phone'] = "الهاتف";
$lang['company.email'] = "البريد الإلكتروني";
$lang['company.website'] = "موقع الكتروني";
$lang['company.youtube'] = "موقع يوتيوب";
$lang['company.keywords'] = "الكلمات الاساسية";
$lang['company.about_us'] = "معلومات عن الشركة";
$lang['company.category'] = "الفئة";
$lang['company.language'] = "اللغات التي تتكلمها";
$lang['company.latitude'] = "خط العرض";
$lang['company.longitude'] = "خط الطول";

#company setting placeholders
$lang['company.logo.placeholder'] = "شعار الشركة";
$lang['company.name.placeholder'] = "Ameublement el Bahja";
$lang['company.salutation.placeholder.mr'] = "السيد";
$lang['company.salutation.placeholder.ms'] = "السيدة";
$lang['company.contact_person.placeholder'] = "Mohamed Kamal";
$lang['company.street.placeholder'] = "138 Boulevard Moulay Youssef, Angle Rue Tiznit";
$lang['company.founded_at.placeholder'] = "2008";
$lang['company.zipcode.placeholder'] = "20240";
$lang['company.phone.placeholder'] = "+212...";
$lang['company.email.placeholder'] = "your@email.com";
$lang['company.website.placeholder'] = "https://www.yourwebsite.com";
$lang['company.youtube.placeholder'] = "https://www.youtube.com/watch?v=iwa7_SHf114";
$lang['company.keywords.placeholder'] = "...زيت أركان ، أركان";
$lang['company.about_us.placeholder'] = "معلومات عنا";
$lang['company.lang.non.selected.placeholder'] = "لا شيء مختار";
$lang['company.latitude.placeholder'] = "33.333333";
$lang['company.longitude.placeholder'] = "43.333333";

#top-search
$lang['topsearch.search'] = "البحث";
$lang['topsearch.placeholder'] = "...عما تبحث";
$lang['topsearch.noresult'] = "No result please remove some filters and try again...";
$lang['topsearch.reset'] = "Reset Filters";

#sidebar
$lang['sidebar.title'] = ""; // Categories

#footer
$lang['footer.about_title'] = "حول made-in.ma";
$lang['footer.about_content'] = 'made-in.ma هي عبارة عن منصة تداول عبر الإنترنت تهدف إلى تعزيز التصدير والتجارة المحلية للشركات الصغرى في المغرب. يمكن للمصنعين والموردين ومقدمي الخدمات التقديم هنا مجانًا للعملاء الدوليين والمحليين.<br><br>يتم تطوير و تشغيل هذه الخدمة من خلال <a href="https://www.inettech.io" title="">INETTECH.io</a><br><br><a href="https://www.youtube.com/channel/UCAqJdNAA1onUmm7KZo4qvqg" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_youtube.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/madein.ma/" title=""><img src = "https://www.made-in.ma/assets/madinma/front/images/icon_instagram.png" alt ="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://fb.me/wwwmadeinma" title=""><img src ="https://www.made-in.ma/assets/madinma/front/images/icon_facebook.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href=" https://wa.me/212688217862" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_whatsapp.png" alt="" height="35" width="35"></a>';
$lang['footer.contact_title'] = "الاتصال والتعاون";
$lang['footer.contact_content'] = "يرجى الاتصال بنا عن طريق البريد";
$lang['footer.mail'] = "بريد";
$lang['footer.statistics_title'] = "الإحصائيات";
$lang['footer.statistics_comapny'] = "الشركات المدرجة";
$lang['footer.statistics_comapny_week'] = "جديد هذا الأسبوع";
$lang['footer.statistics_inquery'] = "عدد المشاهدة";
$lang['footer.statistics_inquery_week'] = "عدد المشاهدة هذا الاسبوع";

#modals
$lang['modal.contact.title'] = "اتصل";
$lang['modal.rating.title'] = "تقييم";
$lang['modal.call.title'] = "مكالمة";
$lang['modal.btn.send'] = "إرسال رسالة";
$lang['modal.placeholder.name'] = "الاسم";
$lang['modal.placeholder.email'] = "البريد الالكتروني";
$lang['modal.placeholder.phone'] = "+212XXXXXXXXX";
$lang['modal.placeholder.text'] = "رسالة";
$lang['modal.placeholder.company_detail'] = "Please write here your text to the company";
$lang['modal.placeholder.rating_detail'] = "Please write here your comment to this company";
$lang['modal.your_rating'] = "تقييمك";
$lang['modal.call_desc'] = " شكرا لك ، made-in.ma اخبرني كيف وجدت اعلاني على";
$lang['modal.confirm.title'] = "التأكيد";
$lang['modal.confirm.question'] = "تأكيد رقم هاتفك!";

$lang['modal.lang.none.selected'] = 'لا شيء محدد';
$lang['modal.option.email'] = "بريد";
$lang['modal.option.phone'] = "هاتف";
$lang['modal.option.all'] = "كلاهما";

#startpage
$lang['startpage.new'] = "شركة جديدة";
$lang['startpage.week'] = "الشركة الجديدة هذا الأسبوع";

#signin
$lang['signin.remember'] = "حفظ البيانات";
$lang['signin.invalid'] = "بيانات الاعتماد غير صالحة";
$lang['signin.blocked'] = "تم حظر حسابك";
$lang['signin.back'] = "رجوع";
$lang['signin.submit'] = "قبول";
$lang['signin.forgot'] = "هل نسيت كلمة المرور؟";

#signup
$lang['signup.desc'] = "سجل هنا على made-in.ma كمصنع أو مزود خدمة مجانًا ودون التزام. يمكنك الإعلان عن منتجاتك وخدماتك على الصعيدين الوطني والدولي";

#sms verification
$lang['sms.desc'] = ".يرجى إدخال رمز التحقق من الرسائل القصيرة";
$lang['sms.code.error'] = "الرمز غير صحيح";
$lang['sms.expire'] = ".انتهت صلاحية الرمز الخاص بك. يرجى محاولة ألتسجيل مرة أخرى";

#myacount
$lang['myaccount.company_profile'] = "الملف الشخصي للشركة";
$lang['myaccount.title'] = "حسابي";
$lang['myaccount.company_information'] = "معلومات عن الشركة";
$lang['myaccount.logo_note'] = "شعار شركتك. سيتم تصغير الصور الأكبر من البكسل ";
$lang['myaccount.avatar_note'] = "صورتك الشخصية. سيتم تصغير الصور الأكبر من البكسل";
$lang['myaccount.company_product'] = "صور منتجات الشركة";
$lang['myaccount.general_information'] = "معلومات الحساب";
$lang['myaccount.change_password'] = "تغير كلمة المرور";
$lang['myaccount.current_password'] = "كلمة المرور الحالية";
$lang['myaccount.promotion'] = 'ترقية وظيفية';
$lang['myaccount.promotion.desc'] = 'الموصى بها ، في أصدقاء الأصدقاء والمعارف. سوف تتلقى 20 درهمًا لكل تسجيل شركة مسجلة تقوم بإدخاله باستخدام رمز الترويج الخاص بك. دفع تعويضات ممكن rom 100 التسجيلات. يتم الدفع عبر Western Union.';
$lang['myaccount.promotion.promotion_code'] = 'رمز الترويج الخاص بك';
$lang['myaccount.promotion.promotion_link'] = 'رابط الترويج';
$lang['myaccount.promotion.current_promotions'] = 'عدد المنخرطين التابعين لك';
$lang['myaccount.new_password'] = "كلمة المرور الجديدة";
$lang['myaccount.select'] = "تحديد";
$lang['myaccount.logo'] = "صورة المحل";
$lang['myaccount.contact_avatar'] = "صورة صاحب الملف";
$lang['myaccount.product_warning'] = "الرجاء إضافة صورة للمنتج";
$lang['myaccount.preview'] = "معاينة";
$lang['myaccount.del_account'] = "حذف الحساب";
$lang['myaccount.del_account_msg'] = "هل تريد بالتأكيد حذف الحساب نهائيًا؟";
$lang['myaccount.messages'] = "رسائل";
$lang['myaccount.support'] = "طلب الدعم";

#companies
$lang['companies.all'] = "الكل";
$lang['companies.sortby'] = "ترتيب حسب";
$lang['companies.rating'] = "تقييم";
$lang['companies.newest'] = "الأحدث";
$lang['companies.oldest'] = "أقدم";
$lang['companies.entries'] = "إدخالات";
$lang['companies.in'] = " في ";

#company detail
$lang['detail.employees'] = "الموظفين";
$lang['detail.date'] = "تاريخ";
$lang['detail.name'] = "الاسم";
$lang['detail.description'] = "وصف";
$lang['detail.noproduct'] = "لا يوجد منتج";
$lang['detail.product.pics'] ='صور المنتج';
$lang['detail.last.update'] = 'اخر تحديث';
$lang['detail.rating'] = 'تقييم';
$lang['detail.youtube'] = 'موقع يوتيوب';
$lang['detail.map'] = 'موقعك';

#rating modal
$lang['rating.name'] = "الاسم";
$lang['rating.country'] = "البلد";
$lang['rating.text'] = "نص";
$lang['no.ratings'] = "بدون تقييم";

#company profile
$lang['profile.since'] = "منذ";
$lang['profile.employees'] = "الموظفين";

#forgot
$lang['forgot.no_email'] = "لا يوجد عنوان بريد إلكتروني كهذا";
$lang['forgot.no_phone'] = ". لا يوجد مثل هذا الهاتف";
$lang['forgot.success_email'] = "أرسل رابط إعادة تعيين عنوان البريد الإلكتروني الخاص بك. يرجى التحقق من مجلد البريد غير المرغوب فيه";
$lang['forgot.success_phone'] = "إعادة تعيين رابط إرسالها إلى هاتفك";
$lang['forgot.invalid_token'] = "رمز غير صالح";
$lang['forgot.update_success'] = "تم تحديث كلمة المرور بنجاح";
$lang['forgot.expire'] = ".لقد انتهى وقت الصلاحية";

#contact sms message
$lang['contact.sms'] = '.لديك رسالة جديدة. made-in.ma يرجى تسجيل الدخول إلى ';
$lang['contact.sms.success'] = '.تم إرسال رسالتك إلى الشركة المصنعة بنجاح';
$lang['contact.sms.error'] = '.لم يتم إرسال رسالتك بسبب مشكلة الرسائل القصيرة';

#phone number invalid message
$lang['phone.error.title'] = "Error";
$lang['phone.invalid'] = "Your phone number is invalid. The count of digits should be 9 and do not start with '0' at first";

#category info
$lang['categoryInfo.imgURL.26'] = "10.jpg";
$lang['categoryInfo.detail.26'] = "";

$lang['categoryInfo.imgURL.25'] = "10.jpg";
$lang['categoryInfo.detail.25'] = "";

$lang['categoryInfo.imgURL.24'] = "10.jpg";
$lang['categoryInfo.detail.24'] = "";

$lang['categoryInfo.imgURL.23'] = "10.jpg";
$lang['categoryInfo.detail.23'] = "";

$lang['categoryInfo.imgURL.22'] = "10.jpg";
$lang['categoryInfo.detail.22'] = "";

$lang['categoryInfo.imgURL.21'] = "10.jpg";
$lang['categoryInfo.detail.21'] = "";

$lang['categoryInfo.imgURL.20'] = "10.jpg";
$lang['categoryInfo.detail.20'] = "";

$lang['categoryInfo.imgURL.19'] = "10.jpg";
$lang['categoryInfo.detail.19'] = "";

$lang['categoryInfo.imgURL.18'] = "10.jpg";
$lang['categoryInfo.detail.18'] = "";

$lang['categoryInfo.imgURL.17'] = "10.jpg";
$lang['categoryInfo.detail.17'] = "";

$lang['categoryInfo.imgURL.16'] = "10.jpg";
$lang['categoryInfo.detail.16'] = "";

$lang['categoryInfo.imgURL.15'] = "10.jpg";
$lang['categoryInfo.detail.15'] = "";

$lang['categoryInfo.imgURL.14'] = "10.jpg";
$lang['categoryInfo.detail.14'] = "";

$lang['categoryInfo.imgURL.13'] = "10.jpg";
$lang['categoryInfo.detail.13'] = "";

$lang['categoryInfo.imgURL.12'] = "10.jpg";
$lang['categoryInfo.detail.12'] = "";

$lang['categoryInfo.imgURL.11'] = "10.jpg";
$lang['categoryInfo.detail.11'] = "";

$lang['categoryInfo.imgURL.10'] = "10.jpg";
$lang['categoryInfo.detail.10'] = "";

$lang['categoryInfo.imgURL.9'] = "10.jpg";
$lang['categoryInfo.detail.9'] = "";

$lang['categoryInfo.imgURL.8'] = "10.jpg";
$lang['categoryInfo.detail.8'] = "";

$lang['categoryInfo.imgURL.7'] = "10.jpg";
$lang['categoryInfo.detail.7'] = "";

$lang['categoryInfo.imgURL.6'] = "10.jpg";
$lang['categoryInfo.detail.6'] = "";

$lang['categoryInfo.imgURL.5'] = "10.jpg";
$lang['categoryInfo.detail.5'] = "";

$lang['categoryInfo.imgURL.4'] = "10.jpg";
$lang['categoryInfo.detail.4'] = "";

$lang['categoryInfo.imgURL.3'] = "10.jpg";
$lang['categoryInfo.detail.3'] = "";

$lang['categoryInfo.imgURL.2'] = "10.jpg";
$lang['categoryInfo.detail.2'] = "";

$lang['categoryInfo.imgURL.1'] = "10.jpg";
$lang['categoryInfo.detail.1'] = "";
