<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['site.description'] = "Encuentre fabricantes, proveedores y empresas de servicios en Marruecos";
$lang['site.title'] = "made-in.ma - El mercado en línea para empresas marroquíes";
$lang['site.keywords'] = "INETTECH, INETTECH.IO, made-in.ma, Made in Morocco, Manufacturer, Suppliers, Service Companies, B2B, B2B-Plattform, Export, Import, Products";

#register
$lang['header.register'] = 'Registro';

#message
$lang['general.lang.en'] = "English";
$lang['general.lang.fr'] = "Français";
$lang['general.lang.it'] = "Italiano";
$lang['general.lang.es'] = "Español";
$lang['general.lang.de'] = "Deutsch";
$lang['general.lang.ar'] = "العربية";
$lang['general.lang.cn'] = "中國";
$lang['general.lang.tu'] = "Türkçe";
// $lang['general.lang.ir'] = "Farsi";

#message
$lang['message.success'] = "Éxito";
$lang['message.error'] = "Error";
$lang['message.warning'] = "Advertencia";
$lang['message.ok'] = "OK";
$lang['message.cancel'] = "Cancelar";
$lang['message.signin'] = "Registrarse";
$lang['message.signup'] = "Regístrate";
$lang['message.signout'] = "Desconectar";
$lang['message.save'] = "Salvar";
$lang['message.mail'] = "Correo";
$lang['message.rating'] = "Clasificación";
$lang['message.call'] = "Llamada";
$lang['message.save_success'] = "Necesita verificación antes del lanzamiento, espere ...";
$lang['message.confirmation'] = "Confirmación";
$lang['message.new_password'] = "Por favor ingrese una nueva contraseña";
$lang['message.recaptacha_error'] = "Lo siento. ¡Google Recaptcha no tuvo éxito!";
$lang['message.phone_duplicate'] = 'Teléfono no registrado.';
$lang['message.sms_error'] = 'Número de teléfono equivocado';
$lang['message.label.name'] = "Nombre";
$lang['message.label.email'] = "Correo electrónico";
$lang['message.label.phone'] = 'Teléfono';
$lang['message.label.phone'] = "Teléfono";
$lang['message.label.country'] = "País";
$lang['message.label.language'] = "Idioma";
$lang['message.label.content'] = "Contenido";
$lang['message.label.created_at'] = "Creado en";
$lang['message.company_error'] = "Company name is already registered.";

#validation
$lang['validation.user.name.salutation'] = "Por favor, introduzca el saludo.";
$lang['validation.user.name.require'] = "Por favor, escriba su nombre.";
$lang['validation.user.email.require'] = "Por favor introduzca su correo electrónico.";
$lang['validation.user.phone.require'] = 'Por favor, introduzca su número de teléfono.';
$lang['validation.user.email.valid'] = "Por favor, introduce una dirección de correo electrónico válida.";
$lang['validation.user.email.unique'] = "Esta dirección de correo electrónico ya está registrada. <br> Utilice una dirección de correo electrónico diferente.";
$lang['validation.user.phone.unique'] = "Este número de teléfono ya está registrado. <br> Utilice un número de teléfono diferente.";
$lang['validation.user.password.require'] = "Por favor, introduzca su contraseña.";
$lang['validation.user.confirm_password.require'] = "Por favor, introduzca una contraseña de confirmación.";
$lang['validation.user.confirm_password.valid'] = "Su contraseña y la contraseña de confirmación no coinciden.";
$lang['validation.user.new_password.require'] = "Por favor ingrese su nueva contraseña.";
$lang['validation.user.current_password.require'] = "Por favor ingrese su contraseña actual.";
$lang['validation.user.sms_code.require'] = "Por favor ingrese su código sms.";

$lang['validation.company.name.require'] = "Por favor, introduzca el nombre de su empresa.";
$lang['validation.company.salutation.require'] = "Por favor, introduzca el saludo.";
$lang['validation.company.contact_person.require'] = "Por favor, introduzca la persona de contacto.";
$lang['validation.company.street.require'] = "Por favor, introduzca la calle.";
$lang['validation.company.city.require'] = "Por favor seleccione ciudad.";
$lang['validation.company.founded_at.require'] = "Por favor, introduzca el año de fundación.";
$lang['validation.company.email.require'] = "Por favor ingrese su correo electrónico.";
$lang['validation.company.zipcode.require'] = "Por favor, introduzca el código postal.";
$lang['validation.company.phone.require'] = "Por favor ingrese el teléfono.";
$lang['validation.company.website.require'] = "Por favor ingrese al sitio web.";
$lang['validation.company.keywords.require'] = "Por favor, introduzca palabras clave.";
$lang['validation.company.lang.require'] = "Por favor seleccione idioma.";
$lang['validation.company.employee_count.require'] = "Por favor, seleccione el número de empleados.";
$lang['validation.company.category.require'] = "Por favor seleccione categoría.";

#user
$lang['user.name'] = "Nombre";
$lang['user.email'] = "Correo electrónico";
$lang['user.phone'] = 'Teléfono';
$lang['user.password'] = "Contraseña";
$lang['user.confirm_password'] = "Confirmar contraseña";
$lang['user.sms_code'] = 'Cocódigode';
$lang['user.promotion_code'] = 'El código de promoción';

#placeholder
$lang['user.phone.flag'] = 'assets/madinma/front/images/1.png';
$lang['user.phone.placeholder'] = '650123456';
$lang['phone.placeholder'] = '+212...';
$lang['promo.placeholder'] = 'A12b';

#company setting labels
$lang['company.logo'] = "Logo de la compañía";
$lang['company.name'] = "Título";
$lang['company.salutation'] = "Saludo";
$lang['company.contact_person'] = "Persona de contacto";
$lang['company.street'] = "Calle";
$lang['company.city'] = "Ciudad";
$lang['company.founded_at'] = "Fundado en";
$lang['company.employee_count'] = "Número de empleados";
$lang['company.zipcode'] = "Código postal";
$lang['company.phone'] = "Teléfono";
$lang['company.email'] = "Correo electrónico";
$lang['company.website'] = "Sitio web";
$lang['company.youtube'] = "Youtube";
$lang['company.keywords'] = "Palabras clave";
$lang['company.about_us'] = "Sobre nosotros";
$lang['company.category'] = "Categoría";
$lang['company.language'] = "Idiomas";
$lang['company.latitude'] = "Latitud";
$lang['company.longitude'] = "Longitud";

#company setting placeholders
$lang['company.logo.placeholder'] = "Logo de la compañía";
$lang['company.name.placeholder'] = "Título";
$lang['company.salutation.placeholder.mr'] = "Señor";
$lang['company.salutation.placeholder.ms'] = "Sra";
$lang['company.contact_person.placeholder'] = "Mohamed Kamal";
$lang['company.street.placeholder'] = "138 Boulevard Moulay Youssef, Angle Rue Tiznit";
$lang['company.founded_at.placeholder'] = "2008";
$lang['company.zipcode.placeholder'] = "47963";
$lang['company.phone.placeholder'] = "+212...";
$lang['company.email.placeholder'] = "your@mail.ma";
$lang['company.website.placeholder'] = "https://yourwebsite.ma";
$lang['company.youtube.placeholder'] = "https://www.youtube.com/watch?v=iwa7_SHf114";
$lang['company.keywords.placeholder'] = "Aceite de argán, argán ...";
$lang['company.about_us.placeholder'] = "Sobre nosotros";
$lang['company.lang.non.selected.placeholder'] = "Nada seleccionado";
$lang['company.latitude.placeholder'] = "33.333333";
$lang['company.longitude.placeholder'] = "33.333333";

#top-search
$lang['topsearch.search'] = "Buscar";
$lang['topsearch.placeholder'] = "Qué estás buscando...";

#sidebar
$lang['sidebar.title'] = ""; // Category

#footer
$lang['footer.about_title'] = "Sobre made-in.ma";
$lang['footer.about_content'] = 'made-in.ma es una organización sin fines de lucro que quiere fortalecer la economía de exportación de pequeñas empresas en Marruecos. Los fabricantes, proveedores de servicios y proveedores pueden presentarse a clientes internacionales.<br><br>Este servicio es proporcionado por <a href="https://www.inettech.io" title="">INETTECH.io</a><br><br><a href="https://www.youtube.com/channel/UCAqJdNAA1onUmm7KZo4qvqg" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_youtube.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/madein.ma/" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_instagram.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href="https://fb.me/wwwmadeinma" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_facebook.png" alt="" height="35" width="35"></a>&nbsp;&nbsp;&nbsp;<a href=" https://wa.me/212688217862" title=""><img src="https://www.made-in.ma/assets/madinma/front/images/icon_whatsapp.png" alt="" height="35" width="35"></a>';
$lang['footer.contact_title'] = "Contacto y cooperación";
$lang['footer.contact_content'] = "Por favor contáctenos por correo";
$lang['footer.mail'] = "Correo";
$lang['footer.statistics_title'] = "Estadística";
$lang['footer.statistics_comapny'] = "Compañías listadas";
$lang['footer.statistics_comapny_week'] = "Nuevo esta semana";
$lang['footer.statistics_inquery'] = "Consulta total";
$lang['footer.statistics_inquery_week'] = "Consulta esta semana";

#modals
$lang['modal.contact.title'] = "Contacto";
$lang['modal.rating.title'] = "Clasificación";
$lang['modal.call.title'] = "Llamada";
$lang['modal.btn.send'] = "Enviar";
$lang['modal.placeholder.name'] = "Nombre";
$lang['modal.placeholder.email'] = "Correo electrónico";
$lang['modal.placeholder.phone'] = "+2125XXXXXXXX";
$lang['modal.placeholder.text'] = "Texto";
$lang['modal.placeholder.company_detail'] = "Please write here your text to the company";
$lang['modal.placeholder.rating_detail'] = "Please write here your comment to this company";
$lang['modal.your_rating'] = "Tu clasificación";
$lang['modal.call_desc'] = "Dime que encontraste mi anuncio en made-in.ma. ¡Gracias!";
$lang['modal.confirm.title'] = "Confirmar";
$lang['modal.confirm.question'] = "¡Confirma tu número de teléfono!";

$lang['modal.lang.none.selected'] = 'Nothing selected';
$lang['modal.option.email'] = "Email";
$lang['modal.option.phone'] = "Phone";
$lang['modal.option.all'] = "All";

#startpage
$lang['startpage.new'] = "Nuevo fabricante";
$lang['startpage.week'] = "Fabricante de la semana";

#signin
$lang['signin.remember'] = "Recuérdame";
$lang['signin.invalid'] = "Credenciales no válidas";
$lang['signin.blocked'] = "Su cuenta fue bloqueada.";
$lang['signin.back'] = "Atrás";
$lang['signin.submit'] = "Enviar";
$lang['signin.forgot'] = "¿Contraseña olvidada?";

#signup
$lang['signup.desc'] = "Regístrese aquí en made-in.ma como fabricante o proveedor de servicios de forma gratuita y sin compromiso. Puede anunciar sus productos y servicios a nivel nacional e internacional.";

#sms verification
$lang['sms.desc'] = "Por favor, introduzca su código de verificación de sms.";
$lang['sms.code.error'] = "Codigo invalido";
$lang['sms.expire'] = "Su código ha caducado Por favor intente registrarse nuevamente.";

#myacount
$lang['myaccount.company_profile'] = "Perfil de la compañía";
$lang['myaccount.title'] = "Mi cuenta";
$lang['myaccount.company_information'] = "Información de la compañía";
$lang['myaccount.logo_note'] = "El logo de tu empresa. Las imágenes más grandes que los píxeles se reducirán.";
$lang['myaccount.avatar_note'] = "Tu cara o imagen virtual. Las imágenes más grandes que los píxeles se reducirán.";
$lang['myaccount.company_product'] = "Producto de la empresa";
$lang['myaccount.general_information'] = "Información general";
$lang['myaccount.change_password'] = "Cambia la contraseña";
$lang['myaccount.current_password'] = "Contraseña actual";
$lang['myaccount.promotion'] = 'Promoción';
$lang['myaccount.promotion.desc'] = 'Recomendado made-in.ma amigos y conocidos. Recibirá 20 dirhams por cada registro de empresa registrada que ingrese utilizando su código de promoción. Un pago es posible desde 100 registros. El pago se realiza a través de Western Union.';
$lang['myaccount.promotion.promotion_code'] = 'Su código de promoción';
$lang['myaccount.promotion.promotion_link'] = 'Enlace de promoción';
$lang['myaccount.promotion.current_promotions'] = 'Promociones actuales';
$lang['myaccount.new_password'] = "Nueva contraseña";
$lang['myaccount.select'] = "Seleccionar";
$lang['myaccount.logo'] = "Logo";
$lang['myaccount.contact_avatar'] = "Foto de la persona de contacto";
$lang['myaccount.product_warning'] = "Por favor agregue un producto.";
$lang['myaccount.preview'] = "Avance";
$lang['myaccount.del_account'] = "Borrar cuenta";
$lang['myaccount.del_account_msg'] = "¿Está seguro que desea eliminar?";
$lang['myaccount.messages'] = "Mensajes";
$lang['myaccount.support'] = "Apoyo";

#companies
$lang['companies.all'] = "Todos";
$lang['companies.sortby'] = "Ordenar por";
$lang['companies.rating'] = "Clasificación";
$lang['companies.newest'] = "El más nuevo";
$lang['companies.oldest'] = "Más antiguo";
$lang['companies.entries'] = "Entradas";
$lang['companies.in'] = "En";

#company detail
$lang['detail.employees'] = "Empleados";
$lang['detail.date'] = "Fecha";
$lang['detail.name'] = "Nombre";
$lang['detail.description'] = "Descripción";
$lang['detail.noproduct'] = "No hay producto";
$lang['detail.product.pics'] = 'Imágenes del producto';
$lang['detail.last.update'] = 'Última actualización';
$lang['detail.rating'] = 'Clasificación';
$lang['detail.youtube'] = 'Youtube';
$lang['detail.map'] = 'Ubicación';

#rating modal
$lang['rating.name'] = "Nombre";
$lang['rating.country'] = "País";
$lang['rating.text'] = "Texto";
$lang['no.ratings'] = "No hay votos";

#company profile
$lang['profile.since'] = "Ya que";
$lang['profile.employees'] = "Empleados";

#forgot
$lang['forgot.no_email'] = "No existe tal dirección de correo electrónico.";
$lang['forgot.no_phone'] = "No existe tal teléfono.";
$lang['forgot.success_email'] = "Restablecer enlace envió su dirección de correo electrónico. Por favor revise su carpeta de correo basura.";
$lang['forgot.success_phone'] = "Reset link sent to your phone";
$lang['forgot.invalid_token'] = "Simbolo no valido.";
$lang['forgot.update_success'] = "La contraseña se actualizó correctamente.";
$lang['forgot.expire'] = "Su tiempo de caducidad ha pasado.";

#contact sms message
$lang['contact.sms'] = 'Inicie sesión en made-in.ma. Tienes un nuevo mensaje.';
$lang['contact.sms.success'] = 'Su mensaje fue enviado al fabricante con éxito.';
$lang['contact.sms.error'] = 'Su mensaje no fue enviado debido a un problema de SMS';

#phone number invalid message
$lang['phone.error.title'] = "Error";
$lang['phone.invalid'] = "Your phone number is invalid. The count of digits should be 9 and do not start with '0' at first";

#category info
$lang['categoryInfo.imgURL.26'] = "10.jpg";
$lang['categoryInfo.detail.26'] = "";

$lang['categoryInfo.imgURL.25'] = "10.jpg";
$lang['categoryInfo.detail.25'] = "";

$lang['categoryInfo.imgURL.24'] = "10.jpg";
$lang['categoryInfo.detail.24'] = "";

$lang['categoryInfo.imgURL.23'] = "10.jpg";
$lang['categoryInfo.detail.23'] = "";

$lang['categoryInfo.imgURL.22'] = "10.jpg";
$lang['categoryInfo.detail.22'] = "";

$lang['categoryInfo.imgURL.21'] = "10.jpg";
$lang['categoryInfo.detail.21'] = "";

$lang['categoryInfo.imgURL.20'] = "10.jpg";
$lang['categoryInfo.detail.20'] = "";

$lang['categoryInfo.imgURL.19'] = "10.jpg";
$lang['categoryInfo.detail.19'] = "";

$lang['categoryInfo.imgURL.18'] = "10.jpg";
$lang['categoryInfo.detail.18'] = "";

$lang['categoryInfo.imgURL.17'] = "10.jpg";
$lang['categoryInfo.detail.17'] = "";

$lang['categoryInfo.imgURL.16'] = "10.jpg";
$lang['categoryInfo.detail.16'] = "";

$lang['categoryInfo.imgURL.15'] = "10.jpg";
$lang['categoryInfo.detail.15'] = "";

$lang['categoryInfo.imgURL.14'] = "10.jpg";
$lang['categoryInfo.detail.14'] = "";

$lang['categoryInfo.imgURL.13'] = "10.jpg";
$lang['categoryInfo.detail.13'] = "";

$lang['categoryInfo.imgURL.12'] = "10.jpg";
$lang['categoryInfo.detail.12'] = "";

$lang['categoryInfo.imgURL.11'] = "10.jpg";
$lang['categoryInfo.detail.11'] = "";

$lang['categoryInfo.imgURL.10'] = "10.jpg";
$lang['categoryInfo.detail.10'] = "";

$lang['categoryInfo.imgURL.9'] = "10.jpg";
$lang['categoryInfo.detail.9'] = "";

$lang['categoryInfo.imgURL.8'] = "10.jpg";
$lang['categoryInfo.detail.8'] = "";

$lang['categoryInfo.imgURL.7'] = "10.jpg";
$lang['categoryInfo.detail.7'] = "";

$lang['categoryInfo.imgURL.6'] = "10.jpg";
$lang['categoryInfo.detail.6'] = "";

$lang['categoryInfo.imgURL.5'] = "10.jpg";
$lang['categoryInfo.detail.5'] = "";

$lang['categoryInfo.imgURL.4'] = "10.jpg";
$lang['categoryInfo.detail.4'] = "";

$lang['categoryInfo.imgURL.3'] = "10.jpg";
$lang['categoryInfo.detail.3'] = "";

$lang['categoryInfo.imgURL.2'] = "10.jpg";
$lang['categoryInfo.detail.2'] = "";

$lang['categoryInfo.imgURL.1'] = "10.jpg";
$lang['categoryInfo.detail.1'] = "";
